// import { WindowUtil } from "./windowutil";
// import { environment } from "../../../environments/environment";
// import { UrlUtil } from "./urlutil";

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilService } from 'app/services/util.service';
import { UrlUtil } from './urlutil';
import { WindowUtil } from './windowutil';
import { HOST } from 'app/_shared/var.constant';

@Injectable({
    providedIn: 'root'
  })
  
export class ReporteUtil {

    url = '';
    
    constructor(private http: HttpClient, private utilService: UtilService) { }

    openWindowHtml(reporte: string, filtro: any): void {
        WindowUtil.openWindow(UrlUtil.urlWithParams(this.getUrl(reporte, 'html'), filtro));
    }

    // tslint:disable-next-line:member-ordering
    downloadXls(reporte: string, filtro: any): void {
        WindowUtil.openWindow(UrlUtil.urlWithParams(this.getUrl(reporte, 'xls'), filtro));
    }
    // tslint:disable-next-line:ban-types
    getUrl(reporte: string, extension: String): string{
        // return 'http://localhost:8080/jasperserver/rest_v2/reports/DHAGEZ/' + reporte + '.' + extension;
        return UrlUtil.getApiReporteUrl() + reporte + '.' + extension;
    }

    donwloadPdf(reporte: string, filtro: any): void {
        
        WindowUtil.openWindow(UrlUtil.urlWithParams(this.getUrl(reporte, 'pdf'), filtro));
    }
    exportarPdf(reporte: string) {

        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        return this.http.get<Blob>(`${reporte}`, 
        { headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/pdf'),
            responseType: 'blob' as 'json',
            observe: 'response',
        }        
        );

    }
}
