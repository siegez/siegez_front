import { REPORT } from '../var.constant';
import { WindowUtil } from './windowutil';

export class UrlUtil {

    url = '';

    static jsonToParamsUrl(obj: any): string{
        const params: URLSearchParams = new URLSearchParams();
        for (const key in obj) {
            if (obj[key]){
                params.set(key, obj[key]);
            }
        }
        return params.toString();
    }

    static urlWithParams(url: string, params: any): string{
        return url + '?' + this.jsonToParamsUrl(params);
    }

    static getHostName(): string{
        return WindowUtil.getWindow().location.hostname;
    }

    static getApiUrl(): string{

        let temp: string = REPORT;
        if (temp.indexOf('http') === -1){
            temp = 'http://' + UrlUtil.getHostName() + temp;
        }
        return temp;
    }

    static getApiSeguridadUrl(): string{
        let temp: string = REPORT;
        if (temp.indexOf('http') === -1){
            temp = 'http://' + UrlUtil.getHostName() + temp;
        }
        return temp;
    }

    static getApiReporteUrl(): string{
        let temp: string = REPORT;
        if (temp.indexOf('http') === -1){
            temp = 'http://' + UrlUtil.getHostName() + temp;
        }
        return temp;
    }

    static getUrl(): string{
        let temp: string = REPORT;
        if (temp){
            return temp; // si es distinto de nulo se retorna el url
        }
        if (temp.indexOf('http') === -1){
            temp = 'http://' + UrlUtil.getHostName() + temp;
        }
        return temp;
    }
}



