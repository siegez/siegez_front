import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { CreatePlanContableDto } from 'app/dto/create.plancontable.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanContableService {

  url = '';

  constructor(private http: HttpClient, private utilService: UtilService) { }

  // tslint:disable-next-line:typedef
  listar(empresa: number, annoproceso: number) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontablevista/` + empresa + `/` + annoproceso;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // tslint:disable-next-line:typedef
  consultar(empresa: number, annoproceso: number, cuentacontable: string)
  {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontable/` + empresa + `/` + annoproceso + `/` + cuentacontable;
    return this.http.get<any[]>(`${this.url}`,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
  }


  // tslint:disable-next-line:typedef
  crear( createPlanContableDto: CreatePlanContableDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();

    this.url = `${HOST}/api/plancontablenuevo`;

    return this.http.post(`${this.url}`, createPlanContableDto,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  eliminar(createPlanContableDto: CreatePlanContableDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      }),
      body: createPlanContableDto,
      responseType: 'text' as 'text'    
    };

    this.url = `${HOST}/api/plancontable`;
    return this.http.delete(`${this.url}`, options);

  }

  // tslint:disable-next-line:typedef
  editar(createPlanContableDto: CreatePlanContableDto)
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();

      this.url = `${HOST}/api/plancontable`;

      return this.http.put(`${this.url}`, createPlanContableDto,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  contar(empresa: number, annoproceso: number, cuentacontable: string)
  {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontablecontador/` + empresa + `/` + annoproceso + `/` + cuentacontable;
    return this.http.get<any[]>(`${this.url}`,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
  }

  // tslint:disable-next-line:typedef
  listarCatalogo(empresa: number, annoproceso: number, cuentacontable: string)
  {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontablecatalogo/` + empresa + `/` + annoproceso + `/` + cuentacontable;
    return this.http.get<any[]>(`${this.url}`,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
  }

  // tslint:disable-next-line:typedef
  listarCatalogoSimple(empresa: number, annoproceso: number) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontablecatalogolista/` + empresa + `/` + annoproceso;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // tslint:disable-next-line:typedef
  exportar(empresa: number, annoproceso: number) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/plancontablelista/` + empresa + `/` + annoproceso;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // tslint:disable-next-line:typedef
  consultarCuentaContable(empresa: number, annoproceso: number, cuentacontable: string)
  {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/plancontablecuentacontable/` + empresa + `/` + annoproceso + `/` + cuentacontable;
       return this.http.get<any[]>(`${this.url}`,
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
  }

}
