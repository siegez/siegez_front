import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, VALORES_INICIALES } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { CreateUsuarioDto } from 'app/dto/create.usuario.dto';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url = '';
  valoresiniciales;

  constructor(private http: HttpClient, private utilService: UtilService) { }

  // tslint:disable-next-line:typedef
  listar() {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    
    this.url = `${HOST}/api/usuariolicencia/` + this.valoresiniciales.licencia;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // tslint:disable-next-line:typedef
  consultar(username: string) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/usuario/` + username;
    console.log(this.url);
    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });

  }

  // tslint:disable-next-line:typedef
  crear( createUsuario: CreateUsuarioDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();

    this.url = `${HOST}/api/usuario`;

    return this.http.post(`${this.url}`, createUsuario,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  eliminar(username: string) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();

    this.url = `${HOST}/api/usuario`;

    return this.http.delete(`${this.url}/${username}`,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  editar(editarUsuario: CreateUsuarioDto)
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();

      this.url = `${HOST}/api/usuario`;

      return this.http.put(`${this.url}`, editarUsuario,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  contar(createUsuario: CreateUsuarioDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/usuariocontador`;

    return this.http.post(`${this.url}`, createUsuario,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
  }

}
