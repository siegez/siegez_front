import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateEmpresaDto } from 'app/dto/create.empresa.dto';

@Injectable({
    providedIn: 'root'
  })
  export class PerfilService {
    url = '';
    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    listar(empresa: string) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/perfilempresa/` + empresa;
  
      return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
  }

//   // tslint:disable-next-line:typedef
//   consultar(empresa: string) {
//     // tslint:disable-next-line:variable-name
//     const access_token = this.utilService.getToken();
//     this.url = `${HOST}/api/empresa/` + empresa;
//     console.log(this.url);
//     return this.http.get<any[]>(`${this.url}` , 
//       {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
//     });

//   }

//   // tslint:disable-next-line:typedef
//   crear( empresa: CreateEmpresaDto) {
//     // tslint:disable-next-line:variable-name
//     const access_token = this.utilService.getToken();
//     this.url = `${HOST}/api/empresa`;
//     return this.http.post(`${this.url}`, empresa,
//       {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
//         responseType: 'text' });
//   }

//   // tslint:disable-next-line:typedef
//   eliminar(empresa: string) {
//     // tslint:disable-next-line:variable-name
//     const access_token = this.utilService.getToken();
//     this.url = `${HOST}/api/empresa`;
//     return this.http.delete(`${this.url}/${empresa}`,
//       {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
//         responseType: 'text' });
//   }

//   // tslint:disable-next-line:typedef
//   editar(empresa: CreateEmpresaDto) {
//       // tslint:disable-next-line:variable-name
//       const access_token = this.utilService.getToken();
//       this.url = `${HOST}/api/empresa`;
//       return this.http.put(`${this.url}`, empresa,
//       {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
//         responseType: 'text' });
//   }
}