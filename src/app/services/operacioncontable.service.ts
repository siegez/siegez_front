import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateOperacionContableDto } from 'app/dto/create.operacioncontable.dto';
import { CreateRegistroContableDto } from 'app/dto/create.registrocontable.dto';


@Injectable({
    providedIn: 'root'
  })

export class OperacionContableService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }
    // tslint:disable-next-line:typedef
    registrar( createOperacionContableDto: CreateOperacionContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontableregistro`;
        
        return this.http.post(`${this.url}`, createOperacionContableDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            });
    }

    // tslint:disable-next-line:typedef
    crear( createOperacionContableDto: CreateOperacionContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablenuevo`;
        
        return this.http.post(`${this.url}`, createOperacionContableDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createOperacionContableDto: CreateOperacionContableDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/operacioncontable`;
    
        return this.http.put(`${this.url}`, createOperacionContableDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
    eliminar(createOperacionContableDto: CreateOperacionContableDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createOperacionContableDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/operacioncontable`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, operacioncontable: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontable/` + empresa + `/` + annoproceso + `/` + operacioncontable;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number, tipooperacion: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablelista/` + empresa + `/` + annoproceso + `/` + tipooperacion;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    listarvista(empresa: number, annoproceso: number, tipooperacion: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablevista/` + empresa + `/` + annoproceso + `/` + tipooperacion;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    contar(empresa: number, annoproceso: number, operacioncontable: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();

        this.url = `${HOST}/api/operacioncontablecontador/` + empresa + `/` + annoproceso + `/` + operacioncontable;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });  

    }

    // tslint:disable-next-line:typedef
    contarComprobante(empresa: number, entidad: string, numeroserie: string, numerocomprobanteini: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();

        this.url = `${HOST}/api/operacioncontablecontadorcom/` + empresa + `/` + entidad + `/` + numeroserie + `/` + numerocomprobanteini;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });  

    }    

    // tslint:disable-next-line:typedef
    listarCatalogo(createOperacionContableDto: CreateOperacionContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablecatalogo/`;
        
        return this.http.post<any[]>(`${this.url}`, createOperacionContableDto , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    listarCatalogoRef(createOperacionContableDto: CreateOperacionContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablecatalogoref/`;
        
        return this.http.post<any[]>(`${this.url}`, createOperacionContableDto , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    listarSaldo(createRegistroContableDto: CreateRegistroContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablesaldo/`;
        
        return this.http.post<any[]>(`${this.url}`, createRegistroContableDto , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number, tipooperacion: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/operacioncontablelista/` + empresa + `/` + annoproceso + `/` + tipooperacion;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
