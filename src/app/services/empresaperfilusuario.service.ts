import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { CreateUsuarioEmpresaPerfilDto } from 'app/dto/create.usuarioempresaperfil.dto';
// import { CreateUsuarioDto } from 'app/dto/create.usuario.dto';


@Injectable({
  providedIn: 'root'
})
export class EmpresaPerfilUsuarioService {

  url = '';

  constructor(private http: HttpClient, private utilService: UtilService) { }

  // tslint:disable-next-line:typedef
  listar(username: string) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/empresaperfilusuario/` + username;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // tslint:disable-next-line:typedef
  consultar(createUsuarioEmpresaPerfilDto: CreateUsuarioEmpresaPerfilDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    
    this.url = `${HOST}/api/empresaperfilusuario`;

    return this.http.post(`${this.url}`, createUsuarioEmpresaPerfilDto,
       {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
  }

  // tslint:disable-next-line:typedef
  crear( createUsuarioEmpresaPerfilDto: CreateUsuarioEmpresaPerfilDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();

    this.url = `${HOST}/api/empresaperfilusuarionuevo`;

    return this.http.post(`${this.url}`, createUsuarioEmpresaPerfilDto,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  eliminar(createUsuarioEmpresaPerfilDto: CreateUsuarioEmpresaPerfilDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      }),
      body: createUsuarioEmpresaPerfilDto,
      responseType: 'text' as 'text'    
    };

    this.url = `${HOST}/api/empresaperfilusuario`;
    return this.http.delete(`${this.url}`, options);

  }

  // tslint:disable-next-line:typedef
  editar(createUsuarioEmpresaPerfilDto: CreateUsuarioEmpresaPerfilDto)
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();

      this.url = `${HOST}/api/empresaperfilusuario`;

      return this.http.put(`${this.url}`, createUsuarioEmpresaPerfilDto,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

}
