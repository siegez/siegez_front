import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateEnteDireccionDto } from 'app/dto/create.entedireccion.dto';

@Injectable({
    providedIn: 'root'
  })

export class EnteDireccionService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear( createEnteDireccionDto: CreateEnteDireccionDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedireccionnuevo`;
        
        return this.http.post(`${this.url}`, createEnteDireccionDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createEnteDireccionDto: CreateEnteDireccionDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/entedireccion`;
    
        return this.http.put(`${this.url}`, createEnteDireccionDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createEnteDireccionDto: CreateEnteDireccionDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createEnteDireccionDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/entedireccion`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, entidad: string, vez: number)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedireccion/` + empresa + `/` + entidad + `/` + vez;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, entidad: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedireccionlista/` + empresa + `/` + entidad ;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
}
