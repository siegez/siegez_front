import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateParametroDto } from 'app/dto/create.parametro.dto';


@Injectable({
    providedIn: 'root'
  })

export class ParametroService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createParametroDto: CreateParametroDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/parametronuevo`;
        
        return this.http.post(`${this.url}`, createParametroDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createParametroDto: CreateParametroDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/parametro`;
    
        return this.http.put(`${this.url}`, createParametroDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createParametroDto: CreateParametroDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createParametroDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/parametro`;
        return this.http.delete(`${this.url}`, options); 
    } 

   // tslint:disable-next-line:typedef
   consultar(empresa: number, modulo: string, parametro: string)
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/parametro/` + empresa + `/` + modulo + `/` + parametro;
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
   }

   // tslint:disable-next-line:typedef
   listar(empresa: number) 
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/parametrolista/` + empresa;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
   }

   // tslint:disable-next-line:typedef
   listarsubmodulo(empresa: number, annoproceso: number, modulo: string, submodulo: string) 
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/parametrolistasubmodulo/` + empresa + `/` + annoproceso + `/` +  modulo + `/` + submodulo;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
   }

   // tslint:disable-next-line:typedef
   contar(empresa: number, modulo: string, parametro: string) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/parametrocontador/` + empresa + `/` + modulo + `/` + parametro;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/parametrocatalogo/` + empresa;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

    // tslint:disable-next-line:typedef
    exportar(empresa: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/parametrolista/` + empresa;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
