import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateRegistroContableDto } from 'app/dto/create.registrocontable.dto';


@Injectable({
    providedIn: 'root'
  })

export class RegistroContableService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createRegistroContableDto: CreateRegistroContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/registrocontablenuevo`;
        
        return this.http.post(`${this.url}`, createRegistroContableDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createRegistroContableDto: CreateRegistroContableDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/registrocontable`;
    
        return this.http.put(`${this.url}`, createRegistroContableDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createRegistroContableDto: CreateRegistroContableDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createRegistroContableDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/registrocontable`;
        return this.http.delete(`${this.url}`, options); 
    } 
    // tslint:disable-next-line:typedef
    eliminarOperacion(createRegistroContableDto: CreateRegistroContableDto) 
    {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       const options = {
           headers: new HttpHeaders({
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${access_token}`
           }),
           body: createRegistroContableDto,
           responseType: 'text' as 'text'    
       };
       
       this.url = `${HOST}/api/registrocontableoperacion`;
       return this.http.delete(`${this.url}`, options); 
   } 

   // tslint:disable-next-line:typedef
   consultar(empresa: number, annoproceso: number, asientocontable: number, registrocontable: number)
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/registrocontable/` + empresa + `/` + annoproceso + `/` + asientocontable + `/` + registrocontable;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
   }

   // tslint:disable-next-line:typedef
   listar(empresa: number, annoproceso: number, asientocontable: number) 
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/registrocontablelista/` + empresa + `/` + annoproceso + `/` + asientocontable;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
   }

   // tslint:disable-next-line:typedef
   listarOperacion(empresa: number, annoproceso: number, operacioncontable: string) 
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/registrocontablelistaoperacion/` + empresa + `/` + annoproceso + `/` + operacioncontable;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
   }

   // tslint:disable-next-line:typedef
   contar(empresa: number, annoproceso: number, asientocontable: number, registrocontable: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/registrocontablecontador/` + empresa + `/` + annoproceso + `/` + asientocontable + `/` + registrocontable;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number, annoproceso: number, asientocontable: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/registrocontablecatalogo/` + empresa + `/` + annoproceso + `/` + asientocontable;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number, asientocontable: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/registrocontablelista/` + empresa + `/` + annoproceso + `/` + asientocontable;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
