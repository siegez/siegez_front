import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateAsientoContableDto } from 'app/dto/create.asientocontable.dto';


@Injectable({
    providedIn: 'root'
  })

export class AsientoContableService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createAsientoContableDto: CreateAsientoContableDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/asientocontablenuevo`;
        
        return this.http.post(`${this.url}`, createAsientoContableDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createAsientoContableDto: CreateAsientoContableDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/asientocontable`;
    
        return this.http.put(`${this.url}`, createAsientoContableDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createAsientoContableDto: CreateAsientoContableDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createAsientoContableDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/asientocontable`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, asientocontable: number)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/asientocontable/` + empresa + `/` + annoproceso + `/` + asientocontable;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/asientocontablelista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

   // tslint:disable-next-line:typedef
   contar(empresa: number, annoproceso: number, asientocontable: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/asientocontablecontador/` + empresa + `/` + annoproceso + `/` + asientocontable;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number, annoproceso: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/asientocontablecatalogo/` + empresa + `/` + annoproceso;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/asientocontablelista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
