import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateCentroCostoDto } from 'app/dto/create.centrocosto.dto';


@Injectable({
    providedIn: 'root'
  })

export class CentroCostoService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createCentroCostoDto: CreateCentroCostoDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/centrocostonuevo`;
        
        return this.http.post(`${this.url}`, createCentroCostoDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createCentroCostoDto: CreateCentroCostoDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/centrocosto`;
    
        return this.http.put(`${this.url}`, createCentroCostoDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    eliminar(createCentroCostoDto: CreateCentroCostoDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createCentroCostoDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/centrocosto`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, centrocosto: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/centrocosto/` + empresa + `/` + annoproceso + `/` + centrocosto;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/centrocostolista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

   // tslint:disable-next-line:typedef
   contar(empresa: number, annoproceso: number, centrocosto: string) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/centrocostocontador/` + empresa + `/` + annoproceso + `/` + centrocosto;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number, annoproceso: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/centrocostocatalogolista/` + empresa + `/` + annoproceso;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

   // tslint:disable-next-line:typedef
   listarCatalogofiltro(empresa: number, annoproceso: number, centrocosto: string) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/centrocostocatalogo/` + empresa + `/` + annoproceso + `/` + centrocosto;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }   

   // tslint:disable-next-line:typedef
   listarCatalogoFiltroNotIn(createCentroCostoDto: CreateCentroCostoDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.url = `${HOST}/api/centrocostofiltro`; 
    return this.http.post<any[]>(`${this.url}` , createCentroCostoDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        }
      );
    }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/centrocostolista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
