import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { TOKEN_NAME, KEY_USER_INFO } from '../_shared/var.constant';
import * as decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  
  constructor(private router: Router) {}

  public getUserLogued(): any {
    const token = localStorage.getItem(TOKEN_NAME);
    return token;
  }

  public isLogued(): boolean {
    const token = localStorage.getItem(TOKEN_NAME);
    console.log(token);
    return token != null && token !== undefined;
  }

  // tslint:disable-next-line:typedef
  public getToken() {
    const token = localStorage.getItem(TOKEN_NAME);
    return token;
  }
  
  // tslint:disable-next-line:typedef
  public saveAccessToken(accessToken: any) {
    const decodedToken = decode(accessToken.token);
    localStorage.setItem(TOKEN_NAME, accessToken.token);
    
  } 

  // tslint:disable-next-line:typedef
  public deleteToken() {
    localStorage.removeItem(TOKEN_NAME);
    localStorage.removeItem(KEY_USER_INFO);
    this.router.navigate(['auth/login']);
  }
}
