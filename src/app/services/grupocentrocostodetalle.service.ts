import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateGrupoCentroCostoDetalleDto } from 'app/dto/create.grupocentrocostodetalle.dto';


@Injectable({
    providedIn: 'root'
  })

export class GrupoCentroCostoDetalleService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createGrupoCentroCostoDetalleDto: CreateGrupoCentroCostoDetalleDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostodetallenuevo`;
        
        return this.http.post(`${this.url}`, createGrupoCentroCostoDetalleDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createGrupoCentroCostoDetalleDto: CreateGrupoCentroCostoDetalleDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/grupocentrocostodetalle`;
    
        return this.http.put(`${this.url}`, createGrupoCentroCostoDetalleDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    eliminar(createGrupoCentroCostoDetalleDto: CreateGrupoCentroCostoDetalleDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createGrupoCentroCostoDetalleDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/grupocentrocostodetalle`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, grupocentrocosto: string, centrocosto: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostodetalle/` + empresa + `/` + annoproceso + `/` + grupocentrocosto + `/` + centrocosto;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number, grupocentrocosto: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostodetallelista/` + empresa + `/` + annoproceso + `/` + grupocentrocosto;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    contar(empresa: number, annoproceso: number, grupocentrocosto: string, centrocosto: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostodetallecontador/` + empresa + `/` + annoproceso + `/` + grupocentrocosto + `/` + centrocosto;
        
        return this.http.get<any[]>(`${this.url}` , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }  

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostodetallelista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
