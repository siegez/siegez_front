import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST, VALORES_INICIALES,  } from 'app/_shared/var.constant';
import { CreateEjercicioDto } from 'app/dto/create.ejercicio.dto';

@Injectable({
    providedIn: 'root'
  })
  export class EjercicioService {

    url = ''; 
    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createEjercicioDto: CreateEjercicioDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/ejercicio`;
        
        return this.http.post(`${this.url}`, createEjercicioDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createEjercicioDto: CreateEjercicioDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/ejercicio`;
    
        return this.http.put(`${this.url}`, createEjercicioDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createEjercicioDto: CreateEjercicioDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createEjercicioDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/ejercicio`;
        return this.http.delete(`${this.url}`, options); 
    } 

   // tslint:disable-next-line:typedef
   consultar(empresa: number, annoproceso: number)
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/ejercicio/` + empresa + `/` + annoproceso;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
   }

   // tslint:disable-next-line:typedef
   listar(empresa: number) 
   {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/ejerciciolista/` + empresa;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
   }

   // tslint:disable-next-line:typedef
   contar(empresa: number, annoproceso: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/ejerciciocontador/` + empresa + `/` + annoproceso;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/ejerciciocatalogo/` + empresa;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

    // tslint:disable-next-line:typedef
    exportar(empresa: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/ejerciciolista/` + empresa;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
}
