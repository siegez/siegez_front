import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateEnteCuentaBancariaDto } from 'app/dto/create.entecuentabancaria.dto';

@Injectable({
    providedIn: 'root'
  })

export class EnteCuentaBancariaService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear( createEnteCuentaBancariaDto: CreateEnteCuentaBancariaDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancarianuevo`;
        
        return this.http.post(`${this.url}`, createEnteCuentaBancariaDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createEnteCuentaBancariaDto: CreateEnteCuentaBancariaDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/entecuentabancaria`;
    
        return this.http.put(`${this.url}`, createEnteCuentaBancariaDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createEnteCuentaBancariaDto: CreateEnteCuentaBancariaDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createEnteCuentaBancariaDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/entecuentabancaria`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, entidad: string, cuentabancaria: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancaria/` + empresa + `/` + annoproceso + `/` + entidad + `/` + cuentabancaria;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });

    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number, entidad: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancarialista/` +  empresa + `/` + annoproceso + `/` + entidad ;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    contar(empresa: number, annoproceso: number, entidad: string, cuentabancaria: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancariacontador/` + empresa + `/` + annoproceso + `/` + entidad + `/` + cuentabancaria;
        
        return this.http.get<any[]>(`${this.url}` , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listarCatalogo(empresa: number, annoproceso: number, entidad: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancariacatalogo/` + empresa + `/` + annoproceso + `/` + entidad;
        
        return this.http.get<any[]>(`${this.url}` ,  
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number, entidad: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entecuentabancarialista/` + empresa + `/` + annoproceso + `/` + entidad;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

}
