import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateGrupoCentroCostoDto } from 'app/dto/create.grupocentrocosto.dto';


@Injectable({
    providedIn: 'root'
  })

export class GrupoCentroCostoService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    registrar( createGrupoCentroCostoDto: CreateGrupoCentroCostoDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostoregistro`;
        
        return this.http.post(`${this.url}`, createGrupoCentroCostoDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            });
    }

    // tslint:disable-next-line:typedef
    crear(createGrupoCentroCostoDto: CreateGrupoCentroCostoDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostonuevo`;
        
        return this.http.post(`${this.url}`, createGrupoCentroCostoDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createGrupoCentroCostoDto: CreateGrupoCentroCostoDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/grupocentrocosto`;
    
        return this.http.put(`${this.url}`, createGrupoCentroCostoDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    eliminar(createGrupoCentroCostoDto: CreateGrupoCentroCostoDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createGrupoCentroCostoDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/grupocentrocosto`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, annoproceso: number, grupocentrocosto: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocosto/` + empresa + `/` + annoproceso + `/` + grupocentrocosto;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostolista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

   // tslint:disable-next-line:typedef
   contar(empresa: number, annoproceso: number, grupocentrocosto: string) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/grupocentrocostocontador/` + empresa + `/` + annoproceso + `/` + grupocentrocosto;
       
       return this.http.get<any[]>(`${this.url}` , 
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
           });
   }

   // tslint:disable-next-line:typedef
   listarCatalogo(empresa: number, annoproceso: number) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/grupocentrocostocatalogolista/` + empresa + `/` + annoproceso;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }

   // tslint:disable-next-line:typedef
   listarCatalogofiltro(empresa: number, annoproceso: number, grupocentrocosto: string) 
   {
       // tslint:disable-next-line:variable-name
       const access_token = this.utilService.getToken();
       
       this.url = `${HOST}/api/grupocentrocostocatalogo/` + empresa + `/` + annoproceso + `/` + grupocentrocosto;
       
       return this.http.get<any[]>(`${this.url}` ,  
           {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
       });
   }   

    // tslint:disable-next-line:typedef
    exportar(empresa: number, annoproceso: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/grupocentrocostolista/` + empresa + `/` + annoproceso;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
