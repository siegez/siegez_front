import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';

@Injectable({
    providedIn: 'root'
  })
  export class ConsultaService {
    url = '';
    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    listarValoresIniciales(username: string) {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/valoresiniciales/` + username;

        return this.http.get<any[]>(`${this.url}` , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    listarTiposEmpresa(empresa: ValorCampoDto) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.url = `${HOST}/api/valorcampo`;
      return this.http.post<any[]>(`${this.url}` , empresa,
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
          }
        );
    }

    // tslint:disable-next-line:typedef
    listarCatalogoDescripcion(valorcampo: ValorCampoDto) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.url = `${HOST}/api/valorcampodescripcion`; 
      return this.http.post<any[]>(`${this.url}` , valorcampo,
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
          }
        );
    }

    // tslint:disable-next-line:typedef
    listarCatalogoDescripcionFiltro(valorcampo: ValorCampoDto) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.url = `${HOST}/api/valorcampofiltro`; 
      return this.http.post<any[]>(`${this.url}` , valorcampo,
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
          }
        );
    }

    // tslint:disable-next-line:typedef
    listarCatalogoCodigo(valorcampo: ValorCampoDto) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.url = `${HOST}/api/valorcampocodigo`; 
      return this.http.post<any[]>(`${this.url}` , valorcampo,
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
          }
        );
    }

    // tslint:disable-next-line:typedef
    listarTablaDescripcion(empresa: string, tabla: number ) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/tabladetalledescripcion/` + empresa + `/` + tabla;

      return this.http.get<any[]>(`${this.url}` , 
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }

    // tslint:disable-next-line:typedef
    listarTablaCodigo(empresa: string, tabla: number ) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/tabladetallecodigo/` + empresa + `/` + tabla;

      return this.http.get<any[]>(`${this.url}` , 
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }

    // tslint:disable-next-line:typedef
    validarDocumentoReferencia(empresa: string, ejeciciocontable: number, tipodocumento: string, moneda: string ) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/documentoreferenciacompra/` + empresa + `/` + ejeciciocontable + `/` + tipodocumento + `/` + moneda;
      return this.http.get<any[]>(`${this.url}` , 
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }    

    // tslint:disable-next-line:typedef
    listarCentroCosto(empresa: number, ejeciciocontable: number) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/listacentrocosto/` + empresa + `/` + ejeciciocontable;

      return this.http.get<any[]>(`${this.url}` , 
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }    

    // tslint:disable-next-line:typedef
    listarUbicacionGeografica() {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/listaubicaciongeografica`;

      return this.http.get<any[]>(`${this.url}` , 
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }  

  }
