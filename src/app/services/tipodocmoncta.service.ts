import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateTipoDocMonCtaDto } from 'app/dto/create.tipodocmoncta.dto';

@Injectable({
    providedIn: 'root'
  })

  
export class TipoDocMonCtaService {
    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createTipoDocMonCtaDto: CreateTipoDocMonCtaDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/tipodocmonctanuevo`;
        
        return this.http.post(`${this.url}`, createTipoDocMonCtaDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createTipoDocMonCtaDto: CreateTipoDocMonCtaDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/tipodocmoncta`;
    
        return this.http.put(`${this.url}`, createTipoDocMonCtaDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createTipoDocMonCtaDto: CreateTipoDocMonCtaDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createTipoDocMonCtaDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/tipodocmoncta`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, ejerciciocontable: number, tipodocumento: string,  moneda: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/tipodocmoncta/` + empresa + `/` + ejerciciocontable + `/` + tipodocumento + `/` + moneda;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, ejerciciocontable: number) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/tipodocmonctalista/` + empresa + `/` + ejerciciocontable;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    contar(empresa: number, ejerciciocontable: number, tipodocumento: string,  moneda: string)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/tipodocmonctacontador/` + empresa + `/` + ejerciciocontable + `/` + tipodocumento + `/` + moneda;
        
        return this.http.get<any[]>(`${this.url}` , 
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    exportar(empresa: number, ejerciciocontable: number)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/tipodocmonctalista/` + empresa + `/` + ejerciciocontable;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
}
