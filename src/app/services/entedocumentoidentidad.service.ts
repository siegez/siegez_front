import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';
import { CreateEnteDocumentoIdentidadDto } from 'app/dto/create.entedocumentoidentidad.dto';

@Injectable({
    providedIn: 'root'
  })

export class EnteDocumentoIdentidadService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    crear(createEnteDocumentoIdentidadDto: CreateEnteDocumentoIdentidadDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedocumentoidentidadnuevo`;
        
        return this.http.post(`${this.url}`, createEnteDocumentoIdentidadDto,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            responseType: 'text' });
    }

    // tslint:disable-next-line:typedef
    editar(createEnteDocumentoIdentidadDto: CreateEnteDocumentoIdentidadDto)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
    
        this.url = `${HOST}/api/entedocumentoidentidad`;
    
        return this.http.put(`${this.url}`, createEnteDocumentoIdentidadDto,
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
    }

     // tslint:disable-next-line:typedef
     eliminar(createEnteDocumentoIdentidadDto: CreateEnteDocumentoIdentidadDto) 
     {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        const options = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
            }),
            body: createEnteDocumentoIdentidadDto,
            responseType: 'text' as 'text'    
        };
        
        this.url = `${HOST}/api/entedocumentoidentidad`;
        return this.http.delete(`${this.url}`, options); 
    } 

    // tslint:disable-next-line:typedef
    consultar(empresa: number, entidad: string, vez: number)
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedocumentoidentidad/` + empresa + `/` + entidad + `/` + vez;
        
        return this.http.get<any[]>(`${this.url}`,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
            });
    }

    // tslint:disable-next-line:typedef
    listar(empresa: number, entidad: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/entedocumentoidentidadlista/` + empresa + `/` + entidad ;
    
        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
}
