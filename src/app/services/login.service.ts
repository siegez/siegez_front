import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { AccessTokenDto } from '../dto/access.token.dto';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = `${HOST}/authenticate`;

  constructor(private http: HttpClient, private router: Router) { }

  // tslint:disable-next-line:typedef
  login(user: string, pass: string) {
    const body = { username: user, password: pass };
    return this.http.post<AccessTokenDto>(`${this.url}`, body, {
      headers: new HttpHeaders().set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
    });
  }  

  // tslint:disable-next-line:typedef
  estaLogeado() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    return token != null;
  } 

}
