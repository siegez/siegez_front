import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST } from 'app/_shared/var.constant';

@Injectable({
    providedIn: 'root'
  })

export class ReporteService {

    url = '';

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    exportarRegistroCompra(empresa: number, annoproceso: number, periodocontable: string, ruc: string, nombreempresa: string, moneda: string, orden: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/listaregistrocompra/` + empresa + `/` + annoproceso + `/` + periodocontable + `/` + ruc + `/` + nombreempresa + `/` + moneda + `/` + orden;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarPLERegistroCompra(empresa: number, annoproceso: number, periodocontable: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/pleregistrocompra/` + empresa + `/` + annoproceso + `/` + periodocontable;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarPLERegistroCompraNoDom(empresa: number, annoproceso: number, periodocontable: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/pleregistrocompranodom/` + empresa + `/` + annoproceso + `/` + periodocontable;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarPLERegistroVenta(empresa: number, annoproceso: number, periodocontable: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/pleregistroventa/` + empresa + `/` + annoproceso + `/` + periodocontable;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarPLAMEPrestadorServicio(empresa: number, annoproceso: number, periodocontable: string, ruc: string, nombreempresa: string, moneda: string, orden: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/plameperser/` + empresa + `/` + annoproceso + `/` + periodocontable + `/` + ruc + `/` + nombreempresa + `/` + moneda + `/` + orden;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarPLELibroDiario(empresa: number, annoproceso: number, periodocontable: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/plelibrodiario/` + empresa + `/` + annoproceso + `/` + periodocontable;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    // tslint:disable-next-line:typedef
    exportarLibroDiario(empresa: number, annoproceso: number, periodocontable: string, subini: string,
                        subfin: string, ruc: string, nombreempresa: string, moneda: string, glosa: string) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/listalibrodiario/` + empresa + `/` + annoproceso + `/` + periodocontable + `/` + subini + `/` + subfin + `/` + ruc + `/` + nombreempresa + `/` + moneda + `/` + glosa;

        return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
    
}
