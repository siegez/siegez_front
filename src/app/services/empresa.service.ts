import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { HOST, VALORES_INICIALES } from 'app/_shared/var.constant';
import { CreateEmpresaDto } from 'app/dto/create.empresa.dto';

@Injectable({
    providedIn: 'root'
  })
  export class EmpresaService {
    url = '';
    valoresiniciales;

    constructor(private http: HttpClient, private utilService: UtilService) { }

    // tslint:disable-next-line:typedef
    registrar( empresa: CreateEmpresaDto) 
    {
        // tslint:disable-next-line:variable-name
        const access_token = this.utilService.getToken();
        
        this.url = `${HOST}/api/empresaregistro`;
        
        return this.http.post(`${this.url}`, empresa,
            {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
            });
    }

    // tslint:disable-next-line:typedef
    listar() {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
      
      this.url = `${HOST}/api/empresalicencia/` + this.valoresiniciales.licencia;
  
      return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }

    // tslint:disable-next-line:typedef
    listarLicencia(licencia: string) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
      
      this.url = `${HOST}/api/empresalicencia/` + licencia;
  
      return this.http.get<any[]>(`${this.url}` , 
        {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
    }

  // tslint:disable-next-line:typedef
  consultar(empresa: string) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.url = `${HOST}/api/empresa/` + empresa;
    console.log(this.url);
    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });

  }

  // tslint:disable-next-line:typedef
  crear( empresa: CreateEmpresaDto) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.url = `${HOST}/api/empresa`;
    return this.http.post(`${this.url}`, empresa,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  eliminar(empresa: number) {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.url = `${HOST}/api/empresa`;
    return this.http.delete(`${this.url}/${empresa}`,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  editar(empresa: CreateEmpresaDto) {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      this.url = `${HOST}/api/empresa`;
      return this.http.put(`${this.url}`, empresa,
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json'),
        responseType: 'text' });
  }

  // tslint:disable-next-line:typedef
  listarCatalogo(licencia: string) 
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/empresacatalogo/` + licencia;
      
      return this.http.get<any[]>(`${this.url}` ,  
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
  }

  // tslint:disable-next-line:typedef
  listarCatalogoUsuario(username: string) 
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/empresausuario/` + username;
      
      return this.http.get<any[]>(`${this.url}` ,  
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
      });
  }

  // tslint:disable-next-line:typedef
  replicar(empresaori: number, annoori: number, empresades: number, annodes: number)
  {
      // tslint:disable-next-line:variable-name
      const access_token = this.utilService.getToken();
      
      this.url = `${HOST}/api/empresareplicar/` + empresaori + `/` + annoori + `/` + empresades + `/` + annodes;
      
      return this.http.get<any[]>(`${this.url}`,
          {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
          });
  }


  // tslint:disable-next-line:typedef
  exportar() 
  {
    // tslint:disable-next-line:variable-name
    const access_token = this.utilService.getToken();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    
    this.url = `${HOST}/api/empresalicencia/` + this.valoresiniciales.licencia;

    return this.http.get<any[]>(`${this.url}` , 
      {  headers: new HttpHeaders().set('Authorization', `Bearer ${access_token}`).set('Content-Type', 'application/json')
    });      
  }
}
