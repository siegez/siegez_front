import { FuseNavigation } from '@fuse/types';
import { id } from '@swimlane/ngx-datatable';

export const navigation: FuseNavigation[] = [
    {
        id       : 'contabilidad',
        title    : 'Contabilidad',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'mantabcon',
                title    : 'Mantenimiento de Tablas',
                type     : 'collapsable',
                icon     : 'table_chart',
                children : [
                    {
                        id   : 'tabmaecon',
                        title: 'Tablas Maestras',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/apps/contabilidad/tablamaestracontabilidad'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Plan General Empresarial',
                        type : 'item',
                        icon : 'format_align_left',
                        url  : '/contabilidad/mantenimiento/plancontable'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Centro de Costo',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/centrocosto'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Grupo de Centros de Costos',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/grupocentrocosto'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Detracción',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/detraccion'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Socio de Negocio',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/socionegocio'
                    },
                    {
                        id   : 'ctabca',
                        title: 'Entidad Financiera',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/cuentabancaria'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Tipo de Cambio',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/contabilidad/mantenimiento/tipocambio'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Configuración Contable',
                        type : 'collapsable',
                        icon : 'wrap_text',
                        children  : [
                            {
                                id   : 'plagenemp',
                                title: 'IGV',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/igv'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'Renta Cuarta Categoría',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/rentacuarta'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'Tipo Documento',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/tipodocumento'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'Tipo Documento por Operación',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/tipodocumentoinicio'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'SubDiario',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/subdiario'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'SubDiario por Operación',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/subdiarioinicio'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'Cuentas Contables Automáticas',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/cuentaautomatica'
                            },
                            {
                                id   : 'plagenemp',
                                title: 'Cuentas Contables por Operación',
                                type : 'item',
                                icon : 'wrap_text',
                                url  : '/contabilidad/mantenimiento/cuentaopedocmon'
                            }
                        ]
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Configuración Reporte',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/apps/contabilidad/plangeneralempresarial'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Configuración Cierre del Ejercicio',
                        type : 'item',
                        icon : 'wrap_text',
                        url  : '/apps/contabilidad/plangeneralempresarial'
                    }
                ]
            },
            {
                id       : 'e-commerce',
                title    : 'Transacciones',
                type     : 'collapsable',
                icon     : 'transform',
                children : [
                    {
                        id        : 'products',
                        title     : 'Compras',
                        type      : 'item',
                        icon      : 'library_books',
                        url       : '/contabilidad/transacciones/operacioncompra',
                        exactMatch: true
                    },
                    {
                        id        : 'productDetail',
                        title     : 'DUAS',
                        type      : 'item',
                        icon      : 'flight',
                        url       : '/contabilidad/transacciones/operaciondua',
                        // exactMatch: true
                    },
                    {
                        id        : 'productDetail',
                        title     : 'No Domiciliado',
                        type      : 'item',
                        icon      : 'directions_boat',
                        url       : '/contabilidad/transacciones/operacionnodomiciliado',
                    },
                    {
                        id        : 'orders',
                        title     : 'Honorarios',
                        type      : 'item',
                        icon      : 'account_box',
                        url       : '/contabilidad/transacciones/operacionhonorario',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Ventas',
                        type      : 'item',
                        icon      : 'attach_money',
                        url       : '/contabilidad/transacciones/operacionventa',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Caja-Egresos',
                        type      : 'item',
                        icon      : 'remove_circle',
                        url       : '/contabilidad/transacciones/operacionegreso',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Caja-Ingresos',
                        type      : 'item',
                        icon      : 'add_circle',
                        url       : '/contabilidad/transacciones/operacioningreso',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Diario',
                        type      : 'item',
                        icon      : 'list',
                        url       : '/contabilidad/transacciones/operaciondiario',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Diferencia de Cambio',
                        type      : 'item',
                        icon      : 'iso',
                        url       : '/contabilidad/transacciones/operaciondiferenciacambio',
                        exactMatch: true
                    },
                    {
                        id        : 'orderDetail',
                        title     : 'Rendición de Cuentas',
                        type      : 'item',
                        icon      : 'local_atm',
                        url       : '/contabilidad/transacciones/operacionrendicion',
                        exactMatch: true
                    } // person receipt
                ]
            },
            {
                id       : 'calendar',
                title    : 'Procesos',
                type     : 'item',
                icon     : 'sync', 
                url      : '/eeesample'
            },
            {
                id       : 'calendar',
                title    : 'Reportes',
                type     : 'item',
                icon     : 'file_copy',
                url      : '/contabilidad/reportescontabilidad'
            },
            {
                id       : 'calendar',
                title    : 'Tablero de Control',
                type     : 'item',
                icon     : 'dashboard',
                url      : '/contabilidad/tablerocontabilidad'
            }
        ]
    }, 
    {
        id       : 'Usuario',
        title    : 'Usuario',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'products',
                title    : 'Cambiar Clave de Acceso',
                type     : 'item',
                icon     : 'vpn_key',
                url       : '/apps/e-commerce/products',
                exactMatch: true
            },
            {
                id        : 'productDetail',
                title     : 'Cambiar Empresa y Año de Proceso',
                type      : 'item',
                icon      : 'business',
                url       : '/sistema/mantenimiento/empresaannoproceso',
                // exactMatch: true
            }
        ]
    }, 
    {
        id       : 'Sistema',
        title    : 'Sistema',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'mantabcon',
                title    : 'Mantenimiento de Tablas',
                type     : 'collapsable',
                icon     : 'table_chart',
                children : [
                    {
                        id   : 'tabmaecon',
                        title: 'Empresas',
                        type : 'item',
                        icon : 'account_balance',
                        url  : '/sistema/mantenimiento/empresas'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Perfiles',
                        type : 'item',
                        icon : 'assignment_ind',
                        url  : '/pages/sample'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Usuarios',
                        type : 'item',
                        icon : 'people',
                        url  : '/sistema/mantenimiento/usuarios'
                    },
                    {
                        id   : 'plagenemp',
                        title: 'Año - Valor Inicial',
                        type : 'item',
                        icon : 'calendar_today',
                        url  : '/sistema/mantenimiento/valorinicialanno'
                    }
                ]
            }            
        ]
    }
];
