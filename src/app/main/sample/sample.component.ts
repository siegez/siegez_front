import { Component, ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { UtilService } from '../../services/util.service';

import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UsuarioService } from 'app/services/usuario.service';
import { CreateUsuarioDto } from 'app/dto/create.usuario.dto';

@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class SampleComponent{

    @ViewChild('modalCreateUser') modalCreateUser: any;
    createUser: CreateUsuarioDto = new CreateUsuarioDto();


    reorderable = true;
    rows: any[];

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private useService: UsuarioService,
        private utilService: UtilService,
        private router: Router,
        private modalService: NgbModal)
    {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
        if (!utilService.isLogued()) {this.router.navigate([`auth/login`]); }
        this.fillData();
    }



    // tslint:disable-next-line:typedef
    openDialogCreateUser(){
        this.modalService.open(this.modalCreateUser, { ariaLabelledBy: 'modal-basic-title' });
    }


    // tslint:disable-next-line:typedef
    delete(event) {
        this.useService.eliminar(event.username).subscribe(data=>{
            this.fillData();
        });
    }


    // tslint:disable-next-line:typedef
    createPerson(myModal: any){
        myModal.dismiss();
        this.useService.crear(this.createUser).subscribe(data => {
            this.fillData();
        });
    }


    // tslint:disable-next-line:typedef
    private fillData(){

        this.useService.listar().subscribe(data => {
            this.rows = data;
            console.log(data);
        });
    }
    
}
