import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmpresaPerfilUsuarioService } from 'app/services/empresaperfilusuario.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { CreateUsuarioEmpresaPerfilDto } from 'app/dto/create.usuarioempresaperfil.dto';
import { EmpresaService } from 'app/services/empresa.service';
import { MatTableDataSource } from '@angular/material/table';
import { PerfilService } from 'app/services/perfil.service';

@Component({
  selector: 'gez-usuario-empresaperfil-form',
  templateUrl: './usuario-empresaperfil-form.component.html',
  styleUrls: ['./usuario-empresaperfil-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class UsuarioEmpresaperfilFormComponent implements OnInit {
  action: string;
  usuarioempresaperfil: any;
  parametroData;
  usuarioempresaperfilForm: FormGroup;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';
  hidegrabar = false;
  dialogTitle: string;
  edit = true;

  hide = true;
  // rows: any[];
  listaEmpresa: any = [];
  listaPerfil: any = [];
  valoresiniciales;
  checkEstado = false;
  
  public dataSource = new MatTableDataSource<any>();

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<UsuarioEmpresaperfilFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private empresaService: EmpresaService,
    private perfilService: PerfilService,
    private empresaperfilusuarioService: EmpresaPerfilUsuarioService,  
  ) 
  { 
    this.action = _data.action;
    this.parametroData = JSON.stringify(this._data);
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
        
    if ( this.action === 'editar' )
    {
        this.dialogTitle = 'Editar Empresa - Perfil';        
        this.usuarioempresaperfil = new CreateUsuarioEmpresaPerfilDto();

        this.empresaperfilusuarioService.consultar(this.parametroData).subscribe(data => {
            this.usuarioempresaperfil = data;
            this.checkEstado = (this.usuarioempresaperfil.estado === 'A' ) ? true : false;  
            this.usuarioempresaperfilForm = this.createUsuarioEmpresaPerfilForm();
            // console.log(this.usuarioempresaperfilForm); 
        }, err => {
          this._matSnackBar.open('Error de consulta', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
      }
      );        
    }
    else
    {
        this.dialogTitle = 'Nueva Empresa - Perfil';
        this.edit = false;
        this.usuarioempresaperfil = new CreateUsuarioEmpresaPerfilDto();
        this.usuarioempresaperfil.empresa = this.valoresiniciales.empresa;
        this.usuarioempresaperfilForm = this.createUsuarioEmpresaPerfilForm();
    }
    
    this.usuarioempresaperfilForm = this.createUsuarioEmpresaPerfilForm();    
  }

  createUsuarioEmpresaPerfilForm(): FormGroup
  {
      return this._formBuilder.group({
        empresa             : [{value: this.usuarioempresaperfil.empresa, disabled: this.edit}],
        perfil              : [{value: this.usuarioempresaperfil.perfil, disabled: this.edit}],
        indDefecto          : [this.usuarioempresaperfil.indDefecto],
        estado              : [this.checkEstado],
        creacionUsuario     : [this.usuarioempresaperfil.creacionUsuario],
        creacionFecha       : [this.usuarioempresaperfil.creacionFecha],
        modificacionUsuario : [this.usuarioempresaperfil.modificacionUsuario],
        modificacionFecha   : [this.usuarioempresaperfil.modificacionFecha]        
      });
      
  }

  // tslint:disable-next-line:typedef
  crearUsuarioEmpresaPerfil(usuarioempresaperfilForm){

    this.usuarioempresaperfil.username = this._data.username;
    this.usuarioempresaperfil.empresa = usuarioempresaperfilForm.value.empresa;
    this.usuarioempresaperfil.perfil = usuarioempresaperfilForm.value.perfil;
    this.usuarioempresaperfil.indDefecto = usuarioempresaperfilForm.value.indDefecto;
    this.usuarioempresaperfil.estado = (this.usuarioempresaperfilForm.value.estado) ? 'A' : 'I';
    this.usuarioempresaperfil.creacionUsuario = this.valoresiniciales.username;
    this.usuarioempresaperfil.creacionFecha = '2020-01-01';
    this.usuarioempresaperfil.modificacionUsuario = this.valoresiniciales.username;
    this.usuarioempresaperfil.modificacionFecha = '2020-01-01';

    this.empresaperfilusuarioService.crear(this.usuarioempresaperfil).subscribe(data => {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

    }, err => {
        this._matSnackBar.open('Error, empresa-perfil no registrado', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }    

  // tslint:disable-next-line:typedef
  editarUsuarioEmpresaPerfil(usuarioempresaperfilForm){

    this.usuarioempresaperfil.indDefecto = usuarioempresaperfilForm.value.indDefecto;
    this.usuarioempresaperfil.estado = (this.usuarioempresaperfilForm.value.estado) ? 'A' : 'I';
    this.usuarioempresaperfil.creacionUsuario = this.valoresiniciales.username;
    this.usuarioempresaperfil.creacionFecha = '2020-01-01';
    this.usuarioempresaperfil.modificacionUsuario = this.valoresiniciales.username;
    this.usuarioempresaperfil.modificacionFecha = '2020-01-01';

    console.log(this.usuarioempresaperfil);

    this.empresaperfilusuarioService.editar(this.usuarioempresaperfil).subscribe(data => {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

    }, err => {
        this._matSnackBar.open('Error, empresa-perfil no registrado', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }

  // tslint:disable-next-line:typedef
  listarEmpresa(){
    this.empresaService.listar().subscribe(data => {
        this.dataSource.data = data;  
        this.listaEmpresa = data;

    });
  }

  // tslint:disable-next-line:typedef
  listarPerfil(empresa: string){
    this.perfilService.listar(empresa).subscribe(data => {
        this.dataSource.data = data;  
        this.listaPerfil = data;

    });
  } 
  
  // tslint:disable-next-line:typedef
  selectEmpresa(empresa: string){
    // console.log(empresa);
    this.listarPerfil(empresa);
  }

  ngOnInit(): void {

    this.listarEmpresa();

    // this.action = this._data.action;
    if ( this._data.action === 'editar' )
    {
      this.listarPerfil(this._data.empresa);
    }
    else
    {
      this.listarPerfil(this.valoresiniciales.empresa);
    }
  }
}
