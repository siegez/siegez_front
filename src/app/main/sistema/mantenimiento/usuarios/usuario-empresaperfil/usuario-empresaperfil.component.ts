import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';
import { REFRESH, MESSAGEBOX, PARAMETRO_VAR, DATOS } from 'app/_shared/var.constant';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmpresaPerfilUsuarioService } from 'app/services/empresaperfilusuario.service';
import { Router } from '@angular/router';
import { UsuarioEmpresaperfilFormComponent } from './usuario-empresaperfil-form/usuario-empresaperfil-form.component';
import { CreateUsuarioEmpresaPerfilDto } from 'app/dto/create.usuarioempresaperfil.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';


@Component({
  selector: 'gez-usuario-empresaperfil',
  templateUrl: './usuario-empresaperfil.component.html',
  styleUrls: ['./usuario-empresaperfil.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class UsuarioEmpresaperfilComponent implements OnInit {

  dialogRef: any;
  misDatos;

  @ViewChild('modalCrearUsuarioEmpresaPerfil') modalCrearUsuarioEMpresaPerfil: any;
  createUsuarioEmpresaPerfil: CreateUsuarioEmpresaPerfilDto = new CreateUsuarioEmpresaPerfilDto();
  
  displayedColumns = ['descEmpresa', 'descPerfil', 'indDefecto', 'estado', 'editar', 'eliminar'];

  listarTabla: any = [];
   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private empresaperfilusuarioService: EmpresaPerfilUsuarioService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router
   )
   { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      

   }

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data: any, filter: string) =>
    (
       data.descEmpresa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descPerfil.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 
    );
        
    this.setSettingTableCourse();
    // tslint:disable-next-line:prefer-const
    this.misDatos = JSON.parse(localStorage.getItem(DATOS));

    this.empresaperfilusuarioService.listar(this.misDatos.username).subscribe(data => {
        this.dataSource.data = data;

        this.listarTabla = data;
        this.setSettingTableCourse();
    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearUsuarioEmpresaPerfil(): void
  {
      this.dialogRef = this._matDialog.open(UsuarioEmpresaperfilFormComponent, {
              panelClass: 'usuarioempresaperfil-form-dialog',
              data      : {
              action: 'nuevo',
              username: this.misDatos.username
          }
      });

      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(REFRESH) === 'S' )
          {            
            this.ngOnInit();
            console.log('se actualiza table - new');
          }

          localStorage.setItem(REFRESH, 'N');

        }        
      );
  }

  editarUsuarioEmpresaPerfil(username: string, empresa: string, perfil: string): void
  {

      this.dialogRef = this._matDialog.open(UsuarioEmpresaperfilFormComponent, {
          panelClass: 'usuarioempresaperfil-form-dialog',
          data      : {
              action: 'editar',
              username: username,
              empresa: empresa,
              perfil: perfil
          }
      });

      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(REFRESH) === 'S' )
          {
            this.ngOnInit();
            console.log('se actualiza table - edit');
          }

          localStorage.setItem(REFRESH, 'N');

        }        
      );

  }

  eliminarUsuarioEmpresaPerfil(username: string, empresa: string, perfil: string): void
  {
      this.dialogRef = this._matDialog.open(MessageboxComponent, {
        panelClass: 'confirm-form-dialog',
          data      : {
              titulo: 'Eliminar Empresa-Perfil',
              subTitulo: '¿Está seguro de eliminar la empresa-perfil?',
              botonAceptar: 'Si',
              botonCancelar: 'No'    
          }
      });

      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(MESSAGEBOX) === 'S' )
          {
            this.createUsuarioEmpresaPerfil.username = username;
            this.createUsuarioEmpresaPerfil.empresa = empresa;
            this.createUsuarioEmpresaPerfil.perfil = perfil;
            this.empresaperfilusuarioService.eliminar(this.createUsuarioEmpresaPerfil).subscribe(data => {
            this._matSnackBar.open('Se eliminó el empresa-perfil del usuario', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 2000
            });

            this.ngOnInit();

            }, err => {
                this._matSnackBar.open('Error, empresa-perfil no eliminado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
            );
          }
          
          localStorage.setItem(MESSAGEBOX, 'N');

        }        
      );
  }
}

