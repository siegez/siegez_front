import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'app/services/usuario.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { CreateUsuarioDto } from 'app/dto/create.usuario.dto';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioFormComponent } from './usuario-form/usuario-form.component';
import { REFRESH, MESSAGEBOX, PARAMETRO_VAR, DATOS } from 'app/_shared/var.constant';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ExcelService } from 'app/services/excel.service';

@Component({
  selector: 'gez-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class UsuariosComponent implements OnInit {
  
  dialogRef: any;

  @ViewChild('modalCrearUsuario') modalCrearUsuario: any;
  createUser: CreateUsuarioDto = new CreateUsuarioDto();
  
  displayedColumns = ['username', 'nombre', 'email', 'telefono', 'estado', 'empresaperfil', 'editar', 'eliminar'];

  listarTabla: any = [];
  listarReporte: any = [];
   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }


  constructor(
    private usuarioService: UsuarioService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
    ) 
    { 
      this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
      this.paginatorlabel.firstPageLabel = 'Primera página';
      this.paginatorlabel.nextPageLabel = 'Página siguiente';
      this.paginatorlabel.previousPageLabel = 'Página anterior';
      this.paginatorlabel.lastPageLabel = 'Última página';       
      // tslint:disable-next-line:max-line-length
      this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
    }

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.username.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.nombre.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       (data.email.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 && data.email) ||
       (data.telefono.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 && data.telefono)
     );
    this.setSettingTableCourse();
    this.usuarioService.listar().subscribe(data => {
        this.dataSource.data = data;
        this.listarTabla = data;
        this.setSettingTableCourse();
    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearUsuario(): void
  {
      this.dialogRef = this._matDialog.open(UsuarioFormComponent, {
          panelClass: 'usuario-form-dialog',
          data      : {
              action: 'nuevo'
          }
      });

      // this.dialogRef.close(true).subscribe(result => {
      //   console.log(result);
      // });

      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(REFRESH) === 'S' )
          {            
            this.ngOnInit();
          }

          localStorage.setItem(REFRESH, 'N');

        }        
      );     
  }

  editarUsuario(username: string): void
  {

      this.dialogRef = this._matDialog.open(UsuarioFormComponent, {
          panelClass: 'usuario-form-dialog',
          data      : {
              action: 'editar',
              username: username
          }
      });

      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(REFRESH) === 'S' )
          {
            this.ngOnInit();
            console.log('se actualiza table - edit');
          }

          localStorage.setItem(REFRESH, 'N');

        }        
      );

  }


  eliminarUsuario(username: string): void
  {
      this.dialogRef = this._matDialog.open(MessageboxComponent, {
          panelClass: 'confirm-form-dialog',
          data      : {
              titulo: 'Eliminar Usuario: ' + username,
              subTitulo: '¿Está seguro de eliminar el usuario?',
              botonAceptar: 'Si',
              botonCancelar: 'No'    
          }
      });

      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => 
        {
          if ( localStorage.getItem(MESSAGEBOX) === 'S' )
          {
            this.usuarioService.eliminar(username).subscribe(data => {
            this._matSnackBar.open('Se eliminó el usuario', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 2000
            });

            this.ngOnInit();

            }, err => {
                this._matSnackBar.open('Error, usuario no eliminado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
            );
          }
          
          localStorage.setItem(MESSAGEBOX, 'N');

        }        
      );
  }
  
  empresaperfilUsuario(username: string, nombre: string): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { username: username, nombre: nombre};
    
    localStorage.setItem(DATOS, JSON.stringify(misDatos));

    this.router.navigate([`sistema/mantenimiento/usuarioempresaperfil`]);
  }

  exportarUsuario(): void
  {
    this.usuarioService.listar().subscribe(data => {
      this.listarReporte = data;
      // console.log(this.listarReporte);

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Usuario'] = item.username;
          objectReport['Nombre'] = item.nombre;   
          objectReport['Sexo'] = item.sexo; 
          objectReport['Correo'] = item.email; 
          objectReport['Teléfono'] = item.telefono; 
          objectReport['Estado'] = item.estado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'Usuario');

      }

    }); 
  }

}


