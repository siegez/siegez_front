import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CreateUsuarioDto } from 'app/dto/create.usuario.dto';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { UsuarioService } from 'app/services/usuario.service';
import { REFRESH, VALORES_INICIALES} from 'app/_shared/var.constant';

@Component({
  selector: 'gez-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class UsuarioFormComponent  
{
  action: string;
  usuario;
  usuarioForm: FormGroup;
  dialogTitle: string;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  valoresiniciales;
  valorcontador;
  checkEstado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<UsuarioFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private usuarioService: UsuarioService   
  ) 
  { 
    this.action = _data.action;
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
        
    if ( this.action === 'editar' )
    {
        this.dialogTitle = 'Editar Usuario';        
        this.usuario = new CreateUsuarioDto();

        this.usuarioService.consultar(_data.username).subscribe(data => {
            this.usuario = data;  
            this.checkEstado = (this.usuario.estado === 'A' ) ? true : false;     
            this.usuarioForm = this.createUsuarioForm();
        }, err => {
          this._matSnackBar.open('Error de consulta', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
      }
      );        
    }
    else
    {
        this.dialogTitle = 'Nuevo Usuario';
        this.edit = false;
        this.usuario = new CreateUsuarioDto();
        this.usuarioForm = this.createUsuarioForm();
    }
    
    this.usuarioForm = this.createUsuarioForm();
  }

  createUsuarioForm(): FormGroup
  {
      return this._formBuilder.group({
          // username            : [{value: this.usuario.username, disabled: this.edit}],
          username            : [this.usuario.username],
          nombre              : [this.usuario.nombre],
          password            : [this.usuario.password],
          email               : [this.usuario.email],
          telefono            : [this.usuario.telefono],
          fechaUltimoIngreso  : [this.usuario.fechaUltimoIngreso],
          sexo                : [this.usuario.sexo],
          foto                : [this.usuario.foto],
          codLicencia         : [this.usuario.codLicencia],
          idUserDB            : [this.usuario.idUserDB],
          estado              : [this.checkEstado]


      });
  }

  // tslint:disable-next-line:typedef
  crearUsuario(usuarioForm){

    this.usuario.username = usuarioForm.value.username.toUpperCase();
    this.usuario.nombre = usuarioForm.value.nombre;
    this.usuario.password = usuarioForm.value.password;
    this.usuario.email = usuarioForm.value.email;
    this.usuario.telefono = usuarioForm.value.telefono;
    this.usuario.fechaUltimoIngreso = '2020-01-01';
    this.usuario.sexo = usuarioForm.value.sexo;
    this.usuario.foto = '28251117.bmp';
    this.usuario.codLicencia = this.valoresiniciales.licencia;
    this.usuario.idUserDB = '0';
    this.usuario.estado = (this.usuarioForm.value.estado) ? 'A' : 'I';
    this.usuario.creacionUsuario = this.valoresiniciales.username;
    this.usuario.creacionFecha = '2020-01-01';
    this.usuario.modificacionUsuario = this.valoresiniciales.username;
    this.usuario.modificacionFecha = '2020-01-01';

    this.usuarioService.crear(this.usuario).subscribe(data => {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

    }, err => {
        this._matSnackBar.open('Error, usuario no registrado', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }    

  onBlurUsuario(): void
  {
    if ( this.edit !== true)
    {
      this.usuario.username = this.usuarioForm.value.username;
  
      this.usuarioService.contar(this.usuario).subscribe(data => {
        this.valorcontador = data;
        console.log(this.valorcontador);
  
        if (this.valorcontador.contador > 0)
          {
            this._matSnackBar.open('Error, Usuario ya existe', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.usuarioForm.controls['username'].reset();
          }
        }, 
        err => 
        {
            this._matSnackBar.open('Error, error de servicio', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
  }

   // tslint:disable-next-line:typedef
  editarUsuario(usuarioForm){

    this.usuario.nombre = usuarioForm.value.nombre;
    this.usuario.password = usuarioForm.value.password;
    this.usuario.email = usuarioForm.value.email;
    this.usuario.telefono = usuarioForm.value.telefono;
    this.usuario.fechaUltimoIngreso = '2020-01-01';
    this.usuario.sexo = usuarioForm.value.sexo;
    this.usuario.foto = '28251117.bmp';
    this.usuario.codLicencia = this.valoresiniciales.licencia;
    this.usuario.idUserDB = '0';
    this.usuario.estado = (this.usuarioForm.value.estado) ? 'A' : 'I';
    this.usuario.creacionUsuario = this.valoresiniciales.username;
    this.usuario.creacionFecha = '2020-01-01';
    this.usuario.modificacionUsuario = this.valoresiniciales.username;
    this.usuario.modificacionFecha = '2020-01-01';

    this.usuarioService.editar(this.usuario).subscribe(data => {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

    }, err => {
        this._matSnackBar.open('Error, usuario no actualizado', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }
}
