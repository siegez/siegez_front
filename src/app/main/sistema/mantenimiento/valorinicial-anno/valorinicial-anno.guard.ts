import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ValorinicialAnnoComponent } from './valorinicial-anno.component';
@Injectable()
export class DeactivateGuard implements CanActivate  {

    canDeact = true;
    dialogRef: any;
    constructor(private _matDialog: MatDialog){}

    canActivate(): boolean {
        this.dialogRef = this._matDialog.open(ValorinicialAnnoComponent, {
            panelClass: 'valorinicial-anno-dialog',
            data      : {
                action: 'nuevo'
            }
        });
        return false;
    }
}
