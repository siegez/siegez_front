import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateParametroDto } from 'app/dto/create.parametro.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { EjercicioService } from 'app/services/ejercicio.service';
import { ParametroService } from 'app/services/parametro.service';
import { MESSAGEBOX, REFRESH, TABLAS, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-valorinicial-anno',
  templateUrl: './valorinicial-anno.component.html',
  styleUrls: ['./valorinicial-anno.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ValorinicialAnnoComponent implements OnInit, AfterContentChecked {

  
  ejercicio;
  parametro;
  valoresiniciales;
  valorcampo;
  ejercicioForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  listaEjercicio: any[];


  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<ValorinicialAnnoComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private ejercicioService: EjercicioService,
    private parametroService: ParametroService,
    private cdref: ChangeDetectorRef
  ) 
  {
    
  }
  
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  ngOnInit(): void 
  {
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.dialogTitle = 'Cambiar Año de Proceso - Valor Inicial';    
   
    this.ejercicioForm = this.crearEjercicioForm();  
    this.listarEjercicio(); 

  }

  crearEjercicioForm(): FormGroup
  {
      return this._formBuilder.group({
        empresa     : [this.valoresiniciales.nombreempresa],
        ejercicio   : [this.valoresiniciales.annoproceso]
      });
  }

  // tslint:disable-next-line:typedef
  valorinicialAnno(ejercicioForm){
    
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Desea Cambiar el Año de Proceso',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });       
    
    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');
          
          this.parametro = new CreateParametroDto();

          this.parametro.empresa                 = this.valoresiniciales.empresa; 
          this.parametro.modulo                  = 'SIS';
          this.parametro.parametro               = 'ANNO';
          this.parametro.valor                   = ejercicioForm.value.ejercicio;
          this.parametro.estado                  = 'A'; 
          this.parametro.creacionUsuario         = this.valoresiniciales.username;
          this.parametro.creacionFecha           = '2020-01-01';
          this.parametro.modificacionUsuario     = this.valoresiniciales.username;
          this.parametro.modificacionFecha       = '2020-01-01';

          this.ejercicio     = ejercicioForm.value.ejercicio;
      
          this.parametroService.editar(this.parametro).subscribe(data => {
            this._matSnackBar.open('Grabación exitosa, Año de Proceso - Valor Inicial', '', {
            verticalPosition: 'bottom',
            duration        : 2000
              });

            this.matDialogRef.close();
    
            }, 
            err => 
            {
                this._matSnackBar.open('Año de Proceso - Valor Inicial, actualizado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
          );       
        }
      }        
    ); 

  }

  // tslint:disable-next-line:typedef
  listarEjercicio(){
    this.ejercicioService.listar(this.valoresiniciales.empresa).subscribe(data => {
      this.listaEjercicio = data;
    });
  }

}





