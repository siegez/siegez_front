import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioFormComponent } from './usuarios/usuario-form/usuario-form.component';
import { UsuarioEmpresaperfilComponent } from './usuarios/usuario-empresaperfil/usuario-empresaperfil.component';
import { UsuarioEmpresaperfilFormComponent } from './usuarios/usuario-empresaperfil/usuario-empresaperfil-form/usuario-empresaperfil-form.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresaFormComponent } from './empresas/empresa-form/empresa-form.component';
import { EmpresaEjerciciosComponent } from './empresas/empresa-ejercicios/empresa-ejercicios.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { EmpresaPeriodosComponent } from './empresas/empresa-periodos/empresa-periodos.component';
import { EmpresaReplicaComponent } from './empresas/empresa-replica/empresa-replica.component';
import { ValorinicialAnnoComponent } from './valorinicial-anno/valorinicial-anno.component';
import { DeactivateGuard } from './valorinicial-anno/valorinicial-anno.guard';
import { EmpresaannoprocesoComponent } from './empresaannoproceso/empresaannoproceso.component';


const routes: Routes = [
    {
        path     : 'usuarios',
        component: UsuariosComponent,
        // resolve  : {
        //     data: UsuariosService
        // }
    },
    {
        path     : 'usuarioempresaperfil',
        component: UsuarioEmpresaperfilComponent,
        // resolve  : {
        //     data: UsuariosService
        // }
    },     
    {
        path     : 'empresas',
        component: EmpresasComponent,
        // resolve  : {
        //     data: EcommerceProductService
        // }
    },
    {
        path     : 'empresa/ejercicios',
        component: EmpresaEjerciciosComponent,
        // resolve  : {
        //     data: EcommerceProductService
        // }
    },
    {
       path     : 'empresa-form',
       component: EmpresaFormComponent,
    },     
    {
        path     : 'valorinicialanno',
        canActivate: [DeactivateGuard]
        // resolve  : {
        //     data: EcommerceProductService
        // }
    },     
    {
        path     : 'empresaannoproceso',
        component: EmpresaannoprocesoComponent,
        // resolve  : {
        //     data: EcommerceProductService
        // }
    }
];

@NgModule({
    declarations: [
        UsuariosComponent,      
        UsuarioFormComponent,
        UsuarioEmpresaperfilComponent,
        UsuarioEmpresaperfilFormComponent,
        EmpresasComponent,
        EmpresaFormComponent,
        EmpresaEjerciciosComponent,
        EmpresaPeriodosComponent,
        EmpresaReplicaComponent,
        ValorinicialAnnoComponent,
        EmpresaannoprocesoComponent, 
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        MatSlideToggleModule,
         
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],

    exports: [
        // MatSnackBar
    ],

    providers   : [
        DeactivateGuard
    ]
})
export class MantenimientoModule
{
}
