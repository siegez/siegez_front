import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO, MESSAGEBOX, TABLAS } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateEnteDto } from 'app/dto/create.ente.dto';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { EmpresaService } from 'app/services/empresa.service';
import { EjercicioService } from 'app/services/ejercicio.service';
import { PeriodoService } from 'app/services/periodo.service';
import { CreateEjercicioDto } from 'app/dto/create.ejercicio.dto';
import { EmpresaEjerciciosComponent } from '../empresa-ejercicios/empresa-ejercicios.component';
import { CreatePeriodoDto } from 'app/dto/create.periodo.dto';
import { EmpresaPeriodosComponent } from '../empresa-periodos/empresa-periodos.component';
import { EmpresaReplicaComponent } from '../empresa-replica/empresa-replica.component';

@Component({
  selector: 'gez-empresa-form',
  templateUrl: './empresa-form.component.html',
  styleUrls: ['./empresa-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class EmpresaFormComponent implements OnInit, AfterContentChecked {

  displayedColumnsEjercicio = ['ejercicio', 'estado', 'editar'];
  displayedColumnsPeriodo = ['ejercicio', 'periodo', 'estado', 'editar'];

  misDatos;  
  empresaForm: FormGroup;
  empresa;
  ejercicio;
  periodo;
  resultadosp;
  valorcampo;
  valoresiniciales;
  valorcontador;
  valorcatalogo;
  excluir;
  edit = true;  
  grabar = true;
  copiar = false;
  listaEmpresaEjercicio: any = [];
  listaEmpresaPeriodo: any = [];
  listaTipoPersona: any[];
  listaMes: any[];
  listaEstado: any[];
  listaExcluir: any[];
  checkEstado = false;
  dialogRef: any;
  codigos;
  
  constructor(
    private empresaService: EmpresaService,
    private consultaService: ConsultaService,
    private empresaejercicioService: EjercicioService,
    private empresaperiodoService: PeriodoService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private cdref: ChangeDetectorRef
  ) 
  {    
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.valorcontador = new ViewValorContadorDto();   
    this.empresa = new CreateEnteDto();

    this.empresaForm = this.createEmpresaForm();

    this.listarTipoPersona();
    this.listarMes();
    this.listarEstado(); 

    // campos pk
    // this.empresa.empresa            = this.valoresiniciales.empresa;

    if ( this.misDatos.action === 'editar' )
    {
      // campo pk
      this.empresa.empresa       = this.misDatos.parametro1;      
      this.obtenerEmpresa();  
      this.obtenerEjercicio();
      this.obtenerPeriodo(0); 
      this.copiar = true;
 
    }
    else{
      this.copiar = false;
      this.edit = false;
      this.checkEstado = true;
      this.iniciarEmpresa();

    }  

  }

  // tslint:disable-next-line:typedef
  mostrarGrabar($event){
    // this.grabar = $event;
    if ($event.index === 0)
    {
      this.grabar = true;
      this.copiar = true;
    }
    else
    {
      this.grabar = false;
      this.copiar = false;
    }
    // console.log($event.index);
  }

  // tslint:disable-next-line:typedef
  obtenerEmpresa(){

    this.empresaService.consultar(this.empresa.empresa).subscribe(data => {
      this.empresa = data;    
      this.checkEstado            = (this.empresa.estado === 'A' ) ? true : false;

      this.empresaForm = this.createEmpresaForm();

      }, err => {
        this._matSnackBar.open('Error de consulta la empresa', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );  
  }

  // tslint:disable-next-line:typedef
  iniciarEmpresa(){
    setTimeout(() => {
      this.empresaForm.controls['empresa'].setValue(0);
      this.empresaForm.controls['descripcion'].setValue('');
      this.empresaForm.controls['ruc'].setValue('');
      this.empresaForm.controls['tipo'].setValue('J');
      this.empresaForm.controls['estado'].setValue(true);
      
    });
  }
  
  createEmpresaForm(): FormGroup
  {
    return this._formBuilder.group({
      empresa      : [this.empresa.empresa],
      descripcion  : [this.empresa.descripcion],
      ruc          : [this.empresa.ruc],
      tipo         : [this.empresa.tipo],
      estado       : [this.checkEstado]    
      });

  }
   

  grabarEmpresa(): void
  {
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Grabar Empresa',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');

          if ( this.misDatos.action === 'nuevo' )
          {
            this.empresa.empresa = 0;
          }
          else
          {
            this.empresa.empresa = this.empresaForm.value.empresa;
          } 
          
          this.empresa.licencia                = this.valoresiniciales.licencia;
          this.empresa.nombres                 = this.empresaForm.value.nombres;
          this.empresa.descripcion             = this.empresaForm.value.descripcion;
          this.empresa.ruc                     = this.empresaForm.value.ruc;
          this.empresa.tipo                    = this.empresaForm.value.tipo;
          this.empresa.estado                  = (this.empresaForm.value.estado) ? 'A' : 'I';
          this.empresa.creacionUsuario         = this.valoresiniciales.username;
          this.empresa.creacionFecha           = '2020-01-01';
          this.empresa.modificacionUsuario     = this.valoresiniciales.username;
          this.empresa.modificacionFecha       = '2020-01-01';
          // this.empresa.empresaejercicio        = this.listaEmpresaEjercicio;
          // this.empresa.empresaperiodo          = this.listaEmpresaPeriodo;
     
          this.resultadosp = new ViewResultadoSPDto();
      
          // consumir servicios para grabar

          this.empresaService.registrar(this.empresa).subscribe(data => {
            this.resultadosp = data;  
            if (this.resultadosp.indicadorexito === 'S')
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                verticalPosition: 'bottom',
                duration        : 2000
                  });
        
                const misDatosNew =  { 
                    action: 'editar',
                    parametro1: this.resultadosp.codigointeger
                  };
        
                localStorage.setItem(DATOS, JSON.stringify(misDatosNew));
        
                this.misDatos = JSON.parse(localStorage.getItem(DATOS));
        
                this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
        
                this.edit = true;
    
                this.empresaForm.controls['empresa'].setValue(this.resultadosp.codigointeger); 
                this.empresa.empresa = this.resultadosp.codigointeger;
    
                this.ngOnInit();

              }
            else
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
              }
    
            }, 
            err => 
            {
                this._matSnackBar.open('Error, empresa no registrado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
          );           
        }
      }        
    ); 

  }
  
  // tslint:disable-next-line:typedef
  listarTipoPersona(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOPERSONA';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoPersona = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarMes(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'MES';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaMes = data;
    });
  }

  
  // tslint:disable-next-line:typedef
  listarEstado(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ESTADO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaEstado = data;
    });
  }

  
  crearEmpresa(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.ngOnInit();

  }

  // tslint:disable-next-line:typedef
  async obtenerEjercicio() 
  {  
    // tslint:disable-next-line:max-line-length
    this.listaEmpresaEjercicio = (await this.empresaejercicioService.listar(this.empresa.empresa).toPromise());
          
  }  

  // tslint:disable-next-line:typedef
  async obtenerPeriodo(ejercicio: number) 
  {
    // tslint:disable-next-line:max-line-length
    this.listaEmpresaPeriodo = (await this.empresaperiodoService.listar(this.empresa.empresa, ejercicio).toPromise());
      
  }  

  
  registrarEjercicio(ejercicio: number, accion: string): void
  {
    this.codigos = '';
    this.ejercicio = new CreateEjercicioDto();

    if (accion === 'insertar')
    { 
      this.ejercicio.empresa       = this.empresaForm.value.empresa;
      this.ejercicio.ejercicio     = 0;
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEmpresaEjercicio.length; i++ )
      {        
        this.codigos = this.codigos + `'` +  this.listaEmpresaEjercicio[i].ejerciciocontable + `',`;       
      }
      
      if (this.codigos !== '')
      {      
        this.codigos = this.codigos.slice(0, -1);                   
      }          
      this.ejercicio.descripcionestado  = this.codigos; 
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEmpresaEjercicio.length; i++ )
      {        
        if ( this.listaEmpresaEjercicio[i].ejerciciocontable === ejercicio)
        {
          this.ejercicio.empresa                 = this.listaEmpresaEjercicio[i].empresa;
          this.ejercicio.ejercicio               = this.listaEmpresaEjercicio[i].ejerciciocontable;
          this.ejercicio.indicador               = this.listaEmpresaEjercicio[i].indicador;
        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.ejercicio));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(EmpresaEjerciciosComponent, { 
        panelClass: 'empresa-ejercicio-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 
          this.obtenerEjercicio();
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  registrarPeriodo(ejercicio: number, periodo: string, accion: string): void
  {
    this.codigos = '';
    this.periodo = new CreatePeriodoDto();

    if (accion === 'insertar')
    { 
      this.periodo.empresa       = this.empresaForm.value.empresa;
      this.periodo.ejercicio     = ejercicio;
      this.periodo.periodo       = '';
      
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEmpresaPeriodo.length; i++ )
      {        
        if ( this.listaEmpresaPeriodo[i].ejerciciocontable === ejercicio && this.listaEmpresaPeriodo[i].periodocontable === periodo)
        {
          this.periodo.empresa                 = this.listaEmpresaPeriodo[i].empresa;
          this.periodo.ejercicio               = this.listaEmpresaPeriodo[i].ejerciciocontable;
          this.periodo.periodo                 = this.listaEmpresaPeriodo[i].periodocontable;
          this.periodo.mes                     = this.listaEmpresaPeriodo[i].mes;
          this.periodo.indicador               = this.listaEmpresaPeriodo[i].indicador;
          this.periodo.estado                  = this.listaEmpresaPeriodo[i].estado;
        }
      }
      
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.periodo));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(EmpresaPeriodosComponent, { 
        panelClass: 'empresa-periodo-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 
          this.obtenerPeriodo(this.periodo.ejercicio);
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  replicarEjercicio(): void
  {
    this.codigos = '';
    this.ejercicio = new CreateEjercicioDto();

    this.ejercicio.empresa       = this.empresaForm.value.empresa;
    this.ejercicio.ejercicio     = 0;
    // tslint:disable-next-line:prefer-for-of
    for ( let i = 0; i < this.listaEmpresaEjercicio.length; i++ )
    {        
     this.codigos = this.codigos + `'` +  this.listaEmpresaEjercicio[i].ejerciciocontable + `',`;       
    }
     
    if (this.codigos !== '')
    {      
      this.codigos = this.codigos.slice(0, -1);                   
    }          
  
    this.ejercicio.descripcionestado  = this.codigos; 
    
    localStorage.setItem(TABLAS, JSON.stringify(this.ejercicio));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(EmpresaReplicaComponent, { 
        panelClass: 'empresa-replica-dialog'
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {           
          this.obtenerEjercicio();
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );  
    
  }
 
}

/*
export class EmpresaFormComponent {

  action: string;
  empresa;
  empresaForm: FormGroup;
  dialogTitle: string;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  listatipos: any[];
  valorCampo: ValorCampoDto;
  valoresiniciales;

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<EmpresaFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private empresaService: EmpresaService,
    private consultaService: ConsultaService) 
    { 
    this.action = _data.action;
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.valorCampo = new ValorCampoDto();
    this.valorCampo.empresa = this.valoresiniciales.empresa;
    this.valorCampo.ambito = 'SHA';
    this.valorCampo.campo = 'EMPTIPO';
    this.consultaService.listarTiposEmpresa(this.valorCampo).subscribe(data => {
      // this.dataSource.data = data;  
      this.listatipos = data;
  });

    if ( this.action === 'editar' ) {
        this.dialogTitle = 'Editar Empresa';        
        this.empresa = new CreateEmpresaDto();

        this.empresaService.consultar(_data.empresa).subscribe(data => {
            this.empresa = data;            
            this.empresaForm = this.createEmpresaForm();
        }, err => {
          this._matSnackBar.open('Error de consulta', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
      }
      );
        this.empresaForm = this.createEmpresaForm();
    }
    else {
        this.dialogTitle = 'Nueva empresa';
        // this.edit = false;
        this.empresa = new CreateEmpresaDto();
        this.empresa.empresa = 'Se autogenerara un codigo';
        this.empresaForm = this.createEmpresaForm();
    }
    // this.empresaForm = this.createEmpresaForm();
  }

  createEmpresaForm(): FormGroup {
      return this._formBuilder.group({
          empresa             : [{value: this.empresa.empresa, disabled: this.edit}],
          licencia            : [this.empresa.licencia],
          descripcion         : [this.empresa.descripcion],
          tipo                : [this.empresa.tipo],
          ruc                 : [this.empresa.ruc],
          logo                : [this.empresa.logo],
          estado              : [this.empresa.estado],
          creacionUsuario     : [this.empresa.creacionUsuario],
          creacionFecha       : [this.empresa.creacionFecha],
          modificacionUsuario : [this.empresa.modificacionUsuario],
          modificacionFecha   : [this.empresa.modificacionFecha]
      });
  }

  // tslint:disable-next-line:typedef
  crearEmpresa(empresaForm) {
    this.empresa.empresa = this.empresaForm.value.empresa;
    this.empresa.licencia = this.valoresiniciales.licencia;
    this.empresa.descripcion = empresaForm.value.descripcion;
    this.empresa.tipo = this.empresaForm.value.tipo;
    this.empresa.ruc = this.empresaForm.value.ruc;
    this.empresa.logo = this.empresaForm.value.logo;
    this.empresa.estado = this.empresaForm.value.estado;
    this.empresa.creacionUsuario = this.valoresiniciales.username;
    this.empresa.creacionFecha = '2020-01-01';
    this.empresa.modificacionUsuario = this.valoresiniciales.username;
    this.empresa.modificacionFecha = '2020-01-01';

    this.empresaService.crear(this.empresa).subscribe(data => {
        this.empresa.empresa = data;
        this.empresaForm = this.createEmpresaForm();
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this._matSnackBar.open('Codigo asignado: '+ data, '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });
        localStorage.setItem(REFRESH, 'S');
    }, err => {
        this._matSnackBar.open('Error, empresa no registrada', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }    

   // tslint:disable-next-line:typedef
   editarEmpresa(empresaForm){
    this.empresa.licencia = this.valoresiniciales.licencia;
    this.empresa.descripcion = empresaForm.value.descripcion;
    this.empresa.tipo = this.empresaForm.value.tipo;
    this.empresa.ruc = this.empresaForm.value.ruc;
    this.empresa.logo = this.empresaForm.value.logo;
    this.empresa.estado = this.empresaForm.value.estado;
    this.empresa.creacionUsuario = this.valoresiniciales.username;
    this.empresa.creacionFecha = '2020-01-01';
    this.empresa.modificacionUsuario = this.valoresiniciales.username;
    this.empresa.modificacionFecha = '2020-01-01';

    this.empresaService.editar(this.empresa).subscribe(data => {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });
        localStorage.setItem(REFRESH, 'S');
    }, err => {
        this._matSnackBar.open('Error, empresa no actualizada', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );
  }
}
*/
