import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ConsultaService } from 'app/services/consulta.service';
import { EjercicioService } from 'app/services/ejercicio.service';
import { EmpresaService } from 'app/services/empresa.service';
import { MESSAGEBOX, REFRESH, TABLAS, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-empresa-replica',
  templateUrl: './empresa-replica.component.html',
  styleUrls: ['./empresa-replica.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EmpresaReplicaComponent implements OnInit, AfterContentChecked {

  
  ejercicio;
  resultadosp;
  valoresiniciales;
  valorcampo;
  replicaForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Copiar Tablas';

  listaEmpresaOrigen: any[];
  listaEjercicioOrigen: any[];
  listaEjercicioDestino: any[];
  empresaorigen;
  ejercicioorigen;
  empresadestino;
  ejerciciodestino;

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<EmpresaReplicaComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService,
    private empresaService: EmpresaService,
    private ejercicioService: EjercicioService,
    private cdref: ChangeDetectorRef
  ) 
  {
    
  }
  
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  ngOnInit(): void 
  {
    this.ejercicio = JSON.parse(localStorage.getItem(TABLAS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.dialogTitle = 'Copiar Tablas';    
   
    this.replicaForm = this.crearReplicaForm();  
    this.listarEmpresa();   
    this.listarEjercicioOrigen(); 
    this.listarEjercicioDestino();
  }

  crearReplicaForm(): FormGroup
  {
      return this._formBuilder.group({
        empresaorigen     : [this.valoresiniciales.empresa],
        ejercicioorigen   : [null],
        empresadestino    : [this.valoresiniciales.nombreempresa],
        ejerciciodestino  : [null]
      });
  }

  // tslint:disable-next-line:typedef
  replicarTablas(replicaForm){
    
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Desea Copiar Tablas',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });       
    
    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');
          
          this.empresaorigen     = replicaForm.value.empresaorigen;
          this.ejercicioorigen   = replicaForm.value.ejercicioorigen;
          this.empresadestino    = this.valoresiniciales.empresa;
          this.ejerciciodestino  = replicaForm.value.ejerciciodestino;

          this.resultadosp = new ViewResultadoSPDto();
      
          // consumir servicios para grabar

          this.empresaService.replicar(this.empresaorigen, this.ejercicioorigen, this.empresadestino, this.ejerciciodestino).subscribe(data => {
            this.resultadosp = data;  
            if (this.resultadosp.indicadorexito === 'S')
              {
                localStorage.setItem(REFRESH, 'S');

                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                verticalPosition: 'bottom',
                duration        : 2000
                  });
                
                this.matDialogRef.close();

              }
            else
              {
                this._matSnackBar.open('Error copiando tablas, ' + this.resultadosp.mensaje, 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
              }
    
            }, 
            err => 
            {
                this._matSnackBar.open('Error, copiar tablas', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
          );           
        }
      }        
    ); 

  }

  // tslint:disable-next-line:typedef
  listarEmpresa(){

    this.empresaService.listarCatalogo(this.valoresiniciales.licencia).subscribe(data => {
      this.listaEmpresaOrigen = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarEjercicioOrigen(){
    this.empresaorigen     = this.replicaForm.value.empresaorigen;
    this.ejercicioService.listar(this.empresaorigen).subscribe(data => {
      this.listaEjercicioOrigen = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarEjercicioDestino(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ANNO';
    this.valorcampo.valor = this.ejercicio.descripcionestado;
    this.consultaService.listarCatalogoDescripcionFiltro(this.valorcampo).subscribe(data => {
      this.listaEjercicioDestino = data;
    });
  }


}





