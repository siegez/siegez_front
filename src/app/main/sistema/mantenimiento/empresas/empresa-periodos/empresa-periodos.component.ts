import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { PeriodoService } from 'app/services/periodo.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';
import { EjercicioService } from '../../../../../services/ejercicio.service';

@Component({
  selector: 'gez-empresa-periodos',
  templateUrl: './empresa-periodos.component.html',
  styleUrls: ['./empresa-periodos.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EmpresaPeriodosComponent implements OnInit {

  accion: string;
 
  periodo;  
  valoresiniciales;
  valorcampo;
  periodoForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaEjercicio: any[];
  checkEstado = false;
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<EmpresaPeriodosComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService,
    private periodoService: PeriodoService
  ) 
  {
    this.accion = _data.accion; 
  }

  ngOnInit(): void 
  {
    this.periodo = JSON.parse(localStorage.getItem(TABLAS));
    

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Mes de Proceso';    
      this.checkEstado = (this.periodo.indicador === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Mes de Proceso';
      this.checkEstado = true;
    }     
    this.periodoForm = this.crearPeriodoForm();  

  }

  crearPeriodoForm(): FormGroup
  {
      return this._formBuilder.group({
        periodo  : [this.periodo.periodo],
        mes      : [this.periodo.mes],
        estado   : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarPeriodo(periodoForm){

    this.periodo.ejerciciocontable       = this.periodo.ejercicio;  
    this.periodo.periodocontable         = periodoForm.value.periodo;
    this.periodo.estado                  = 'A'; 
    this.periodo.indicador               = (this.periodoForm.value.estado) ? 'A' : 'C';
    this.periodo.creacionUsuario         = this.valoresiniciales.username;
    this.periodo.creacionFecha           = '2020-01-01';
    this.periodo.modificacionUsuario     = this.valoresiniciales.username;
    this.periodo.modificacionFecha       = '2020-01-01';

    // localStorage.setItem(TABLAS, JSON.stringify(this.ejercicio));
    // localStorage.setItem(REFRESH, 'S');
 
    // consumir servicios para grabar
    
    if ( this.accion === 'insertar' )
    {
      this.periodoService.crear(this.periodo).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
          });
        
        // this.botonExit = 'Salir';
        // this.hidegrabar = true;
        // this.edit = true;

        localStorage.setItem(REFRESH, 'S');

        }, 
        err => 
        {
            this._matSnackBar.open('Error, Mes de proceso no registrado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
    else
    {
      // console.log('grab');
      // console.log(this.periodo);
      this.periodoService.editar(this.periodo).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        // this.botonExit = 'Salir';
        // this.hidegrabar = true;
        // this.edit = true;
        // console.log(this.periodo);
        localStorage.setItem(REFRESH, 'S');

        }, err => {
            this._matSnackBar.open('Error, Mes de proceso no actualizado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }

    this.matDialogRef.close();

  }

}




