import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmpresaService } from 'app/services/empresa.service';
import { fuseAnimations } from '@fuse/animations';
import { REFRESH, DATOS, MESSAGEBOX, VALORES_INICIALES } from 'app/_shared/var.constant';
import { CreateEmpresaDto } from 'app/dto/create.empresa.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ExcelService } from 'app/services/excel.service';

@Component({
  selector: 'gez-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EmpresasComponent implements OnInit {

  resultado;
  valoresiniciales;
  empresa;
  dialogRef: any;
  @ViewChild('modalCrearEmpresa') modalCrearEmpresa: any;
  createUser: CreateEmpresaDto = new CreateEmpresaDto();
  displayedColumns = ['empresa', 'descripcion', 'ruc', 'estado', 'editar', 'eliminar'];
  listarTabla: any = [];
  listarReporte: any = [];
   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private empresaService: EmpresaService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private excelService: ExcelService,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router ) { 
      this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
      this.paginatorlabel.firstPageLabel = 'Primera página';
      this.paginatorlabel.nextPageLabel = 'Página siguiente';
      this.paginatorlabel.previousPageLabel = 'Página anterior';
      this.paginatorlabel.lastPageLabel = 'Última página';       
      // tslint:disable-next-line:max-line-length
      this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
    }

  ngOnInit(): void {

    this.empresa = new CreateEmpresaDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.dataSource.filterPredicate = (data: any, filter: string) => (
       data.empresa.toString().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.ruc.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.logo.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 
     );
    this.setSettingTableCourse();
    this.empresaService.listar().subscribe(data => {
        this.dataSource.data = data;
        this.listarTabla = data;
        this.setSettingTableCourse();
    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearEmpresa(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`sistema/mantenimiento/empresa-form`]);
  }

  editarEmpresa(empresa: string): void
  {
    const misDatos =  { 
                        action: 'editar',
                        parametro1: empresa
                      };

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`sistema/mantenimiento/empresa-form`]);
    
  }

  eliminarEmpresa(empresa: string): void
  {
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Empresa: ' + empresa,
          subTitulo: '¿Está seguro de eliminar la empresa?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

  // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.empresa.empresa    = empresa;

          this._matSnackBar.open('Eliminando empresa ...', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 200
            });

          this.empresaService.eliminar(this.empresa.empresa).subscribe(data => {
          this._matSnackBar.open('Se eliminó le empresa', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, empresa no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );    
  }

  exportarEmpresa(): void
  {
    this.empresaService.exportar().subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = item.empresa;
          objectReport['Nombre'] = item.descripcion;   
          objectReport['Tipo Persona'] = item.tipo; 
          objectReport['RUC'] = item.ruc; 
          objectReport['Estado'] = item.estado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'Empresa');

      }

    }); 
  }

  /*
  crearEmpresa(): void {
      this.dialogRef = this._matDialog.open(EmpresaFormComponent, {
          panelClass: 'empresa-form-dialog',
          data      : { action: 'nuevo' }
      });
      this.dialogRef.afterClosed().subscribe(result => {
          if ( localStorage.getItem(REFRESH) === 'S' ) {            
            this.ngOnInit();
            console.log('se actualiza table - new');
          }
          localStorage.setItem(REFRESH, 'N');
        }        
      );
  }

  editarEmpresa(empresa: string): void{
      this.dialogRef = this._matDialog.open(EmpresaFormComponent, {
          panelClass: 'empresa-form-dialog',
          data      : {
              action: 'editar',
              empresa: empresa
          }
      });
      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => {
          if ( localStorage.getItem(REFRESH) === 'S' ) {
            this.ngOnInit();
            console.log('se actualiza table - edit');
          }
          localStorage.setItem(REFRESH, 'N');
        }        
      );
  }

  eliminarEmpresa(empresa: string, descripcion: string): void {
      this.dialogRef = this._matDialog.open(MessageboxComponent, {
          panelClass: 'confirm-form-dialog',
          data      : {
              titulo: 'Eliminar empresa: ' + descripcion,
              subTitulo: '¿Está seguro de eliminar la empresa?',
              botonAceptar: 'Si',
              botonCancelar: 'No'    
          }
      });
      // tslint:disable-next-line:no-conditional-assignment
      this.dialogRef.afterClosed().subscribe(result => {
          if ( localStorage.getItem(MESSAGEBOX) === 'S' ) {
            this.empresaService.eliminar(empresa).subscribe(data => {
            this._matSnackBar.open('Se eliminó la empresa', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 2000
            });
            this.ngOnInit();
            }, err => {
                this._matSnackBar.open('Error, empresa no eliminada', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
            );
          }
          localStorage.setItem(MESSAGEBOX, 'N');
        }        
      );
  }
  
  empresaEjercicios(empresa: string, descripcion: string): void {
    localStorage.setItem(DATOS, descripcion);
    // localStorage.setItem(CODIGO_EMPRESA,empresa);
    this.router.navigate([`sistema/mantenimiento/empresa/ejercicios`]);
  }
  */
}
