import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';
import { EjercicioService } from '../../../../../services/ejercicio.service';

@Component({
  selector: 'gez-empresa-ejercicios',
  templateUrl: './empresa-ejercicios.component.html',
  styleUrls: ['./empresa-ejercicios.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EmpresaEjerciciosComponent implements OnInit {

  accion: string;
 
  ejercicio;  
  valoresiniciales;
  valorcampo;
  ejercicioForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaEjercicio: any[];
  checkEstado = false;
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<EmpresaEjerciciosComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService,
    private ejercicioService: EjercicioService
  ) 
  {
    this.accion = _data.accion; 
  }

  ngOnInit(): void 
  {
    this.ejercicio = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Año de Proceso';    
      this.checkEstado = (this.ejercicio.indicador === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Año de Proceso';
      this.checkEstado = true;
    }     
    this.ejercicioForm = this.crearEjercicioForm();  
    this.listarEjercicio();   
  }

  crearEjercicioForm(): FormGroup
  {
      return this._formBuilder.group({
        ejercicio  : [this.ejercicio.ejercicio],
        estado     : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarEjercicio(ejercicioForm){

    this.ejercicio.ejerciciocontable       = ejercicioForm.value.ejercicio;  
    this.ejercicio.estado                  = 'A'; 
    this.ejercicio.indicador               = (this.ejercicioForm.value.estado) ? 'A' : 'C';
    this.ejercicio.creacionUsuario         = this.valoresiniciales.username;
    this.ejercicio.creacionFecha           = '2020-01-01';
    this.ejercicio.modificacionUsuario     = this.valoresiniciales.username;
    this.ejercicio.modificacionFecha       = '2020-01-01';

    // localStorage.setItem(TABLAS, JSON.stringify(this.ejercicio));
    // localStorage.setItem(REFRESH, 'S');
 
    // consumir servicios para grabar
    
    if ( this.accion === 'insertar' )
    {
      this.ejercicioService.crear(this.ejercicio).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
          });
        
        // this.botonExit = 'Salir';
        // this.hidegrabar = true;
        // this.edit = true;
 

        localStorage.setItem(REFRESH, 'S');

        }, 
        err => 
        {
            this._matSnackBar.open('Error, Año de proceso no registrado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
    else
    {

      this.ejercicioService.editar(this.ejercicio).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        // this.botonExit = 'Salir';
        // this.hidegrabar = true;
        // this.edit = true;
   
        localStorage.setItem(REFRESH, 'S');

        }, err => {
            this._matSnackBar.open('Error, Año de proceso no actualizado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }

    this.matDialogRef.close();

  }

  // tslint:disable-next-line:typedef
  listarEjercicio(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ANNO';
    this.valorcampo.valor = this.ejercicio.descripcionestado;
    this.consultaService.listarCatalogoDescripcionFiltro(this.valorcampo).subscribe(data => {
      this.listaEjercicio = data;
    });
  }


}




