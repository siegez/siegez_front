import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO, MESSAGEBOX, TABLAS } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConsultaService } from 'app/services/consulta.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { EjercicioService } from 'app/services/ejercicio.service';
import { EmpresaService } from 'app/services/empresa.service';

@Component({
  selector: 'gez-empresaannoproceso',
  templateUrl: './empresaannoproceso.component.html',
  styleUrls: ['./empresaannoproceso.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class EmpresaannoprocesoComponent implements OnInit, AfterContentChecked {
 
  empresa;
  annoproceso;
  empresaannoprocesoForm: FormGroup;  
  valoresiniciales;
  listaEmpresa: any = [];
  listaAnnoproceso: any = [];
  dialogRef: any;
  
  constructor(
    private consultaService: ConsultaService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private empresaService: EmpresaService,
    private ejercicioService: EjercicioService,
    private cdref: ChangeDetectorRef
  ) 
  {    
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }
  
  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.empresa = this.valoresiniciales.empresa;
    this.annoproceso = this.valoresiniciales.annoproceso;
    this.empresaannoprocesoForm = this.createEmpresaAnnoProcesoForm();

    this.listarEmpresa(); 
    this.listarAnnoProceso(); 
    this.iniciarEmpresaAnnoProceso();

  }


  // tslint:disable-next-line:typedef
  iniciarEmpresaAnnoProceso(){
    setTimeout(() => {
      this.empresaannoprocesoForm.controls['empresa'].setValue(this.empresa);
      this.empresaannoprocesoForm.controls['annoproceso'].setValue(this.annoproceso);
      
    });
  }
  
  createEmpresaAnnoProcesoForm(): FormGroup
  {
    return this._formBuilder.group({
      empresa        : [this.empresa],
      annoproceso    : [this.annoproceso] 
      });
  }   

  cambiarEmmpresaAnnoProceso(empresaannoprocesoForm): void
  {
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Cambiar Empresa y Año de Proceso',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N'); 
          this.valoresiniciales.empresa      = this.empresaannoprocesoForm.value.empresa;  
          this.valoresiniciales.annoproceso  = this.empresaannoprocesoForm.value.annoproceso; 
          localStorage.setItem(VALORES_INICIALES, JSON.stringify(this.valoresiniciales)); 
          window.location.reload();
                   
        }
      }        
    ); 

  }
 
  // tslint:disable-next-line:typedef
  listarEmpresa(){
    this.empresaService.listarCatalogoUsuario(this.valoresiniciales.username).subscribe(data => {
        this.listaEmpresa = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarAnnoProceso(){
    this.ejercicioService.listar(this.empresaannoprocesoForm.value.empresa).subscribe(data => {
      this.listaAnnoproceso = data;
    });
  }

}

