import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseSharedModule } from '@fuse/shared.module';
import { TablerocontabilidadComponent } from './tablero/tablerocontabilidad/tablerocontabilidad.component';
import { ReportesContabilidadComponent } from './reportes/reportes-contabilidad/reportes-contabilidad.component';
import { MatRadioModule } from '@angular/material/radio';
// import { ReporteUtil } from 'app/_shared/util/reporteutil';

const routes = [
    {
        path        : 'mantenimiento',
        loadChildren: () => import('./mantenimiento/mantenimiento.module').then(m => m.MantenimientoModule)
    },
    {
        path        : 'transacciones',
        loadChildren: () => import('./transacciones/transacciones.module').then(m => m.TransaccionesModule)
    },
    {
        path : 'reportescontabilidad',
        component : ReportesContabilidadComponent
    },
    {
        path : 'tablerocontabilidad',
        component : TablerocontabilidadComponent
    }
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule,
        // ReporteUtil,
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        NgxChartsModule,
        MatRadioModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule
    ],
    declarations: [TablerocontabilidadComponent, ReportesContabilidadComponent]

})

export class ContabilidadModule
{
}
