
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ExcelService } from 'app/services/excel.service';
import { TextoService } from 'app/services/texto.service';
import { HOST, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ReporteService } from 'app/services/reporte.service';
import { ReporteUtil } from '../../../../_shared/util/reporteutil';
import { FiltroReporteDto } from 'app/dto/create.filtroreporte.dto';
import { SubDiarioService } from 'app/services/subdiario.service';
import { PlanContableService } from 'app/services/plancontable.service';

@Component({
  selector: 'gez-reportes-contabilidad',
  templateUrl: './reportes-contabilidad.component.html',
  styleUrls: ['./reportes-contabilidad.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ReportesContabilidadComponent implements OnInit {
  url = '';
  dialogRef: any;
  valoresiniciales;
  valorcampo;
  listaMes: any[];
  listaSubDiarioIni: any[];
  listaSubDiarioFin: any[];
  listaCtaIni: any[];
  listaCtaFin: any[];
  listaCategoria: any[];
  listarReporte: any = [];
  filtroReporte;
  id = 0;
  title = 'Seleccionar reporte';
  filtroForm: FormGroup;
  viewRegistros = false;
  viewElectronicos = false;
  viewAnalisis = false;
  viewEstadosFinancieros = false;
  viewExcel = false;
  viewTexto = false;
  viewPdf = false;
  viewMoneda = false;
  viewOrden = false;
  viewSubdiario = false;
  viewCategoria = false;  
  viewCuenta = false;  
  viewFecha = false;
  viewEstado = false;
  viewSubDiarioIni = false;
  viewSubDiarioFin = false;
  viewGlosa = false;
  viewGlosaCaja = false;
  viewEstadoDocumento = false;

  
  librosregistros = [
    {
      id    : 100,
      handle: 'starred',
      title : 'Registro Compras',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 101,
      handle: 'important',
      title : 'Registro Ventas',
      icon  : 'label',
      color : 'green-fg'
    },
    // {
    //   id    : 103,
    //   handle: 'important',
    //   title : 'Honorarios',
    //   icon  : 'label',
    //   color : 'green-fg'
    // },
    {
      id    : 104,
      handle: 'important',
      title : 'Libro Diario',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 105,
      handle: 'important',
      title : 'Libro Diario Simplificado',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 106,
      handle: 'important',
      title : 'Libro Mayor',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 107,
      handle: 'important',
      title : 'Libro Mayor - Binomeda',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 108,
      handle: 'important',
      title : 'Caja y Bancos',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 109,
      handle: 'important',
      title : 'Balance de Comprobación',
      icon  : 'label',
      color : 'green-fg'
    }
  ];
  libroselectronicos = [
    {
      id    : 200,
      handle: 'starred',
      title : 'PLE 5.1 Libro Diario',
      icon  : 'label',
      color : 'green-fg'
    },
    /*
    {
      id    : 201,
      handle: 'important',
      title : 'PLE 5.2 Libro Diario',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 202,
      handle: 'important',
      title : 'PLE 5.3 Plan Contable',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 203,
      handle: 'important',
      title : 'PLE 5.4 Plan Contable',
      icon  : 'label',
      color : 'green-fg'
    },
    */
    {
      id    : 204,
      handle: 'important',
      title : 'PLE 8.1 Registro de Compras',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 205,
      handle: 'important',
      title : 'PLE 8.2 Registro de Compras No Domiciliado',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 206,
      handle: 'important',
      title : 'PLE 8.3 Registro de Compras - Simplificado',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 207,
      handle: 'important',
      title : 'PLE 14.1 Registro de Ventas',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 208,
      handle: 'important',
      title : 'PLE 14.2 Registro de Ventas - Simplificado',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 209,
      handle: 'important',
      title : 'PLAME Prestador de Servicios',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 210,
      handle: 'important',
      title : 'PLAME Prestador de Servicios - Comprobante',
      icon  : 'label',
      color : 'green-fg'
    }
  ];
  analisis = [
    {
      id    : 301,
      handle: 'starred',
      title : 'Saldo por Cuenta-Entidad-Documento',
      icon  : 'label',
      color : 'green-fg'
    }
  ];
  estadosfinancieros = [
    {
      id    : 401,
      handle: 'starred',
      title : 'Estado Situación Financiera',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 402,
      handle: 'starred',
      title : 'Estado Situación Financiera Vertical',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 403,
      handle: 'starred',
      title : 'Estado de Resultados por Naturaleza',
      icon  : 'label',
      color : 'green-fg'
    },
    {
      id    : 404,
      handle: 'starred',
      title : 'Estado de Resultados por Función',
      icon  : 'label',
      color : 'green-fg'
    }
  ];
  
 
  constructor(
    private _formBuilder: FormBuilder,
    private subdiarioService: SubDiarioService,
    private consultaService: ConsultaService,
    private reporteService: ReporteService,
    private excelService: ExcelService, 
    private textoService: TextoService,
    private reporteUtil: ReporteUtil,
    private plancontableService: PlanContableService,
  ) 
  {}

  ngOnInit(): void 
  {
    const dateObj = new Date();
    const month = (dateObj.getMonth() + 1).toString().length === 1 ? '0' + (dateObj.getMonth() + 1) : (dateObj.getMonth() + 1); 
    const day = dateObj.getDate().toString().length === 1 ? '0' + dateObj.getDate() : dateObj.getDate();
    const year = dateObj.getFullYear();
    const today = year + '-' + month + '-' + day;
    this.valorcampo = new ValorCampoDto();
    this.filtroReporte = new FiltroReporteDto();
    
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.filtroForm = this.createFiltroForm();
    this.listarMes(); 
    this.listarSubDiario(); 
    this.listarCategoria();
    this.listarCuenta();
    this.filtroForm.controls['mes'].setValue('01');
    this.filtroForm.controls['moneda'].setValue('PEN');
    this.filtroForm.controls['orden'].setValue('E');
    this.filtroForm.controls['subdiarioini'].setValue('00');
    this.filtroForm.controls['subdiariofin'].setValue('99');
    this.filtroForm.controls['glosa'].setValue('P');
    this.filtroForm.controls['glosacaja'].setValue('P');
    this.filtroForm.controls['categoria'].setValue('001');
    this.filtroForm.controls['ctacodini'].setValue('1');
    this.filtroForm.controls['ctacodfin'].setValue('9');
    this.filtroForm.controls['fecha'].setValue(today);
    this.filtroForm.controls['estadodoc'].setValue('P');
    
  }

  createFiltroForm(): FormGroup
  {
    return this._formBuilder.group({
          mes            : String,
          moneda         : String,
          orden          : String,
          subdiario      : String,
          categoria      : String,
          cuenta         : String,          
          fecha          : String,
          estado         : String,
          subdiarioini   : String,
          subdiariofin   : String,
          ctacodini      : String,
          ctacodfin      : String,
          glosa          : String,
          glosacaja      : String,
          estadodoc      : String
      });

  }

  // tslint:disable-next-line:typedef
  listarMes(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'MES';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaMes = data;
    });
  }
  // tslint:disable-next-line:typedef
  listarSubDiario(){

    this.subdiarioService.listarCatalogo(this.valoresiniciales.empresa).subscribe(data => {
      this.listaSubDiarioIni = data;
      this.listaSubDiarioFin = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarCategoria(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDCATEGORIACUENTA';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaCategoria = data;
      });
  }

  // tslint:disable-next-line:typedef
  listarCuenta(){
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCtaIni = data;
      this.listaCtaFin = data;
    });

  }

  mostrarMenu(id: number): void
  {
    if (id === 100)
    {
      if (this.viewRegistros === false)
      {
        this.viewRegistros = true;
      }
      else
      {
        this.viewRegistros = false;
      }      
    }

    if (id === 200)
    {
      if (this.viewElectronicos === false)
      {
        this.viewElectronicos = true;
      }
      else
      {
        this.viewElectronicos = false;
      }      
    }

    if (id === 300)
    {
      if (this.viewAnalisis === false)
      {
        this.viewAnalisis = true;
      }
      else
      {
        this.viewAnalisis = false;
      }      
    }

    if (id === 400)
    {
      if (this.viewEstadosFinancieros === false)
      {
        this.viewEstadosFinancieros = true;
      }
      else
      {
        this.viewEstadosFinancieros = false;
      }      
    }
  }

  mostrarFiltro(id: number, title: string): void
  {
    this.viewExcel = false;
    this.viewTexto = false;
    this.viewPdf = false;
    this.viewMoneda = false;
    this.viewOrden = false;
    this.viewSubdiario = false;
    this.viewCategoria = false;  
    this.viewCuenta = false;
    this.viewFecha = false;
    this.viewEstado = false;
    this.viewSubDiarioIni = false;
    this.viewSubDiarioFin = false;
    this.viewGlosa = false;
    this.viewGlosaCaja = false;
    this.viewEstadoDocumento = false;


    this.id = id;
    this.title = title;

    // Visualizar botón Excel
    if (this.id === 100 || this.id === 101 || this.id === 103 || this.id === 104 || 
        this.id === 105 || this.id === 106 || this.id === 107 || this.id === 108 || this.id === 109 ||
        this.id === 200 || this.id === 204 || this.id === 205 || this.id === 206 ||
        this.id === 207 || this.id === 208 || this.id === 209 || this.id === 210 ||
        this.id === 301 ||
        this.id === 401 || this.id === 402 || this.id === 403 || this.id === 404)
    {
      this.viewExcel = true;
    }

    // Visualizar botón Texto
    if (this.id === 200 || this.id === 204 || this.id === 205 || this.id === 206 ||
        this.id === 207 || this.id === 208 || this.id === 209 || this.id === 210)
    {
      this.viewTexto = true;
    }

    // Visualizar botón PDF
    if (this.id === 100 || this.id === 101 || this.id === 103 || this.id === 104 || 
        this.id === 105 || this.id === 106 || this.id === 107 || this.id === 108 || this.id === 109 ||
        this.id === 301 ||
        this.id === 401 || this.id === 402 || this.id === 403 || this.id === 404)
    {
      this.viewPdf = true;
    }

    // Visualizar Moneda
    if (this.id === 100 || this.id === 101 || this.id === 104 || this.id === 105 || 
        this.id === 106 || this.id === 108 || this.id === 109 ||
        this.id === 401 || this.id === 402 || this.id === 403 || this.id === 404)
    {
      this.viewMoneda = true;
    }
    // Visualizar Orden
    if (this.id === 100 || this.id === 101)
    {
      this.viewOrden = true;
    }
    // Subdiario inicio
    if (this.id === 104 || this.id === 105)
    {
      this.viewSubDiarioIni = true;
      this.viewSubDiarioFin = true;
    }
    // Visualizar Glosa
    if (this.id === 104 || this.id === 106 || this.id === 107)
    {
      this.viewGlosa = true;
    }

    // Visualizar Glosa Caja
    if (this.id === 108)
    {
      this.viewGlosaCaja = true;
    }

    // Visualizar Categoria
    if (this.id === 105)
    {
      this.viewCategoria = true;
    }

    // Visualizar Cuenta
    if (this.id === 106 || this.id === 107 || this.id === 108 || this.id === 301)
    {
      this.viewCuenta = true;
    }

    // Visualizar fecha
    if (this.id === 301)
    {
      this.viewFecha = true;
    }

    // Visualizar Estado Documento
    if (this.id === 301)
    {
      this.viewEstadoDocumento = true;
    }

  } 

  exportarExcel(): void
  {
    let nombrearchivo = '';    
    
    // Registro Compras
    if (this.id === 100) {
      this.exportarExcelRegistroCompra(); 
    }
 
    // Registro Ventas
    if (this.id === 101)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.orden = this.filtroForm.value.orden;
      this.reporteUtil.downloadXls('DHAGEZ_RegistroVentas', this.filtroReporte);
    } 

    // Libro Diario
    if (this.id === 104)
    {      
      
      nombrearchivo = 'LibroDiario' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes;
      console.log("holi");

      this.reporteService.exportarLibroDiario(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
                                              this.filtroForm.value.subdiarioini, this.filtroForm.value.subdiariofin, this.valoresiniciales.ruc,
                                              this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, 
                                              this.filtroForm.value.glosa).subscribe(data => {
        this.listarReporte = data;
  
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};
          
          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['RUC']   = item.c1;
            objectReport['EMPRESA']   = item.c2;
            objectReport['EJERCICIO']   = item.c3;
            objectReport['PERIODO']   = item.c4;
            objectReport['MONEDA']   = item.c5;
            objectReport['NÚMERO OPERACIÓN']   = item.c6;
            objectReport['FECHA OPERACIÓN']   = item.c7;
            objectReport['GLOSA']   = item.c8;
            objectReport['TIPO COMPROBANTE DE PAGO O DOCUMENTO']   = item.c9;
            objectReport['SERIE DE COMPROBANTE DE PAGO O DOCUMENTO']  = item.c10;
            objectReport['NÚMERO DE COMPROBANTE DE PAGO O DOCUMENTO']  = item.c11;
            objectReport['FECHA EMISIÓN']  = item.c12;
            objectReport['CUENTA CONTABLE']  = item.c13;
            objectReport['DESCRIPCIÓN CUENTA CONTABLE']  = item.c14;
            objectReport['ANEXO']  = item.c15;
            objectReport['DEBE']  = item.c16;
            objectReport['HABER']  = item.c17;
            objectReport['SUBDIARIO']  = item.c18;
            objectReport['DESCRIPCIÓN SUBDIARIO']  = item.c20;

            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    } 

    // Libro Diario Simplificado
    if (this.id === 105)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.subini = this.filtroForm.value.subdiarioini;
      this.filtroReporte.subfin = this.filtroForm.value.subdiariofin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.categoria = this.filtroForm.value.categoria;
      this.reporteUtil.downloadXls('DHAGEZ_LibroDiarioSimplificado', this.filtroReporte);
    } 

    // Libro Mayor
    if (this.id === 106)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosa;
      this.reporteUtil.downloadXls('DHAGEZ_LibroMayor', this.filtroReporte);
    }

    // Libro Mayor Simplificado
    if (this.id === 107)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosa;
      this.reporteUtil.downloadXls('DHAGEZ_LibroMayorBiMoneda', this.filtroReporte);
    }

    // Libro Caja y Banco
    if (this.id === 108)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosacaja;
      this.reporteUtil.downloadXls('DHAGEZ_CajaBanco', this.filtroReporte);
    }

    // Libro Balance de Comprobación
    if (this.id === 109)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.downloadXls('DHAGEZ_BalanceComprobacion', this.filtroReporte);
    }

    // PLE Libro Diario
    if (this.id === 200)
    { 
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00050100001111';

      this.reporteService.exportarPLELibroDiario(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
  
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'CODIGO UNICO DE LA OPERACION (CUO)';
          objectReport['c3']   = 'NUMERO CORRELATIVO Ó CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c4']   = 'CÓDIGO CUENTA CONTABLE';
          objectReport['c5']   = 'CÓDIGO ÚNICO DE LA OPERACIÓN UEA, UN, UP';
          objectReport['c6']   = 'CÓDIGO CENTRO COSTO';
          objectReport['c7']   = 'MONEDA';
          objectReport['c8']   = 'TIPO DOCUMENTO IDENTIDAD EMISOR';
          objectReport['c9']   = 'NÚMERO DOCUMENTO IDENTIDAD EMISOR';
          objectReport['c10']  = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c11']  = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c12']  = 'NÚMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c13']  = 'FECHA CONTABLE';
          objectReport['c14']  = 'FECHA VENCIMIENTO';
          objectReport['c15']  = 'FECHA OPERACIÓN O EMISIÓN';
          objectReport['c16']  = 'GLOSA';
          objectReport['c17']  = 'GLOSA REFERENCIAL';
          objectReport['c18']  = 'DEBE';
          objectReport['c19']  = 'HABER';
          objectReport['c20']  = 'CÓDIGO DEL LIBRO';          
          objectReport['c21']  = 'ESTADO DE LA OPERACIÓN';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c8;
            objectReport['c9']   = item.c9;
            objectReport['c10']  = item.c10;
            objectReport['c11']  = item.c11;
            objectReport['c12']  = item.c12;
            objectReport['c13']  = item.c13;
            objectReport['c14']  = item.c14;
            objectReport['c15']  = item.c15;
            objectReport['c16']  = item.c16;
            objectReport['c17']  = item.c17;
            objectReport['c18']  = item.c18;
            objectReport['c19']  = item.c19;
            objectReport['c20']  = item.c20;
            objectReport['c21']  = item.c21;

            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 204)
    {
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080100001111';

      this.reporteService.exportarPLERegistroCompra(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
  
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'CODIGO UNICO DE LA OPERACION (CUO)';
          objectReport['c3']   = 'NUMERO CORRELATIVO Ó CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c4']   = 'FECHA DE EMISION DEL COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'FECHA DE VENCIMIENTO O FECHA DE PAGO';
          objectReport['c6']   = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c7']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c8']   = 'AÑO DUAL O DSI';
          objectReport['c9']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c10']  = 'EN CASO DE OPTAR POR ANOTAR EL IMPORTE TOTAL DE LAS OPERACIONES DIARIAS';
          objectReport['c11']  = 'TIPO DOCUMENTO IDENTIDAD PROVEEDOR';
          objectReport['c12']  = 'NUMERO DOCUMENTO IDENTIDAD PROVEEDOR';
          objectReport['c13']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL PROVEEDOR';
          objectReport['c14']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACION';
          objectReport['c15']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          objectReport['c16']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACION YA OPERACIONES NO GRAVADAS';
          objectReport['c17']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          objectReport['c18']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES NO GRAVADAS';
          objectReport['c19']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          objectReport['c20']  = 'VALOR DE LAS ADQUISICIONES NO GRAVADAS';
          objectReport['c21']  = 'MONTO DEL IMPUESTO SELECTIVO AL CONSUMO EN LOS CASOS EN QUE EL SUJERO PUEDA UTILIZARLO COMO DEDUCCION';
          objectReport['c22']  = 'IMPUESTO AL CONSUMO DE LAS BOLSAS DE PLÁSTICO';
          objectReport['c23']  = 'OTROS CONCEPTOS, TRIBUTOS Y CARGOS QUE NO FORMEN PARTE DE LA BASE IMPONIBLE';
          objectReport['c24']  = 'IMPORTE TOTAL DE LAS ADQUISICIONES REGISTRADAS SEGUN COMPROBANTE DE PAGO';
          objectReport['c25']  = 'MONEDA';
          objectReport['c26']  = 'TIPO DE CAMBIO';
          objectReport['c27']  = 'FECHA DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c28']  = 'TIPO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c29']  = 'NUMERO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c30']  = 'CODIGO DEP. ADUANERA DE LA DECLARACION UNICA DE ADUANES (DUA) O DECLARACION SIMPLIFICA DE IMPORTACION (DSI)';
          objectReport['c31']  = 'NÚMERO DEL COMPROBANTE DE PAGO QUE SE MODIFICA';
          objectReport['c32']  = 'FECHA DE EMISIÓN DE LA CONSTANCIA DE DEPOSITO DE DETRACCION';
          objectReport['c33']  = 'NUMERO DE EMISIÓN DE LA CONSTANCIA DE DEPOSITO DE DETRACCION';
          objectReport['c34']  = 'MARCA DEL COMPROBANTE DE PAGO SUJETO A RETENCION';
          objectReport['c35']  = 'CLASIFICACIÓN DE LOS BIENES Y SERVICIOS ADQUIRIDOS';
          objectReport['c36']  = 'IDENTIFICACION DEL CONTRATO, PROYECTO EN EL CASO DE LOS OPERADORES DE LAS SOCIEDADES IRREGULARES, JOINT VENTURES';
          objectReport['c37']  = 'ERROR TIPO 1: INCONSISTENCIA EN EL TIPO DE CAMBIO';
          objectReport['c38']  = 'ERROR TIPO 2: INCONSISTENCIA POR PROVEEDORES NO HABIDOS';
          objectReport['c39']  = 'ERROR TIPO 3: INCONSISTENCIA POR PROVEEDORES QUE RENUNCIARON A LA EXONEARACION DEL APENDICE DEL IGV';
          objectReport['c40']  = 'ERROR TIPO 4: INCONSISTENCIA POR DNIs QUE FUERON UTILIZADOS EN LAS LIQUIDACIONES QUE YA CUENTAN CON RUC';
          objectReport['c41']  = 'INDICADOR DE COMPROBANTES DE PAGO CANCELADOS CON MEDIOS DE PAGO';
          objectReport['c42']  = 'ESTADO QUE IDENTIFICA LA OPORTUNIDAD DE LA ANOTACION O INDICACION SI ESTA CORRESPONDE A UN AJUSTE';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c8;
            objectReport['c9']   = item.c9;
            objectReport['c10']  = item.c10;
            objectReport['c11']  = item.c11;
            objectReport['c12']  = item.c12;
            objectReport['c13']  = item.c13;
            objectReport['c14']  = item.c14;
            objectReport['c15']  = item.c15;
            objectReport['c16']  = item.c16;
            objectReport['c17']  = item.c17;
            objectReport['c18']  = item.c18;
            objectReport['c19']  = item.c19;
            objectReport['c20']  = item.c20;
            objectReport['c21']  = item.c21;
            objectReport['c22']  = item.c22;
            objectReport['c23']  = item.c23;
            objectReport['c24']  = item.c24;
            objectReport['c25']  = item.c25;
            objectReport['c26']  = item.c26;
            objectReport['c27']  = item.c27;
            objectReport['c28']  = item.c28;
            objectReport['c29']  = item.c29;
            objectReport['c30']  = item.c30;
            objectReport['c31']  = item.c31;
            objectReport['c32']  = item.c32;
            objectReport['c33']  = item.c33;
            objectReport['c34']  = item.c34;
            objectReport['c35']  = item.c35;
            objectReport['c36']  = item.c36;
            objectReport['c37']  = item.c37;
            objectReport['c38']  = item.c38;
            objectReport['c39']  = item.c39;
            objectReport['c40']  = item.c40;
            objectReport['c41']  = item.c41;
            objectReport['c42']  = item.c42;
            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 205)
    {
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080200001111';

      this.reporteService.exportarPLERegistroCompraNoDom(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
  
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'CODIGO UNICO DE LA OPERACION (CUO)';
          objectReport['c3']   = 'NUMERO CORRELATIVO Ó CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c4']   = 'FECHA DE EMISION DEL COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO DEL SUJETO NO DOMICILIADO';
          objectReport['c6']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c7']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c8']   = 'VALOR DE LAS ADQUISICIONES';
          objectReport['c9']   = 'OTROS CONCEPTOOS ADICIONALES';
          objectReport['c10']  = 'IMPORTE TOTAL DE LAS ADQUISICIONES REGISTRADAS SEGUN COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c11']  = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO QUE SUSTENTA EL CRÉDITO FISCAL';
          objectReport['c12']  = 'SERIE DEL COMPROBANTE DE PAGO O DOCUMENTO QUE SUSTENTA EL CRÉDITO FISCAL';
          objectReport['c13']  = 'AÑO DE EMISIÓN DE LA DUA O DSI QUE SUSTENTA EL CRÉDITO FISCAL';
          objectReport['c14']  = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO QUE SUSTENTA EL CRÉDITO FISCAL';
          objectReport['c15']  = 'MONTO DE RETENCIÓN DEL IGV';
          objectReport['c16']  = 'MONEDA';
          objectReport['c17']  = 'TIPO DE CAMBIO';
          objectReport['c18']  = 'PAIS DE LA RESIDENCIA DEL SUJETO NO DOMICILIADO';
          objectReport['c19']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL DEL SUJETO NO DOMICILIADO';
          objectReport['c20']  = 'DOMICILIO EN EL EXTRANJERO DEL SUJETO NO DOMICILIADO';
          objectReport['c21']  = 'NÚMERO DE IDENTIFICACIÓN DEL SUJETO NO DOMICILIADO';
          objectReport['c22']  = 'NÚMERO DE IDENTIFICACIÓN FISCAL DEL BENEFICIARIO EFECTIVO DE LOS PAGOS';
          objectReport['c23']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL DEL BENEFICIARIO';
          objectReport['c24']  = 'PAIS DE LA RESIDENCIA DEL BENEFICIARIO EFECTIVO DE LOS PAGOS';
          objectReport['c25']  = 'VÍNCULO ENTRE EL CONTRIBUYENTE Y EL RESIDENTE EN EL EXTRANJERO';
          objectReport['c26']  = 'RENTA BRUTA';
          objectReport['c27']  = 'DEDUCCIÓN / COSTO DE ENAJENACIÓN DE BIENES DE CAPITAL';
          objectReport['c28']  = 'RENTA NETA';
          objectReport['c29']  = 'TASA DE RETENCIÓN';
          objectReport['c30']  = 'IMPUESTO RETENIDO';
          objectReport['c31']  = 'CONVENIOS PARA EVITAR LA DOBLE IMPOSICIÓN';
          objectReport['c32']  = 'EXONERACIÓN APLICADA';
          objectReport['c33']  = 'TIPO DE RENTA';
          objectReport['c34']  = 'MODALIDAD DEL SERVICIO PRESTADO POR EL NO DOMICILIADO';
          objectReport['c35']  = 'APLICACIÓN DEL PENULTIMO PARRAFO DEL ART. 76° DE LA LEY DEL IMPUESTO A LA RENTA';
          objectReport['c36']  = 'ESTADO QUE IDENTIFICA LA OPORTUNIDAD DE LA ANOTACION O INDICACION SI ESTA CORRESPONDE A UN AJUSTE';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c8;
            objectReport['c9']   = item.c9;
            objectReport['c10']  = item.c10;
            objectReport['c11']  = item.c11;
            objectReport['c12']  = item.c12;
            objectReport['c13']  = item.c13;
            objectReport['c14']  = item.c14;
            objectReport['c15']  = item.c15;
            objectReport['c16']  = item.c16;
            objectReport['c17']  = item.c17;
            objectReport['c18']  = item.c18;
            objectReport['c19']  = item.c19;
            objectReport['c20']  = item.c20;
            objectReport['c21']  = item.c21;
            objectReport['c22']  = item.c22;
            objectReport['c23']  = item.c23;
            objectReport['c24']  = item.c24;
            objectReport['c25']  = item.c25;
            objectReport['c26']  = item.c26;
            objectReport['c27']  = item.c27;
            objectReport['c28']  = item.c28;
            objectReport['c29']  = item.c29;
            objectReport['c30']  = item.c30;
            objectReport['c31']  = item.c31;
            objectReport['c32']  = item.c32;
            objectReport['c33']  = item.c33;
            objectReport['c34']  = item.c34;
            objectReport['c35']  = item.c35;
            objectReport['c36']  = item.c36;
            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 206)
    {
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080300001111';

      this.reporteService.exportarPLERegistroCompra(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
  
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'CODIGO UNICO DE LA OPERACION (CUO)';
          objectReport['c3']   = 'NUMERO CORRELATIVO Ó CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c4']   = 'FECHA DE EMISION DEL COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'FECHA DE VENCIMIENTO O FECHA DE PAGO';
          objectReport['c6']   = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c7']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          // objectReport['c8']   = 'AÑO DUAL O DSI';
          objectReport['c8']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c9']  = 'EN CASO DE OPTAR POR ANOTAR EL IMPORTE TOTAL DE LAS OPERACIONES DIARIAS';
          objectReport['c10']  = 'TIPO DOCUMENTO IDENTIDAD PROVEEDOR';
          objectReport['c11']  = 'NUMERO DOCUMENTO IDENTIDAD PROVEEDOR';
          objectReport['c12']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL PROVEEDOR';
          objectReport['c13']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACION';
          objectReport['c14']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          // objectReport['c16']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES GRAVADAS Y/O DE EXPORTACION YA OPERACIONES NO GRAVADAS';
          // objectReport['c17']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          // objectReport['c18']  = 'BASE IMPONIBLE DE LAS ADQUISICIONES GRAVADAS DESTINADAS A OPERACIONES NO GRAVADAS';
          // objectReport['c19']  = 'MONTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          // objectReport['c20']  = 'VALOR DE LAS ADQUISICIONES NO GRAVADAS';
          // objectReport['c21']  = 'MONTO DEL IMPUESTO SELECTIVO AL CONSUMO EN LOS CASOS EN QUE EL SUJERO PUEDA UTILIZARLO COMO DEDUCCION';
          objectReport['c15']  = 'IMPUESTO AL CONSUMO DE LAS BOLSAS DE PLÁSTICO';
          objectReport['c16']  = 'OTROS CONCEPTOS, TRIBUTOS Y CARGOS QUE NO FORMEN PARTE DE LA BASE IMPONIBLE';
          objectReport['c17']  = 'IMPORTE TOTAL DE LAS ADQUISICIONES REGISTRADAS SEGUN COMPROBANTE DE PAGO';
          objectReport['c18']  = 'MONEDA';
          objectReport['c19']  = 'TIPO DE CAMBIO';
          objectReport['c20']  = 'FECHA DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c21']  = 'TIPO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c22']  = 'NUMERO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          // objectReport['c29']  = 'CODIGO DEP. ADUANERA DE LA DECLARACION UNICA DE ADUANES (DUA) O DECLARACION SIMPLIFICA DE IMPORTACION (DSI)';
          objectReport['c23']  = 'NÚMERO DEL COMPROBANTE DE PAGO QUE SE MODIFICA';
          objectReport['c24']  = 'FECHA DE EMISIÓN DE LA CONSTANCIA DE DEPOSITO DE DETRACCION';
          objectReport['c25']  = 'NUMERO DE EMISIÓN DE LA CONSTANCIA DE DEPOSITO DE DETRACCION';
          objectReport['c26']  = 'MARCA DEL COMPROBANTE DE PAGO SUJETO A RETENCION';
          objectReport['c27']  = 'CLASIFICACIÓN DE LOS BIENES Y SERVICIOS ADQUIRIDOS';
          // objectReport['c35']  = 'IDENTIFICACION DEL CONTRATO, PROYECTO EN EL CASO DE LOS OPERADORES DE LAS SOCIEDADES IRREGULARES, JOINT VENTURES';
          objectReport['c28']  = 'ERROR TIPO 1: INCONSISTENCIA EN EL TIPO DE CAMBIO';
          objectReport['c29']  = 'ERROR TIPO 2: INCONSISTENCIA POR PROVEEDORES NO HABIDOS';
          objectReport['c30']  = 'ERROR TIPO 3: INCONSISTENCIA POR PROVEEDORES QUE RENUNCIARON A LA EXONEARACION DEL APENDICE DEL IGV';
          // objectReport['c39']  = 'ERROR TIPO 4: INCONSISTENCIA POR DNIs QUE FUERON UTILIZADOS EN LAS LIQUIDACIONES QUE YA CUENTAN CON RUC';
          objectReport['c31']  = 'INDICADOR DE COMPROBANTES DE PAGO CANCELADOS CON MEDIOS DE PAGO';
          objectReport['c32']  = 'ESTADO QUE IDENTIFICA LA OPORTUNIDAD DE LA ANOTACION O INDICACION SI ESTA CORRESPONDE A UN AJUSTE';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c9;
            objectReport['c9']   = item.c10;
            objectReport['c10']  = item.c11;
            objectReport['c11']  = item.c12;
            objectReport['c12']  = item.c13;
            objectReport['c13']  = item.c14;
            objectReport['c14']  = item.c15;
            objectReport['c15']  = item.c22;
            objectReport['c16']  = item.c23;
            objectReport['c17']  = item.c24;
            objectReport['c18']  = item.c25;
            objectReport['c19']  = item.c26;
            objectReport['c20']  = item.c27;
            objectReport['c21']  = item.c28;
            objectReport['c22']  = item.c29;
            objectReport['c23']  = item.c31;
            objectReport['c24']  = item.c32;
            objectReport['c25']  = item.c33;
            objectReport['c26']  = item.c34;
            objectReport['c27']  = item.c35;
            objectReport['c28']  = item.c37;
            objectReport['c29']  = item.c38;
            objectReport['c30']  = item.c39;
            objectReport['c31']  = item.c41;
            objectReport['c32']  = item.c42;
            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 207)
    {
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140100001111';

      this.reporteService.exportarPLERegistroVenta(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'NUMERO CORRELATIVO O CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c3']   = 'SECUENCIA';
          objectReport['c4']   = 'FECHA DE EMISION DEL COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'FECHA DE VENCIMIENTO O FECHA DE PAGO';
          objectReport['c6']   = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c7']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c8']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c9']   = 'PARA EFECTOS DEL REGISTRO DE TICKETS O CINTAS EMITIDOS POR MAQUINAS REGISTRADORAS';
          objectReport['c10']  = 'TIPO DOCUMENTO IDENTIDAD CLIENTE';
          objectReport['c11']  = 'NUMERO DOCUMENTO IDENTIDAD CLIENTE';
          objectReport['c12']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL CLIENTE';
          objectReport['c13']  = 'VALOR FACTURADO DE LA EXPORTACION';
          objectReport['c14']  = 'BASE IMPONIBLE DE LA OPERACIÓN GRAVADA';
          objectReport['c15']  = 'DESCUENTO DE LA BASE IMPONIBLE';
          objectReport['c16']  = 'IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          objectReport['c17']  = 'DESCUENTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          objectReport['c18']  = 'IMPORTE TOTAL DELA OPERACIÓN EXONERADA';
          objectReport['c19']  = 'IMPORTE TOTAL DELA OPERACIÓN INAFECTA';
          objectReport['c20']  = 'IMPUESTO SELECTIVO AL CONSUMO';
          objectReport['c21']  = 'BASE IMPONIBLE DE LA OPERACION GRAVADA VENTAS ARROZ PILADO';
          objectReport['c22']  = 'IMPUESTO A LAS VENTAS ARROZ PILADO';
          objectReport['c23']  = 'IMPUESTO AL CONSUMO DE LAS BOLSAS DE PLÁSTICO';
          objectReport['c24']  = 'OTROS TRIBUTOS Y CARGOS QUE NO FORMAN PARTE DE LA BASE IMPONIBLE';
          objectReport['c25']  = 'IMPORTE TOTAL DEL COMPROBANTE DE PAGO';
          objectReport['c26']  = 'MONEDA';
          objectReport['c27']  = 'TIPO DE CAMBIO';
          objectReport['c28']  = 'FECHA DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c29']  = 'TIPO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c30']  = 'SERIE DE REF. DEL COMPROBANTE DE PAGO  O DOCUMENTO MODIFICADO';
          objectReport['c31']  = 'NUMERO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c32']  = 'IDENTIFICACION DEL CONTRATO O PROYECTO (JOINT VENTURES)';
          objectReport['c33']  = 'ERROR 1: INCONSISTENCIA EN EL TIPO DE CAMBIO';
          objectReport['c34']  = 'INDICADOR DE COMPROBANTES DE PAGO CANCELADOS CON MEDIOS DE PAGO';
          objectReport['c35']  = 'ESTADO';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c8;
            objectReport['c9']   = item.c9;
            objectReport['c10']  = item.c10;
            objectReport['c11']  = item.c11;
            objectReport['c12']  = item.c12;
            objectReport['c13']  = item.c13;
            objectReport['c14']  = item.c14;
            objectReport['c15']  = item.c15;
            objectReport['c16']  = item.c16;
            objectReport['c17']  = item.c17;
            objectReport['c18']  = item.c18;
            objectReport['c19']  = item.c19;
            objectReport['c20']  = item.c20;
            objectReport['c21']  = item.c21;
            objectReport['c22']  = item.c22;
            objectReport['c23']  = item.c23;
            objectReport['c24']  = item.c24;
            objectReport['c25']  = item.c25;
            objectReport['c26']  = item.c26;
            objectReport['c27']  = item.c27;
            objectReport['c28']  = item.c28;
            objectReport['c29']  = item.c29;
            objectReport['c30']  = item.c30;
            objectReport['c31']  = item.c31;
            objectReport['c32']  = item.c32;
            objectReport['c33']  = item.c33;
            objectReport['c34']  = item.c34;
            objectReport['c35']  = item.c35;
            reportExcel.push(objectReport);
          }  
  
          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 208)
    {
      nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140200001111';

      this.reporteService.exportarPLERegistroVenta(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'PERIODO';
          objectReport['c2']   = 'NUMERO CORRELATIVO O CODIGO UNICO DE LA OPERACIÓN';
          objectReport['c3']   = 'SECUENCIA';
          objectReport['c4']   = 'FECHA DE EMISION DEL COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'FECHA DE VENCIMIENTO O FECHA DE PAGO';
          objectReport['c6']   = 'TIPO COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c7']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c8']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c9']   = 'PARA EFECTOS DEL REGISTRO DE TICKETS O CINTAS EMITIDOS POR MAQUINAS REGISTRADORAS';
          objectReport['c10']  = 'TIPO DOCUMENTO IDENTIDAD CLIENTE';
          objectReport['c11']  = 'NUMERO DOCUMENTO IDENTIDAD CLIENTE';
          objectReport['c12']  = 'APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL CLIENTE';
          // objectReport['c13']  = 'VALOR FACTURADO DE LA EXPORTACION';
          objectReport['c13']  = 'BASE IMPONIBLE DE LA OPERACIÓN GRAVADA';
          // objectReport['c15']  = 'DESCUENTO DE LA BASE IMPONIBLE';
          objectReport['c14']  = 'IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          // objectReport['c17']  = 'DESCUENTO DEL IMPUESTO GENERAL A LAS VENTAS Y/O IMPUESTO DE PROMOCION MUNICIPAL';
          // objectReport['c18']  = 'IMPORTE TOTAL DELA OPERACIÓN EXONERADA';
          // objectReport['c19']  = 'IMPORTE TOTAL DELA OPERACIÓN INAFECTA';
          // objectReport['c20']  = 'IMPUESTO SELECTIVO AL CONSUMO';
          // objectReport['c21']  = 'BASE IMPONIBLE DE LA OPERACION GRAVADA VENTAS ARROZ PILADO';
          // objectReport['c22']  = 'IMPUESTO A LAS VENTAS ARROZ PILADO';
          objectReport['c15']  = 'IMPUESTO AL CONSUMO DE LAS BOLSAS DE PLÁSTICO';
          objectReport['c16']  = 'OTROS TRIBUTOS Y CARGOS QUE NO FORMAN PARTE DE LA BASE IMPONIBLE';
          objectReport['c17']  = 'IMPORTE TOTAL DEL COMPROBANTE DE PAGO';
          objectReport['c18']  = 'MONEDA';
          objectReport['c19']  = 'TIPO DE CAMBIO';
          objectReport['c20']  = 'FECHA DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c21']  = 'TIPO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          objectReport['c22']  = 'SERIE DE REF. DEL COMPROBANTE DE PAGO  O DOCUMENTO MODIFICADO';
          objectReport['c23']  = 'NUMERO DE REF. DEL COMPROBANTE DE PAGO O DOCUMENTO ORIGINAL QUE SE MODIFICA';
          // objectReport['c31']  = 'IDENTIFICACION DEL CONTRATO O PROYECTO (JOINT VENTURES)';
          objectReport['c24']  = 'ERROR 1: INCONSISTENCIA EN EL TIPO DE CAMBIO';
          objectReport['c25']  = 'INDICADOR DE COMPROBANTES DE PAGO CANCELADOS CON MEDIOS DE PAGO';
          objectReport['c26']  = 'ESTADO';
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c1;
            objectReport['c2']   = item.c2;
            objectReport['c3']   = item.c3;
            objectReport['c4']   = item.c4;
            objectReport['c5']   = item.c5;
            objectReport['c6']   = item.c6;
            objectReport['c7']   = item.c7;
            objectReport['c8']   = item.c8;
            objectReport['c9']   = item.c9;
            objectReport['c10']  = item.c10;
            objectReport['c11']  = item.c11;
            objectReport['c12']  = item.c12;
            objectReport['c13']  = item.c14;
            objectReport['c14']  = item.c16;
            objectReport['c15']  = item.c23;
            objectReport['c16']  = item.c24;
            objectReport['c17']  = item.c25;
            objectReport['c18']  = item.c26;
            objectReport['c19']  = item.c27;
            objectReport['c20']  = item.c28;
            objectReport['c21']  = item.c29;
            objectReport['c22']  = item.c30;
            objectReport['c23']  = item.c31;
            objectReport['c24']  = item.c33;
            objectReport['c25']  = item.c34;
            objectReport['c26']  = item.c35;
            reportExcel.push(objectReport);
          }  
  

          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 209)
    { 
      nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';

      this.reporteService.exportarPLAMEPrestadorServicio(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
        this.valoresiniciales.ruc, this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, this.filtroForm.value.orden).subscribe(data => {
        this.listarReporte = data;
        // console.log(this.listarReporte);
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'TIPO DOCUMENTO IDENTIDAD';
          objectReport['c2']   = 'NUMERO DOCUMENTO IDENTIDAD';
          objectReport['c3']   = 'APELLIDO PATERNO';
          objectReport['c4']   = 'APELLIDO MATERNO';
          objectReport['c5']   = 'NOMBRES';
          objectReport['c6']   = 'DOMICILIADO';
          objectReport['c7']   = 'CONVENIO EVITA DOBLE TRIBUTACIÓN';
          
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c14;
            objectReport['c2']   = item.c15;
            objectReport['c3']   = item.c16;
            objectReport['c4']   = item.c17;
            objectReport['c5']   = item.c18;
            objectReport['c6']   = item.c19;
            objectReport['c7']   = item.c28;
            
            reportExcel.push(objectReport);
          }    

          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    if (this.id === 210)
    { 
      nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';

      this.reporteService.exportarPLAMEPrestadorServicio(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
        this.valoresiniciales.ruc, this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, this.filtroForm.value.orden).subscribe(data => {
        this.listarReporte = data;
        // console.log(this.listarReporte);
        if (this.listarReporte.length > 0) {
          const reportExcel = [];
          const objectReport = {};

          objectReport['c1']   = 'TIPO DOCUMENTO IDENTIDAD';
          objectReport['c2']   = 'NUMERO DOCUMENTO IDENTIDAD';
          objectReport['c3']   = 'TIPO COMPROBANTE';
          objectReport['c4']   = 'SERIE DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c5']   = 'NUMERO DE COMPROBANTE DE PAGO O DOCUMENTO';
          objectReport['c6']   = 'MONTO TOTAL DE SERVICIO';
          objectReport['c7']   = 'FECHA DE EMISION DEL COMPROBANTE';
          objectReport['c8']   = 'FECHA DE PAGO';
          objectReport['c9']   = 'INDICADOR RETENCIÓN';
          objectReport['c10']  = 'INDICADOR RETENCIÓN REG.PEN.';
          objectReport['c11']  = 'IMPORTE APORTE REG.PEN.';

          
          reportExcel.push(objectReport);

          for (const item of this.listarReporte ){
            // tslint:disable-next-line:no-shadowed-variable
            const objectReport = {};
            objectReport['c1']   = item.c14;
            objectReport['c2']   = item.c15;
            objectReport['c3']   = item.c10;
            objectReport['c4']   = item.c11;
            objectReport['c5']   = item.c12;
            objectReport['c6']   = item.c20;
            objectReport['c7']   = item.c8;
            objectReport['c8']   = item.c9;
            objectReport['c9']   = item.c31;
            objectReport['c10']  = item.c30;
            objectReport['c11']  = '';
            
            reportExcel.push(objectReport);
          }  
  

          this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);
  
        }
  
      }); 
    }

    // Saldo por Cuenta - Entidad - Documento
    if (this.id === 301)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.tiposaldo = this.filtroForm.value.estadodoc;      
      this.filtroReporte.date = this.filtroForm.value.fecha.substr(8, 2) + '/'  +
                                this.filtroForm.value.fecha.substr(5, 2) + '/'  +
                                this.filtroForm.value.fecha.substr(0, 4);
      // console.log(this.filtroReporte.date);
      this.reporteUtil.downloadXls('DHAGEZ_SaldoCuentaEntidadDocumento', this.filtroReporte);
    }

    // Estado de situación financiera
    if (this.id === 401)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.downloadXls('DHAGEZ_EstadoSituacion', this.filtroReporte);
    }

    // Estado de situación financiera vertical
    if (this.id === 402)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.downloadXls('DHAGEZ_EstadoSituacionVertical', this.filtroReporte);
    }

    // Estado de resultado por naturaleza
    if (this.id === 403)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.downloadXls('DHAGEZ_EstadoResultadoNaturaleza', this.filtroReporte);
    }

    // Estado de resultado por función
    if (this.id === 404)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.downloadXls('DHAGEZ_EstadoResultadoFuncion', this.filtroReporte);
    }

  }

  exportarExcelRegistroCompra(): void{
    let nombrearchivo = ''; 

    nombrearchivo = 'RegistroCompra' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes;

    this.reporteService.exportarRegistroCompra(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
      this.valoresiniciales.ruc, this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, this.filtroForm.value.orden).subscribe(data => {
      this.listarReporte = data;
      // console.log(this.listarReporte);

      if (this.listarReporte.length > 0) {
        const reportExcel = [];      

        for (const item of this.listarReporte ){
          // tslint:disable-next-line:no-shadowed-variable
          const objectReport = {};
          objectReport['RUC']   = item.c1;
          objectReport['Nombre Empresa']   = item.c2;
          objectReport['Ejercicio Contable']   = item.c3;
          objectReport['Período Contable']   = item.c4;
          objectReport['Moneda']   = item.c5;
          objectReport['Número Correlativo']   = item.c6;
          objectReport['Fecha de Emisión']   = item.c7;
          objectReport['Fecha de Vencimiento o Pago']   = item.c8;
          objectReport['Tipo Comprobante']   = item.c9;
          objectReport['Serie Comprobante']  = item.c10;
          objectReport['Año Comprobante']  = item.c11;
          objectReport['Número Comprobante']  = item.c12;
          objectReport['Tipo Documento - Proveedor']  = item.c13;
          objectReport['Número Documento - Proveedor']  = item.c14;
          objectReport['Proveedor']  = item.c15;
          objectReport['Base Imponible - Gravadas']  = item.c16;
          objectReport['IGV - Gravadas']  = item.c17;
          objectReport['Base Imponible - Si/No Gravadas']  = item.c18;
          objectReport['IGV - Si/No Gravadas']  = item.c19;
          objectReport['Base Imponible - No Gravadas']  = item.c20;
          objectReport['IGV - No Gravadas']  = item.c21;
          objectReport['Adquisiciones No Gravadas']  = item.c22;
          objectReport['ISC']  = item.c23;
          objectReport['Otros Tributos y Cargos']  = item.c24;
          objectReport['Importe Total']  = item.c16 + item.c17 + item.c18 + item.c19 + item.c20 + item.c21 + item.c22 + item.c23 + item.c24 + item.c25;
          objectReport['Número Comprobante - No Domiciliado']  = item.c26;
          objectReport['Número Detracción']  = item.c27;
          objectReport['Fecha Detracción']  = item.c28;
          objectReport['Tipo de Cambio']  = item.c29;
          objectReport['Fecha de Emisión - Referencia']  = item.c30;
          objectReport['Tipo Documento - Referencia']  = item.c31;
          objectReport['Serie Documento - Referencia']  = item.c32;
          objectReport['Número Documento - Referencia']  = item.c33;

          reportExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFileNotDate(reportExcel, nombrearchivo);

      }

    }); 
  }

  exportarTexto(): void
  {
    let nombrearchivo = '';
    let texto = '';    
    // PLE Libro Diario
    if (this.id === 200)
    {
      this.reporteService.exportarPLELibroDiario(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00050100001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c8  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c13 + '|' + item.c14 + '|' + item.c15 + '|' +
                      item.c16 + '|' + item.c17 + '|' + parseFloat(item.c18).toFixed(2) + '|' + parseFloat(item.c19).toFixed(2) + '|' +
                      item.c20 + '|' + item.c21 + '|' + '\r\n';
            // console.log(parseFloat(item.c24).toFixed(2));
          }                
        }  
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00050100001111';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);  
      }); 
    }  

    if (this.id === 204)
    {
      this.reporteService.exportarPLERegistroCompra(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080100001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c8  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c13 + '|' + item.c14 + '|' + item.c15 + '|' +
                      item.c16 + '|' + item.c17 + '|' + item.c18 + '|' + item.c19 + '|' + item.c20 + '|' +
                      item.c21 + '|' + item.c22 + '|' + item.c23 + '|' + parseFloat(item.c24).toFixed(2) + '|' + item.c25 + '|' +
                      item.c26 + '|' + item.c27 + '|' + item.c28 + '|' + item.c29 + '|' + item.c30 + '|' +
                      item.c31 + '|' + item.c32 + '|' + item.c33 + '|' + item.c34 + '|' + item.c35 + '|' +
                      item.c36 + '|' + item.c37 + '|' + item.c38 + '|' + item.c39 + '|' + item.c40 + '|' +
                      item.c41 + '|' + item.c42 + '|' + '\r\n';
            // console.log(parseFloat(item.c24).toFixed(2));
          }                
        }  
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080100001011';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);  
      }); 
    } 

    if (this.id === 205)
    {
      this.reporteService.exportarPLERegistroCompraNoDom(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080200001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c8  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c13 + '|' + item.c14 + '|' + item.c15 + '|' +
                      item.c16 + '|' + item.c17 + '|' + item.c18 + '|' + item.c19 + '|' + item.c20 + '|' +
                      item.c21 + '|' + item.c22 + '|' + item.c23 + '|' + item.c24 + '|' + item.c25 + '|' +
                      item.c26 + '|' + item.c27 + '|' + item.c28 + '|' + item.c29 + '|' + item.c30 + '|' +
                      item.c31 + '|' + item.c32 + '|' + item.c33 + '|' + item.c34 + '|' + item.c35 + '|' +
                      item.c36 + '|' + '\r\n';
          }    
        }
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080200001011';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);    
      }); 
    }

    if (this.id === 206)
    {
      this.reporteService.exportarPLERegistroCompra(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080300001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c13 + '|' + item.c14 + '|' + item.c15 + '|' +                      
                      item.c22 + '|' + item.c23 + '|' + item.c24 + '|' + item.c25 + '|' +
                      item.c26 + '|' + item.c27 + '|' + item.c28 + '|' + item.c29 + '|' +
                      item.c31 + '|' + item.c32 + '|' + item.c33 + '|' + item.c34 + '|' +
                      item.c35 + '|' + item.c37 + '|' + item.c38 + '|' + item.c39 + '|' +
                      item.c41 + '|' + item.c42 + '|' + '\r\n';
          }    
        }
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00080300001011';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);       
      }); 
    }

    if (this.id === 207)
    {
      this.reporteService.exportarPLERegistroVenta(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140100001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c8  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c13 + '|' + item.c14 + '|' + item.c15 + '|' +
                      item.c16 + '|' + item.c17 + '|' + item.c18 + '|' + item.c19 + '|' + item.c20 + '|' ;
            if (item.c21 === 0)
            {
              texto += '|';
            }
            else
            {
              texto += item.c21 + '|';
            }
            
            if (item.c22 === 0)
            {
              texto += '|';
            }
            else
            {
              texto += item.c22 + '|';
            }
            texto +=  item.c21 + '|' + item.c22 + '|' + item.c23 + '|' + item.c24 + '|' + item.c25 + '|' +
                      item.c26 + '|' + item.c27 + '|' + item.c28 + '|' + item.c29 + '|' + item.c30 + '|' +
                      item.c31 + '|' + item.c32 + '|' + item.c33 + '|' + item.c34 + '|' + item.c35 + '|' + '\r\n';
          }    
        }     
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140100001011';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);   
      }); 
    }

    if (this.id === 208)
    {
      this.reporteService.exportarPLERegistroVenta(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140200001111';
          for (const item of this.listarReporte ){

            texto +=  item.c1  + '|' + item.c2  + '|' + item.c3  + '|' + item.c4  + '|' + item.c5  + '|' +
                      item.c6  + '|' + item.c7  + '|' + item.c8  + '|' + item.c9  + '|' + item.c10 + '|' +
                      item.c11 + '|' + item.c12 + '|' + item.c14 + '|' + 
                      item.c16 + '|' + 
                      item.c23 + '|' + item.c24 + '|' + item.c25 + '|' +
                      item.c26 + '|' + item.c27 + '|' + item.c28 + '|' + item.c29 + '|' + item.c30 + '|' +
                      item.c31 + '|' + item.c33 + '|' + item.c34 + '|' + item.c35 + '|' + '\r\n';
          }    
        }   
        else
        {
          nombrearchivo = 'LE' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '00140200001011';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);     
      }); 
    }

    if (this.id === 209)
    {

      this.reporteService.exportarPLAMEPrestadorServicio(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
        this.valoresiniciales.ruc, this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, this.filtroForm.value.orden).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';
          for (const item of this.listarReporte ){

            texto +=  item.c14  + '|' + item.c15  + '|' + item.c16  + '|' + item.c17  + '|' + item.c18  + '|' +
                      item.c19  + '|' + item.c28  + '|' + '\r\n';

          }                
        }  
        else
        {
          nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);  
      }); 
    }  

    if (this.id === 210)
    {

      this.reporteService.exportarPLAMEPrestadorServicio(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.filtroForm.value.mes, 
        this.valoresiniciales.ruc, this.valoresiniciales.nombreempresa, this.filtroForm.value.moneda, this.filtroForm.value.orden).subscribe(data => {
        this.listarReporte = data;
       
        if (this.listarReporte.length > 0) {
          nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';
          for (const item of this.listarReporte ){

            texto +=  item.c14  + '|' + item.c15  + '|' + item.c10  + '|' + item.c11  + '|' + item.c12  + '|' +
                      parseFloat(item.c20).toFixed(2)  + '|' + item.c8  + '|' + item.c9  + '|' + item.c31  + '|' + item.c30 + '|' +
                     '|' + '\r\n';
          }                
        }  
        else
        {
          nombrearchivo = 'PLA' + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + '20553547581';
          texto = '';
        }  
        this.textoService.downloadFile(texto, nombrearchivo);  
      }); 
    } 

  }

  exportarPDF(): void
  {
    
    
    // Registro Compras
    if (this.id === 100)
    {     
      
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.orden = this.filtroForm.value.orden;

      this.reporteUtil.donwloadPdf('DHAGEZ_RegistroCompras', this.filtroReporte);

    }    

    // Registro Ventas
    if (this.id === 101)
    {     
      
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.orden = this.filtroForm.value.orden;

      this.reporteUtil.donwloadPdf('DHAGEZ_RegistroVentas', this.filtroReporte);

    } 

    // Libro Diario
    if (this.id === 104)
    {           
      this.url = `${HOST}/api/librodiario/` + this.valoresiniciales.empresa + `/` + this.valoresiniciales.annoproceso + `/` + this.filtroForm.value.mes + `/` +
                this.filtroForm.value.subdiarioini + `/` + this.filtroForm.value.subdiariofin + `/` + this.valoresiniciales.ruc + `/` +
                this.valoresiniciales.nombreempresa + `/` + this.filtroForm.value.moneda + `/` + this.filtroForm.value.glosa;
      
      this.reporteUtil.exportarPdf(this.url).subscribe(response=>{
        var url = window.URL.createObjectURL(response.body);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.href = url;
        a.download = "LibroDiario" + this.valoresiniciales.ruc + this.valoresiniciales.annoproceso + this.filtroForm.value.mes + ".pdf";
        a.click();
        });
    } 

    // Libro Diario Simplificado
    if (this.id === 105)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.subini = this.filtroForm.value.subdiarioini;
      this.filtroReporte.subfin = this.filtroForm.value.subdiariofin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.categoria = this.filtroForm.value.categoria;
      
      this.reporteUtil.donwloadPdf('DHAGEZ_LibroDiarioSimplificado', this.filtroReporte);
    } 

    // Libro Mayor
    if (this.id === 106)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosa;
      this.reporteUtil.donwloadPdf('DHAGEZ_LibroMayor', this.filtroReporte);
    }

    // Libro Mayor Simplificado
    if (this.id === 107)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosa;
      this.reporteUtil.donwloadPdf('DHAGEZ_LibroMayorBiMoneda', this.filtroReporte);
    }

    // Libro Caja y Banco
    if (this.id === 108)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.glosa = this.filtroForm.value.glosacaja;
      this.reporteUtil.donwloadPdf('DHAGEZ_CajaBanco', this.filtroReporte);
    }

    // Libro Balance de Comprobación
    if (this.id === 109)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.donwloadPdf('DHAGEZ_BalanceComprobacion', this.filtroReporte);
    }

    // Saldo por Cuenta - Entidad - Documento
    if (this.id === 301)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.filtroReporte.ctacodini = this.filtroForm.value.ctacodini;
      this.filtroReporte.ctacodfin = this.filtroForm.value.ctacodfin;
      this.filtroReporte.tiposaldo = this.filtroForm.value.estadodoc;      
      this.filtroReporte.date = this.filtroForm.value.fecha.substr(8, 2) + '/'  +
                                this.filtroForm.value.fecha.substr(5, 2) + '/'  +
                                this.filtroForm.value.fecha.substr(0, 4);
      // console.log(this.filtroReporte.date);
      this.reporteUtil.donwloadPdf('DHAGEZ_SaldoCuentaEntidadDocumento', this.filtroReporte);
    }


    // Estado de situación financiera
    if (this.id === 401)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.donwloadPdf('DHAGEZ_EstadoSituacion', this.filtroReporte);
    }

    // Estado de situación financiera vertical
    if (this.id === 402)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.donwloadPdf('DHAGEZ_EstadoSituacionVertical', this.filtroReporte);
    }

    // Estado de resultado por naturaleza
    if (this.id === 403)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.donwloadPdf('DHAGEZ_EstadoResultadoNaturaleza', this.filtroReporte);
    }

    // Estado de resultado por función
    if (this.id === 404)
    {           
      this.filtroReporte.empresa = this.valoresiniciales.empresa;
      this.filtroReporte.annoproceso = this.valoresiniciales.annoproceso;
      this.filtroReporte.periodo = this.filtroForm.value.mes;
      this.filtroReporte.ruc = this.valoresiniciales.ruc;
      this.filtroReporte.nombreempresa = this.valoresiniciales.nombreempresa;
      this.filtroReporte.moneda = this.filtroForm.value.moneda;
      this.reporteUtil.donwloadPdf('DHAGEZ_EstadoResultadoFuncion', this.filtroReporte);
    }

  }
 
}

