import { Component, OnInit, ViewEncapsulation, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { VALORES_INICIALES, CATALOGO } from 'app/_shared/var.constant';
import { OperacionContableService } from 'app/services/operacioncontable.service';
import { CreateRegistroContableDto } from 'app/dto/create.registrocontable.dto';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'gez-saldobox',
  templateUrl: './saldobox.component.html',
  styleUrls: ['./saldobox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class SaldoboxComponent implements OnInit {

  @ViewChild('buscar') buscarInputField: MatInput;
  displayedColumns = ['fecha', 'subdiario', 'operacion', 'tipodocumento', 'serie', 'numero', 'moneda', 'saldoorigen', 'saldonacional', 'saldodolares', 'glosa'];
  valoresiniciales;
  valorcatalogo;
  registrocontable;
  resultado;
  
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }
  
  constructor(
    public matDialogRef: MatDialogRef<SaldoboxComponent>,
    private operacioncontableService: OperacionContableService,
    private paginatorlabel: MatPaginatorIntl,
  ) 
  {
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void {


    this.setSettingTableCourse();

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.valorcatalogo    = JSON.parse(localStorage.getItem(CATALOGO));
    this.resultado = '';
   
    this.buscarSaldo();  

  }
  
  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;       
  }  

  buscarSaldo(): void
  {
    this.resultado = 'Cargando ... ';
    
    this.registrocontable = new CreateRegistroContableDto();
    this.registrocontable.empresa              = this.valoresiniciales.empresa;
    this.registrocontable.ejerciciocontable    = this.valoresiniciales.annoproceso;
    this.registrocontable.cuentacontable       = this.valorcatalogo.cuentacontable;
    this.registrocontable.entidad              = this.valorcatalogo.entidad;
    this.registrocontable.tipodocumento        = this.valorcatalogo.tipodocumento;
    this.registrocontable.numeroserie          = this.valorcatalogo.numeroserie;
    this.registrocontable.numerocomprobanteini = this.valorcatalogo.numerocomprobanteini;
    this.registrocontable.cuentacontable       = this.valorcatalogo.cuentacontable;

    // this.registrocontable.cuentacontable       = '421211';
    // this.registrocontable.entidad              = '00000031';
    // this.registrocontable.tipodocumento        = '01';
    // this.registrocontable.numeroserie          = '001';
    // this.registrocontable.numerocomprobanteini = '000081';
    // console.log(this.registrocontable);
  
    this.operacioncontableService.listarSaldo(this.registrocontable).subscribe(data => {
        this.dataSource.data = data;   

        setTimeout(() => {
          if ( this.dataSource.data.length === 0)
          {
            this.resultado = 'Resultado de la búsqueda, 0 registros... ';
          }
          else
          {
            this.resultado = '';
          }
        });
    });  

  }


  seleccionarRegistro(numeroserie: string, numerocomprobanteini: string, montoorigen: string, montonacional: string, montodolares: string): void
  {
    this.valorcatalogo = {  cuentacontable: '',
                            entidad:  '',
                            tipodocumento: '',
                            numeroserie: numeroserie,
                            numerocomprobanteini: numerocomprobanteini,
                            montoorigen: montoorigen,
                            montonacional: montonacional,
                            montodolares: montodolares,
                            estado: 'S'};                              

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));

    this.matDialogRef.close();
    
  }
}
