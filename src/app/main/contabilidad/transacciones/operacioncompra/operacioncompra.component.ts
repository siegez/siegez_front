import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { OperacionContableService } from 'app/services/operacioncontable.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ExcelService } from 'app/services/excel.service';
import { VALORES_INICIALES, DATOS, MESSAGEBOX } from 'app/_shared/var.constant';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { CreateOperacionContableDto } from 'app/dto/create.operacioncontable.dto';

@Component({
  selector: 'gez-operacioncompra',
  templateUrl: './operacioncompra.component.html',
  styleUrls: ['./operacioncompra.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class OperacioncompraComponent implements OnInit {
  dialogRef: any;
  operacioncontable;
  valoresiniciales;
  resultado;

  displayedColumns = ['operacioncontable', 'fechaoperacion', 'descripcionestado', 'ruc', 'nombreentidad', 
                      'descripciontipodocumento', 'numeroserie', 'numerocomprobanteini', 'descripcionsubdiario', 'editar'];

  listarReporte: any = [];

  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private operacioncontableService: OperacionContableService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService    
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {
    this.operacioncontable = new CreateOperacionContableDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES)); 
    this.resultado = '';

    this.obtenerVista();  

    this.setSettingTableCourse();  
    
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.operacioncontableService.listarvista(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, 'C').toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  }    

  public doFilter = (value: string) => 
  {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() 
  {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  

  crearOperacionContable(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`contabilidad/transacciones/operacioncompraform`]);
  }  

  editarOperacionContable(operacioncontable: string): void
  {
    const misDatos =  { 
                        action: 'editar',
                        parametro1: operacioncontable
                      };

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
   
    this.router.navigate([`contabilidad/transacciones/operacioncompraform`]);
    
  }

  eliminarOperacionContable(operacioncontable: string): void
  {
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Operación Contable: ' + operacioncontable,
          subTitulo: '¿Está seguro de eliminar la operación contable?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });
    
    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.operacioncontable.empresa            = this.valoresiniciales.empresa;
          this.operacioncontable.ejerciciocontable  = this.valoresiniciales.annoproceso;
          this.operacioncontable.operacioncontable  = operacioncontable;
    
          this.operacioncontableService.eliminar(this.operacioncontable).subscribe(data => {
          this._matSnackBar.open('Se eliminó la operación contable', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
    
          this.ngOnInit();
    
          }, err => {
              this._matSnackBar.open('Error, operación contable no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
    
      }        
      );    
  }

  exportarOperacionContable(): void
  {
    this.operacioncontableService.exportar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, 'C').subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Ejercicio Contable'] = this.valoresiniciales.annoproceso;
          objectReport['Código Período Contable'] = item.periodocontable;
          objectReport['Período Contable'] = item.descripcionperiodocontable;
          objectReport['Operación Contable'] = item.operacioncontable;                    
          objectReport['Código Tipo Operación'] = item.tipooperacion;
          objectReport['Tipo Operación'] = item.descripciontipooperacion;
          objectReport['Fecha Operación'] = item.fechaoperacion;
          objectReport['Fecha Emisión'] = item.fechaemision;
          objectReport['Fecha Vencimiento'] = item.fechavencimiento;
          objectReport['Código Proveedor'] = item.entidad; // cambiar
          objectReport['Nombre Proveedor'] = item.nombreentidad; // cambiar
          objectReport['Documento Identidad'] = item.ruc;
          // objectReport['Codigo Banco'] = item.banco;
          // objectReport['Banco'] = item.nombrebanco;
          // objectReport['Cuenta Bancaria'] = item.cuentabancaria;
          objectReport['Código Tipo Documento'] = item.tipodocumento;
          objectReport['Tipo Documento'] = item.descripciontipodocumento;
          objectReport['Código SubDiario'] = item.subdiario;
          objectReport['SubDiario'] = item.descripcionsubdiario;
          // objectReport['Código Aduana'] = item.aduana;
          // objectReport['Nombre Aduana'] = item.descripcionaduana;
          objectReport['Número Serie'] = item.numeroserie;
          objectReport['Número Comprobante Inicio'] = item.numerocomprobanteini;
          objectReport['Número Comprobante Fin'] = item.numerocomprobantefin;
          objectReport['Código Moneda'] = item.moneda;
          objectReport['Moneda'] = item.descripcionmoneda;
          objectReport['Tipo Cambio'] = item.tipocambio;
          objectReport['Descripción Tipo Cambio'] = item.descripciontipocambio;
          objectReport['Tipo Cambio a Soles'] = item.tipocambiosoles;
          objectReport['Tipo Cambio a Dólares'] = item.tipocambiodolares;
          objectReport['Base Imponible'] = item.baseimponible;
          objectReport['Inafecto'] = item.inafecto;
          // objectReport['Otros Conceptos Adicionales'] = item.adicionalnodom;
          // objectReport['Impuesto Renta'] = item.indimpuestorenta;
          objectReport['Porcentaje Impuesto'] = item.porcentajeimpuesto;
          objectReport['Impuesto'] = item.impuesto;
          // objectReport['Deducción Costo Enajen BS de Capital'] = item.deduccionnodom;
          objectReport['Precio Moneda Origen'] = item.precioorigen;
          objectReport['Precio Moneda Soles'] = item.preciosoles;
          objectReport['Precio Moneda Dólares'] = item.preciodolares;
          objectReport['Código Medio Pago'] = item.mediopago;
          objectReport['Medio Pago'] = item.descripcionmediopago;
          objectReport['Código Flujo Efectivo'] = item.flujoefectivo;
          objectReport['Flujo Efectivo'] = item.descripcionflujoefectivo;
          objectReport['Código BS'] = item.bienservicio;
          objectReport['Bien o Servicio'] = item.descripcionbienservicio;
          objectReport['Código Detracción'] = item.detraccion;
          objectReport['Detracción'] = item.descripciondetraccion;
          objectReport['Tasa Detracción'] = item.tasadetraccion;
          objectReport['Número Detracción'] = item.numerodetraccion;
          objectReport['Fecha Detracción'] = item.fechadetraccion;
          objectReport['Base Detracción'] = item.basedetraccion;
          objectReport['Monto Detracción'] = item.montodetraccion;
          objectReport['Código Tipo Documento Referencia'] = item.tipodocumentoref;
          objectReport['Tipo Documento Referencia'] = item.descripciontipodocumentoref;
          objectReport['Ejercicio Contable Referencia'] = item.ejerciciocontableref;
          objectReport['Operación Contable Referencia'] = item.operacioncontableref;
          objectReport['Número Serie Referencia'] = item.numeroserieref;
          objectReport['Número Comprobante Referencia'] = item.numerocomprobanteiniref;
          objectReport['Fecha Emisión Referencia'] = item.fechaemisionref;
          objectReport['Base Imponible Referencia'] = item.baseimponibleref;
          objectReport['Inafecto Referencia'] = item.inafectoref;
          objectReport['Impuesto Referencia'] = item.impuestoref;
          objectReport['Precio Moneda Origen Referencia'] = item.precioorigenref;
          // objectReport['Caja Girado'] = item.girado;
          objectReport['Glosa'] = item.glosa;
          // objectReport['Código Tipo Documento No Domiciliado'] = item.tipodocumentonodom;
          // objectReport['Tipo Documento No Domiciliado'] = item.descripciontipodocumentonodom;
          // objectReport['Serie No Domiciliado'] = item.numeroserienodom;
          // objectReport['Año Documento No Domiciliado'] = item.annocomprobantenodom;
          // objectReport['Número Comprobante No Domiciliado'] = item.numerocomprobantenodom;
          // objectReport['Código Proveedor No Domiciliado'] = item.entidadnodom;
          // objectReport['Proveedor No Domiciliado'] = item.nombreentidadnodom;
          // objectReport['Código Vínculo de Contribuyente No Domiciliado'] = item.vinculoeconomiconodom;
          // objectReport['Vínculo de Contribuyente No Domiciliado'] = item.descripcionvinculoeconomiconodom;
          // objectReport['Tasa Detracción No Domiciliado'] = item.tasaretencionnodom;
          // objectReport['Impuesto No Domiciliado'] = item.impuestoretenidonodom;
          // objectReport['Código Convenio No Domiciliado'] = item.convenionodom;
          // objectReport['Convenio No Domiciliado'] = item.descripcionconvenionodom;
          // objectReport['Código Exoneración No Domiciliado'] = item.exoneracionnodom;
          // objectReport['Exoneración No Domiciliado'] = item.descripcionexoneracionnodom;
          // objectReport['Código Renta No Domiciliado'] = item.tiporentanodom;
          // objectReport['Renta No Domiciliado'] = item.descripciontiporentanodom;
          // objectReport['Código Modalidad Servicio No Domiciliado'] = item.modalidadnodom;
          // objectReport['Modalidad Servicio No Domiciliado'] = item.descripcionmodalidadnodom;
          // objectReport['Columna Base Imponible'] = item.columnabaseimponible;
          // objectReport['Columna Impuesto'] = item.columnaimpuesto;
          objectReport['Código Estado'] = item.estado;
          objectReport['Estado'] = item.descripcionestado;
          objectReport['Usuario Creación'] = item.creacionUsuario;
          objectReport['Fecha Creación'] = item.creacionFecha;
          objectReport['Usuario Modificación'] = item.modificacionUsuario;
          objectReport['Fecha Modificación'] = item.modificacionFecha;

          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'OperacionContableCompra');

      }

    }); 
  }

}
