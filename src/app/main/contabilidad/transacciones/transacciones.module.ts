import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { OperacioncompraComponent } from './operacioncompra/operacioncompra.component';
import { OperacioncompraFormComponent } from './operacioncompra/operacioncompra-form/operacioncompra-form.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DecimalNumberDirectiveTwo } from 'app/directive/valid-decimal-directive-Two';
import { DecimalNumberDirectiveThree } from 'app/directive/valid-decimal-directive-three';
import { RegistrocontableFormComponent } from './registrocontable-form/registrocontable-form.component';
import { SaldoboxComponent } from './saldobox/saldobox.component';
import { OperacionventaComponent } from './operacionventa/operacionventa.component';
import { OperacionventaFormComponent } from './operacionventa/operacionventa-form/operacionventa-form.component';
import { OperacionhonorarioComponent } from './operacionhonorario/operacionhonorario.component';
import { OperacionhonorarioFormComponent } from './operacionhonorario/operacionhonorario-form/operacionhonorario-form.component';
import { OperacionnodomiciliadoComponent } from './operacionnodomiciliado/operacionnodomiciliado.component';
import { OperacionnodomiciliadoFormComponent } from './operacionnodomiciliado/operacionnodomiciliado-form/operacionnodomiciliado-form.component';
import { OperacionduaComponent } from './operaciondua/operaciondua.component';
import { OperacionduaFormComponent } from './operaciondua/operaciondua-form/operaciondua-form.component';
import { OperacioningresoComponent } from './operacioningreso/operacioningreso.component';
import { OperacionegresoComponent } from './operacionegreso/operacionegreso.component';
import { OperaciondiarioComponent } from './operaciondiario/operaciondiario.component';
import { OperaciondiarioFormComponent } from './operaciondiario/operaciondiario-form/operaciondiario-form.component';
import { OperacionegresoFormComponent } from './operacionegreso/operacionegreso-form/operacionegreso-form.component';
import { OperacioningresoFormComponent } from './operacioningreso/operacioningreso-form/operacioningreso-form.component';



const routes: Routes = [
     {
        path     : 'operacioncompra',
        component: OperacioncompraComponent,
     },
     {
        path     : 'operacioncompraform',
        component: OperacioncompraFormComponent,
     },
     {
      path     : 'operaciondua',
      component: OperacionduaComponent,
      },
      {
         path     : 'operacionduaform',
         component: OperacionduaFormComponent,
      },
     {
        path     : 'operacionventa',
        component: OperacionventaComponent,
     },
     {
        path     : 'operacionventaform',
        component: OperacionventaFormComponent,
     },
     {
        path     : 'operacionhonorario',
        component: OperacionhonorarioComponent,
     },
     {
        path     : 'operacionhonorarioform',
        component: OperacionhonorarioFormComponent,
     },
     {
        path     : 'operacionnodomiciliado',
        component: OperacionnodomiciliadoComponent,
     },
     {
        path     : 'operacionnodomiciliadoform',
        component: OperacionnodomiciliadoFormComponent,
     },
     {
        path     : 'operaciondiario',
        component: OperaciondiarioComponent,
     },
     {
        path     : 'operaciondiarioform',
        component: OperaciondiarioFormComponent,
     },
     {
        path     : 'operacionegreso',
        component: OperacionegresoComponent,
     },
     {
        path     : 'operacionegresoform',
        component: OperacionegresoFormComponent,
     },
     {
        path     : 'operacioningreso',
        component: OperacioningresoComponent,
     },
     {
        path     : 'operacioningresoform',
        component: OperacioningresoFormComponent,
     },
     {
        path     : 'operaciondiario',
        component: OperaciondiarioComponent,
     },
     {
        path     : 'operaciondiarioform',
        component: OperaciondiarioFormComponent,
     }
];

@NgModule({
    declarations: [
        OperacioncompraComponent,
        OperacioncompraFormComponent,
        DecimalNumberDirectiveTwo,
        DecimalNumberDirectiveThree,
        RegistrocontableFormComponent,
        SaldoboxComponent,
        OperacionventaComponent,
        OperacionventaFormComponent,
        OperacionhonorarioComponent,
        OperacionhonorarioFormComponent,
        OperacionnodomiciliadoComponent,
        OperacionnodomiciliadoFormComponent,
        OperacionduaComponent,
        OperacionduaFormComponent,
        OperacioningresoComponent,
        OperacionegresoComponent,
        OperaciondiarioComponent,
        OperaciondiarioFormComponent,
        OperacionegresoFormComponent,
        OperacioningresoFormComponent 
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatSlideToggleModule,
        MatDatepickerModule,
         
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],

    exports: [
        DecimalNumberDirectiveTwo,
        DecimalNumberDirectiveThree
    ],

    providers   : [
        
    ]
})
export class TransaccionesModule
{
}
