import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { REGISTROCONTABLE, REFRESH, CATALOGO } from 'app/_shared/var.constant';
import { PlanContableService } from 'app/services/plancontable.service';
import { EnteService } from 'app/services/ente.service';
import { ConsultaService } from 'app/services/consulta.service';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { TipoCambioService } from 'app/services/tipocambio.service';
import { CreateParametroDto } from 'app/dto/create.parametro.dto';
import { ParametroService } from 'app/services/parametro.service';
import { CreateConsultaStringDto } from 'app/dto/create.consultastring.dto';
import { CreateTipoCambioDto } from 'app/dto/create.tipocambio.dto';
import { CreateEnteDto } from 'app/dto/create.ente.dto';
import { SaldoboxComponent } from '../saldobox/saldobox.component';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'gez-registrocontable-form',
  templateUrl: './registrocontable-form.component.html',
  styleUrls: ['./registrocontable-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})

export class RegistrocontableFormComponent implements OnInit, AfterContentChecked
{
  /*
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  valoresiniciales;
  valorcontador;
  checkEstado = false;
  */
  accion: string;
  tipooperacion: string;
  monedanacional: string;
  monedadolares: string;
  registrocontable;
  valorcampo;
  valorcatalogo;
  parametro;
  tipocambio;
  consultastring;
  ente;
  viewmediopago = false;
  viewcentrocosto = false;
  viewgrupocentrocosto = false;
  viewflujoefectivo = false;
  viewsaldo = false;
  viewentidad = false;
  entidad = 'Entidad';
  registrocontableForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  readonly = true;
  requiredDoc = false;
  mensaje = '';

  // Parámetros
  cuentacontable10 = '';

  // Listas

  listaCuentaContable: any[];
  listaEntidad: any[];
  listaTipoAnotacion: any[];
  listaCentroCosto: any[];
  listaTipoDocumento: any[];
  listaTipoCambio: any[];
  listaMedioPago: any[];
  listaFlujoEfectivo: any[];

  checkIndicadorCalculo = true;
  editTC = true;

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<RegistrocontableFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private plancontableService: PlanContableService,
    private enteService: EnteService,
    private consultaService: ConsultaService,
    private tipodocumentoService: TipoDocumentoService,
    private parametroService: ParametroService,
    private tipocambioService: TipoCambioService,
    private _matDialog: MatDialog,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion;
    this.tipooperacion = _data.tipooperacion;
    this.monedanacional = _data.monedanacional;
    this.monedadolares = _data.monedadolares;    
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.parametro = new CreateParametroDto();
    this.consultastring = new CreateConsultaStringDto();
    this.tipocambio = new CreateTipoCambioDto();

    this.registrocontable = JSON.parse(localStorage.getItem(REGISTROCONTABLE));    
    
    this.checkIndicadorCalculo = (this.registrocontable.indicadorcalculo === 'S' ) ? true : false;      
    this.editTC                = (this.registrocontable.tipocambio !== 'M' ) ? true : false;  
    
    if ( this.accion === 'editar' )
    {
        this.dialogTitle = 'Editar Registro Contable';  
     
    }
    else
    {
        this.dialogTitle = 'Nuevo Registro Contable';
    }
    
    if ( this.registrocontable.tiporegistro !== 'A' )
    {
      this.readonly = false;
    }

    this.registrocontableForm = this.crearRegistroContableForm();

    // Obtener parámetro caja    
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'CAJA') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Caja', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.cuentacontable10 = this.parametro.valor;
    
    this.modificarCuentaContable();
    /*
    this.mostrarMedioPago();
    this.mostrarCentroCosto();
    this.mostrarFlujoEfectivo();
    this.mostrarEntidad();
    */
    this.listarCuentaContable(); 
    // this.listarEntidad(); 
    this.listarTipoAnotacion();
    this.listarCentroCosto();
    this.listarTipoDocumento();
    this.listarTipoCambio();
    this.listarMedioPago();
    this.listarFlujoEfectivo();
    this.mostrarSaldo();

    this.checkIndicadorCalculo = (this.registrocontable.indicadorcalculo === 'S' ) ? true : false;      
    this.editTC                = (this.registrocontable.tipocambio !== 'M' ) ? true : false;  
 
  }

  crearRegistroContableForm(): FormGroup
  {
      return this._formBuilder.group({
          cuentacontable                  : [this.registrocontable.cuentacontable],
          descripcioncuentacontable       : [this.registrocontable.descripcioncuentacontable],
          entidad                         : [this.registrocontable.entidad],
          nombreentidad                   : [this.registrocontable.nombreentidad],
          tipoanotacion                   : [this.registrocontable.tipoanotacion],
          descripciontipoanotacion        : [this.registrocontable.descripciontipoanotacion],
          centrocosto                     : [this.registrocontable.centrocosto],
          descripcioncentrocosto          : [this.registrocontable.descripcioncentrocosto],
          porcentajecentrocosto           : [this.registrocontable.porcentajecentrocosto],
          tipodocumento                   : [this.registrocontable.tipodocumento],    
          descripciontipodocumento        : [this.registrocontable.descripciontipodocumento],
          numeroserie                     : [this.registrocontable.numeroserie],
          numerocomprobanteini            : [this.registrocontable.numerocomprobanteini],
          numerocomprobantefin            : [this.registrocontable.numerocomprobantefin],
          fechaemision                    : [this.registrocontable.fechaemision],
          fechavencimiento                : [this.registrocontable.fechavencimiento],
          indicadorcalculo                : [this.checkIndicadorCalculo],
          moneda                          : [this.registrocontable.moneda],
          tipocambio                      : [this.registrocontable.tipocambio],
          descripciontipocambio           : [this.registrocontable.descripciontipocambio],
          tipocambiosoles                 : [this.registrocontable.tipocambiosoles],
          tipocambiodolares               : [this.registrocontable.tipocambiodolares],
          montoorigen                     : [this.registrocontable.montoorigen],
          montonacional                   : [this.registrocontable.montonacional],
          montodolares                    : [this.registrocontable.montodolares],
          mediopago                       : [this.registrocontable.mediopago],
          descripcionmediopago            : [this.registrocontable.descripcionmediopago],
          flujoefectivo                   : [this.registrocontable.flujoefectivo],
          descripcionflujoefectivo        : [this.registrocontable.descripcionflujoefectivo],
          glosa                           : [this.registrocontable.glosa]    
      });
  }

  // tslint:disable-next-line:typedef
  editarRegistroContable(registrocontableForm){

    // Validar campos obligatorios
    this.mensaje = '';

    if (this.registrocontableForm.value.cuentacontable <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Cuenta Contable, ';
    }
 
    if (this.registrocontableForm.value.entidad === '' && this.viewentidad) 
    {
      this.mensaje = this.mensaje + this.entidad + ', ';
    }

    if (this.registrocontableForm.value.centrocosto === '' && this.viewcentrocosto) 
    {
      this.mensaje = this.mensaje + 'Centro de Costo, ';
    }
    
    if (this.registrocontableForm.value.numerocomprobanteini === '' && this.requiredDoc) 
    {
      this.mensaje = this.mensaje + 'Número de Comprobante, ';
    }

    if (this.registrocontableForm.value.tipocambiosoles <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Tipo de cambio soles, ';
    }

    if (this.registrocontableForm.value.tipocambiodolares <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Tipo de cambio dólares, ';
    }

    if (this.registrocontableForm.value.montoorigen <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Monto Origen, ';
    }

    if (this.registrocontableForm.value.montonacional <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Monto Nacional, ';
    }

    if (this.registrocontableForm.value.montodolares <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Monto Dólares, ';
    }

    if (this.registrocontableForm.value.mediopago === '' && this.viewmediopago) 
    {
      this.mensaje = this.mensaje + 'Medio de Pago, ';
    }

    if (this.mensaje !== '')
    {      
      this.mensaje = this.mensaje.slice(0, -2);

      this._matSnackBar.open('Falta ingresar: ' + this.mensaje + '. Ingrese los campos correctamente', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
      
      return;
    }
    else
    {
      // this.registrocontable.empresa                 = registrocontableForm.value.empresa;
      // this.registrocontable.ejerciciocontable       = registrocontableForm.value.ejerciciocontable;
      // this.registrocontable.operacioncontable       = registrocontableForm.value.operacioncontable;
      // this.registrocontable.asientocontable         = registrocontableForm.value.asientocontable;
      // this.registrocontable.registrocontable        = registrocontableForm.value.registrocontable;

      this.registrocontable.cuentacontable          = registrocontableForm.value.cuentacontable;    
      this.registrocontable.tipoanotacion           = registrocontableForm.value.tipoanotacion;
      // this.registrocontable.tiporegistro            = registrocontableForm.value.tiporegistro;
      this.registrocontable.centrocosto             = registrocontableForm.value.centrocosto;    
      this.registrocontable.porcentajecentrocosto   = registrocontableForm.value.porcentajecentrocosto;
      this.registrocontable.entidad                 = registrocontableForm.value.entidad;    
      this.registrocontable.tipodocumento           = registrocontableForm.value.tipodocumento;    
      this.registrocontable.numeroserie             = registrocontableForm.value.numeroserie;
      this.registrocontable.numerocomprobanteini    = registrocontableForm.value.numerocomprobanteini;
      this.registrocontable.numerocomprobantefin    = registrocontableForm.value.numerocomprobantefin;
      this.registrocontable.fechaemision            = registrocontableForm.value.fechaemision;
      this.registrocontable.fechavencimiento        = registrocontableForm.value.fechavencimiento;
      this.registrocontable.indicadorcalculo        = (this.registrocontableForm.value.indicadorcalculo) ? 'S' : 'N';
      // this.registrocontable.moneda                  = registrocontableForm.value.moneda;
      this.registrocontable.tipocambio              = registrocontableForm.value.tipocambio;
      this.registrocontable.tipocambiosoles         = registrocontableForm.value.tipocambiosoles;
      this.registrocontable.tipocambiodolares       = registrocontableForm.value.tipocambiodolares;
      this.registrocontable.mediopago               = registrocontableForm.value.mediopago;
      this.registrocontable.flujoefectivo           = registrocontableForm.value.flujoefectivo;
      this.registrocontable.glosa                   = registrocontableForm.value.glosa;
      this.registrocontable.montoorigen             = registrocontableForm.value.montoorigen;
      this.registrocontable.montonacional           = registrocontableForm.value.montonacional;
      this.registrocontable.montodolares            = registrocontableForm.value.montodolares;
  
      localStorage.setItem(REGISTROCONTABLE, JSON.stringify(this.registrocontable));
      localStorage.setItem(REFRESH, 'S');
  
      this.matDialogRef.close();
    }
  }    

  listarCuentaContable(): void
  {
    this.plancontableService.listarCatalogoSimple(this.registrocontable.empresa, this.registrocontable.ejerciciocontable).subscribe(data => {
      this.listaCuentaContable = data;
    });
  }

  // tslint:disable-next-line:typedef
  async listarEntidad(tipoentidad: string)
  {
    this.listaEntidad = [];
    // Proveedor
    if (tipoentidad === '02' || tipoentidad === '05')
    {
      if (this.tipooperacion === 'C' || this.tipooperacion === 'D' || this.tipooperacion === 'H')
      {
        this.enteService.listarProveedor(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }
      if (this.tipooperacion === 'N')
      {
        this.enteService.listarProveedorNoDom(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }
      if (this.tipooperacion === 'E' || this.tipooperacion === 'I' || this.tipooperacion === 'A')
      {
        this.enteService.listarSocioNegocio(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }
    }

    // Cliente
    if (tipoentidad === '03' || tipoentidad === '06')
    {
      if (this.tipooperacion === 'E' || this.tipooperacion === 'I' || this.tipooperacion === 'A')
      {
        this.enteService.listarSocioNegocio(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }
      else
      {
        this.enteService.listarCliente(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }      
    }

    // Entidad Financiera
    if (tipoentidad === '04')
    {
      if (this.tipooperacion === 'E' || this.tipooperacion === 'I' || this.tipooperacion === 'A')
      {
        this.enteService.listarSocioNegocio(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }
      else
      {
        this.enteService.listarBanco(this.registrocontable.empresa).subscribe(data => {
          this.listaEntidad = data;
        });
      }      
    }

    // Otros
    if (tipoentidad === '07' || tipoentidad === '08' || tipoentidad === '09')
    {
      this.enteService.listarSocioNegocio(this.registrocontable.empresa).subscribe(data => {
        this.listaEntidad = data;
      });      
    }

    // vacio
    if (tipoentidad === '')
    {
      this.enteService.listarSocioNegocio(9999).subscribe(data => {
        this.listaEntidad = data;
      });      
    }
  
    /*
    IDSOCIONEGOCIO	DESCRIPCION
    02	Proveedor
    03	Cliente
    04	Entidad Financiera
    05	Proveedor Relacionado
    06	Cliente Relacionado
    07	Socio
    08	Trabajador
    09	Varios
    */
  }

  listarTipoAnotacion(): void
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.registrocontable.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDTIPOANOTACION';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaTipoAnotacion = data;
     });
  }

  listarCentroCosto(): void
  {
    this.consultaService.listarCentroCosto(this.registrocontable.empresa, this.registrocontable.ejerciciocontable).subscribe(data => {
      this.listaCentroCosto = data;
    });
  }

  
  listarTipoDocumento(): void
  {
    if (this.tipooperacion === 'C')
    {
      this.tipodocumentoService.listarCatalogoCompras(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'D')
    {
      this.tipodocumentoService.listarCatalogoImportaciones(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'H')
    {
      this.tipodocumentoService.listarCatalogoHonorarios(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'V')
    {
      this.tipodocumentoService.listarCatalogoVentas(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'N')
    {
      this.tipodocumentoService.listarCatalogoNoDomiciliados(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'E')
    {
      this.tipodocumentoService.listarCatalogoEgresos(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'I')
    {
      this.tipodocumentoService.listarCatalogoIngresos(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }
    if (this.tipooperacion === 'A')
    {
      this.tipodocumentoService.listarCatalogoDiarios(this.registrocontable.empresa).subscribe(data => {
        this.listaTipoDocumento = data;
      });
    }


  }

  listarTipoCambio(): void
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.registrocontable.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOCAMBIO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaTipoCambio = data;
    });
  }

  listarMedioPago(): void
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.registrocontable.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDMEDIOPAGO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaMedioPago = data;
    });
  }

  listarFlujoEfectivo(): void
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.registrocontable.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDFLUJOEFECTIVO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaFlujoEfectivo = data;
    });
  }  

  mostrarCentroCosto(indicador: string): void
  {
    if (indicador === 'S')
    {
      this.viewcentrocosto = true;
    }
    else
    {
      this.registrocontableForm.controls['centrocosto'].setValue('');
      this.registrocontableForm.controls['porcentajecentrocosto'].setValue('0');
      this.viewcentrocosto = false;   
    }
    
  }

  mostrarMedioPago(): void
  {
    if (this.registrocontableForm.value.cuentacontable.substr(0, 2) === this.cuentacontable10)
    {
      this.viewmediopago = true;
    }
    else
    {
      this.registrocontableForm.controls['mediopago'].setValue('');
      this.viewmediopago = false;   
    }
    
  }

  mostrarFlujoEfectivo(): void
  {
    if (this.registrocontableForm.value.cuentacontable.substr(0, 2) === this.cuentacontable10)
    {
      this.viewflujoefectivo = true;
    }
    else
    {
      this.registrocontableForm.controls['flujoefectivo'].setValue('');
      this.viewflujoefectivo = false;   
    }
  }

  mostrarEntidad(indicador: string): void
  {
    
    if (indicador !== null && indicador.replace(/\s+$/g, '') !== '')    
    {      
      this.viewentidad = true;
      // this.registrocontableForm.controls['entidad'].setValue('');
      this.registrocontableForm.controls['entidad'].setValue(this.registrocontable.entidad);
    }
    else
    {
      this.viewentidad = false; 
      this.registrocontableForm.controls['entidad'].setValue('');        
    }

    console.log(this.registrocontableForm.value.entidad);
  }

  // tslint:disable-next-line:typedef
  async modificarCuentaContable()
  {
    
    try {
      // tslint:disable-next-line:max-line-length this.parametro = new CreateParametroDto();
      this.consultastring = (await this.plancontableService.consultarCuentaContable(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontableForm.value.cuentacontable).toPromise());       
      
      if (this.tipooperacion === 'E' || this.tipooperacion === 'I' || this.tipooperacion === 'D')
      {
        this.entidad = 'Socio de Negocio';
      }
      else
      {
        this.entidad = this.consultastring.campo5;
      }
      // console.log(this.consultastring);

      this.requiredDoc = (this.consultastring.campo6 === 'S' ) ? true : false;
      
      this.listarEntidad(this.consultastring.campo7);
      this.mostrarCentroCosto(this.consultastring.campo2);
      this.mostrarMedioPago();
      this.mostrarFlujoEfectivo();
      // this.listarEntidad(this.consultastring.campo7).then(res => {
      //   this.mostrarEntidad(this.consultastring.campo3);  
      // }               
      // );
      this.mostrarEntidad(this.consultastring.campo3);

    } catch (error) {
      // this._matSnackBar.open('Error de Servicio, Plan Contable', 'ok', {
      //   verticalPosition: 'bottom',
      //   panelClass: ['my-snack-bar'] 
      //   });
    }      

    this.mostrarSaldo();   

  }

  async consultarParametro(modulo: string, parametro: string): Promise<CreateParametroDto> 
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.parametro = (await this.parametroService.consultar(this.registrocontable.empresa, modulo, parametro).toPromise()); 
      return this.parametro as CreateParametroDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Parámetros', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.parametro = new CreateParametroDto();
    }   

  }

  // tslint:disable-next-line:typedef
  async modificarFechaEmision()
  {     
    
    await this.modificarTipoCambio();
    
  }

  // tslint:disable-next-line:typedef
  async modificarCalculoAutomatico()
  {     

    await this.modificarTipoCambio();
    
  }

  // tslint:disable-next-line:typedef
  async procesarCalculoAutomatico()
  {     
    
    if ( this.registrocontableForm.value.indicadorcalculo === true )
    {
      try {
        // tslint:disable-next-line:max-line-length this.parametro = new CreateParametroDto();
        this.consultastring = (await this.plancontableService.consultarCuentaContable(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontableForm.value.cuentacontable).toPromise());       
        
        if (this.consultastring.campo4 === this.monedanacional) 
        {
          this.registrocontableForm.controls['montonacional'].setValue(this.registrocontableForm.value.montoorigen * this.registrocontableForm.value.tipocambiosoles);
          // tslint:disable-next-line:max-line-length
          this.registrocontableForm.controls['montodolares'].setValue((this.registrocontableForm.value.montonacional / this.registrocontableForm.value.tipocambiodolares).toFixed(3));
        }
        else
        {
          this.registrocontableForm.controls['montonacional'].setValue(this.registrocontableForm.value.montoorigen);
          // tslint:disable-next-line:max-line-length
          this.registrocontableForm.controls['montodolares'].setValue((this.registrocontableForm.value.montonacional / this.registrocontableForm.value.tipocambiodolares).toFixed(3));
        }  
      } catch (error) {
        this._matSnackBar.open('Error de Servicio, Plan Contable', 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
      }    

    }     
    
  }

  // tslint:disable-next-line:typedef
  async modificarTipoCambio()
  {           
    
    if ( this.registrocontableForm.value.tipocambio !== 'M' )
    {
      this.editTC = true; 
      if ( this.registrocontable.moneda === this.monedanacional) 
      {
        this.registrocontableForm.controls['tipocambiosoles'].setValue(1);
      }
      else
      {
        // Tipo de Cambio Origen a Soles
        // tslint:disable-next-line:max-line-length
        this.tipocambio = this.tipocambio ? await this.consultarTipoCambio(this.registrocontable.moneda) : new CreateTipoCambioDto();      
        if ( this.tipocambio.compra !== -1 )
        {
          if (this.registrocontableForm.value.tipocambio === 'C')
          {
            this.registrocontableForm.controls['tipocambiosoles'].setValue(this.tipocambio.tipocambiocompra);
          }
          else
          {
            this.registrocontableForm.controls['tipocambiosoles'].setValue(this.tipocambio.tipocambioventa);
          }        
        }
      }

      // Tipo de Cambio Dolares a Soles
      // console.log(this.monedadolares);
      // console.log(this.registrocontableForm.value.fechaemision);
      // this.tipocambio = this.tipocambio ? await this.consultarTipoCambio(this.monedadolares, this.registrocontableForm.value.fechaemision) : new CreateTipoCambioDto();    
      this.tipocambio = this.tipocambio ? await this.consultarTipoCambio(this.monedadolares) : new CreateTipoCambioDto();
      // console.log(this.tipocambio);

      if ( this.tipocambio.compra !== -1 )
      {
        if (this.registrocontableForm.value.tipocambio === 'C')
        {
          this.registrocontableForm.controls['tipocambiodolares'].setValue(this.tipocambio.tipocambiocompra);
        }
        else
        {
          this.registrocontableForm.controls['tipocambiodolares'].setValue(this.tipocambio.tipocambioventa);
        }        
      }

    }   
    else
    {
      this.editTC = false; 
    }

    await this.procesarCalculoAutomatico();

  }

  async consultarTipoCambio(moneda: string): Promise<CreateTipoCambioDto>
  {
    try 
    {      
      // tslint:disable-next-line:max-line-length
      this.tipocambio = (await this.tipocambioService.consultarDia(this.registrocontable.empresa, this.registrocontableForm.value.fechaemision, moneda, 'N' ).toPromise());
      
      if (this.tipocambio.tipocambiocompra === -1)
      {
        this._matSnackBar.open('Falta ingresar tipo de cambio', 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
      }

      return this.tipocambio;  
      
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Tipo de Cambio', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      console.log('malo');
      return this.tipocambio = new CreateTipoCambioDto();
    }

  }

  // tslint:disable-next-line:typedef
  async modificarEntidad()
  {    
    this.ente = this.ente ? await this.consultarEnte() : new CreateEnteDto();  
    this.registrocontable.ruc = this.ente.ruc;    
    this.mostrarSaldo();

  }

  async consultarEnte(): Promise<CreateEnteDto>
  {
    try {

      // tslint:disable-next-line:max-line-length
      this.ente = (await this.enteService.consultar(this.registrocontable.empresa, this.registrocontableForm.value.entidad ).toPromise());
      return this.ente as CreateEnteDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Socio de Negocio', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.ente = new CreateEnteDto();
    }   

  }

  // tslint:disable-next-line:typedef
  obtenerTextoSelect(lista: string)
  {   
    // Centro costo
    if (lista === 'listaCentroCosto')
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCentroCosto.length; i++ )      
      {
        if ( this.listaCentroCosto[i].codigo === this.registrocontableForm.value.centrocosto )
        {
         this.registrocontable.tablacentrocosto = this.listaCentroCosto[i].tipo;
        }
      }
    }
  } 

  mostrarSaldo(): void
  {
    if (this.registrocontable.tiporegistro === 'M' && 
        this.registrocontableForm.value.cuentacontable !== null && this.registrocontableForm.value.cuentacontable !== '' && 
        this.registrocontableForm.value.cuentacontable !== '000000' &&
        this.registrocontableForm.value.entidad !== null && this.registrocontableForm.value.entidad !== '' &&
        this.registrocontableForm.value.tipodocumento !== null && this.registrocontableForm.value.tipodocumento !== '')
    {
      this.viewsaldo = true;
    }
    else
    {
      this.viewsaldo = false;
    }

  }

  // tslint:disable-next-line:typedef
  async buscarSaldo(){

    this.valorcatalogo = { 
                          cuentacontable: this.registrocontableForm.value.cuentacontable, 
                          entidad:  this.registrocontableForm.value.entidad,
                          tipodocumento: this.registrocontableForm.value.tipodocumento,
                          numeroserie: this.registrocontableForm.value.numeroserie,
                          numerocomprobanteini: this.registrocontableForm.value.numerocomprobanteini,
                          montoorigen: '',
                          montonacional: '',
                          montodolares: '',
                          estado: 'N'
                         };

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
    
    this.dialogRef = this._matDialog.open(SaldoboxComponent, 
      {
      panelClass: 'saldobox-dialog'
    });
  
    this.dialogRef.afterClosed().subscribe(result => 
      {
        this.valorcatalogo = JSON.parse(localStorage.getItem(CATALOGO));
  
        if ( this.valorcatalogo.estado === 'S' )
        {         

          this.registrocontableForm.controls['numeroserie'].setValue(this.valorcatalogo.numeroserie);   
          this.registrocontableForm.controls['numerocomprobanteini'].setValue(this.valorcatalogo.numerocomprobanteini); 
          this.registrocontableForm.controls['montoorigen'].setValue(this.valorcatalogo.montoorigen); 
          this.registrocontableForm.controls['montonacional'].setValue(this.valorcatalogo.montonacional); 
          this.registrocontableForm.controls['montodolares'].setValue(this.valorcatalogo.montodolares);            
        }
  
        this.valorcatalogo = { cuentacontable: '',
                               entidad:  '',
                               tipodocumento: '',
                               numeroserie: '',
                               numerocomprobanteini: '',
                               montoorigen: '',
                               montonacional: '',
                               montodolares: '',
                               estado: ''
                             };
        localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
  
      }        
    ); 
  
  }
  
}
