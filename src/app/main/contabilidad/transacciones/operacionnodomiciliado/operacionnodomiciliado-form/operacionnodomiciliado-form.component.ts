import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef, AfterContentChecked} from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { OperacionContableService } from 'app/services/operacioncontable.service';
import { EnteService } from 'app/services/ente.service';
import { ConsultaService } from 'app/services/consulta.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DATOS, VALORES_INICIALES, CATALOGO, MESSAGEBOX, REGISTROCONTABLE, REFRESH } from 'app/_shared/var.constant';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { CreateOperacionContableDto } from 'app/dto/create.operacioncontable.dto';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { SubDiarioService } from 'app/services/subdiario.service';
import { DetraccionService } from 'app/services/detraccion.service';
import { CreateEnteDto } from 'app/dto/create.ente.dto';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { SearchboxComponent } from 'app/main/shared/searchbox/searchbox.component';
import { CreateTipoCambioDto } from 'app/dto/create.tipocambio.dto';
import { ViewValorDto } from 'app/dto/view.valor.dto';
import { TipoCambioService } from 'app/services/tipocambio.service';
import { ValorFechaService } from 'app/services/valorfecha.service';
import { CreateDetraccionDto } from 'app/dto/create.detraccion.dto';
import { MatTableDataSource } from '@angular/material/table';
import { RegistroContableService } from 'app/services/registrocontable.service';
import { MatSort } from '@angular/material/sort';
import { CreateRegistroContableDto } from 'app/dto/create.registrocontable.dto';
import { CreateSubDiarioDto } from 'app/dto/create.subdiario.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { CreateTipoDocMonCtaDto } from 'app/dto/create.tipodocmoncta.dto';
import { TipoDocMonCtaService } from 'app/services/tipodocmoncta.service';
import { ParametroService } from 'app/services/parametro.service';
import { CreateParametroDto } from 'app/dto/create.parametro.dto';
import { EjercicioService } from 'app/services/ejercicio.service';
import { PeriodoService } from 'app/services/periodo.service';
import * as moment from 'moment';
import { CreateEjercicioDto } from 'app/dto/create.ejercicio.dto';
import { CreatePeriodoDto } from 'app/dto/create.periodo.dto';
import { RegistrocontableFormComponent } from '../../registrocontable-form/registrocontable-form.component';
import { PlanContableService } from 'app/services/plancontable.service';
import { CreatePlanContableDto } from 'app/dto/create.plancontable.dto';
import { CentroCostoService } from 'app/services/centrocosto.service';
import { GrupoCentroCostoDetalleService } from 'app/services/grupocentrocostodetalle.service';
import { CreateCentroCostoDto } from 'app/dto/create.centrocosto.dto';


@Component({
  selector: 'gez-operacionnodomiciliado-form',
  templateUrl: './operacionnodomiciliado-form.component.html',
  styleUrls: ['./operacionnodomiciliado-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class OperacionnodomiciliadoFormComponent implements OnInit, AfterContentChecked  {

  displayedColumnsNaturaleza = ['cuentacontable', 'proveedor', 'tipoanotacion', 'centrocosto', 'porcentaje', 
                                'tipodocumento', 'serie', 'documento', 'fechaemision', 'fechaven', 'imporigen', 'impnacional', 'impextranjera', 
                                'mediopago', 'glosa', 'editar', 'eliminar'];

  displayedColumnsDestino  = ['cuentacontable', 'proveedor', 'tipoanotacion', 'centrocosto', 'porcentaje', 
                              'tipodocumento', 'serie', 'documento', 'fechaemision', 'fechaven', 'imporigen', 'impnacional', 'impextranjera', 
                              'mediopago', 'glosadestino'];

  misDatos;
  valoresiniciales;
  valorcampo;
  valorcontador;
  valorcatalogo;
  operacioncontable;
  registrooperacion;
  operacioncontableref;
  registrocontable;
  registrocontableinsert;
  cuentacontable;
  tipodocmoncta;
  parametro;
  ente;
  tipocambio;
  igv;
  detraccion;
  subdiario;
  ejercicio;
  periodo;
  centrocosto;  
  operacioncontableForm: FormGroup;  
  subdiarioinicio;
  tipodocumentoinicio;

  edit = true; 
  editTCSoles = true;
  editTCDolares = true;
  editDetraccion = true;
  editReferencia = true;
  checkArticuloNoDom = true;
  mensaje = '';

  ruc;
  // Parámetros
  monedanacional = '';
  monedadolares = '';
  notacredito = '';
  notadebito = '';


  listaAsientoContable: any = [];
  listaAsientoContableNaturaleza: any = [];
  listaAsientoContableDestino: any = [];
  listaTotales: any = [];
  listaProveedor: any[];
  listaBeneficiario: any[];
  listaTipoDocumento: any[];
  listaTipoSustento: any[];
  listaSubDiario: any[];
  listaBienServicio: any[];
  listaMoneda: any[];
  listaTipoCambio: any[];
  listaDetraccion: any[];
  listaTipoAnotacion: any[];
  listaCentroCosto: any[];
  listaGrupoCentroCostoDetalle: any[];
  listaMedioPago: any[];
  listaFlujoEfectivo: any[];
  listaAduana: any[];
  listaVinculacion: any[];
  listaConvenio: any[];
  listaExoneracion: any[];
  listaRenta: any[];
  listaModalidad: any[];
  listaEstado: any[];
  listaFound: any[];
  dialogRef: any;

  

  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort: MatSort;  
  // public listaAsientoContableNaturaleza = new MatTableDataSource<any>();
  // @ViewChild(MatSort) sortnat: MatSort;   
  
  constructor(
    private enteService: EnteService,
    private tipodocumentoService: TipoDocumentoService,
    private subdiarioService: SubDiarioService,
    private detraccionService: DetraccionService,
    private operacioncontableService: OperacionContableService,
    private registrocontableService: RegistroContableService,
    private consultaService: ConsultaService,
    private tipocambioService: TipoCambioService,
    private valorfechaService: ValorFechaService,
    private tipodocmonctaService: TipoDocMonCtaService,
    private parametroService: ParametroService,
    private ejercicioService: EjercicioService,
    private periodoService: PeriodoService,
    private plancontableService: PlanContableService,
    private centrocostoService: CentroCostoService,
    private grupocentrocostodetalleService: GrupoCentroCostoDetalleService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private cdref: ChangeDetectorRef
  ) 
  {

  }
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {
    this.edit = true; 
    this.editTCSoles = true;
    this.editTCDolares = true;
    this.editDetraccion = true;
    this.editReferencia = true;
    this.mensaje = '';  
    this.monedanacional = '';
    this.monedadolares = '';
    this.notacredito = '';
    this.notadebito = '';

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.ente = new CreateEnteDto();
    this.valorcontador = new ViewValorContadorDto();  
    this.operacioncontable = new CreateOperacionContableDto();
    this.registrooperacion = new CreateOperacionContableDto();
    this.operacioncontableref = new CreateOperacionContableDto();
    this.registrocontable = new CreateRegistroContableDto();
    this.tipocambio = new CreateTipoCambioDto();
    this.igv = new ViewValorDto();
    this.detraccion = new CreateDetraccionDto();
    this.subdiario = new CreateSubDiarioDto();
    this.tipodocmoncta = new CreateTipoDocMonCtaDto();
    this.parametro = new CreateParametroDto();
    this.ejercicio = new CreateEjercicioDto();
    this.periodo = new CreatePeriodoDto();
    this.cuentacontable = new CreatePlanContableDto();
    this.centrocosto = new CreateCentroCostoDto();
  
    this.operacioncontableForm = this.createOperacionContableForm();
  
    this.listarProveedor();
    this.listarBeneficiario();
    this.listarTipoDocumento();
    this.listarTipoSustento();
    this.listarSubDiario();
    this.listarBienServicio();
    this.listarMoneda();
    this.listarTipoCambio();
    this.listarDetraccion(); 
    this.listarTipoAnotacion();
    this.listarCentroCosto();
    this.listarMedioPago();
    this.listarFlujoEfectivo();
    this.listarAduana();
    this.listarVinculacion();
    this.listarConvenio();
    this.listarExoneracion();
    this.listarRenta();
    this.listarModalidad();
    this.listarEstado(); 

    // campos pk
    this.operacioncontable.empresa            = this.valoresiniciales.empresa;
    this.operacioncontable.ejerciciocontable  = this.valoresiniciales.annoproceso;

    // Obtener moneda nacional
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'MON_SOLES') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Moneda Nacional', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.monedanacional = this.parametro.valor;

    // Obtener moneda dólares
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'MON_DOLARES') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Moneda Dólares', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.monedadolares = this.parametro.valor;

    // Obtener nota crédito para referencia
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'NOTACREDITO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Nota Crédito', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.notacredito = this.parametro.valor;

    // Obtener subdiario de inicio
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'SUBDIARIO_NDO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: SubDiario por Operación', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.subdiarioinicio = this.parametro.valor;

    // Obtener tipo documento de inicio
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'TIPDOC_NDO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: SubDiario por Operación', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.tipodocumentoinicio = this.parametro.valor;


    // Obtener nota débito para referencia
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'NOTADEBITO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Nota Débito', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    this.notadebito = this.parametro.valor;

    if ( this.misDatos.action === 'editar' )
    {
      // campo pk
      this.operacioncontable.operacioncontable       = this.misDatos.parametro1;

      this.operacioncontable = this.operacioncontable ? await this.obtenerOperacionContable() : new CreateOperacionContableDto();       
      
      this.checkArticuloNoDom  = (this.operacioncontable.articulonodom === 'S' ) ? true : false;
      
      this.operacioncontableForm = this.createOperacionContableForm();        
      
      // Tipo de cambio manual
      this.actualizarTipoCambio();

      // Detracción
      if (this.operacioncontableForm.value.detraccion === null || this.operacioncontableForm.value.detraccion  === '')
      {  
        this.editDetraccion = true;
      }
      else
      {
        this.editDetraccion = false;
      }

      // Documento Referencia
      if (this.operacioncontableForm.value.tipodocumento === this.notacredito || this.operacioncontableForm.value.tipodocumento  === this.notadebito)
      {  
        this.editReferencia = false;
      }
      else
      {
        this.editReferencia = true;
      }

      
      // if (this.operacioncontableForm.value.tipocambio === 'M')
      // {
      //   this.editTCSoles = false;
      //   this.editTCDolares = false;
      // }
      // else
      // {
      //   this.editTCSoles = true;
      //   this.editTCDolares = true;
      // }
      
      // Validar ejercicio y período abierto

      // Obtener Asiento contable
      this.obtenerRegistroContable();   
      this.setSettingTableCourse();

    }
    else{

      this.edit = false;
      this.checkArticuloNoDom = false;  
      this.iniciarOperacionContable();   

    }    
    
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() 
  {
    this.listaAsientoContableNaturaleza.sort = this.sort;
  }

  // tslint:disable-next-line:typedef
  async obtenerOperacionContable(): Promise<CreateOperacionContableDto>
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.operacioncontable = (await this.operacioncontableService.consultar(this.operacioncontable.empresa, this.operacioncontable.ejerciciocontable, this.operacioncontable.operacioncontable ).toPromise());
      return this.operacioncontable as CreateOperacionContableDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Operación Contable', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.operacioncontable = new CreateOperacionContableDto();
    }
  }  

  // tslint:disable-next-line:typedef
  async obtenerRegistroContable() 
  {
    // tslint:disable-next-line:max-line-length
    this.listaAsientoContable = (await this.registrocontableService.listarOperacion(this.operacioncontable.empresa, this.operacioncontable.ejerciciocontable, this.operacioncontable.operacioncontable).toPromise());
    
    this.obtenerRegistroContableNaturaleza();    

     
  }   

  // tslint:disable-next-line:typedef
  obtenerRegistroContableNaturaleza(): void
  {
    this.listaAsientoContableNaturaleza  = this.listaAsientoContable.filter(x => x.correlativoorigen === 0);
    
    // this.listaAsientoContableDestino     = this.listaAsientoContable.filter(x => x.correlativoorigen !== 0);
    
    this.setSettingTableCourse();

  }  

  // tslint:disable-next-line:typedef
  obtenerRegistroContableDestino(correlativoorigen: number): void
  {
    this.listaAsientoContableDestino = this.listaAsientoContable.filter(x => x.correlativoorigen === correlativoorigen);
  } 



  // tslint:disable-next-line:typedef
  iniciarOperacionContable()
  {
    setTimeout(() => {
      const dateObj = new Date();
      const month = (dateObj.getMonth() + 1).toString().length === 1 ? '0' + (dateObj.getMonth() + 1) : (dateObj.getMonth() + 1); 
      const day = dateObj.getDate().toString().length === 1 ? '0' + dateObj.getDate() : dateObj.getDate();
      const year = dateObj.getFullYear();
      const today = year + '-' + month + '-' + day;

      this.operacioncontableForm.controls['entidad'].setValue('');
      this.operacioncontableForm.controls['tipodocumento'].setValue(this.tipodocumentoinicio);
      this.operacioncontableForm.controls['subdiario'].setValue(this.subdiarioinicio);
      this.operacioncontableForm.controls['descripcionsubdiario'].setValue(''); 
      this.operacioncontableForm.controls['numerocomprobanteini'].setValue('');
      this.operacioncontableForm.controls['fechaoperacion'].setValue(today);
      this.operacioncontableForm.controls['fechaemision'].setValue(today);
      this.operacioncontableForm.controls['moneda'].setValue(this.monedanacional);
      this.operacioncontableForm.controls['tipocambio'].setValue('C');
      this.operacioncontableForm.controls['tipocambiosoles'].setValue(0.000);   
      this.operacioncontableForm.controls['tipocambiodolares'].setValue(0.000); 
      this.operacioncontableForm.controls['baseimponible'].setValue(0.00);
      this.operacioncontableForm.controls['inafecto'].setValue(0.00);
      this.operacioncontableForm.controls['impuesto'].setValue(0.00);
      this.operacioncontableForm.controls['precioorigen'].setValue(0.00);

      this.operacioncontableForm.controls['entidadnodom'].setValue('');
      this.operacioncontableForm.controls['rucnodom'].setValue('');
      this.operacioncontableForm.controls['aduana'].setValue('');
      this.operacioncontableForm.controls['adicionalnodom'].setValue(0.000);
      this.operacioncontableForm.controls['deduccionnodom'].setValue(0.000);
      this.operacioncontableForm.controls['tipodocumentonodom'].setValue('');
      this.operacioncontableForm.controls['numeroserienodom'].setValue('');
      this.operacioncontableForm.controls['annocomprobantenodom'].setValue('');
      this.operacioncontableForm.controls['numerocomprobantenodom'].setValue('');
      this.operacioncontableForm.controls['vinculoeconomiconodom'].setValue('');
      this.operacioncontableForm.controls['tasaretencionnodom'].setValue(0.000);
      this.operacioncontableForm.controls['impuestoretenidonodom'].setValue(0.000);
      this.operacioncontableForm.controls['convenionodom'].setValue('');
      this.operacioncontableForm.controls['exoneracionnodom'].setValue('');
      this.operacioncontableForm.controls['tiporentanodom'].setValue('');
      this.operacioncontableForm.controls['modalidadnodom'].setValue('');
      this.operacioncontableForm.controls['articulonodom'].setValue(this.checkArticuloNoDom);

      this.operacioncontableForm.controls['columnabaseimponible'].setValue(0);
      this.operacioncontableForm.controls['columnaimpuesto'].setValue(0);
      this.operacioncontableForm.controls['estado'].setValue('A');
      

      // Obtener IGV
      this.calcularIGV();

    });
 
  }

  createOperacionContableForm(): FormGroup
  {    

    return this._formBuilder.group(
    {
      operacioncontable            : [this.operacioncontable.operacioncontable],      
      ruc                          : [this.operacioncontable.ruc],
      entidad                      : [this.operacioncontable.entidad],
      tipodocumento                : [this.operacioncontable.tipodocumento],
      subdiario                    : [this.operacioncontable.subdiario],
      descripcionsubdiario         : [this.operacioncontable.descripcionsubdiario],
      numeroserie                  : [this.operacioncontable.numeroserie],
      numerocomprobanteini         : [this.operacioncontable.numerocomprobanteini],
      numerocomprobantefin         : [this.operacioncontable.numerocomprobantefin],
      fechaoperacion               : [this.operacioncontable.fechaoperacion],
      fechaemision                 : [this.operacioncontable.fechaemision],
      fechavencimiento             : [this.operacioncontable.fechavencimiento],
      moneda                       : [this.operacioncontable.moneda],
      tipocambio                   : [this.operacioncontable.tipocambio],
      tipocambiosoles              : [this.operacioncontable.tipocambiosoles],
      tipocambiodolares            : [this.operacioncontable.tipocambiodolares],
      baseimponible                : [this.operacioncontable.baseimponible],
      inafecto                     : [this.operacioncontable.inafecto],
      impuesto                     : [this.operacioncontable.impuesto],
      precioorigen                 : [this.operacioncontable.precioorigen],
      glosa                        : [this.operacioncontable.glosa],
      bienservicio                 : [this.operacioncontable.bienservicio],
      detraccion                   : [this.operacioncontable.detraccion],
      numerodetraccion             : [this.operacioncontable.numerodetraccion],
      fechadetraccion              : [this.operacioncontable.fechadetraccion],
      basedetraccion               : [this.operacioncontable.basedetraccion],
      montodetraccion              : [this.operacioncontable.montodetraccion],
      descripciontipodocumentoref  : [this.operacioncontable.descripciontipodocumentoref],
      numeroserieref               : [this.operacioncontable.numeroserieref],
      numerocomprobanteiniref      : [this.operacioncontable.numerocomprobanteiniref],
      operacioncontableref         : [this.operacioncontable.operacioncontableref],
      fechaemisionref              : [this.operacioncontable.fechaemisionref],
      baseimponibleref             : [this.operacioncontable.baseimponibleref],
      inafectoref                  : [this.operacioncontable.inafectoref],
      impuestoref                  : [this.operacioncontable.impuestoref],
      precioorigenref              : [this.operacioncontable.precioorigenref],
      rucnodom                     : [this.operacioncontable.rucnodom],
      entidadnodom                 : [this.operacioncontable.entidadnodom],
      aduana                       : [this.operacioncontable.aduana],
      adicionalnodom               : [this.operacioncontable.adicionalnodom],
      deduccionnodom               : [this.operacioncontable.deduccionnodom],
      tipodocumentonodom           : [this.operacioncontable.tipodocumentonodom],
      numeroserienodom             : [this.operacioncontable.numeroserienodom],
      annocomprobantenodom         : [this.operacioncontable.annocomprobantenodom],
      numerocomprobantenodom       : [this.operacioncontable.numerocomprobantenodom],
      vinculoeconomiconodom        : [this.operacioncontable.vinculoeconomiconodom],
      tasaretencionnodom           : [this.operacioncontable.tasaretencionnodom],
      impuestoretenidonodom        : [this.operacioncontable.impuestoretenidonodom],
      convenionodom                : [this.operacioncontable.convenionodom],
      exoneracionnodom             : [this.operacioncontable.exoneracionnodom],
      tiporentanodom               : [this.operacioncontable.tiporentanodom],
      modalidadnodom               : [this.operacioncontable.modalidadnodom],
      articulonodom                : [this.checkArticuloNoDom],
      columnabaseimponible         : [this.operacioncontable.columnabaseimponible],
      columnaimpuesto              : [this.operacioncontable.columnaimpuesto],
      estado                       : [this.operacioncontable.estado]
      
    });  

  }

  // tslint:disable-next-line:typedef
  listarProveedor(){

    this.enteService.listarProveedorNoDom(this.valoresiniciales.empresa).subscribe(data => {
      this.listaProveedor = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarBeneficiario(){

    this.enteService.listarProveedor(this.valoresiniciales.empresa).subscribe(data => {
      this.listaBeneficiario = data;
    });
  }
  
  // tslint:disable-next-line:typedef
  refrescarProveedor(){

    this.listarProveedor();

    setTimeout(() => {
       this.obtenerProveedor();
    });

  }

  // tslint:disable-next-line:typedef
  refrescarBeneficiario(){

    this.listarBeneficiario();

    setTimeout(() => {
       this.obtenerBeneficiario();
    });

  }

  // tslint:disable-next-line:typedef
  listarTipoDocumento(){

    this.tipodocumentoService.listarCatalogoNoDomiciliados(this.valoresiniciales.empresa).subscribe(data => {
      this.listaTipoDocumento = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoSustento(){

    this.tipodocumentoService.listarCatalogoNoDomSustentos(this.valoresiniciales.empresa).subscribe(data => {
      this.listaTipoSustento = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarSubDiario(){

    this.subdiarioService.listarCatalogoNoDomiciliados(this.valoresiniciales.empresa).subscribe(data => {
      this.listaSubDiario = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarBienServicio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDBIENSERVICIO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaBienServicio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarMoneda(){

    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 4).subscribe(data => {
      this.listaMoneda = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoCambio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOCAMBIO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaTipoCambio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarDetraccion(){

    this.detraccionService.listarCatalogo(this.valoresiniciales.empresa).subscribe(data => {
      this.listaDetraccion = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoAnotacion(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDTIPOANOTACION'; 
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaTipoAnotacion = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarCentroCosto()
  {
    this.consultaService.listarCentroCosto(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCentroCosto = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarMedioPago()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDMEDIOPAGO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaMedioPago = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarFlujoEfectivo()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDFLUJOEFECTIVO';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaFlujoEfectivo = data;
    });
  }  

  // tslint:disable-next-line:typedef
  listarAduana()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDADUANA';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaAduana = data;
    });
  }  

  // tslint:disable-next-line:typedef
  listarVinculacion()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDVINCULOECONODOM';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaVinculacion = data;
    });
  }  

  // tslint:disable-next-line:typedef
  listarConvenio()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDCONVENIOTRIBNODOM';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaConvenio = data;
    });
  }  

  // tslint:disable-next-line:typedef
  listarExoneracion()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDEXONERACIONNODOM';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaExoneracion = data;
    });
  } 

  // tslint:disable-next-line:typedef
  listarRenta()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDTIPORENTANODOM';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaRenta = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarModalidad()
  {
    this.valorcampo = new ValorCampoDto();
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDMODALIDADSERVNODOM';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaModalidad = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarEstado(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'OPEESTADO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaEstado = data;
    });
  }

  // tslint:disable-next-line:typedef
  obtenerProveedor()
  {
    this.ruc = this.operacioncontableForm.value.ruc;

    if ( this.ruc !== '' )
    {
      this.ente.ruc       = this.ruc;
      this.ente.empresa   = this.valoresiniciales.empresa;
  
      this.enteService.contarRuc(this.ente).subscribe(data => 
      {
        this.valorcontador = data;
  
        if (this.valorcontador.contador === 0)
          {
            this._matSnackBar.open('Error, Documento de Identidad no existe', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.operacioncontableForm.controls['entidad'].setValue('');
          }
        // tslint:disable-next-line:align
        else if (this.valorcontador.contador > 1) 
          {
            this._matSnackBar.open('Error, Documento de Identidad, asignado a mas de 1 proveedor', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.operacioncontableForm.controls['entidad'].setValue('');
          }
          else if (this.valorcontador.contador === 1) 
            {
              // tslint:disable-next-line:no-shadowed-variable
              this.enteService.consultarRucAnexo(this.valoresiniciales.empresa, this.ruc, 'N' ).subscribe(data => {
                this.ente = data;   
                console.log(this.ente); 

                if (this.ente.entidad !== null && this.ente.entidad !== '')
                {
                  this.operacioncontableForm.controls['entidad'].setValue(this.ente.entidad);
                }
                else
                {
                  this.operacioncontableForm.controls['entidad'].setValue('');
                  this._matSnackBar.open('Número de documento no registrado para Proveedor', 'ok', {
                    verticalPosition: 'bottom',
                    panelClass: ['my-snack-bar'] 
                    });
                }
          
                }, err => {
                  this.operacioncontableForm.controls['entidad'].setValue('');
                  this._matSnackBar.open('Error de consulta de proveedor', 'ok', {
                      verticalPosition: 'bottom',
                      panelClass: ['my-snack-bar'] 
                      });
                }
              );  
            }
      }
      );
    }
  }
  // tslint:disable-next-line:typedef
  obtenerBeneficiario()
  {
    this.ruc = this.operacioncontableForm.value.rucnodom;

    if ( this.ruc !== '' )
    {
      this.ente.ruc       = this.ruc;
      this.ente.empresa   = this.valoresiniciales.empresa;
  
      this.enteService.contarRuc(this.ente).subscribe(data => 
      {
        this.valorcontador = data;
  
        if (this.valorcontador.contador === 0)
          {
            this._matSnackBar.open('Error, Documento de Identidad no existe', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.operacioncontableForm.controls['entidadnodom'].setValue('');
          }
        // tslint:disable-next-line:align
        else if (this.valorcontador.contador > 1) 
          {
            this._matSnackBar.open('Error, Documento de Identidad, asignado a mas de 1 socio negocio', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.operacioncontableForm.controls['entidadnodom'].setValue('');
          }
          else if (this.valorcontador.contador === 1) 
            {
              // tslint:disable-next-line:no-shadowed-variable
              this.enteService.consultarRucAnexo(this.valoresiniciales.empresa, this.ruc, 'P' ).subscribe(data => {
                this.ente = data;   

                if (this.ente.entidad !== null && this.ente.entidad !== '')
                {
                  this.operacioncontableForm.controls['entidadnodom'].setValue(this.ente.entidad);
                }
                else
                {
                  this.operacioncontableForm.controls['entidadnodom'].setValue('');
                  this._matSnackBar.open('Número de documento no registrado para Beneficiario', 'ok', {
                    verticalPosition: 'bottom',
                    panelClass: ['my-snack-bar'] 
                    });
                }
          
                }, err => {
                  this.operacioncontableForm.controls['entidadnodom'].setValue('');
                  this._matSnackBar.open('Error de consulta de Beneficiario', 'ok', {
                      verticalPosition: 'bottom',
                      panelClass: ['my-snack-bar'] 
                      });
                }
              );  
            }
      }
      );
    }
  }

  // tslint:disable-next-line:typedef
  modificarTipoDocumento()
  {
    if (this.operacioncontableForm.value.tipodocumento === this.notacredito || this.operacioncontableForm.value.tipodocumento  === this.notadebito)
    {  
      this.editReferencia = false;
    }
    else
    {
      this.editReferencia = true;

      this.operacioncontable.ejerciciocontableref = null;
      this.operacioncontable.operacioncontableref = null;
      this.operacioncontable.tipodocumentoref = null;
      this.operacioncontable.tipocambiosolesref = null;

      this.operacioncontableForm.controls['operacioncontableref'].setValue(null);
      this.operacioncontableForm.controls['descripciontipodocumentoref'].setValue(null);
      this.operacioncontableForm.controls['numeroserieref'].setValue(null);
      this.operacioncontableForm.controls['numerocomprobanteiniref'].setValue(null);
      this.operacioncontableForm.controls['fechaemisionref'].setValue(null);
      this.operacioncontableForm.controls['baseimponibleref'].setValue(null);
      this.operacioncontableForm.controls['inafectoref'].setValue(null);
      this.operacioncontableForm.controls['impuestoref'].setValue(null);
      this.operacioncontableForm.controls['precioorigenref'].setValue(null);
    }
  }

  // tslint:disable-next-line:typedef
  obtenerSubDiario()
  {

    if (this.operacioncontableForm.value.subdiario.replace(/\s+$/g, '') === null || this.operacioncontableForm.value.subdiario.replace(/\s+$/g, '')  === '')
    {
      this.operacioncontableForm.controls['columnabaseimponible'].setValue(0);
      this.operacioncontableForm.controls['columnaimpuesto'].setValue(0);
    }
    else
    {
      this.subdiarioService.consultar(this.valoresiniciales.empresa, this.operacioncontableForm.value.subdiario ).subscribe(data => {
        this.subdiario = data;    
  
        this.operacioncontableForm.controls['columnabaseimponible'].setValue(this.subdiario.columnacompra);
        this.operacioncontableForm.controls['columnaimpuesto'].setValue(this.subdiario.columnaigvcompra);
  
        }, err => {
          this._matSnackBar.open('Error de consulta del subdiario', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      ); 
    }
  }

  // tslint:disable-next-line:typedef
  async buscarOperacion(){

    // verificar valore de tipo de documento y moneda
    if ( this.operacioncontableForm.value.tipodocumento === '' || this.operacioncontableForm.value.moneda === '' )
    {
      this._matSnackBar.open('Ingresar: Tipo de Documento y Moneda', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
        
      return;
    }

    this.valorcontador = this.valorcontador ? await this.permitirDocumentoReferencia() : new ViewValorContadorDto();

    if ( this.valorcontador.contador === 0 )
    {
      this._matSnackBar.open('Error, Falta configurar Tipo Documento y Moneda, para permitir documento de referencia', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });

      return;
    }    

    this.valorcatalogo = 
    { 
      codigo: '', 
      descripcion: '',
      operacioncontable: this.operacioncontableForm.value.operacioncontable, 
      entidad:  this.operacioncontableForm.value.entidad
    };

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
    
    this.dialogRef = this._matDialog.open(SearchboxComponent, 
      {
      panelClass: 'searchbox-dialog',
      data      : {
          catalogo: 'operacioncontable',
          titulo: 'Buscar Operación Contable',
          codigo: 'Operación',
          descripcion: 'Referencia',
          detalle: 'Fecha Emisión'
      }
    });
  
    this.dialogRef.afterClosed().subscribe(result => 
      {
        this.valorcatalogo = JSON.parse(localStorage.getItem(CATALOGO));
  
        if ( this.valorcatalogo.codigo !== '' )
        {         
          this.operacioncontableref.empresa            = this.valoresiniciales.empresa;
          this.operacioncontableref.ejerciciocontable  = this.valoresiniciales.annoproceso;
          this.operacioncontableref.operacioncontable  = this.valorcatalogo.codigo;

          // tslint:disable-next-line:max-line-length
          this.operacioncontableService.consultar(this.operacioncontableref.empresa, this.operacioncontableref.ejerciciocontable, this.operacioncontableref.operacioncontable ).subscribe(data => {
            this.operacioncontableref = data;    

            this.operacioncontable.ejerciciocontableref = this.operacioncontableref.ejerciciocontable;
            this.operacioncontable.operacioncontableref = this.operacioncontableref.operacioncontable;
            this.operacioncontable.tipodocumentoref = this.operacioncontableref.tipodocumento;
            this.operacioncontable.tipocambiosolesref = this.operacioncontableref.tipocambiosolesref;

            this.operacioncontableForm.controls['operacioncontableref'].setValue(this.operacioncontableref.operacioncontable);
            this.operacioncontableForm.controls['descripciontipodocumentoref'].setValue(this.operacioncontableref.descripciontipodocumento);
            this.operacioncontableForm.controls['numeroserieref'].setValue(this.operacioncontableref.numeroserie);
            this.operacioncontableForm.controls['numerocomprobanteiniref'].setValue(this.operacioncontableref.numerocomprobanteini);
            this.operacioncontableForm.controls['fechaemisionref'].setValue(this.operacioncontableref.fechaemision);
            this.operacioncontableForm.controls['baseimponibleref'].setValue(this.operacioncontableref.baseimponible);
            this.operacioncontableForm.controls['inafectoref'].setValue(this.operacioncontableref.inafecto);
            this.operacioncontableForm.controls['impuestoref'].setValue(this.operacioncontableref.impuesto);
            this.operacioncontableForm.controls['precioorigenref'].setValue(this.operacioncontableref.precioorigen);

            }, err => {
              this._matSnackBar.open('Error de consulta a la operación contable de referencia', 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
            }
          );                     
          
        }
  
        this.valorcatalogo = { codigo: '', descripcion: ''};
        localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
  
      }        
    ); 
  
  }

  // tslint:disable-next-line:typedef
  async buscarProveedor(){

    this.valorcatalogo = 
    { 
      codigo: '', 
      entidad:  ''
    };

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
    
    this.dialogRef = this._matDialog.open(SearchboxComponent, 
      {
      panelClass: 'searchbox-dialog',
      data      : {
          catalogo: 'proveedornodom',
          titulo: 'Buscar Proveedor No Domiciliado',
          codigo: 'Doc.Identidad',
          descripcion: 'Proveedor',
          detalle: 'Código'
      }
    });
  
    this.dialogRef.afterClosed().subscribe(result => 
      {
        this.valorcatalogo = JSON.parse(localStorage.getItem(CATALOGO));
  
        if ( this.valorcatalogo.codigo !== '' )
        {         
          this.operacioncontableForm.controls['ruc'].setValue(this.valorcatalogo.codigo); 
          this.obtenerProveedor();
          // this.operacioncontableForm.controls['entidad'].setValue(this.valorcatalogo.entidad);                     
        }  

        this.valorcatalogo = { codigo: '', entidad: ''};
        localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
  
      }        
    ); 
  
  }

  // tslint:disable-next-line:typedef
  async buscarBeneficiario(){

    this.valorcatalogo = 
    { 
      codigo: '', 
      entidad:  ''
    };

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
    
    this.dialogRef = this._matDialog.open(SearchboxComponent, 
      {
      panelClass: 'searchbox-dialog',
      data      : {
          catalogo: 'proveedor',
          titulo: 'Buscar Beneficiario',
          codigo: 'Doc.Identidad',
          descripcion: 'Beneficiario',
          detalle: 'Código'
      }
    });
  
    this.dialogRef.afterClosed().subscribe(result => 
      {
        this.valorcatalogo = JSON.parse(localStorage.getItem(CATALOGO));
  
        if ( this.valorcatalogo.codigo !== '' )
        {         
          this.operacioncontableForm.controls['rucnodom'].setValue(this.valorcatalogo.codigo); 
          this.obtenerBeneficiario();                    
        }  

        this.valorcatalogo = { codigo: '', entidad: ''};
        localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
  
      }        
    ); 
  
  }

  // tslint:disable-next-line:typedef
  async permitirDocumentoReferencia(): Promise<ViewValorContadorDto>
  {
    // verificar la configuración si esta permitido ingresar documento de referencia
    // tslint:disable-next-line:max-line-length
    this.valorcontador = (await this.consultaService.validarDocumentoReferencia(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.operacioncontableForm.value.tipodocumento, this.operacioncontableForm.value.moneda).toPromise());
    return this.valorcontador as ViewValorContadorDto;    
  }  

  // tslint:disable-next-line:typedef
  async modificarEntidad()
  {    
    if (this.operacioncontableForm.value.entidad.replace(/\s+$/g, '') === null || this.operacioncontableForm.value.entidad.replace(/\s+$/g, '')  === '')
    {
      this.operacioncontableForm.controls['entidad'].setValue('');
      this.operacioncontableForm.controls['ruc'].setValue('');
    }
    else
    {
      this.ente = this.ente ? await this.consultarEnte(this.operacioncontableForm.value.entidad) : new CreateEnteDto();  
      this.operacioncontableForm.controls['ruc'].setValue(this.ente.ruc);    
    }
  }

  // tslint:disable-next-line:typedef
  async modificarBeneficiario()
  {    
    if (this.operacioncontableForm.value.entidadnodom.replace(/\s+$/g, '') === null || this.operacioncontableForm.value.entidadnodom.replace(/\s+$/g, '')  === '')
    {
      this.operacioncontableForm.controls['entidadnodom'].setValue('');
      this.operacioncontableForm.controls['rucnodom'].setValue('');
    }
    else
    {
      this.ente = this.ente ? await this.consultarEnte(this.operacioncontableForm.value.entidadnodom) : new CreateEnteDto();  
      this.operacioncontableForm.controls['rucnodom'].setValue(this.ente.ruc);    
    }
  }

  // tslint:disable-next-line:typedef
  async modificarFechaContable()
  {     
    this.operacioncontableForm.controls['fechaemision'].setValue(this.operacioncontableForm.value.fechaoperacion);
    this.operacioncontableForm.controls['fechavencimiento'].setValue(this.operacioncontableForm.value.fechaoperacion);
  }
  // tslint:disable-next-line:typedef
  async modificarFechaEmision()
  {         
    // Tipo de Cambio - wf_con_tipocambio(lde_opefechaemision, ls_idmoncod, ls_opeindtipocambio,  row)
    await this.calcularTipoCambio();

    // IGV - fn_sha_igv(gs_idempcod, 'IGV',  lde_opefechaemision, '1', ld_opeimpuesto, ls_msg)
    await this.calcularIGV();
    
    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularMontos();
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }

  // tslint:disable-next-line:typedef
  async modificarMoneda()
  {     
    // actualizar tipo de cambio
    this.actualizarTipoCambio();

    // Tipo de Cambio - wf_con_tipocambio(lde_opefechaemision, ls_idmoncod, ls_opeindtipocambio,  row)
    await this.calcularTipoCambio();
   
    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularMontos();
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }

  // tslint:disable-next-line:typedef
  async modificarTipoCambio()
  {     
    // actualizar tipo de cambio
    this.actualizarTipoCambio();

    // Tipo de Cambio - wf_con_tipocambio(lde_opefechaemision, ls_idmoncod, ls_opeindtipocambio,  row)
    await this.calcularTipoCambio();
    
    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularMontos();
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }
  
  actualizarTipoCambio(): void
  {
    if (this.operacioncontableForm.value.tipocambio === 'M')
    {

      if (this.operacioncontableForm.value.moneda === this.monedanacional)
      {
        this.editTCSoles = true;
        this.operacioncontableForm.controls['tipocambiosoles'].setValue(1);  
      }
      else
      {
        this.editTCSoles = false;
      }

      if (this.operacioncontableForm.value.moneda === this.monedadolares)
      {
        this.editTCDolares = true;
      }
      else
      {
        this.editTCDolares = false;
      }

    }
    else
    {
      this.editTCSoles = true;
      this.editTCDolares = true;
    }
  }

  // tslint:disable-next-line:typedef
  async modificarMontoTipoCambio(tipomonto: string)
  {     
    if (this.operacioncontableForm.value.tipocambio === 'M' && 
         this.operacioncontableForm.value.moneda === this.monedadolares &&
         tipomonto === 'S')
    {
      this.operacioncontableForm.controls['tipocambiodolares'].setValue(this.operacioncontableForm.value.tipocambiosoles);
    }

    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularMontos();
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }
  
  // tslint:disable-next-line:typedef
  async modificarMontos()
  {     
       
    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularMontos();
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }

  // tslint:disable-next-line:typedef
  async modificarInafectoNoDom()
  {     
       
    // Calcular montos - wf_con_montos_nomodigv(ld_opeimporte, ld_opeinafecto, row)
    this.calcularIGVNoDom();    
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }

  // tslint:disable-next-line:typedef
  async modificarMontosDetraccion()
  {     
  
    // Calcular detracción - wf_con_montos_detraccion(ls_idmoncod, row)
    this.calcularMontosDetraccion();
  
    // Eliminar los registros automáticos
    this.eliminarAsientoAutomatico();

    // Recalcular montos de los registros contables
    this.recalcularAsientoManual();

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();

    // Inicializa la vista de los registros ppor destino
    this.obtenerRegistroContableDestino(-1);
    
  }

  // tslint:disable-next-line:typedef
  async modificarDetraccion()
  {    
    // if (this.operacioncontableForm.value.detraccion.replace(/\s+$/g, '') === null || this.operacioncontableForm.value.detraccion.replace(/\s+$/g, '')  === '')
    if (this.operacioncontableForm.value.detraccion.replace(/\s+$/g, '') === null || 
        this.operacioncontableForm.value.detraccion === '0' ||
        this.operacioncontableForm.value.detraccion.replace(/\s+$/g, '') === '')
    {
      this.operacioncontable.tasadetraccion = 0;
      this.operacioncontableForm.controls['numerodetraccion'].setValue('');
      this.operacioncontableForm.controls['fechadetraccion'].setValue(null);

      this.editDetraccion = true;

      this.modificarMontosDetraccion();  
    }
    else
    {
      this.detraccion = this.detraccion ? await this.consultarDetraccion() : new CreateDetraccionDto();  
      if ( this.detraccion.tasa === -1 )
      {
        this._matSnackBar.open('Falta configurar la Detracción', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });
      }
      else
      {
        this.operacioncontable.tasadetraccion = this.detraccion.tasa;

        this.editDetraccion = false;

        this.modificarMontosDetraccion();  
      } 
    }
  }

  // tslint:disable-next-line:typedef
  async calcularTipoCambio()
  {           
    if ( this.operacioncontableForm.value.tipocambio !== 'M' )
    {
      
      if ( this.operacioncontableForm.value.moneda === this.monedanacional) 
      {
        this.operacioncontableForm.controls['tipocambiosoles'].setValue(1);
      }
      else
      {
        // Tipo de Cambio Origen a Soles
        this.tipocambio = this.tipocambio ? await this.consultarTipoCambio(this.operacioncontableForm.value.moneda) : new CreateTipoCambioDto();      
        if ( this.tipocambio.compra !== -1 )
        {
          if (this.operacioncontableForm.value.tipocambio === 'C')
          {
            this.operacioncontableForm.controls['tipocambiosoles'].setValue(this.tipocambio.tipocambiocompra);
          }
          else
          {
            this.operacioncontableForm.controls['tipocambiosoles'].setValue(this.tipocambio.tipocambioventa);
          }        
        }
      }

      // Tipo de Cambio Dolares a Soles
      this.tipocambio = this.tipocambio ? await this.consultarTipoCambio(this.monedadolares) : new CreateTipoCambioDto();      
      if ( this.tipocambio.compra !== -1 )
      {
        if (this.operacioncontableForm.value.tipocambio === 'C')
        {
          this.operacioncontableForm.controls['tipocambiodolares'].setValue(this.tipocambio.tipocambiocompra);
        }
        else
        {
          this.operacioncontableForm.controls['tipocambiodolares'].setValue(this.tipocambio.tipocambioventa);
        }        
      }

    }   
  }

  // tslint:disable-next-line:typedef
  async calcularIGV()
  {    
    this.igv = this.igv ? await this.consultarIGV() : new ViewValorDto();  
    if ( this.igv.valor === -1 )
    {
      this._matSnackBar.open('Falta configurar el IGV', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }
    else
    {
      this.operacioncontable.porcentajeimpuesto = this.igv.valor;
    }
  }

  // tslint:disable-next-line:typedef  
  calcularMontos() 
  {    
    
    // tslint:disable-next-line:no-bitwise
    // this.operacioncontable.impuesto     = ((Number(this.operacioncontableForm.value.baseimponible) + Number(this.operacioncontableForm.value.adicionalnodom)
    //                                        ) * Number(this.operacioncontable.porcentajeimpuesto) / 100).toFixed(3);
    
    this.operacioncontable.precioorigen =  (Number(this.operacioncontableForm.value.baseimponible) + Number(this.operacioncontableForm.value.adicionalnodom)).toFixed(2);
    
    // this.operacioncontableForm.controls['impuesto'].setValue(this.operacioncontable.impuesto);
    this.operacioncontableForm.controls['precioorigen'].setValue(this.operacioncontable.precioorigen);

    if (this.operacioncontableForm.value.tasaretencionnodom > 0)
    {
      this.operacioncontable.impuestoretenidonodom = ((Number(this.operacioncontableForm.value.baseimponible) +
                                                       Number(this.operacioncontableForm.value.adicionalnodom) -
                                                       Number(this.operacioncontableForm.value.deduccionnodom)
                                                      ) * Number(this.operacioncontableForm.value.tasaretencionnodom) / 100).toFixed(3);
      this.operacioncontableForm.controls['impuestoretenidonodom'].setValue(this.operacioncontable.impuestoretenidonodom);
    }
    else
    {
      this.operacioncontableForm.controls['impuestoretenidonodom'].setValue(0.000);
    }

    this.operacioncontable.precioorigen = +(this.operacioncontableForm.value.precioorigen);
    this.operacioncontable.tipocambiosoles = +(this.operacioncontableForm.value.tipocambiosoles);
    this.operacioncontable.tipocambiodolares = +(this.operacioncontableForm.value.tipocambiodolares);

    // moneda nacional
    if ( this.operacioncontableForm.value.moneda === this.monedanacional)
    {
      this.operacioncontable.preciosoles = this.operacioncontable.precioorigen;
      this.operacioncontable.preciodolares = +(this.operacioncontable.precioorigen / this.operacioncontable.tipocambiodolares).toFixed(2);
    }
    // moneda dólares
    else if ( this.operacioncontableForm.value.moneda === this.monedadolares)
    {
      this.operacioncontable.preciosoles = +(this.operacioncontable.precioorigen * this.operacioncontable.tipocambiosoles).toFixed(2);
      this.operacioncontable.preciodolares = this.operacioncontable.precioorigen;
    }
    // otras monedas
    else
    {
      this.operacioncontable.preciosoles = +(this.operacioncontable.precioorigen * this.operacioncontable.tipocambiosoles).toFixed(2);
      this.operacioncontable.preciodolares = +(this.operacioncontable.preciosoles / this.operacioncontable.tipocambiodolares).toFixed(2);
    }

  }

  // tslint:disable-next-line:adjacent-overload-signatures
  // tslint:disable-next-line:typedef
  calcularIGVNoDom() 
  {    
    
    // tslint:disable-next-line:no-bitwise
    this.operacioncontable.impuesto     = (Number(this.operacioncontableForm.value.inafecto) * Number(this.operacioncontable.porcentajeimpuesto) / 100).toFixed(3);
   
    this.operacioncontableForm.controls['impuesto'].setValue(this.operacioncontable.impuesto);

  }

  // tslint:disable-next-line:typedef
  calcularMontosDetraccion()
  {          
    if ( this.operacioncontableForm.value.detraccion !== '0' && this.operacioncontableForm.value.detraccion !== null && this.operacioncontableForm.value.detraccion !== '')  
    {
      if ( this.operacioncontableForm.value.moneda === this.monedanacional ) 
      {
        this.operacioncontableForm.controls['basedetraccion'].setValue(this.operacioncontableForm.value.precioorigen);
        this.operacioncontableForm.controls['montodetraccion'].
          setValue((Number(this.operacioncontableForm.value.basedetraccion) * this.operacioncontable.tasadetraccion / 100).toFixed(2));
      }
      else
      {
        this.operacioncontableForm.controls['basedetraccion'].
          setValue((Number(this.operacioncontableForm.value.precioorigen) * Number(this.operacioncontableForm.value.tipocambiosoles)).toFixed(2));
        this.operacioncontableForm.controls['montodetraccion'].
          setValue((Number(this.operacioncontableForm.value.basedetraccion) * this.operacioncontable.tasadetraccion / 100).toFixed(2));
      }

    }
    else
    {      
      this.operacioncontableForm.controls['basedetraccion'].setValue(0.00);
      this.operacioncontableForm.controls['montodetraccion'].setValue(0.00);
    }
  }

  // tslint:disable-next-line:typedef
  async eliminarAsientoAutomatico()
  {         
    // for ( let i = 0; i < this.listaAsientoContable.length; i++ )
    for ( let i = this.listaAsientoContable.length - 1; i > -1; i-- )
    {
        if ( this.listaAsientoContable[i].tiporegistro === 'A' )
        {
          this.listaAsientoContable.splice(i, 1);
        }
        
    }    

  }

  // tslint:disable-next-line:typedef
  recalcularAsientoManual()
  {    
    const data = this.listaAsientoContable;
    // tslint:disable-next-line:prefer-for-of
    for ( let i = 0; i < data.length; i++ )
    {
      data[i].tipocambiosoles   = this.operacioncontableForm.value.tipocambiosoles;
      data[i].tipocambiodolares = this.operacioncontableForm.value.tipocambiodolares;
      // moneda nacional
      if ( this.operacioncontableForm.value.moneda === this.monedanacional)
      {
        data[i].montonacional = data[i].montoorigen;
        data[i].montodolares  = +(data[i].montoorigen / data[i].tipocambiodolares).toFixed(2);
      }
      // moneda dólares
      else if ( this.operacioncontableForm.value.moneda === this.monedadolares)
      {
        data[i].montonacional = +(data[i].montoorigen * data[i].tipocambiosoles).toFixed(2);
        data[i].montodolares  = data[i].montoorigen;
      }
      // otras monedas
      else
      {
        data[i].montonacional = +(data[i].montoorigen * data[i].tipocambiosoles).toFixed(2);
        data[i].montodolares  = +(data[i].montonacional / data[i].tipocambiodolares).toFixed(2);
      }
       
    }

    this.listaAsientoContable = data;

  }

  async consultarEnte(entidad: string): Promise<CreateEnteDto>
  {
    try {

      // tslint:disable-next-line:max-line-length
      this.ente = (await this.enteService.consultar(this.operacioncontable.empresa, entidad ).toPromise());
      return this.ente as CreateEnteDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Socio de Negocio', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.ente = new CreateEnteDto();
    }   

  }

  async consultarTipoCambio(moneda: string): Promise<CreateTipoCambioDto>
  {
    try 
    {      
      // tslint:disable-next-line:max-line-length
      this.tipocambio = (await this.tipocambioService.consultarDia(this.operacioncontable.empresa, this.operacioncontableForm.value.fechaemision, moneda, 'N' ).toPromise());
    
      if (this.tipocambio.tipocambiocompra === -1)
      {
        this._matSnackBar.open('Falta ingresar tipo de cambio para la fecha de emisión', 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
      }

      return this.tipocambio as CreateTipoCambioDto;  
      
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Consultar Tipo de Cambio', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.tipocambio = new CreateTipoCambioDto();
    }

  }

  async consultarIGV(): Promise<ViewValorDto>
  {
    try {

      // tslint:disable-next-line:max-line-length
      this.igv = (await this.valorfechaService.consultarDia(this.operacioncontable.empresa, 'IGV', this.operacioncontableForm.value.fechaemision ).toPromise());
      return this.igv as ViewValorDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, IGV', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.igv = new ViewValorDto();
    }   

  }

  async consultarDetraccion(): Promise<CreateDetraccionDto>
  {
    try {

      // tslint:disable-next-line:max-line-length
      this.detraccion = (await this.detraccionService.consultar(this.operacioncontable.empresa, this.operacioncontableForm.value.detraccion ).toPromise());
      return this.detraccion as CreateDetraccionDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Detracción', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.detraccion = new CreateDetraccionDto();
    }   

  }

  // tslint:disable-next-line:typedef
  getTotalImpOrigenDebe() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'D');
    return Number((this.listaTotales.map(t => t.montoorigen).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpOrigenHaber() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'H');
    return Number((this.listaTotales.map(t => t.montoorigen).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpNacionalDebe() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'D');
    return Number((this.listaTotales.map(t => t.montonacional).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpNacionalHaber() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'H');
    return Number((this.listaTotales.map(t => t.montonacional).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpDolaresDebe() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'D');
    return Number((this.listaTotales.map(t => t.montodolares).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpDolaresHaber() {
    this.listaTotales = this.listaAsientoContableNaturaleza.filter(x => x.tipoanotacion === 'H');
    return Number((this.listaTotales.map(t => t.montodolares).reduce((acc, value) => acc + value, 0)).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpOrigen() {
    return Number((this.getTotalImpOrigenDebe() - this.getTotalImpOrigenHaber()).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpNacional() {
    return Number((this.getTotalImpNacionalDebe() - this.getTotalImpNacionalHaber()).toFixed(2));
  }
  // tslint:disable-next-line:typedef
  getTotalImpDolares() {
    return Number((this.getTotalImpDolaresDebe() - this.getTotalImpDolaresHaber()).toFixed(2));
  }
  
  // genera el asiento automático
  // tslint:disable-next-line:typedef
  async preGenerarAsientoAutomatico()
  {
    
    // Validar campos obligatorios
    this.mensaje = '';

    if (this.operacioncontableForm.value.tipocambiosoles <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Tipo de cambio, ';
    }

    if (this.operacioncontableForm.value.precioorigen <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Montos, ';
    }

    if (this.operacioncontableForm.value.entidad === '' ) 
    {
      this.mensaje = this.mensaje + 'Proveedor, ';
    }

    if (this.operacioncontableForm.value.numeroserie === '' ) 
    {
      this.mensaje = this.mensaje + 'Número Serie, ';
    }

    if (this.operacioncontableForm.value.numerocomprobanteini === '' ) 
    {
      this.mensaje = this.mensaje + 'Número Comprobante, ';
    }

    if (this.operacioncontableForm.value.glosa === '' ) 
    {
      this.mensaje = this.mensaje + 'Glosa, ';
    }

    if (this.mensaje !== '')
    {      
      this.mensaje = this.mensaje.slice(0, -2);

      this._matSnackBar.open('Falta ingresar: ' + this.mensaje + '. Ingrese los campos correctamente', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });

    }
    else
    {
      // Asiento Automáticos ya existen
      this.mensaje = '';
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaAsientoContable.length; i++ )
      {
        if ( this.listaAsientoContable[i].tiporegistro === 'A')
        {
          this.mensaje = 'A';
        }
      }
  
      if (this.mensaje === 'A')
      {      
        this.dialogRef = this._matDialog.open(MessageboxComponent, {
          panelClass: 'confirm-form-dialog',
          data      : {
              titulo: 'Asiento Automático, ya existe',
              subTitulo: '¿Desea continuar?',
              botonAceptar: 'Si',
              botonCancelar: 'No'    
          }
        });
  
        this.dialogRef.afterClosed().subscribe(result => 
          {
            if ( localStorage.getItem(MESSAGEBOX) === 'S' )
            {
              localStorage.setItem(MESSAGEBOX, 'N');
              // Eliminar los registros automáticos
              this.eliminarAsientoAutomatico().then(res => {
                // console.log(res);
                // Generara asiento automáticos
                this.generarAsientoAutomatico();  
              }               
              );   
            }
          }        
        );
      }    
      else
      {      
        this.eliminarAsientoAutomatico().then(res => {
          // console.log(res);
          // Generara asiento automáticos
          this.generarAsientoAutomatico();  
        }               
        ); 
      }
    }

  }
  
  // genera los registros automáticos
  // tslint:disable-next-line:typedef
  async generarAsientoAutomatico()
  {
    let proveedorrel = 'N';
    let cuentaigvnodomaplica = '';
    let cuentaigvnodompagar = '';
    let cuentadetraccion = '';
    let notacredito = '';
    let notadebito = '';
    

    // Validar la configuración de Tipo Documento - Moneda - Cuenta Contable
    this.tipodocmoncta = this.tipodocmoncta ? await this.consultarTipoDocMonCta() : new CreateTipoDocMonCtaDto();  

    if (this.tipodocmoncta.cuentacompra === null || this.tipodocmoncta.cuentacomprarel === null ||
        this.tipodocmoncta.cuentacompra === '' || this.tipodocmoncta.cuentacomprarel === '' )
    {
      this._matSnackBar.open('Actualizar la configuración: Tipo Documento - Moneda - Cuenta Contable', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }
    
    // Obtener IGV
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'IGV_NODOMAPLICA') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: IGV', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    cuentaigvnodomaplica = this.parametro.valor;

    // Obtener IGV No Domiciliado
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'IGV_NODOMPAGAR') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: IGV', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }

    cuentaigvnodompagar = this.parametro.valor;

    // Obtener nota crédito
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'NOTACREDITO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Nota Crédito', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }    
    
    notacredito = this.parametro.valor;

    // Obtener nota crédito
    this.parametro = this.parametro ? await this.consultarParametro('CON', 'NOTADEBITO') : new CreateParametroDto();  

    if (this.parametro.valor === null || this.parametro.valor === '')
    {
      this._matSnackBar.open('Actualizar la configuración: Nota Débito', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }    
    
    notadebito = this.parametro.valor;

    // Cuenta Detracción
    this.ente.ruc       = this.ruc;
    this.ente.entidad   = this.operacioncontableForm.value.entidad;
    
    this.valorcontador = this.valorcontador ? await this.contarProveedorRel() : new ViewValorContadorDto();  
 
    if (this.valorcontador.contador === 1)
    {

      proveedorrel = 'S';
      
      // Cuenta detracción de proveedor relacionado
      this.parametro = this.parametro ? await this.consultarParametro('CON', 'DET_CUENTAREL') : new CreateParametroDto();  

      if (this.parametro.valor === null || this.parametro.valor === '')
      {
        this._matSnackBar.open('Actualizar la configuración: Cuenta Detracción de Proveedor Relacionado', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });
      }
      
      cuentadetraccion = this.parametro.valor;

    }
    else 
    {
      // Cuenta Detracción de proveedor NO relacionado
      this.parametro = this.parametro ? await this.consultarParametro('CON', 'DET_CUENTA') : new CreateParametroDto();  

      if (this.parametro.valor === null || this.parametro.valor === '')
      {
        this._matSnackBar.open('Actualizar la configuración: Cuenta Detracción de Proveedor', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });
      }   

      cuentadetraccion = this.parametro.valor;

    }
          
    // Genera los asientos automáticos
    if (this.tipodocmoncta.cuentacompra !== null && this.tipodocmoncta.cuentacomprarel !== null &&
        this.tipodocmoncta.cuentacompra !== '' && this.tipodocmoncta.cuentacomprarel !== '' &&
        cuentaigvnodomaplica !== null && cuentaigvnodomaplica !== '' && 
        cuentaigvnodompagar !== null && cuentaigvnodompagar !== '' &&
        notacredito !== null && notacredito !== '' && notadebito !== null && notadebito !== '' &&
        this.monedanacional !== null && this.monedanacional !== '' && this.monedadolares !== null && this.monedadolares !== '' 
        )
    {

      // Registro Precio
      this.registrocontable = new CreateRegistroContableDto();
      this.registrocontable.empresa = this.operacioncontable.empresa;
      this.registrocontable.ejerciciocontable = this.operacioncontable.ejerciciocontable;
      // this.registrocontable.asientocontable = 0;	
      this.registrocontable.registrocontable = 1001;
      if (proveedorrel === 'S')
      {
        this.registrocontable.cuentacontable =  this.tipodocmoncta.cuentacomprarel;
      }
      else
      {
        this.registrocontable.cuentacontable =  this.tipodocmoncta.cuentacompra;
      }
      this.registrocontable.entidad = this.operacioncontableForm.value.entidad;
      this.registrocontable.nombreentidad = this.operacioncontableForm.value.ruc + '-' + this.obtenerTextoSelect(this.operacioncontableForm.value.entidad, 'listaProveedor');
      this.registrocontable.ruc = this.operacioncontableForm.value.ruc;
      if (this.operacioncontableForm.value.tipodocumento !== notacredito)
      {
        this.registrocontable.tipoanotacion = 'H';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('H', 'listaTipoAnotacion');
      }
      else
      {
        this.registrocontable.tipoanotacion = 'D';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('D', 'listaTipoAnotacion');
      }      
      this.registrocontable.tiporegistro = 'A';
      this.registrocontable.moneda = this.operacioncontableForm.value.moneda;
      this.registrocontable.tipocambio = this.operacioncontableForm.value.tipocambio;
      this.registrocontable.tipocambiosoles = Number(this.operacioncontableForm.value.tipocambiosoles);
      this.registrocontable.tipocambiodolares = Number(this.operacioncontableForm.value.tipocambiodolares);
      this.registrocontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
      this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.operacioncontableForm.value.tipodocumento, 'listaTipoDocumento');
      this.registrocontable.numeroserie = this.operacioncontableForm.value.numeroserie;
      this.registrocontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
      this.registrocontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
      if (this.operacioncontableForm.value.fechaemision !== null && this.operacioncontableForm.value.fechaemision !== '')
      {
        this.registrocontable.fechaemision = moment(this.operacioncontableForm.value.fechaemision).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechaemision = '';
      }
      
      if (this.operacioncontableForm.value.fechavencimiento !== null && this.operacioncontableForm.value.fechavencimiento !== '')
      {
        this.registrocontable.fechavencimiento = moment(this.operacioncontableForm.value.fechavencimiento).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechavencimiento = null;
      }
      
      // this.registrocontable.mediopago = this.operacioncontableForm.value.;
      // this.registrocontable.tablacentrocosto = this.operacioncontableForm.value.;
      // this.registrocontable.centrocosto = this.operacioncontableForm.value.;
      // this.registrocontable.porcentajecentrocosto = this.operacioncontableForm.value.;	
      this.registrocontable.correlativoorigen = 0;
      this.registrocontable.glosa = this.operacioncontableForm.value.glosa;
      this.registrocontable.incluirregistrocompra = 'S';
      this.registrocontable.indicadorcalculo = 'S';
      this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
      this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
      this.registrocontable.estado = 'A';
      this.registrocontable.creacionUsuario = this.valoresiniciales.username;
      this.registrocontable.modificacionUsuario = this.valoresiniciales.username;

      this.registrocontable.montoorigen = Number(this.operacioncontableForm.value.precioorigen);      
      if (this.operacioncontableForm.value.moneda === this.monedanacional)
      {        
        this.registrocontable.montonacional = this.operacioncontableForm.value.precioorigen;
        this.registrocontable.montodolares = (this.operacioncontableForm.value.precioorigen / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        // tslint:disable-next-line:max-line-length
        // this.registrocontable.montodolares = Number((Number(this.operacioncontableForm.value.precioorigen) / Number(this.operacioncontableForm.value.tipocambiodolares)).toFixed(2));
      }

      else if (this.operacioncontableForm.value.moneda === this.monedadolares)
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.precioorigen * this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        this.registrocontable.montodolares = this.operacioncontableForm.value.precioorigen;
      }
      else
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.precioorigen * this.operacioncontableForm.value.tipocambiosoles).toFixed(2);
        this.registrocontable.montodolares = (this.registrocontable.montonacional / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
      }

      this.registrocontable.montonacional = Number(this.registrocontable.montonacional);
      this.registrocontable.montodolares = Number(this.registrocontable.montodolares);

      this.listaAsientoContable.push(this.registrocontable);

      // Registro IGV cuentaigvnodomaplica
      this.registrocontable = new CreateRegistroContableDto();
      this.registrocontable.empresa = this.operacioncontable.empresa;
      this.registrocontable.ejerciciocontable = this.operacioncontable.ejerciciocontable;
      this.registrocontable.registrocontable = 1002;
      this.registrocontable.cuentacontable =  cuentaigvnodomaplica;
      this.registrocontable.entidad = this.operacioncontableForm.value.entidad;
      this.registrocontable.nombreentidad = this.operacioncontableForm.value.ruc + '-' + this.obtenerTextoSelect(this.operacioncontableForm.value.entidad, 'listaProveedor');
      this.registrocontable.ruc = this.operacioncontableForm.value.ruc;
      if (this.operacioncontableForm.value.tipodocumento !== notacredito)
      {
        this.registrocontable.tipoanotacion = 'D';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('D', 'listaTipoAnotacion');
      }
      else
      {
        this.registrocontable.tipoanotacion = 'H';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('H', 'listaTipoAnotacion');
      }      
      this.registrocontable.tiporegistro = 'A';
      this.registrocontable.moneda = this.operacioncontableForm.value.moneda;
      this.registrocontable.tipocambio = this.operacioncontableForm.value.tipocambio;
      this.registrocontable.tipocambiosoles = Number(this.operacioncontableForm.value.tipocambiosoles);
      this.registrocontable.tipocambiodolares = Number(this.operacioncontableForm.value.tipocambiodolares);
      this.registrocontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
      this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.operacioncontableForm.value.tipodocumento, 'listaTipoDocumento');
      this.registrocontable.numeroserie = this.operacioncontableForm.value.numeroserie;
      this.registrocontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
      this.registrocontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
      if (this.operacioncontableForm.value.fechaemision !== null && this.operacioncontableForm.value.fechaemision !== '')
      {
        this.registrocontable.fechaemision = moment(this.operacioncontableForm.value.fechaemision).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechaemision = '';
      }
      if (this.operacioncontableForm.value.fechavencimiento !== null && this.operacioncontableForm.value.fechavencimiento !== '')
      {
        this.registrocontable.fechavencimiento = moment(this.operacioncontableForm.value.fechavencimiento).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechavencimiento = '';
      }      
      this.registrocontable.correlativoorigen = 0;
      this.registrocontable.glosa = this.operacioncontableForm.value.glosa;
      this.registrocontable.incluirregistrocompra = 'S';
      this.registrocontable.indicadorcalculo = 'S';
      this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
      this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
      this.registrocontable.estado = 'A';
      this.registrocontable.creacionUsuario = this.valoresiniciales.username;
      this.registrocontable.modificacionUsuario = this.valoresiniciales.username;
      
      this.registrocontable.montoorigen = Number(this.operacioncontableForm.value.impuesto);      
      if (this.operacioncontableForm.value.moneda === this.monedanacional)
      {        
        this.registrocontable.montonacional = this.operacioncontableForm.value.impuesto;
        this.registrocontable.montodolares = (this.operacioncontableForm.value.impuesto / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
      }
      
      else if (this.operacioncontableForm.value.moneda === this.monedadolares)
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.impuesto * this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        this.registrocontable.montodolares = this.operacioncontableForm.value.impuesto;
      }
      else
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.impuesto * this.operacioncontableForm.value.tipocambiosoles).toFixed(2);
        this.registrocontable.montodolares = (this.registrocontable.montonacional / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
      }
      
      this.registrocontable.montonacional = Number(this.registrocontable.montonacional);
      this.registrocontable.montodolares = Number(this.registrocontable.montodolares);
      
      if (this.registrocontable.montonacional > 0)
      {
        this.listaAsientoContable.push(this.registrocontable);
      } 

      // Registro IGV cuentaigvnodompagar
      this.registrocontable = new CreateRegistroContableDto();
      this.registrocontable.empresa = this.operacioncontable.empresa;
      this.registrocontable.ejerciciocontable = this.operacioncontable.ejerciciocontable;
      this.registrocontable.registrocontable = 1003;
      this.registrocontable.cuentacontable =  cuentaigvnodompagar;
      this.registrocontable.entidad = this.operacioncontableForm.value.entidad;
      this.registrocontable.nombreentidad = this.operacioncontableForm.value.ruc + '-' + this.obtenerTextoSelect(this.operacioncontableForm.value.entidad, 'listaProveedor');
      this.registrocontable.ruc = this.operacioncontableForm.value.ruc;
      if (this.operacioncontableForm.value.tipodocumento !== notacredito)
      {
        this.registrocontable.tipoanotacion = 'H';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('H', 'listaTipoAnotacion');
      }
      else
      {
        this.registrocontable.tipoanotacion = 'D';
        this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('D', 'listaTipoAnotacion');
      }      
      this.registrocontable.tiporegistro = 'A';
      this.registrocontable.moneda = this.operacioncontableForm.value.moneda;
      this.registrocontable.tipocambio = this.operacioncontableForm.value.tipocambio;
      this.registrocontable.tipocambiosoles = Number(this.operacioncontableForm.value.tipocambiosoles);
      this.registrocontable.tipocambiodolares = Number(this.operacioncontableForm.value.tipocambiodolares);
      this.registrocontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
      this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.operacioncontableForm.value.tipodocumento, 'listaTipoDocumento');
      this.registrocontable.numeroserie = this.operacioncontableForm.value.numeroserie;
      this.registrocontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
      this.registrocontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
      if (this.operacioncontableForm.value.fechaemision !== null && this.operacioncontableForm.value.fechaemision !== '')
      {
        this.registrocontable.fechaemision = moment(this.operacioncontableForm.value.fechaemision).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechaemision = '';
      }
      if (this.operacioncontableForm.value.fechavencimiento !== null && this.operacioncontableForm.value.fechavencimiento !== '')
      {
        this.registrocontable.fechavencimiento = moment(this.operacioncontableForm.value.fechavencimiento).format('DD/MM/YYYY');
      }
      else
      {
        this.registrocontable.fechavencimiento = '';
      }      
      this.registrocontable.correlativoorigen = 0;
      this.registrocontable.glosa = this.operacioncontableForm.value.glosa;
      this.registrocontable.incluirregistrocompra = 'S';
      this.registrocontable.indicadorcalculo = 'S';
      this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
      this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
      this.registrocontable.estado = 'A';
      this.registrocontable.creacionUsuario = this.valoresiniciales.username;
      this.registrocontable.modificacionUsuario = this.valoresiniciales.username;
      
      this.registrocontable.montoorigen = Number(this.operacioncontableForm.value.impuesto);      
      if (this.operacioncontableForm.value.moneda === this.monedanacional)
      {        
        this.registrocontable.montonacional = this.operacioncontableForm.value.impuesto;
        this.registrocontable.montodolares = (this.operacioncontableForm.value.impuesto / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
      }
      
      else if (this.operacioncontableForm.value.moneda === this.monedadolares)
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.impuesto * this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        this.registrocontable.montodolares = this.operacioncontableForm.value.impuesto;
      }
      else
      {
        this.registrocontable.montonacional = (this.operacioncontableForm.value.impuesto * this.operacioncontableForm.value.tipocambiosoles).toFixed(2);
        this.registrocontable.montodolares = (this.registrocontable.montonacional / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
      }
      
      this.registrocontable.montonacional = Number(this.registrocontable.montonacional);
      this.registrocontable.montodolares = Number(this.registrocontable.montodolares);
      
      if (this.registrocontable.montonacional > 0)
      {
        this.listaAsientoContable.push(this.registrocontable);
      } 

      // Registro Detracción
      if (Number(this.operacioncontableForm.value.montodetraccion) !== 0)
      {
        // Cuenta Detracción
        this.registrocontable = new CreateRegistroContableDto();
        this.registrocontable.empresa = this.operacioncontable.empresa;
        this.registrocontable.ejerciciocontable = this.operacioncontable.ejerciciocontable;
        this.registrocontable.registrocontable = 1004;
        this.registrocontable.cuentacontable =  cuentadetraccion;
        this.registrocontable.entidad = this.operacioncontableForm.value.entidad;
        this.registrocontable.nombreentidad = this.operacioncontableForm.value.ruc + '-' + this.obtenerTextoSelect(this.operacioncontableForm.value.entidad, 'listaProveedor');
        this.registrocontable.ruc = this.operacioncontableForm.value.ruc;
        if (this.operacioncontableForm.value.tipodocumento !== notacredito)
        {
          this.registrocontable.tipoanotacion = 'H';
          this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('H', 'listaTipoAnotacion');
        }
        else
        {
          this.registrocontable.tipoanotacion = 'D';
          this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('D', 'listaTipoAnotacion');
        }      
        this.registrocontable.tiporegistro = 'A';
        this.registrocontable.moneda = this.operacioncontableForm.value.moneda;
        this.registrocontable.tipocambio = this.operacioncontableForm.value.tipocambio;
        this.registrocontable.tipocambiosoles = Number(this.operacioncontableForm.value.tipocambiosoles);
        this.registrocontable.tipocambiodolares = Number(this.operacioncontableForm.value.tipocambiodolares);
        this.registrocontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
        this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.operacioncontableForm.value.tipodocumento, 'listaTipoDocumento');
        this.registrocontable.numeroserie = this.operacioncontableForm.value.numeroserie;
        this.registrocontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
        this.registrocontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
        if (this.operacioncontableForm.value.fechaemision !== null && this.operacioncontableForm.value.fechaemision !== '')
        {
          this.registrocontable.fechaemision = moment(this.operacioncontableForm.value.fechaemision).format('DD/MM/YYYY');
        }
        else
        {
          this.registrocontable.fechaemision = '';
        }
        if (this.operacioncontableForm.value.fechavencimiento !== null && this.operacioncontableForm.value.fechavencimiento !== '')
        {
          this.registrocontable.fechavencimiento = moment(this.operacioncontableForm.value.fechavencimiento).format('DD/MM/YYYY');
        }
        else
        {
          this.registrocontable.fechavencimiento = '';
        }      
        this.registrocontable.correlativoorigen = 0;
        this.registrocontable.glosa = this.operacioncontableForm.value.glosa;
        this.registrocontable.incluirregistrocompra = 'S';
        this.registrocontable.indicadorcalculo = 'S';
        this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
        this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
        this.registrocontable.estado = 'A';
        this.registrocontable.creacionUsuario = this.valoresiniciales.username;
        this.registrocontable.modificacionUsuario = this.valoresiniciales.username;
        
        this.registrocontable.montoorigen = Number(this.operacioncontableForm.value.montodetraccion);      
        if (this.operacioncontableForm.value.moneda === this.monedanacional)
        {        
          this.registrocontable.montonacional = this.operacioncontableForm.value.montodetraccion;
          this.registrocontable.montodolares = (this.operacioncontableForm.value.montodetraccion / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        }
        
        else if (this.operacioncontableForm.value.moneda === this.monedadolares)
        {
          this.registrocontable.montonacional = (this.operacioncontableForm.value.montodetraccion * this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
          this.registrocontable.montodolares = this.operacioncontableForm.value.montodetraccion;
        }
        else
        {
          this.registrocontable.montonacional = (this.operacioncontableForm.value.montodetraccion * this.operacioncontableForm.value.tipocambiosoles).toFixed(2);
          this.registrocontable.montodolares = (this.registrocontable.montonacional / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        }
        
        this.registrocontable.montonacional = Number(this.registrocontable.montonacional);
        this.registrocontable.montodolares = Number(this.registrocontable.montodolares);
        
        this.listaAsientoContable.push(this.registrocontable);
        
        // Cuenta 
        this.registrocontable = new CreateRegistroContableDto();
        this.registrocontable.empresa = this.operacioncontable.empresa;
        this.registrocontable.ejerciciocontable = this.operacioncontable.ejerciciocontable;
        this.registrocontable.registrocontable = 1005;
        if (proveedorrel === 'S')
        {
          this.registrocontable.cuentacontable =  this.tipodocmoncta.cuentacomprarel;
        }
        else
        {
          this.registrocontable.cuentacontable =  this.tipodocmoncta.cuentacompra;
        }
        this.registrocontable.entidad = this.operacioncontableForm.value.entidad;
        this.registrocontable.nombreentidad = this.operacioncontableForm.value.ruc + '-' + this.obtenerTextoSelect(this.operacioncontableForm.value.entidad, 'listaProveedor');
        this.registrocontable.ruc = this.operacioncontableForm.value.ruc;
        if (this.operacioncontableForm.value.tipodocumento !== notacredito)
        {
          this.registrocontable.tipoanotacion = 'D';
          this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('D', 'listaTipoAnotacion');
        }
        else
        {
          this.registrocontable.tipoanotacion = 'H';
          this.registrocontable.descripciontipoanotacion = this.obtenerTextoSelect('H', 'listaTipoAnotacion');
        }      
        this.registrocontable.tiporegistro = 'A';
        this.registrocontable.moneda = this.operacioncontableForm.value.moneda;
        this.registrocontable.tipocambio = this.operacioncontableForm.value.tipocambio;
        this.registrocontable.tipocambiosoles = Number(this.operacioncontableForm.value.tipocambiosoles);
        this.registrocontable.tipocambiodolares = Number(this.operacioncontableForm.value.tipocambiodolares);
        this.registrocontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
        this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.operacioncontableForm.value.tipodocumento, 'listaTipoDocumento');
        this.registrocontable.numeroserie = this.operacioncontableForm.value.numeroserie;
        this.registrocontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
        this.registrocontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
        if (this.operacioncontableForm.value.fechaemision !== null && this.operacioncontableForm.value.fechaemision !== '')
        {
          this.registrocontable.fechaemision = moment(this.operacioncontableForm.value.fechaemision).format('DD/MM/YYYY');
        }
        else
        {
          this.registrocontable.fechaemision = '';
        }
        if (this.operacioncontableForm.value.fechavencimiento !== null && this.operacioncontableForm.value.fechavencimiento !== '')
        {
          this.registrocontable.fechavencimiento = moment(this.operacioncontableForm.value.fechavencimiento).format('DD/MM/YYYY');
        }
        else
        {
          this.registrocontable.fechavencimiento = '';
        }      
        this.registrocontable.correlativoorigen = 0;
        this.registrocontable.glosa = this.operacioncontableForm.value.glosa;
        this.registrocontable.incluirregistrocompra = 'S';
        this.registrocontable.indicadorcalculo = 'S';
        this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
        this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
        this.registrocontable.estado = 'A';
        this.registrocontable.creacionUsuario = this.valoresiniciales.username;
        this.registrocontable.modificacionUsuario = this.valoresiniciales.username;
        
        this.registrocontable.montoorigen = Number(this.operacioncontableForm.value.montodetraccion);      
        if (this.operacioncontableForm.value.moneda === this.monedanacional)
        {        
          this.registrocontable.montonacional = this.operacioncontableForm.value.montodetraccion;
          this.registrocontable.montodolares = (this.operacioncontableForm.value.montodetraccion / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        }
        
        else if (this.operacioncontableForm.value.moneda === this.monedadolares)
        {
          this.registrocontable.montonacional = (this.operacioncontableForm.value.montodetraccion * this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
          this.registrocontable.montodolares = this.operacioncontableForm.value.montodetraccion;
        }
        else
        {
          this.registrocontable.montonacional = (this.operacioncontableForm.value.montodetraccion * this.operacioncontableForm.value.tipocambiosoles).toFixed(2);
          this.registrocontable.montodolares = (this.registrocontable.montonacional / this.operacioncontableForm.value.tipocambiodolares).toFixed(2);
        }
        
        this.registrocontable.montonacional = Number(this.registrocontable.montonacional);
        this.registrocontable.montodolares = Number(this.registrocontable.montodolares);
        
        this.listaAsientoContable.push(this.registrocontable);

      }

      // actualiza la vista de los registros por naturaleza
      this.obtenerRegistroContableNaturaleza();

      // Inicializa la vista de los registros ppor destino
      this.obtenerRegistroContableDestino(-1);

    }

  }

  async consultarTipoDocMonCta(): Promise<CreateTipoDocMonCtaDto>
  {
    try {

      // tslint:disable-next-line:max-line-length
      this.tipodocmoncta = (await this.tipodocmonctaService.consultar(this.operacioncontable.empresa, this.operacioncontable.ejerciciocontable,
                                                                      this.operacioncontableForm.value.tipodocumento, 
                                                                      this.operacioncontableForm.value.moneda).toPromise());
      return this.tipodocmoncta as CreateTipoDocMonCtaDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Tipo documento - Moneda - Cuenta Contable', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.tipodocmoncta = new CreateTipoDocMonCtaDto();
    }   

  }

  async consultarParametro(modulo: string, parametro: string): Promise<CreateParametroDto> 
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.parametro = (await this.parametroService.consultar(this.operacioncontable.empresa, modulo, parametro).toPromise()); 
      return this.parametro as CreateParametroDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Parámetros', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.parametro = new CreateParametroDto();
    }   

  }

  async contarProveedorRel(): Promise<ViewValorContadorDto>
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.valorcontador = (await this.enteService.contarProveedorRel(this.ente).toPromise()); 
      return this.valorcontador as ViewValorContadorDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Proveedor Relacionado', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.valorcontador = new ViewValorContadorDto();
    }   

  }

  // tslint:disable-next-line:typedef
  obtenerTextoSelect(codigo: string, lista: string)
  {
   
    // Proveedor
    if (lista === 'listaProveedor')
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaProveedor.length; i++ )      
      {
        if ( this.listaProveedor[i].codigo === codigo )
        {
         return this.listaProveedor[i].nombre;
        }
      }
    }
    
    // Tipo Anotación
    if (lista === 'listaTipoAnotacion') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaTipoAnotacion.length; i++ )      
      {
        if ( this.listaTipoAnotacion[i].valor === codigo )
        {
         return this.listaTipoAnotacion[i].descripcion;
        }
      }
    }
    // Centro Costo
    if (lista === 'listaCentroCosto') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCentroCosto.length; i++ )      
      {
        if ( this.listaCentroCosto[i].codigo === codigo )
        {
         return this.listaCentroCosto[i].descripcion;
        }
      }
    } 

    // Medio Pago
    if (lista === 'listaMedioPago') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaMedioPago.length; i++ )      
      {
        if ( this.listaMedioPago[i].valor === codigo )
        {
         return this.listaMedioPago[i].valor + '-' + this.listaMedioPago[i].descripcion;
        }
      }
    }

    // Flujo Ejectivo
    if (lista === 'listaFlujoEfectivo') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaFlujoEfectivo.length; i++ )      
      {
        if ( this.listaFlujoEfectivo[i].valor === codigo )
        {
         return this.listaFlujoEfectivo[i].valor + '-' + this.listaFlujoEfectivo[i].descripcion;
        }
      }
    }

    // Tipo Documento  
    if (lista === 'listaTipoDocumento')
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaTipoDocumento.length; i++ )      
      {
        if ( this.listaTipoDocumento[i].codigo === codigo )
        {
         return this.listaTipoDocumento[i].codigo + '-' + this.listaTipoDocumento[i].descripcion;
        }
      }
      // this.listaFound = this.listaTipoDocumento.find(e => e.codigo === codigo);
     
      // if (this.listaFound !== undefined)
      // {
      //   return this.listaTipoDocumento[0].descripcion;
      // }
      // else
      // {
      //   return '';
      // }      
    }
  }  

  // tslint:disable-next-line:typedef
  async preGrabarOperacionContable()
  {
    const periodocontable = this.operacioncontableForm.value.fechaoperacion.substr(5, 2);

    // Validar ejercicio abierto
    this.ejercicio = this.ejercicio ? await this.consultarEjercicio() : new CreateEjercicioDto();  

    if (this.ejercicio.indicador === null || this.ejercicio.indicador === '' || this.ejercicio.indicador === 'C')
    {
      this._matSnackBar.open('Ejercicio Contable se encuentra cerrado, no es posible grabar la operación', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
    }
    
    // Validar período abierto
    this.periodo = this.periodo ? await this.consultarPeriodo(periodocontable) : new CreatePeriodoDto();  
 
    if (this.periodo.indicador === null || this.periodo.indicador === '' || this.periodo.indicador === 'C')
    {
      this._matSnackBar.open('Período Contable se encuentra cerrado, no es posible grabar la operación', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
      return;
    }

    // Validar campos obligatorios
    this.mensaje = '';

    if (this.operacioncontableForm.value.tipocambiosoles <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Tipo de cambio, ';
    }

    if (this.operacioncontableForm.value.precioorigen <= 0 ) 
    {
      this.mensaje = this.mensaje + 'Montos, ';
    }

    if (this.operacioncontableForm.value.entidad === '' ) 
    {
      this.mensaje = this.mensaje + 'Proveedor, ';
    }

    if (this.operacioncontableForm.value.numeroserie === '' ) 
    {
      this.mensaje = this.mensaje + 'Número Serie, ';
    }

    if (this.operacioncontableForm.value.numerocomprobanteini === '' ) 
    {
      this.mensaje = this.mensaje + 'Número Comprobante, ';
    }

    if (this.operacioncontableForm.value.glosa === '' ) 
    {
      this.mensaje = this.mensaje + 'Glosa, ';
    }

    if (this.mensaje !== '')
    {      
      this.mensaje = this.mensaje.slice(0, -2);

      this._matSnackBar.open('Falta ingresar: ' + this.mensaje + '. Ingrese los campos correctamente', 'Ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar']
      });
      
      return;
    }
    
    // para operaciones CERRADAS
    if (this.operacioncontableForm.value.estado === 'C' ) 
    {
      if (this.listaAsientoContable.length === 0)
      {
        this._matSnackBar.open('No se generarón asientos contables...', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });                
        return;        
      }

      if (this.getTotalImpOrigenDebe() - this.getTotalImpOrigenHaber() !== 0)
      {
        this._matSnackBar.open('Montos Moneda Origen, DEBE y HABER no cuadran', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });                
        return;        
      }

      if (this.getTotalImpNacionalDebe() - this.getTotalImpNacionalHaber() !== 0)
      {
        this._matSnackBar.open('Montos Moneda Nacional, DEBE y HABER no cuadran', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });                
        return;        
      }

      if (this.getTotalImpDolaresDebe() - this.getTotalImpDolaresHaber() !== 0)
      {
        this._matSnackBar.open('Montos Moneda Dólares, DEBE y HABER no cuadran', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });                
        return;        
      }

    }

    // Si la operación es nueva, validar que no exista registrado el número de comprobante
    if (this.edit === false)
    {
      this.valorcontador = this.valorcontador ? await this.contarComprobante() : new ViewValorContadorDto();  
 
      if (this.valorcontador.contador === 0)
      {
        this.grabarOperacionContable();  
      }
      else
      {
        this._matSnackBar.open('Ya fue ingresado el número de comprobante', 'Ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar']
        });
      }
    }
    else
    {
      this.grabarOperacionContable();
    }

  }

  // tslint:disable-next-line:typedef
  async grabarOperacionContable()
  {
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Grabar Operación Contable',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');
          // Asignar datos al componente
          // this.operacioncontable.empresa            = this.valoresiniciales.empresa; // Valor ya asignado
          // this.operacioncontable.ejerciciocontable  = this.valoresiniciales.annoproceso; // Valor ya asignado
          // this.operacioncontable.operacioncontable = '' ; // Valor no editable o para crear en el servicio
          this.operacioncontable.periodocontable = this.operacioncontableForm.value.fechaoperacion.substr(5, 2);
          this.operacioncontable.tipooperacion = 'N' ;
          this.operacioncontable.fechaoperacion = this.operacioncontableForm.value.fechaoperacion;
          this.operacioncontable.fechaemision = this.operacioncontableForm.value.fechaemision;
          this.operacioncontable.fechavencimiento = this.operacioncontableForm.value.fechavencimiento;
          this.operacioncontable.entidad = this.operacioncontableForm.value.entidad;
          this.operacioncontable.ruc = this.operacioncontableForm.value.ruc;
          this.operacioncontable.entidadnodom = this.operacioncontableForm.value.entidadnodom;
          this.operacioncontable.rucnodom = this.operacioncontableForm.value.rucnodom;
          // this.operacioncontable.banco = '' ;
          // this.operacioncontable.cuentabancaria = '' ;
          this.operacioncontable.tipodocumento = this.operacioncontableForm.value.tipodocumento;
          this.operacioncontable.subdiario = this.operacioncontableForm.value.subdiario;
          this.operacioncontable.aduana = this.operacioncontableForm.value.aduana;
          this.operacioncontable.numeroserie = this.operacioncontableForm.value.numeroserie;
          this.operacioncontable.numerocomprobanteini = this.operacioncontableForm.value.numerocomprobanteini;
          this.operacioncontable.numerocomprobantefin = this.operacioncontableForm.value.numerocomprobantefin;
          this.operacioncontable.moneda = this.operacioncontableForm.value.moneda;
          this.operacioncontable.tipocambio = this.operacioncontableForm.value.tipocambio;
          this.operacioncontable.tipocambiosoles = this.operacioncontableForm.value.tipocambiosoles;
          this.operacioncontable.tipocambiodolares = this.operacioncontableForm.value.tipocambiodolares;
          this.operacioncontable.baseimponible = this.operacioncontableForm.value.baseimponible;
          this.operacioncontable.inafecto = this.operacioncontableForm.value.inafecto;
          this.operacioncontable.adicionalnodom = this.operacioncontableForm.value.adicionalnodom;
          // this.operacioncontable.indimpuestorenta = '' ;
          // this.operacioncontable.porcentajeimpuesto = 0; // Valor ya asignado
          this.operacioncontable.impuesto = this.operacioncontableForm.value.impuesto; 
          this.operacioncontable.deduccionnodom = this.operacioncontableForm.value.deduccionnodom;
          // this.operacioncontable.precioorigen = 0; // Valor ya asignado
          // this.operacioncontable.preciosoles = 0 ; // Valor ya asignado
          // this.operacioncontable.preciodolares = 0 ; // Valor ya asignado
          // this.operacioncontable.mediopago = '' ;
          // this.operacioncontable.flujoefectivo = '' ;
          // this.operacioncontable.bienservicio = this.operacioncontableForm.value.bienservicio; 
          // this.operacioncontable.detraccion = this.operacioncontableForm.value.detraccion; 
          // this.operacioncontable.tasadetraccion = 0; // Valor ya asignado
          // this.operacioncontable.numerodetraccion = this.operacioncontableForm.value.numerodetraccion; 
          // this.operacioncontable.fechadetraccion = this.operacioncontableForm.value.fechadetraccion; 
          // this.operacioncontable.basedetraccion = this.operacioncontableForm.value.basedetraccion; 
          // this.operacioncontable.montodetraccion = this.operacioncontableForm.value.montodetraccion; 
          // this.operacioncontable.tipodocumentoref = '' ; // Valor ya asignado
          // this.operacioncontable.ejerciciocontableref = 0 ; // Valor ya asignado
          // this.operacioncontable.operacioncontableref = this.operacioncontableForm.value.operacioncontableref; 
          // this.operacioncontable.numeroserieref = this.operacioncontableForm.value.numeroserieref; 
          // this.operacioncontable.numerocomprobanteiniref = this.operacioncontableForm.value.numerocomprobanteiniref; 
          // this.operacioncontable.fechaemisionref = this.operacioncontableForm.value.fechaemisionref; 
          // this.operacioncontable.tipocambiosolesref = 0 ; // Valor ya asignado
          // this.operacioncontable.baseimponibleref = this.operacioncontableForm.value.baseimponibleref; 
          // this.operacioncontable.inafectoref = this.operacioncontableForm.value.inafectoref; 
          // this.operacioncontable.impuestoref = this.operacioncontableForm.value.impuestoref;
          // this.operacioncontable.precioorigenref = this.operacioncontableForm.value.precioorigenref;
          // this.operacioncontable.girado = '' ;
          this.operacioncontable.glosa = this.operacioncontableForm.value.glosa;
          this.operacioncontable.tipodocumentonodom = this.operacioncontableForm.value.tipodocumentonodom;
          this.operacioncontable.numeroserienodom = this.operacioncontableForm.value.numeroserienodom;
          this.operacioncontable.annocomprobantenodom = this.operacioncontableForm.value.annocomprobantenodom;
          this.operacioncontable.numerocomprobantenodom = this.operacioncontableForm.value.numerocomprobantenodom;
          this.operacioncontable.vinculoeconomiconodom = this.operacioncontableForm.value.vinculoeconomiconodom;
          this.operacioncontable.tasaretencionnodom = this.operacioncontableForm.value.tasaretencionnodom;
          this.operacioncontable.impuestoretenidonodom = this.operacioncontableForm.value.impuestoretenidonodom;
          this.operacioncontable.convenionodom = this.operacioncontableForm.value.convenionodom;
          this.operacioncontable.exoneracionnodom = this.operacioncontableForm.value.exoneracionnodom;
          this.operacioncontable.tiporentanodom = this.operacioncontableForm.value.tiporentanodom;
          this.operacioncontable.modalidadnodom = this.operacioncontableForm.value.modalidadnodom;
          this.operacioncontable.articulonodom = (this.operacioncontableForm.value.articulonodom) ? 'S' : 'N';
          this.operacioncontable.columnabaseimponible = this.operacioncontableForm.value.columnabaseimponible;
          this.operacioncontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
          this.operacioncontable.estado = this.operacioncontableForm.value.estado;
          this.operacioncontable.creacionUsuario = this.valoresiniciales.username;
          this.operacioncontable.creacionFecha = '2020-01-01';
          this.operacioncontable.modificacionUsuario = this.valoresiniciales.username;
          this.operacioncontable.modificacionFecha = '2020-01-01';
          this.operacioncontable.registrocontable = this.listaAsientoContable;
          // console.log(this.operacioncontable);
          // return;

          // Grabar Operación Contable
          this.registrooperacion = this.registrooperacion ? await this.registrarOperacion() : new CreateOperacionContableDto();  
          if (this.registrooperacion.operacioncontable !== null && this.registrooperacion.operacioncontable !== '')
          {
            this._matSnackBar.open('Grabación Exitosa de la Operación Contable', 'Ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar']
            });

            this.edit = true;

            this.operacioncontableForm.controls['operacioncontable'].setValue(this.registrooperacion.operacioncontable);

            const misDatos =  { 
              action: 'editar',
              parametro1: this.registrooperacion.operacioncontable
            };
            
            localStorage.setItem(DATOS, JSON.stringify(misDatos));

            this.ngOnInit();
            // return; 

          }
          else
          {
            this._matSnackBar.open('Error, NO se pudo Grabar la Operación Contable', 'Ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar']
            });
            return;
          }
        
        }
      }        
    );   
    
  }

  async consultarEjercicio(): Promise<CreateEjercicioDto> 
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.ejercicio = (await this.ejercicioService.consultar(this.operacioncontable.empresa, this.operacioncontable.ejerciciocontable).toPromise()); 
      return this.ejercicio as CreateEjercicioDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Ejercicio Contable', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.ejercicio = new CreateEjercicioDto();
    }   

  }

  async consultarPeriodo(periodo: string): Promise<CreatePeriodoDto> 
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.periodo = (await this.periodoService.consultar(this.operacioncontable.empresa, this.operacioncontable.ejerciciocontable, periodo).toPromise()); 
      return this.periodo as CreatePeriodoDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Período Contable', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.periodo = new CreatePeriodoDto();
    }   

  }

  async contarComprobante(): Promise<ViewValorContadorDto>
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.valorcontador = (await this.operacioncontableService.contarComprobante(this.operacioncontable.empresa, 
                                                                                  this.operacioncontableForm.value.entidad,
                                                                                  this.operacioncontableForm.value.numeroserie,
                                                                                  this.operacioncontableForm.value.numerocomprobanteini).toPromise()); 
      return this.valorcontador as ViewValorContadorDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Buscar comprobante registrado', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.valorcontador = new ViewValorContadorDto();
    }   

  }

  async registrarOperacion(): Promise<CreateOperacionContableDto> 
  {
    try {
      // tslint:disable-next-line:max-line-length
      this.registrooperacion = (await this.operacioncontableService.registrar(this.operacioncontable).toPromise()); 
      return this.registrooperacion as CreateOperacionContableDto;  
    } catch (error) {
      this._matSnackBar.open('Error de Servicio, Grabar Operación', 'ok', {
        verticalPosition: 'bottom',
        panelClass: ['my-snack-bar'] 
        });
      return this.registrooperacion = new CreateOperacionContableDto();
    }   

  }

  // tslint:disable-next-line:typedef
  async eliminarRegistrocontable(registrocontable: number)
  {
    // Elimina registros por destino
    for ( let i = this.listaAsientoContable.length - 1; i > -1; i-- )
    {                
      if ( this.listaAsientoContable[i].correlativoorigen === registrocontable )
      {
        this.listaAsientoContable.splice(i, 1);
      }                
    }

    // Elimina registro por naturaleza
    for ( let i = this.listaAsientoContable.length - 1; i > -1; i-- )
    {                
      if ( this.listaAsientoContable[i].registrocontable === registrocontable )
      {
        // Eliminar actuales
        this.listaAsientoContable.splice(i, 1);
      }                
    }

    // actualiza la vista de los registros por naturaleza
    this.obtenerRegistroContableNaturaleza();
            
    // Inicializa la vista de los registros por destino
    this.obtenerRegistroContableDestino(-1);

  }

  // tslint:disable-next-line:typedef
  async registrarRegistrocontable(registrocontable: number, accion: string)
  {
    let registrocontablenext = 0;
    this.registrocontable = new CreateRegistroContableDto();

    if (accion === 'insertar')
    {
 
      this.registrocontable.empresa                 = this.valoresiniciales.empresa;
      this.registrocontable.ejerciciocontable       = this.valoresiniciales.annoproceso;
      this.registrocontable.operacioncontable       = this.operacioncontableForm.value.operacioncontable;
      this.registrocontable.asientocontable         = 0;
      this.registrocontable.registrocontable        = 0;
      this.registrocontable.cuentacontable          = '000000';
      this.registrocontable.descripcioncuentacontable = '';      
      this.registrocontable.tipoanotacion           = 'D';
      this.registrocontable.descripciontipoanotacion = '';
      this.registrocontable.tiporegistro            = 'M';
      this.registrocontable.centrocosto             = '';
      this.registrocontable.descripcioncentrocosto  = '';
      this.registrocontable.porcentajecentrocosto   = 100;
      this.registrocontable.entidad                 = this.operacioncontableForm.value.entidad;
      this.registrocontable.nombreentidad           = '';
      this.registrocontable.tipodocumento           = this.operacioncontableForm.value.tipodocumento;
      this.registrocontable.descripciontipodocumento = '';
      this.registrocontable.numeroserie             = this.operacioncontableForm.value.numeroserie;
      this.registrocontable.numerocomprobanteini    = this.operacioncontableForm.value.numerocomprobanteini;
      this.registrocontable.numerocomprobantefin    = this.operacioncontableForm.value.numerocomprobantefin;
      this.registrocontable.fechaemision            = this.operacioncontableForm.value.fechaemision;
      this.registrocontable.fechavencimiento        = this.operacioncontableForm.value.fechavencimiento;
      this.registrocontable.indicadorcalculo        = 'S';
      this.registrocontable.moneda                  = this.operacioncontableForm.value.moneda;
      this.registrocontable.tipocambio              = this.operacioncontableForm.value.tipocambio;
      this.registrocontable.descripciontipocambio   = '';
      this.registrocontable.tipocambiosoles         = this.operacioncontableForm.value.tipocambiosoles;
      this.registrocontable.tipocambiodolares       = this.operacioncontableForm.value.tipocambiodolares;
      this.registrocontable.mediopago               = this.operacioncontableForm.value.mediopago;
      this.registrocontable.descripcionmediopago    = '';
      this.registrocontable.flujoefectivo           = this.operacioncontableForm.value.flujoefectivo;
      this.registrocontable.descripcionflujoefectivo = '';
      this.registrocontable.glosa                   = this.operacioncontableForm.value.glosa;
      this.registrocontable.montoorigen             = 0;
      this.registrocontable.montonacional           = 0;
      this.registrocontable.montodolares            = 0;

      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaAsientoContableNaturaleza.length; i++ )
      {
        if ( this.listaAsientoContableNaturaleza[i].tipoanotacion === 'D')
        {
          this.registrocontable.montoorigen     = (Number(this.registrocontable.montoorigen) + Number(this.listaAsientoContableNaturaleza[i].montoorigen)).toFixed(2);
          this.registrocontable.montonacional   = (Number(this.registrocontable.montonacional) + Number(this.listaAsientoContableNaturaleza[i].montonacional)).toFixed(2);
          this.registrocontable.montodolares    = (Number(this.registrocontable.montodolares) + Number(this.listaAsientoContableNaturaleza[i].montodolares)).toFixed(2);
        }
        else
        {
          this.registrocontable.montoorigen     = (Number(this.registrocontable.montoorigen) - Number(this.listaAsientoContableNaturaleza[i].montoorigen)).toFixed(2);
          this.registrocontable.montonacional   = (Number(this.registrocontable.montonacional) - Number(this.listaAsientoContableNaturaleza[i].montonacional)).toFixed(2);
          this.registrocontable.montodolares    = (Number(this.registrocontable.montodolares) - Number(this.listaAsientoContableNaturaleza[i].montodolares)).toFixed(2);
        }
      }
      
      if ( this.registrocontable.montoorigen < 0)
      {
        this.registrocontable.montoorigen =  this.registrocontable.montoorigen * -1;
        this.registrocontable.montonacional =  this.registrocontable.montonacional * -1;
        this.registrocontable.montodolares =  this.registrocontable.montodolares * -1;
      }
      
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaAsientoContable.length; i++ )
      {
        if ( this.listaAsientoContable[i].registrocontable === registrocontable)
        {
          
          this.registrocontable.empresa                 = this.listaAsientoContable[i].empresa;
          this.registrocontable.ejerciciocontable       = this.listaAsientoContable[i].ejerciciocontable;
          this.registrocontable.operacioncontable       = this.listaAsientoContable[i].operacioncontable;
          this.registrocontable.asientocontable         = this.listaAsientoContable[i].asientocontable;
          this.registrocontable.registrocontable        = this.listaAsientoContable[i].registrocontable;
          this.registrocontable.cuentacontable          = this.listaAsientoContable[i].cuentacontable;
          this.registrocontable.descripcioncuentacontable = this.listaAsientoContable[i].descripcioncuentacontable;
          this.registrocontable.tipoanotacion           = this.listaAsientoContable[i].tipoanotacion;
          this.registrocontable.descripciontipoanotacion = this.listaAsientoContable[i].descripciontipoanotacion;
          this.registrocontable.tiporegistro            = this.listaAsientoContable[i].tiporegistro;
          this.registrocontable.tablacentrocosto        = this.listaAsientoContable[i].tablacentrocosto;
          this.registrocontable.centrocosto             = this.listaAsientoContable[i].centrocosto;
          this.registrocontable.descripcioncentrocosto  = this.listaAsientoContable[i].descripcioncentrocosto;
          this.registrocontable.porcentajecentrocosto   = this.listaAsientoContable[i].porcentajecentrocosto;
          this.registrocontable.entidad                 = this.listaAsientoContable[i].entidad;
          this.registrocontable.nombreentidad           = this.listaAsientoContable[i].nombreentidad;
          this.registrocontable.tipodocumento           = this.listaAsientoContable[i].tipodocumento;
          this.registrocontable.descripciontipodocumento = this.listaAsientoContable[i].descripciontipodocumento;
          this.registrocontable.numeroserie             = this.listaAsientoContable[i].numeroserie;
          this.registrocontable.numerocomprobanteini    = this.listaAsientoContable[i].numerocomprobanteini;
          this.registrocontable.numerocomprobantefin    = this.listaAsientoContable[i].numerocomprobantefin;
          this.registrocontable.fechaemision            = this.listaAsientoContable[i].fechaemision;
          this.registrocontable.fechavencimiento        = this.listaAsientoContable[i].fechavencimiento;
          this.registrocontable.indicadorcalculo        = this.listaAsientoContable[i].indicadorcalculo;
          this.registrocontable.moneda                  = this.listaAsientoContable[i].moneda;
          this.registrocontable.tipocambio              = this.listaAsientoContable[i].tipocambio;
          this.registrocontable.descripciontipocambio   = this.listaAsientoContable[i].descripciontipocambio;
          this.registrocontable.tipocambiosoles         = this.listaAsientoContable[i].tipocambiosoles;
          this.registrocontable.tipocambiodolares       = this.listaAsientoContable[i].tipocambiodolares;
          this.registrocontable.mediopago               = this.listaAsientoContable[i].mediopago;
          this.registrocontable.descripcionmediopago    = this.listaAsientoContable[i].descripcionmediopago;
          this.registrocontable.flujoefectivo           = this.listaAsientoContable[i].flujoefectivo;
          this.registrocontable.descripcionflujoefectivo = this.listaAsientoContable[i].descripcionflujoefectivo;
          this.registrocontable.glosa                   = this.listaAsientoContable[i].glosa;
          this.registrocontable.tiporegistro            = this.listaAsientoContable[i].tiporegistro;
          this.registrocontable.montoorigen             = this.listaAsientoContable[i].montoorigen;
          this.registrocontable.montonacional           = this.listaAsientoContable[i].montonacional;
          this.registrocontable.montodolares            = this.listaAsientoContable[i].montodolares;

          this.registrocontable.fechaemision  = this.registrocontable.fechaemision.substr(6, 4) + '-' + 
                                                this.registrocontable.fechaemision.substr(3, 2) + '-' +
                                                this.registrocontable.fechaemision.substr(0, 2);

          if (this.registrocontable.fechavencimiento !== null && this.registrocontable.fechavencimiento !== '')
          {
            this.registrocontable.fechavencimiento  = this.registrocontable.fechavencimiento.substr(6, 4) + '-' + 
                                                      this.registrocontable.fechavencimiento.substr(3, 2) + '-' +
                                                      this.registrocontable.fechavencimiento.substr(0, 2);
          }
          
        }
      }
    }
    
    localStorage.setItem(REGISTROCONTABLE, JSON.stringify(this.registrocontable));
 
    localStorage.setItem(REFRESH, 'N'); 
    
    this.dialogRef = this._matDialog.open(RegistrocontableFormComponent, 
      {
      panelClass: 'registrocontable-form-dialog',
      data      : {
          accion: accion,
          tipooperacion: 'N',
          monedanacional: this.monedanacional,
          monedadolares: this.monedadolares          
      }
    });
  
    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       

          this.registrocontable = JSON.parse(localStorage.getItem(REGISTROCONTABLE));   
          
          this.registrocontable.fechaemision =  this.registrocontable.fechaemision.substr(8, 2) + '/' + 
                                                this.registrocontable.fechaemision.substr(5, 2) + '/' +
                                                this.registrocontable.fechaemision.substr(0, 4);

          if (this.registrocontable.fechavencimiento !== null && this.registrocontable.fechavencimiento !== '')
          {
            this.registrocontable.fechavencimiento =  this.registrocontable.fechavencimiento.substr(8, 2) + '/' + 
                                                    this.registrocontable.fechavencimiento.substr(5, 2) + '/' +
                                                    this.registrocontable.fechavencimiento.substr(0, 4); 
          }    
          
          if (this.registrocontable.entidad !== '' && this.registrocontable.entidad !== null)
          {
            this.ente = this.ente ? await this.consultarEnte(this.registrocontable.entidad) : new CreateEnteDto();  
            this.registrocontable.ruc = this.ente.ruc;  
            this.registrocontable.nombreentidad = this.registrocontable.ruc + '-' + this.obtenerTextoSelect(this.registrocontable.entidad, 'listaProveedor');
          }
          else
          {
            this.registrocontable.nombreentidad = '';
          }
          this.registrocontable.descripciontipodocumento = this.obtenerTextoSelect(this.registrocontable.tipodocumento, 'listaTipoDocumento');
          this.registrocontable.descripcioncentrocosto = this.obtenerTextoSelect(this.registrocontable.centrocosto, 'listaCentroCosto');
          this.registrocontable.descripcionmediopago = this.obtenerTextoSelect(this.registrocontable.mediopago, 'listaMedioPago');
          this.registrocontable.descripcionflujoefectivo = this.obtenerTextoSelect(this.registrocontable.flujoefectivo, 'listaFlujoEfectivo');

          this.registrocontable.incluirregistrocompra = 'S';
          this.registrocontable.columnamonto = this.operacioncontableForm.value.columnabaseimponible;
          this.registrocontable.columnaimpuesto = this.operacioncontableForm.value.columnaimpuesto;
          this.registrocontable.estado = 'A';
          this.registrocontable.creacionUsuario = this.valoresiniciales.username;
          this.registrocontable.modificacionUsuario = this.valoresiniciales.username;

          // ASIENTOMANUAL

          if (accion === 'insertar')
          {
            // this.registrocontable = new CreateRegistroContableDto();
            // Número asiento, se asigna en el BACK  

            registrocontablenext = 1010;
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaAsientoContable.length; i++ )
            {  
              if (this.listaAsientoContable[i].registrocontable > registrocontablenext)
              {
                registrocontablenext = this.listaAsientoContable[i].registrocontable;
              }               
            }

            // Nuevo registro contable
            this.registrocontableinsert = new CreateRegistroContableDto();
            registrocontablenext = registrocontablenext + 1;

            this.registrocontable.registrocontable = registrocontablenext;
  
            this.registrocontableinsert.empresa                = this.registrocontable.empresa;
            this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
            this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
            this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
            this.registrocontableinsert.registrocontable       = this.registrocontable.registrocontable; // ---
            this.registrocontableinsert.correlativoorigen      = 0; // ---
            this.registrocontableinsert.cuentacontable         = this.registrocontable.cuentacontable; // ---
            this.registrocontableinsert.entidad                = this.registrocontable.entidad;
            this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
            this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
            if (this.registrocontable.tipoanotacion === 'D')
            {
              this.registrocontableinsert.tipoanotacion            = 'D'; // ---
              this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
            }                  
            else
            {
              this.registrocontableinsert.tipoanotacion            = 'H'; // ---
              this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
            }
            this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
            this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
            this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
            this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
            this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
            this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
            this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
            this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
            this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
            this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
            this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
            this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
            this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
            this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
            this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
            this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
            this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
            this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
            this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
            this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
            this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
            this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
            this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
            this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
            this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
            this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
            this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
            this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
            this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
            this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
            this.registrocontableinsert.estado                 = this.registrocontable.estado;
            this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
            this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
            this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
            this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;

            this.listaAsientoContable.push(this.registrocontableinsert);
            // console.log(this.registrocontableinsert);   
            
            try {
              
              // tslint:disable-next-line:max-line-length
              this.cuentacontable = (await this.plancontableService.consultar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.cuentacontable).toPromise());
              if (this.cuentacontable.centrocosto === 'N')
              {
                // DEBE
                this.registrocontableinsert = new CreateRegistroContableDto();
                registrocontablenext = registrocontablenext + 1;

                this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                this.registrocontableinsert.cuentacontable         = this.cuentacontable.asientodebe; // ---
                this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                if (this.registrocontable.tipoanotacion === 'D')
                {
                  this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                  this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                }                  
                else
                {
                  this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                  this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                }
                this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                this.registrocontableinsert.estado                 = this.registrocontable.estado;
                this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;

                if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                {
                  this.listaAsientoContable.push(this.registrocontableinsert);
                } 

                // HABER
                this.registrocontableinsert = new CreateRegistroContableDto();
                registrocontablenext = registrocontablenext + 1;

                this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                this.registrocontableinsert.cuentacontable         = this.cuentacontable.asientohaber; // ---
                this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                if (this.registrocontable.tipoanotacion === 'D')
                {
                  this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                  this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                }                  
                else
                {
                  this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                  this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                }
                this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                this.registrocontableinsert.estado                 = this.registrocontable.estado;
                this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;

                if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                {
                  this.listaAsientoContable.push(this.registrocontableinsert);
                } 

              }
              else
              {
                // Indentificar tipo centro de costo I=Individual / D = Distribuido
                if (this.registrocontable.tablacentrocosto === 'I')
                {
                  // centro de costo individual
                  // tslint:disable-next-line:max-line-length
                  this.centrocosto = (await this.centrocostoService.consultar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.centrocosto).toPromise());
                  if (this.centrocosto.centrocosto !== '' && this.centrocosto.centrocosto !== null &&
                      this.centrocosto.cuentacontablecargo !== '' && this.centrocosto.cuentacontablecargo !== null &&
                      this.centrocosto.cuentacontableabono !== '' && this.centrocosto.cuentacontableabono !== null)
                  {

                    // DEBE
                    this.registrocontableinsert = new CreateRegistroContableDto();
                    registrocontablenext = registrocontablenext + 1;
  
                    this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                    this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                    this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                    this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                    this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                    this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                    this.registrocontableinsert.cuentacontable         = this.centrocosto.cuentacontablecargo; // ---
                    this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                    this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                    this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                    if (this.registrocontable.tipoanotacion === 'D')
                    {
                      this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                      this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                    }                  
                    else
                    {
                      this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                      this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                    }
                    this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                    this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                    this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                    this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                    this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                    this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                    this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                    this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                    this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                    this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                    this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                    this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                    this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                    this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                    this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                    this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                    this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                    this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                    this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                    this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                    this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                    this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                    this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                    this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                    this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                    this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                    this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                    this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                    this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                    this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                    this.registrocontableinsert.estado                 = this.registrocontable.estado;
                    this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                    this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                    this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                    this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
  
                    if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                    {
                      this.listaAsientoContable.push(this.registrocontableinsert);
                    }  

                    // HABER
                    this.registrocontableinsert = new CreateRegistroContableDto();
                    registrocontablenext = registrocontablenext + 1;

                    this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                    this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                    this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                    this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                    this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                    this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                    this.registrocontableinsert.cuentacontable         = this.centrocosto.cuentacontableabono; // ---
                    this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                    this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                    this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                    if (this.registrocontable.tipoanotacion === 'D')
                    {
                      this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                      this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                    }                  
                    else
                    {
                      this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                      this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                    }
                    this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                    this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                    this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                    this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                    this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                    this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                    this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                    this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                    this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                    this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                    this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                    this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                    this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                    this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                    this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                    this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                    this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                    this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                    this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                    this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                    this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                    this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                    this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                    this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                    this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                    this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                    this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                    this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                    this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                    this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                    this.registrocontableinsert.estado                 = this.registrocontable.estado;
                    this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                    this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                    this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                    this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
  
                    if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                    {
                      this.listaAsientoContable.push(this.registrocontableinsert);
                    } 
                  }
                }
                else
                {
                  // centro de costo distribuido
                  // tslint:disable-next-line:max-line-length
                  this.listaGrupoCentroCostoDetalle = (await this.grupocentrocostodetalleService.listar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.centrocosto).toPromise());

                  // bucle por cada uno - Inicio

                  // tslint:disable-next-line:prefer-for-of
                  for ( let i = 0; i < this.listaGrupoCentroCostoDetalle.length; i++ )
                  {
                    if (this.listaGrupoCentroCostoDetalle[i].centrocosto !== '' && this.listaGrupoCentroCostoDetalle[i].centrocosto !== null &&
                        this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo !== '' && this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo !== null &&
                        this.listaGrupoCentroCostoDetalle[i].cuentacontableabono !== '' && this.listaGrupoCentroCostoDetalle[i].cuentacontableabono !== null)
                    {
                      // DEBE      
                      this.registrocontableinsert = new CreateRegistroContableDto();                  
                      registrocontablenext = registrocontablenext + 1;

                      this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                      this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                      this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                      this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                      this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                      this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                      this.registrocontableinsert.cuentacontable         = this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo; // ---
                      this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                      this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                      this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                      if (this.registrocontable.tipoanotacion === 'D')
                      {
                        this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                      }                  
                      else
                      {
                        this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                      }
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                      this.registrocontableinsert.centrocosto            = this.listaGrupoCentroCostoDetalle[i].centrocosto;
                      this.registrocontableinsert.descripcioncentrocosto = this.listaGrupoCentroCostoDetalle[i].nombrecentrocosto;
                      this.registrocontableinsert.porcentajecentrocosto  = this.listaGrupoCentroCostoDetalle[i].porcentaje;
                      this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                      this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                      this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                      this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                      this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                      this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                      this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                      this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                      this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                      this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                      this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                      this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                      this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                      this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                      this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montoorigen            = (Number(this.registrocontable.montoorigen) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montonacional          = (Number(this.registrocontable.montonacional) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montodolares           = (Number(this.registrocontable.montodolares) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                      this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                      this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                      this.registrocontableinsert.estado                 = this.registrocontable.estado;
                      this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                      this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                      this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                      this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
    
                      if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                      {
                        this.listaAsientoContable.push(this.registrocontableinsert);
                      } 

                      // HABER
                      this.registrocontableinsert = new CreateRegistroContableDto();  
                      registrocontablenext = registrocontablenext + 1;

                      this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                      this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                      this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                      this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                      this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                      this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                      this.registrocontableinsert.cuentacontable         = this.listaGrupoCentroCostoDetalle[i].cuentacontableabono; // ---
                      this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                      this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                      this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                      if (this.registrocontable.tipoanotacion === 'D')
                      {
                        this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                      }                  
                      else
                      {
                        this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                      }
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                      this.registrocontableinsert.centrocosto            = this.listaGrupoCentroCostoDetalle[i].centrocosto;
                      this.registrocontableinsert.descripcioncentrocosto = this.listaGrupoCentroCostoDetalle[i].nombrecentrocosto;
                      this.registrocontableinsert.porcentajecentrocosto  = this.listaGrupoCentroCostoDetalle[i].porcentaje;
                      this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                      this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                      this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                      this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                      this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                      this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                      this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                      this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                      this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                      this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                      this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                      this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                      this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                      this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                      this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montoorigen            = (Number(this.registrocontable.montoorigen) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montonacional          = (Number(this.registrocontable.montonacional) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      // tslint:disable-next-line:max-line-length
                      this.registrocontableinsert.montodolares           = (Number(this.registrocontable.montodolares) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                      this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                      this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                      this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                      this.registrocontableinsert.estado                 = this.registrocontable.estado;
                      this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                      this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                      this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                      this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
    
                      if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                      {
                        this.listaAsientoContable.push(this.registrocontableinsert);
                      } 
                    }
                  }
                  
                }
              }
            } catch (error) {
              this._matSnackBar.open('Error de Servicio, Plan Contable', 'ok', {
                verticalPosition: 'bottom',
                panelClass: ['my-snack-bar'] 
                });
            }  

          }
          else
          {
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaAsientoContable.length; i++ )
            {
              if ( this.listaAsientoContable[i].registrocontable === registrocontable)
              {
                if (this.registrocontable.tiporegistro === 'A')
                {
                  this.listaAsientoContable[i].glosa = this.registrocontable.glosa;
                  this.listaAsientoContable[i].modificacionUsuario = this.valoresiniciales.username;
                }
                else
                {                  
                  this.listaAsientoContable[i].cuentacontable       = this.registrocontable.cuentacontable;
                  this.listaAsientoContable[i].tipoanotacion        = this.registrocontable.tipoanotacion;
                  this.listaAsientoContable[i].centrocosto          = this.registrocontable.centrocosto;
                  this.listaAsientoContable[i].descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                  this.listaAsientoContable[i].porcentajecentrocosto =  this.registrocontable.porcentajecentrocosto; 
                  this.listaAsientoContable[i].entidad              = this.registrocontable.entidad;
                  this.listaAsientoContable[i].nombreentidad        = this.registrocontable.nombreentidad;
                  this.listaAsientoContable[i].ruc                  = this.registrocontable.ruc;
                  this.listaAsientoContable[i].tipodocumento        = this.registrocontable.tipodocumento; 
                  this.listaAsientoContable[i].descripciontipodocumento = this.registrocontable.descripciontipodocumento;    
                  this.listaAsientoContable[i].numeroserie          = this.registrocontable.numeroserie;             
                  this.listaAsientoContable[i].numerocomprobanteini = this.registrocontable.numerocomprobanteini;    
                  this.listaAsientoContable[i].numerocomprobantefin = this.registrocontable.numerocomprobantefin; 
                  this.listaAsientoContable[i].indicadorcalculo     = this.registrocontable.indicadorcalculo; 
                  this.listaAsientoContable[i].tipocambio           = this.registrocontable.tipocambio;  
                  this.listaAsientoContable[i].tipocambiosoles      = Number(this.registrocontable.tipocambiosoles);
                  this.listaAsientoContable[i].tipocambiodolares    = Number(this.registrocontable.tipocambiodolares); 
                  this.listaAsientoContable[i].mediopago            = this.registrocontable.mediopago; 
                  this.listaAsientoContable[i].descripcionmediopago = this.registrocontable.descripcionmediopago;   
                  this.listaAsientoContable[i].flujoefectivo        = this.registrocontable.flujoefectivo; 
                  this.listaAsientoContable[i].descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;   
                  this.listaAsientoContable[i].glosa                = this.registrocontable.glosa;   
                  this.listaAsientoContable[i].montoorigen          = Number(this.registrocontable.montoorigen); 
                  this.listaAsientoContable[i].montonacional        = Number(this.registrocontable.montonacional); 
                  this.listaAsientoContable[i].montodolares         = Number(this.registrocontable.montodolares); 
                  this.listaAsientoContable[i].modificacionUsuario  = this.valoresiniciales.username;
                  this.listaAsientoContable[i].fechaemision         =  this.registrocontable.fechaemision;
                  this.listaAsientoContable[i].fechavencimiento     =  this.registrocontable.fechavencimiento;
                  this.listaAsientoContable[i].incluirregistrocompra =  this.registrocontable.incluirregistrocompra;
                  this.listaAsientoContable[i].columnamonto         =  this.registrocontable.columnamonto;
                  this.listaAsientoContable[i].columnaimpuesto      =  this.registrocontable.columnaimpuesto;
                  this.listaAsientoContable[i].estado               =  this.registrocontable.estado;
                  this.listaAsientoContable[i].creacionUsuario      =  this.registrocontable.creacionUsuario;
                  this.listaAsientoContable[i].creacionFecha        =  this.registrocontable.creacionFecha;
                  this.listaAsientoContable[i].modificacionUsuario  =  this.registrocontable.modificacionUsuario;
                  this.listaAsientoContable[i].modificacionFecha    =  this.registrocontable.modificacionFecha;

  
                }
              }
            }
            
            if (this.registrocontable.tiporegistro !== 'A')
            {              
              registrocontablenext = 1010;
              
              for ( let i = this.listaAsientoContable.length - 1; i > -1; i-- )
              {                
                if ( this.listaAsientoContable[i].correlativoorigen === registrocontable )
                {
                  // Eliminar actuales
                  this.listaAsientoContable.splice(i, 1);
                }   
                else
                {
                  if (this.listaAsientoContable[i].registrocontable > registrocontablenext)
                  {
                    registrocontablenext = this.listaAsientoContable[i].registrocontable;
                  }
                }             
              }   

              // obtener datos de la cuenta contable
              try {
                
                // tslint:disable-next-line:max-line-length
                this.cuentacontable = (await this.plancontableService.consultar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.cuentacontable).toPromise());
                if (this.cuentacontable.centrocosto === 'N')
                {
                  // DEBE
                  this.registrocontableinsert = new CreateRegistroContableDto();
                  registrocontablenext = registrocontablenext + 1;
                  

                  this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                  this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                  this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                  this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                  this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                  this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                  this.registrocontableinsert.cuentacontable         = this.cuentacontable.asientodebe; // ---
                  this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                  this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                  this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                  if (this.registrocontable.tipoanotacion === 'D')
                  {
                    this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                    this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                  }                  
                  else
                  {
                    this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                    this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                  }
                  this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                  this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                  this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                  this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                  this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                  this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                  this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                  this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                  this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                  this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                  this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                  this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                  this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                  this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                  this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                  this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                  this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                  this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                  this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                  this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                  this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                  this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                  this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                  this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                  this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                  this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                  this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                  this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                  this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                  this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                  this.registrocontableinsert.estado                 = this.registrocontable.estado;
                  this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                  this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                  this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                  this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;

                  if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                  {
                    this.listaAsientoContable.push(this.registrocontableinsert);
                  } 

                  // HABER
                  this.registrocontableinsert = new CreateRegistroContableDto();
                  registrocontablenext = registrocontablenext + 1;

                  this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                  this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                  this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                  this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                  this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                  this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                  this.registrocontableinsert.cuentacontable         = this.cuentacontable.asientohaber; // ---
                  this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                  this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                  this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                  if (this.registrocontable.tipoanotacion === 'D')
                  {
                    this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                    this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                  }                  
                  else
                  {
                    this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                    this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                  }
                  this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                  this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                  this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                  this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                  this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                  this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                  this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                  this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                  this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                  this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                  this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                  this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                  this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                  this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                  this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                  this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                  this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                  this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                  this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                  this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                  this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                  this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                  this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                  this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                  this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                  this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                  this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                  this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                  this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                  this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                  this.registrocontableinsert.estado                 = this.registrocontable.estado;
                  this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                  this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                  this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                  this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;

                  if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                  {
                    this.listaAsientoContable.push(this.registrocontableinsert);
                  } 

                }
                else
                {
                  // Indentificar tipo centro de costo I=Individual / D = Distribuido
                  if (this.registrocontable.tablacentrocosto === 'I')
                  {
                    // centro de costo individual
                    // tslint:disable-next-line:max-line-length
                    this.centrocosto = (await this.centrocostoService.consultar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.centrocosto).toPromise());
                    if (this.centrocosto.centrocosto !== '' && this.centrocosto.centrocosto !== null &&
                        this.centrocosto.cuentacontablecargo !== '' && this.centrocosto.cuentacontablecargo !== null &&
                        this.centrocosto.cuentacontableabono !== '' && this.centrocosto.cuentacontableabono !== null)
                    {

                      // DEBE
                      this.registrocontableinsert = new CreateRegistroContableDto();
                      registrocontablenext = registrocontablenext + 1;
    
                      this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                      this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                      this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                      this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                      this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                      this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                      this.registrocontableinsert.cuentacontable         = this.centrocosto.cuentacontablecargo; // ---
                      this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                      this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                      this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                      if (this.registrocontable.tipoanotacion === 'D')
                      {
                        this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                      }                  
                      else
                      {
                        this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                      }
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                      this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                      this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                      this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                      this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                      this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                      this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                      this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                      this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                      this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                      this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                      this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                      this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                      this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                      this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                      this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                      this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                      this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                      this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                      this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                      this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                      this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                      this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                      this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                      this.registrocontableinsert.estado                 = this.registrocontable.estado;
                      this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                      this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                      this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                      this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
    
                      if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                      {
                        this.listaAsientoContable.push(this.registrocontableinsert);
                      }  

                      // HABER
                      this.registrocontableinsert = new CreateRegistroContableDto();
                      registrocontablenext = registrocontablenext + 1;

                      this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                      this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                      this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                      this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                      this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                      this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                      this.registrocontableinsert.cuentacontable         = this.centrocosto.cuentacontableabono; // ---
                      this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                      this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                      this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                      if (this.registrocontable.tipoanotacion === 'D')
                      {
                        this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                      }                  
                      else
                      {
                        this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                        this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                      }
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                      this.registrocontableinsert.centrocosto            = this.registrocontable.centrocosto;
                      this.registrocontableinsert.descripcioncentrocosto = this.registrocontable.descripcioncentrocosto;
                      this.registrocontableinsert.porcentajecentrocosto  = this.registrocontable.porcentajecentrocosto;
                      this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                      this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                      this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                      this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                      this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                      this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                      this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                      this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                      this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                      this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                      this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                      this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                      this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                      this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                      this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                      this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                      this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                      this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                      this.registrocontableinsert.montoorigen            = Number(this.registrocontable.montoorigen);
                      this.registrocontableinsert.montonacional          = Number(this.registrocontable.montonacional);
                      this.registrocontableinsert.montodolares           = Number(this.registrocontable.montodolares);
                      this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                      this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                      this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                      this.registrocontableinsert.estado                 = this.registrocontable.estado;
                      this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                      this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                      this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                      this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
    
                      if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                      {
                        this.listaAsientoContable.push(this.registrocontableinsert);
                      } 
                    }
                  }
                  else
                  {
                    // centro de costo distribuido
                    // tslint:disable-next-line:max-line-length
                    this.listaGrupoCentroCostoDetalle = (await this.grupocentrocostodetalleService.listar(this.registrocontable.empresa, this.registrocontable.ejerciciocontable, this.registrocontable.centrocosto).toPromise());
 
                    // bucle por cada uno - Inicio

                    // tslint:disable-next-line:prefer-for-of
                    for ( let i = 0; i < this.listaGrupoCentroCostoDetalle.length; i++ )
                    {
                      if (this.listaGrupoCentroCostoDetalle[i].centrocosto !== '' && this.listaGrupoCentroCostoDetalle[i].centrocosto !== null &&
                          this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo !== '' && this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo !== null &&
                          this.listaGrupoCentroCostoDetalle[i].cuentacontableabono !== '' && this.listaGrupoCentroCostoDetalle[i].cuentacontableabono !== null)
                      {
                        // DEBE      
                        this.registrocontableinsert = new CreateRegistroContableDto();                  
                        registrocontablenext = registrocontablenext + 1;

                        this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                        this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                        this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                        this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                        this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                        this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                        this.registrocontableinsert.cuentacontable         = this.listaGrupoCentroCostoDetalle[i].cuentacontablecargo; // ---
                        this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                        this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                        this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                        if (this.registrocontable.tipoanotacion === 'D')
                        {
                          this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                          this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                        }                  
                        else
                        {
                          this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                          this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                        }
                        this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                        this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                        this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                        this.registrocontableinsert.centrocosto            = this.listaGrupoCentroCostoDetalle[i].centrocosto;
                        this.registrocontableinsert.descripcioncentrocosto = this.listaGrupoCentroCostoDetalle[i].nombrecentrocosto;
                        this.registrocontableinsert.porcentajecentrocosto  = this.listaGrupoCentroCostoDetalle[i].porcentaje;
                        this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                        this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                        this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                        this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                        this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                        this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                        this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                        this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                        this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                        this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                        this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                        this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                        this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                        this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                        this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                        this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                        this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                        this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montoorigen            = (Number(this.registrocontable.montoorigen) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montonacional          = (Number(this.registrocontable.montonacional) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montodolares           = (Number(this.registrocontable.montodolares) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                        this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                        this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                        this.registrocontableinsert.estado                 = this.registrocontable.estado;
                        this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                        this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                        this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                        this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
      
                        if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                        {
                          this.listaAsientoContable.push(this.registrocontableinsert);
                        }   
  
                        // HABER
                        this.registrocontableinsert = new CreateRegistroContableDto();  
                        registrocontablenext = registrocontablenext + 1;

                        this.registrocontableinsert.empresa                = this.registrocontable.empresa;
                        this.registrocontableinsert.ejerciciocontable      = this.registrocontable.ejerciciocontable;
                        this.registrocontableinsert.operacioncontable      = this.registrocontable.operacioncontable;
                        this.registrocontableinsert.asientocontable        = this.registrocontable.asientocontable;
                        this.registrocontableinsert.registrocontable       = registrocontablenext; // ---
                        this.registrocontableinsert.correlativoorigen      = this.registrocontable.registrocontable; // ---
                        this.registrocontableinsert.cuentacontable         = this.listaGrupoCentroCostoDetalle[i].cuentacontableabono; // ---
                        this.registrocontableinsert.entidad                = this.registrocontable.entidad;
                        this.registrocontableinsert.nombreentidad          = this.registrocontable.nombreentidad;
                        this.registrocontableinsert.ruc                    = this.registrocontable.ruc;
                        if (this.registrocontable.tipoanotacion === 'D')
                        {
                          this.registrocontableinsert.tipoanotacion            = 'H'; // ---
                          this.registrocontableinsert.descripciontipoanotacion = 'Haber'; // ---
                        }                  
                        else
                        {
                          this.registrocontableinsert.tipoanotacion            = 'D'; // ---
                          this.registrocontableinsert.descripciontipoanotacion = 'Debe'; // ---
                        }
                        this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                        this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                        this.registrocontableinsert.tablacentrocosto       = this.registrocontable.tablacentrocosto;
                        this.registrocontableinsert.centrocosto            = this.listaGrupoCentroCostoDetalle[i].centrocosto;
                        this.registrocontableinsert.descripcioncentrocosto = this.listaGrupoCentroCostoDetalle[i].nombrecentrocosto;
                        this.registrocontableinsert.porcentajecentrocosto  = this.listaGrupoCentroCostoDetalle[i].porcentaje;
                        this.registrocontableinsert.tipodocumento          = this.registrocontable.tipodocumento;
                        this.registrocontableinsert.descripciontipodocumento    = this.registrocontable.descripciontipodocumento;
                        this.registrocontableinsert.numeroserie            = this.registrocontable.numeroserie;
                        this.registrocontableinsert.numerocomprobanteini   = this.registrocontable.numerocomprobanteini;
                        this.registrocontableinsert.numerocomprobantefin   = this.registrocontable.numerocomprobantefin;
                        this.registrocontableinsert.fechaemision           = this.registrocontable.fechaemision;
                        this.registrocontableinsert.fechavencimiento       = this.registrocontable.fechavencimiento;
                        this.registrocontableinsert.indicadorcalculo       = this.registrocontable.indicadorcalculo;
                        this.registrocontableinsert.moneda                 = this.registrocontable.moneda;
                        this.registrocontableinsert.tipocambio             = this.registrocontable.tipocambio;
                        this.registrocontableinsert.tipocambiosoles        = Number(this.registrocontable.tipocambiosoles);
                        this.registrocontableinsert.tipocambiodolares      = Number(this.registrocontable.tipocambiodolares);
                        this.registrocontableinsert.mediopago              = this.registrocontable.mediopago;
                        this.registrocontableinsert.descripcionmediopago   = this.registrocontable.descripcionmediopago;
                        this.registrocontableinsert.flujoefectivo          = this.registrocontable.flujoefectivo;
                        this.registrocontableinsert.descripcionflujoefectivo = this.registrocontable.descripcionflujoefectivo;
                        this.registrocontableinsert.glosa                  = this.registrocontable.glosa;
                        this.registrocontableinsert.tiporegistro           = this.registrocontable.tiporegistro;
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montoorigen            = (Number(this.registrocontable.montoorigen) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montonacional          = (Number(this.registrocontable.montonacional) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        // tslint:disable-next-line:max-line-length
                        this.registrocontableinsert.montodolares           = (Number(this.registrocontable.montodolares) * Number(this.listaGrupoCentroCostoDetalle[i].porcentaje) / 100 ).toFixed(2);
                        this.registrocontableinsert.incluirregistrocompra  = this.registrocontable.incluirregistrocompra;
                        this.registrocontableinsert.columnamonto           = this.registrocontable.columnamonto;
                        this.registrocontableinsert.columnaimpuesto        = this.registrocontable.columnaimpuesto;
                        this.registrocontableinsert.estado                 = this.registrocontable.estado;
                        this.registrocontableinsert.creacionUsuario        = this.registrocontable.creacionUsuario;
                        this.registrocontableinsert.creacionFecha          = this.registrocontable.creacionFecha;
                        this.registrocontableinsert.modificacionUsuario    = this.registrocontable.modificacionUsuario;
                        this.registrocontableinsert.modificacionFecha      = this.registrocontable.modificacionFecha;
      
                        if (this.registrocontableinsert.cuentacontable !== null && this.registrocontableinsert.cuentacontable !== '')
                        {
                          this.listaAsientoContable.push(this.registrocontableinsert);
                        } 
                      }
                    }
                    
                  }
                }
              } catch (error) {
                this._matSnackBar.open('Error de Servicio, Plan Contable', 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
              }  

            }
          
          }
          // actualiza la vista de los registros por naturaleza
          this.obtenerRegistroContableNaturaleza();
            
          // Inicializa la vista de los registros ppor destino
          this.obtenerRegistroContableDestino(-1);
        }
  
        this.registrocontable = new CreateRegistroContableDto();
        localStorage.setItem(REGISTROCONTABLE, JSON.stringify(this.registrocontable));
        localStorage.setItem(REFRESH, 'N'); 

      }        
    ); 
  }

  crearOperacionContable(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));

    this.ngOnInit();
   
  }  

}

