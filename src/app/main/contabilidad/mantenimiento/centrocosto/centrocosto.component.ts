import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { CreateCentroCostoDto } from 'app/dto/create.centrocosto.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { CentroCostoService } from 'app/services/centrocosto.service';
import { ExcelService } from 'app/services/excel.service';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { CentrocostoFormComponent } from './centrocosto-form/centrocosto-form.component';

@Component({
  selector: 'gez-centrocosto',
  templateUrl: './centrocosto.component.html',
  styleUrls: ['./centrocosto.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class CentrocostoComponent implements OnInit {
  
  dialogRef: any;
  centrocosto;
  valoresiniciales;
  resultado;

  displayedColumns = ['centrocosto', 'nombrecentrocosto', 'cuentacontablecargo', 'cuentacontableabono', 'descripcionestado', 'editar', 'eliminar'];

  listarReporte: any = [];

  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private centrocostoService: CentroCostoService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService,
    
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {
    this.centrocosto = new CreateCentroCostoDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.centrocosto.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.nombrecentrocosto.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.cuentacontablecargo.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.cuentacontableabono.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 || 
       data.descripcionestado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1
       );

    this.setSettingTableCourse();

  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.centrocostoService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearCentroCosto(): void
  {
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(CentrocostoFormComponent, {
        panelClass: 'centrocosto-form-dialog',
        data      : {
          accion: 'nuevo'
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {            
          this.ngOnInit();
        }

        localStorage.setItem(REFRESH, 'N');

      }        
    );     
  }

  editarCentroCosto(centrocosto: string): void
  {
    
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(CentrocostoFormComponent, {
        panelClass: 'centrocosto-form-dialog',
        data      : {
          accion: 'editar',
          codigo: centrocosto
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {
          this.ngOnInit();
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );
  }

  eliminarCentroCosto(centrocosto: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(MessageboxComponent, {
        panelClass: 'confirm-form-dialog',
        data      : {
            titulo: 'Eliminar Centro de Costo: ' + centrocosto,
            subTitulo: '¿Está seguro de eliminar el centro de costo?',
            botonAceptar: 'Si',
            botonCancelar: 'No'    
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.centrocosto.empresa = this.valoresiniciales.empresa;
          this.centrocosto.ejerciciocontable = this.valoresiniciales.annoproceso;
          this.centrocosto.centrocosto     = centrocosto;

          this.centrocostoService.eliminar(this.centrocosto).subscribe(data => {
          this._matSnackBar.open('Se eliminó el centro de costo', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });

          this.ngOnInit();

          }, err => {
              this._matSnackBar.open('Error, centro de costo no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');

      }        
    );
  }

  exportarCentroCosto(): void
  {
    this.centrocostoService.exportar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Ejercicio Contable'] = this.valoresiniciales.annoproceso;
          objectReport['Centro Costo'] = item.centrocosto;
          objectReport['Denominación'] = item.nombrecentrocosto;   
          objectReport['Cuenta Cargo'] = item.cuentacontablecargo; 
          objectReport['Cuenta Abono'] = item.cuentacontableabono; 
          objectReport['Estado'] = item.descripcionestado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'CentroCosto');

      }

    }); 
  }

}
