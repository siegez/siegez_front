import { Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { CentroCostoService } from 'app/services/centrocosto.service';
import { CreateCentroCostoDto } from 'app/dto/create.centrocosto.dto';
import { PlanContableService } from 'app/services/plancontable.service';

@Component({
  selector: 'gez-centrocosto-form',
  templateUrl: './centrocosto-form.component.html',
  styleUrls: ['./centrocosto-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class CentrocostoFormComponent implements OnInit  {
  // @ViewChild('fecha') fechaField: ElementRef;
  
  accion: string;
  codigo: string; 
  centrocosto;
  resultadosp;
  centrocostoForm: FormGroup;
  dialogTitle: string;
  botonExit = 'Cancelar';
  botonAgregar = 'Grabar';
  botonAceptar = 'Grabar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  valoresiniciales;
  valorcontador;
  checkEstado = false;

  listaCtaCargo: any[];
  listaCtaAbono: any[];

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<CentrocostoFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private centrocostoService: CentroCostoService,
    private consultaService: ConsultaService,
    private plancontableService: PlanContableService,
  ) 
  { 
    this.accion = _data.accion;
    this.codigo = _data.codigo;
  }


  ngOnInit(): void 
  {   

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.listarCtaCargo();
    this.listarCtaAbono();
    
    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Centro de Costo';  
      this.centrocosto = new CreateCentroCostoDto();

      this.centrocostoService.consultar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.codigo).subscribe(data => {
        this.centrocosto = data;  
        this.checkEstado = (this.centrocosto.estado === 'A' ) ? true : false;    
        this.centrocostoForm = this.createCentroCostoForm();
      }, err => {
        this._matSnackBar.open('Error de consulta', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
      ); 
    }
    else
    {
      this.dialogTitle = 'Nuevo Centro de Costo';
      this.edit = false;
      this.checkEstado = true;
      this.centrocosto = new CreateCentroCostoDto();
      this.centrocostoForm = this.createCentroCostoForm();
    }
    
    this.centrocostoForm = this.createCentroCostoForm();

  }

  createCentroCostoForm(): FormGroup
  {
    return this._formBuilder.group({

      centrocosto          : [this.centrocosto.centrocosto],
      nombrecentrocosto    : [this.centrocosto.nombrecentrocosto],
      cuentacontablecargo  : [this.centrocosto.cuentacontablecargo],
      cuentacontableabono  : [this.centrocosto.cuentacontableabono],
      estado               : [this.checkEstado]

    });
  }

  // tslint:disable-next-line:typedef
  registrarCentroCosto(centrocostoForm){

    this.centrocosto.empresa = this.valoresiniciales.empresa;    
    this.centrocosto.ejerciciocontable = this.valoresiniciales.annoproceso;   
    this.centrocosto.centrocosto = centrocostoForm.value.centrocosto;
    this.centrocosto.nombrecentrocosto = centrocostoForm.value.nombrecentrocosto;
    this.centrocosto.cuentacontablecargo = centrocostoForm.value.cuentacontablecargo;
    this.centrocosto.cuentacontableabono = centrocostoForm.value.cuentacontableabono;
    this.centrocosto.estado = (this.centrocostoForm.value.estado) ? 'A' : 'I';
    this.centrocosto.creacionUsuario = this.valoresiniciales.username;
    this.centrocosto.creacionFecha = '2020-01-01';
    this.centrocosto.modificacionUsuario = this.valoresiniciales.username;
    this.centrocosto.modificacionFecha = '2020-01-01';
    
    // consumir servicios para grabar
    if ( this.accion === 'nuevo' )
    {
      this.centrocostoService.crear(this.centrocosto).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
          });
        
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;

        localStorage.setItem(REFRESH, 'S');

        }, 
        err => 
        {
            this._matSnackBar.open('Error, centro costo no registrado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
    else
    {

      this.centrocostoService.editar(this.centrocosto).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;

        localStorage.setItem(REFRESH, 'S');

        }, err => {
            this._matSnackBar.open('Error, centro costo no actualizado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
  }   
  
  // tslint:disable-next-line:typedef
  listarCtaCargo(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCtaCargo = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarCtaAbono(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCtaAbono = data;
    });
  }

}

