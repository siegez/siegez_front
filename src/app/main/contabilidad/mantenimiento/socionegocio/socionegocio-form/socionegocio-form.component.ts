import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO, MESSAGEBOX, TABLAS } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EnteService } from 'app/services/ente.service';
import { CreateEnteDto } from 'app/dto/create.ente.dto';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { EnteSocioNegocioService } from 'app/services/entesocionegocio.service';
import { EnteDocumentoIdentidadService } from 'app/services/entedocumentoidentidad.service';
import { SocionegocioAnexoComponent } from './socionegocio-anexo/socionegocio-anexo.component';
import { CreateEnteSocioNegocioDto } from 'app/dto/create.entesocionegocio.dto';
import { CreateEnteDocumentoIdentidadDto } from 'app/dto/create.entedocumentoidentidad.dto';
import { SocionegocioDocumentoComponent } from './socionegocio-documento/socionegocio-documento.component';
import { EnteDireccionService } from 'app/services/entedireccion.service';
import { CreateEnteDireccionDto } from 'app/dto/create.entedireccion.dto';
import { SocionegocioDomicilioComponent } from './socionegocio-domicilio/socionegocio-domicilio.component';

@Component({
  selector: 'gez-socionegocio-form',
  templateUrl: './socionegocio-form.component.html',
  styleUrls: ['./socionegocio-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class SocionegocioFormComponent implements OnInit, AfterContentChecked {

  
  displayedColumnsSocioNegocio = ['anexo', 'estado', 'editar', 'eliminar'];
  displayedColumnsDocumentoIdentidad = ['tipodocumento', 'numero', 'estado', 'editar', 'eliminar'];
  displayedColumnsDomicilio = ['tipodomicilio', 'direccion', 'ubicaciongeografica', 'pais', 'estado', 'editar', 'eliminar'];

  misDatos;  
  enteForm: FormGroup;
  ente;
  anexo;
  documento;
  socionegocio;
  domicilio;
  resultadosp;
  valorcampo;
  valoresiniciales;
  valorcontador;
  valorcatalogo;
  ruc;
  excluir;
  edit = true;  
  listaEnteSocioNegocio: any = [];
  listaEnteDocumentoIdentidad: any = [];
  listaEnteDomicilio: any = [];
  listaTipoPersona: any[];
  listaPais: any[];
  listaSocioNegocio: any[];
  listaDocumento: any[];
  listaTipoDomicilio: any[];
  listaUbicacionGeografica: any[];
  listaEstado: any[];
  listaExcluir: any[];

  checkNoDomiciliado = false;
  checkEstado = false;
  requiredApe = false;
  dialogRef: any;

  codigos;
  
  constructor(
    private enteService: EnteService,
    private consultaService: ConsultaService,
    private entesocionegocioService: EnteSocioNegocioService,
    private entedocumentoidentidadService: EnteDocumentoIdentidadService,
    private entedomicilioService: EnteDireccionService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private cdref: ChangeDetectorRef
  ) 
  {    
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.valorcontador = new ViewValorContadorDto();   
    this.ente = new CreateEnteDto();

    this.ente.entidad       = null;     
    this.ente.pais          = '9589'; 

    this.enteForm = this.createEnteForm();

    this.listarTipoPersona();
    this.listarPais();
    this.listarSocioNegocio();
    this.listarDocumento();
    this.listarTipoDomicilio();
    this.listarUbicacionGeografica();
    this.listarEstado(); 

    // campos pk
    this.ente.empresa            = this.valoresiniciales.empresa;

    if ( this.misDatos.action === 'editar' )
    {
      // campo pk
      this.ente.entidad       = this.misDatos.parametro1;      
      this.obtenerEnte();  
      this.obtenerSocioNegocio();
      this.obtenerDocumentoIdentidad(); 
      this.obtenerDomicilio();  
  
    }
    else{
 
      this.iniciarEnte();
      this.obtenerSocioNegocio();
      this.obtenerDocumentoIdentidad(); 
      this.obtenerDomicilio();

      this.edit = false;
      this.checkEstado = true;
      // console.log(this.enteForm.value.pais);


    }  

  }

  // tslint:disable-next-line:typedef
  obtenerEnte(){

    this.enteService.consultar(this.ente.empresa, this.ente.entidad).subscribe(data => {
      this.ente = data;    
      this.checkNoDomiciliado     = (this.ente.nodomiciliado === 'S' ) ? true : false; 
      this.checkEstado            = (this.ente.estado === 'A' ) ? true : false;

      this.enteForm = this.createEnteForm();

      if (this.ente.tipopersona === 'J')
      {
        this.requiredApe = false;        
      }
      else
      {
        this.requiredApe = true;        
      }

      }, err => {
        this._matSnackBar.open('Error de consulta al socio de negocio', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );  
  }

  // tslint:disable-next-line:typedef
  iniciarEnte(){
    setTimeout(() => {
      this.enteForm.controls['entidad'].setValue('');
      this.enteForm.controls['apellidopaterno'].setValue('');
      this.enteForm.controls['apellidomaterno'].setValue('');
      this.enteForm.controls['nombres'].setValue('');
      this.enteForm.controls['tipopersona'].setValue('J');
      this.enteForm.controls['ruc'].setValue('');
      this.enteForm.controls['pais'].setValue('9589');
      this.enteForm.controls['nodomiciliado'].setValue(false);
      this.enteForm.controls['estado'].setValue(true);
      
    });
  }
  
  createEnteForm(): FormGroup
  {
    this.ruc = this.ente.ruc;

    return this._formBuilder.group({
          entidad            : [this.ente.entidad],
          apellidopaterno    : [this.ente.apellidopaterno],
          apellidomaterno    : [this.ente.apellidomaterno],
          nombres            : [this.ente.nombres],
          tipopersona        : [this.ente.tipopersona],
          ruc                : [this.ente.ruc],
          pais               : [this.ente.pais],
          nodomiciliado      : [this.checkNoDomiciliado],
          estado             : [this.checkEstado]    
      });

  }
   

  grabarEnte(): void
  {
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Grabar Socio de Negocio',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');

          if ( this.misDatos.action === 'nuevo' )
          {
            this.ente.entidad = null;
          }
          else
          {
            this.ente.entidad = this.enteForm.value.entidad;
          }

          if (this.enteForm.value.tipopersona === 'J')
          {
          this.ente.apellidopaterno     = '';
          this.ente.apellidomaterno     = '';
          }
          else
          {
            this.ente.apellidopaterno     = this.enteForm.value.apellidopaterno;
            this.ente.apellidomaterno     = this.enteForm.value.apellidomaterno;
          }
      
          
          this.ente.nombres                 = this.enteForm.value.nombres;
          this.ente.tipopersona             = this.enteForm.value.tipopersona;
          this.ente.ruc                     = this.enteForm.value.ruc;
          this.ente.pais                    = this.enteForm.value.pais;
          this.ente.nodomiciliado           = (this.enteForm.value.nodomiciliado) ? 'S' : 'N';
          this.ente.estado                  = (this.enteForm.value.estado) ? 'A' : 'I';
          this.ente.creacionUsuario         = this.valoresiniciales.username;
          this.ente.creacionFecha           = '2020-01-01';
          this.ente.modificacionUsuario     = this.valoresiniciales.username;
          this.ente.modificacionFecha       = '2020-01-01';
          this.ente.entesocionegocio        = this.listaEnteSocioNegocio;
          this.ente.entedocumentoidentidad  = this.listaEnteDocumentoIdentidad;
          this.ente.entedireccion           = this.listaEnteDomicilio;
      
          this.resultadosp = new ViewResultadoSPDto();
      
          // consumir servicios para grabar

          this.enteService.registrar(this.ente).subscribe(data => {
            this.resultadosp = data;  
            if (this.resultadosp.indicadorexito === 'S')
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                verticalPosition: 'bottom',
                duration        : 2000
                  });
        
                const misDatosNew =  { 
                    action: 'editar',
                    parametro1: this.resultadosp.codigovarchar
                  };
        
                localStorage.setItem(DATOS, JSON.stringify(misDatosNew));
        
                this.misDatos = JSON.parse(localStorage.getItem(DATOS));
        
                this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
        
                this.edit = true;
    
                this.enteForm.controls['entidad'].setValue(this.resultadosp.codigovarchar); 
                this.ente.entidad = this.resultadosp.codigovarchar;
    
                this.ngOnInit();

              }
            else
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
              }
    
            }, 
            err => 
            {
                this._matSnackBar.open('Error, socio negocio no registrado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
          );           
        }
      }        
    ); 

  }

  // tslint:disable-next-line:typedef
  listarTipoPersona(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOPERSONA';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoPersona = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarPais(){
    this.listaPais = [];
    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 35).subscribe(data => {
      this.listaPais = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarSocioNegocio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDSOCIONEGOCIO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaSocioNegocio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoDomicilio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPODOMICILIO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoDomicilio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarDocumento(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDDOCIDENTIDAD';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaDocumento = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarUbicacionGeografica(){

    this.consultaService.listarUbicacionGeografica().subscribe(data => {
      this.listaUbicacionGeografica = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarEstado(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ESTADO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaEstado = data;
    });
  }

  // tslint:disable-next-line:typedef
  modificarTipoPersona()
  {
    if (this.enteForm.value.tipopersona === 'J')
    {
      this.enteForm.controls['apellidopaterno'].setValue('x');
      this.enteForm.controls['apellidomaterno'].setValue('x');
      this.ente.tipopersona = 'J';
      this.requiredApe = false;
      
    }
    else
    {
      this.enteForm.controls['apellidopaterno'].setValue('');
      this.enteForm.controls['apellidomaterno'].setValue('');
      this.ente.tipopersona = 'N';
      this.requiredApe = true;
      
    }

    // this.enteForm.controls['nombres'].setValue('');

  }  

  crearEnte(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.ngOnInit();

  }

  // tslint:disable-next-line:typedef
  async obtenerSocioNegocio() 
  {  
    // tslint:disable-next-line:max-line-length
    this.listaEnteSocioNegocio = (await this.entesocionegocioService.listar(this.valoresiniciales.empresa, this.ente.entidad).toPromise());
          
  }  

  // tslint:disable-next-line:typedef
  async obtenerDocumentoIdentidad() 
  {
    // tslint:disable-next-line:max-line-length
    this.listaEnteDocumentoIdentidad = (await this.entedocumentoidentidadService.listar(this.valoresiniciales.empresa, this.ente.entidad).toPromise());
      
  }  

  // tslint:disable-next-line:typedef
  async obtenerDomicilio() 
  {
    // tslint:disable-next-line:max-line-length
    this.listaEnteDomicilio = (await this.entedomicilioService.listar(this.valoresiniciales.empresa, this.ente.entidad).toPromise());
      
  } 

  registrarAnexo(vez: number, accion: string): void
  {
    this.codigos = '';
    this.anexo = new CreateEnteSocioNegocioDto();

    if (accion === 'insertar')
    { 
      this.anexo.empresa       = this.valoresiniciales.empresa;
      this.anexo.entidad       = this.enteForm.value.entidad;
      this.anexo.vez           = 0;
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteSocioNegocio.length; i++ )
      {        
        this.codigos = this.codigos + `'` + this.listaEnteSocioNegocio[i].socionegocio + `',`;       
      }
      
      if (this.codigos !== '')
      {      
        this.codigos = this.codigos.slice(0, -1);                   
      }          
      this.anexo.descripcionestado  = this.codigos; 
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteSocioNegocio.length; i++ )
      {        
        if ( this.listaEnteSocioNegocio[i].vez === vez)
        {
          this.anexo.empresa                 = this.listaEnteSocioNegocio[i].empresa;
          this.anexo.entidad                 = this.listaEnteSocioNegocio[i].entidad;
          this.anexo.vez                     = this.listaEnteSocioNegocio[i].vez;
          this.anexo.socionegocio            = this.listaEnteSocioNegocio[i].socionegocio;
          this.anexo.descripcionsocionegocio = this.listaEnteSocioNegocio[i].descripcionsocionegocio;
          this.anexo.estado                  = this.listaEnteSocioNegocio[i].estado;
        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.anexo));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(SocionegocioAnexoComponent, { 
        panelClass: 'socionegocio-anexo-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 

          this.anexo = JSON.parse(localStorage.getItem(TABLAS));   

          if (accion === 'insertar')
          {
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteSocioNegocio.length; i++ )
            {        
              if ( this.listaEnteSocioNegocio[i].vez > this.anexo.vez)
              {
                vez = this.listaEnteSocioNegocio[i].vez + 1;
              }
            }

            this.anexo.vez                      = vez;
            this.anexo.descripcionsocionegocio  = this.obtenerTextoSelect(this.anexo.socionegocio, 'listaSocioNegocio');
            this.anexo.descripcionestado        = this.obtenerTextoSelect(this.anexo.estado, 'listaEstado');
            this.listaEnteSocioNegocio.push(this.anexo);  
            this.listaEnteSocioNegocio = this.listaEnteSocioNegocio.slice();  
           
          }
          else
          {            
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteSocioNegocio.length; i++ )
            {        
              if ( this.listaEnteSocioNegocio[i].vez === this.anexo.vez)
              {
                this.listaEnteSocioNegocio[i].descripcionsocionegocio  = this.obtenerTextoSelect(this.anexo.socionegocio, 'listaSocioNegocio');
                this.listaEnteSocioNegocio[i].estado                   = this.anexo.estado;  
                this.listaEnteSocioNegocio[i].descripcionestado        = this.obtenerTextoSelect(this.anexo.estado, 'listaEstado');
              }
            }
          }

        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  registrarDocumento(vez: number, accion: string): void
  {
    this.codigos = '';
    this.documento = new CreateEnteDocumentoIdentidadDto();

    if (accion === 'insertar')
    { 
      this.documento.empresa       = this.valoresiniciales.empresa;
      this.documento.entidad       = this.enteForm.value.entidad;
      this.documento.vez           = 0;
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteDocumentoIdentidad.length; i++ )
      {        
        this.codigos = this.codigos + `'` + this.listaEnteDocumentoIdentidad[i].documentoidentidad + `',`;       
      }
      
      if (this.codigos !== '')
      {      
        this.codigos = this.codigos.slice(0, -1);                   
      }          
      this.documento.descripcionestado  = this.codigos; 
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteDocumentoIdentidad.length; i++ )
      {        
        if ( this.listaEnteDocumentoIdentidad[i].vez === vez)
        {
          this.documento.empresa                       = this.listaEnteDocumentoIdentidad[i].empresa;
          this.documento.entidad                       = this.listaEnteDocumentoIdentidad[i].entidad;
          this.documento.vez                           = this.listaEnteDocumentoIdentidad[i].vez;
          this.documento.documentoidentidad            = this.listaEnteDocumentoIdentidad[i].documentoidentidad;
          this.documento.descripciondocumentoidentidad = this.listaEnteDocumentoIdentidad[i].descripciondocumentoidentidad;
          this.documento.numerodocumento               = this.listaEnteDocumentoIdentidad[i].numerodocumento;
          this.documento.estado                        = this.listaEnteDocumentoIdentidad[i].estado;
        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.documento));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(SocionegocioDocumentoComponent, { 
        panelClass: 'socionegocio-documento-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 

          this.documento = JSON.parse(localStorage.getItem(TABLAS));   

          if (accion === 'insertar')
          {
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteDocumentoIdentidad.length; i++ )
            {        
              if ( this.listaEnteDocumentoIdentidad[i].vez > this.documento.vez)
              {
                vez = this.listaEnteDocumentoIdentidad[i].vez + 1;
              }
            }

            this.documento.vez                            = vez;
            this.documento.descripciondocumentoidentidad  = this.obtenerTextoSelect(this.documento.documentoidentidad, 'listaDocumento');
            this.documento.descripcionestado              = this.obtenerTextoSelect(this.documento.estado, 'listaEstado');
            this.listaEnteDocumentoIdentidad.push(this.documento);  
            this.listaEnteDocumentoIdentidad = this.listaEnteDocumentoIdentidad.slice();  
           
          }
          else
          {            
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteDocumentoIdentidad.length; i++ )
            {        
              if ( this.listaEnteDocumentoIdentidad[i].vez === this.documento.vez)
              {
                this.listaEnteDocumentoIdentidad[i].descripciondocumentoidentidad  = this.obtenerTextoSelect(this.documento.documentoidentidad, 'listaDocumento');
                this.listaEnteDocumentoIdentidad[i].numerodocumento                = this.documento.numerodocumento;  
                this.listaEnteDocumentoIdentidad[i].estado                         = this.documento.estado;
                this.listaEnteDocumentoIdentidad[i].descripcionestado              = this.obtenerTextoSelect(this.documento.estado, 'listaEstado');
              }
            }
          }

        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  registrarDomicilio(vez: number, accion: string): void
  {
    this.codigos = '';
    this.domicilio = new CreateEnteDireccionDto();

    if (accion === 'insertar')
    { 
      this.domicilio.empresa       = this.valoresiniciales.empresa;
      this.domicilio.entidad       = this.enteForm.value.entidad;
      this.domicilio.vez           = 0;
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteDomicilio.length; i++ )
      {        
        if ( this.listaEnteDomicilio[i].vez === vez)
        {
          this.domicilio.empresa                       = this.listaEnteDomicilio[i].empresa;
          this.domicilio.entidad                       = this.listaEnteDomicilio[i].entidad;
          this.domicilio.vez                           = this.listaEnteDomicilio[i].vez;
          this.domicilio.tipodomicilio                 = this.listaEnteDomicilio[i].tipodomicilio;
          this.domicilio.descripciontipodomicilio      = this.listaEnteDomicilio[i].descripciontipodomicilio;
          this.domicilio.pais                          = this.listaEnteDomicilio[i].pais;
          this.domicilio.nombrepais                    = this.listaEnteDomicilio[i].nombrepais;
          this.domicilio.ubigeo                        = this.listaEnteDomicilio[i].ubigeo;
          this.domicilio.descripcionubigeo             = this.listaEnteDomicilio[i].descripcionubigeo;
          this.domicilio.direccion                     = this.listaEnteDomicilio[i].direccion;
          this.domicilio.estado                        = this.listaEnteDomicilio[i].estado;
        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.domicilio));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(SocionegocioDomicilioComponent, { 
        panelClass: 'socionegocio-domicilio-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 

          this.domicilio = JSON.parse(localStorage.getItem(TABLAS));   

          if (accion === 'insertar')
          {
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteDomicilio.length; i++ )
            {        
              if ( this.listaEnteDomicilio[i].vez > this.domicilio.vez)
              {
                vez = this.listaEnteDomicilio[i].vez + 1;
              }
            }

            this.domicilio.vez                            = vez;
            this.domicilio.descripciontipodomicilio       = this.obtenerTextoSelect(this.domicilio.tipodomicilio, 'listaTipoDomicilio');
            this.domicilio.nombrepais                     = this.obtenerTextoSelect(this.domicilio.pais, 'listaPais');
            this.domicilio.descripcionubigeo              = this.obtenerTextoSelect(this.domicilio.ubigeo, 'listaUbicacionGeografica');
            this.domicilio.descripcionestado              = this.obtenerTextoSelect(this.domicilio.estado, 'listaEstado');
            this.listaEnteDomicilio.push(this.domicilio);  
            this.listaEnteDomicilio = this.listaEnteDomicilio.slice();  
           
          }
          else
          {            
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaEnteDomicilio.length; i++ )
            {        
              if ( this.listaEnteDomicilio[i].vez === this.domicilio.vez)
              {
                this.listaEnteDomicilio[i].tipodomicilio             = this.domicilio.tipodomicilio;  
                this.listaEnteDomicilio[i].descripciontipodomicilio  = this.obtenerTextoSelect(this.domicilio.tipodomicilio, 'listaTipoDomicilio');
                this.listaEnteDomicilio[i].pais                      = this.domicilio.pais;  
                this.listaEnteDomicilio[i].nombrepais                = this.obtenerTextoSelect(this.domicilio.pais, 'listaPais');
                this.listaEnteDomicilio[i].ubigeo                    = this.domicilio.ubigeo;  
                this.listaEnteDomicilio[i].descripcionubigeo         = this.obtenerTextoSelect(this.domicilio.ubigeo, 'listaUbicacionGeografica');
                this.listaEnteDomicilio[i].estado                    = this.domicilio.estado;  
                this.listaEnteDomicilio[i].descripcionestado         = this.obtenerTextoSelect(this.domicilio.estado, 'listaEstado');
              }
            }
          }

        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarAnexo(vez: number)
  {
    // Elimina registros por destino
    for ( let i = this.listaEnteSocioNegocio.length - 1; i > -1; i-- )
    {                
      if ( this.listaEnteSocioNegocio[i].vez === vez )
      {
        this.listaEnteSocioNegocio.splice(i, 1);
      }                
    }

    this.listaEnteSocioNegocio = this.listaEnteSocioNegocio.slice();  

  }

  // tslint:disable-next-line:typedef
  async eliminarDocumento(vez: number)
  {
    // Elimina registros por destino
    for ( let i = this.listaEnteDocumentoIdentidad.length - 1; i > -1; i-- )
    {                
      if ( this.listaEnteDocumentoIdentidad[i].vez === vez )
      {
        this.listaEnteDocumentoIdentidad.splice(i, 1);
      }                
    }

    this.listaEnteDocumentoIdentidad = this.listaEnteDocumentoIdentidad.slice();  

  }  

  // tslint:disable-next-line:typedef
  async eliminarDomicilio(vez: number)
  {
    // Elimina registros por destino
    for ( let i = this.listaEnteDomicilio.length - 1; i > -1; i-- )
    {                
      if ( this.listaEnteDomicilio[i].vez === vez )
      {
        this.listaEnteDomicilio.splice(i, 1);
      }                
    }

    this.listaEnteDomicilio = this.listaEnteDomicilio.slice();  

  } 

  // tslint:disable-next-line:typedef
  obtenerTextoSelect(codigo: string, lista: string)
  {
 
    // Socio Negocio
    if (lista === 'listaSocioNegocio') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaSocioNegocio.length; i++ )      
      {
        if ( this.listaSocioNegocio[i].valor === codigo )
        {
         return this.listaSocioNegocio[i].descripcion;
        }
      }
    }  

    // Tipo Documento
    if (lista === 'listaDocumento') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaDocumento.length; i++ )      
      {
        if ( this.listaDocumento[i].valor === codigo )
        {
        return this.listaDocumento[i].descripcion;
        }
      }
    } 

    // Tipo Domicilio
    if (lista === 'listaTipoDomicilio') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaTipoDomicilio.length; i++ )      
      {
        if ( this.listaTipoDomicilio[i].valor === codigo )
        {
        return this.listaTipoDomicilio[i].descripcion;
        }
      }
    } 

    // País
    if (lista === 'listaPais') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaPais.length; i++ )      
      {
        if ( this.listaPais[i].valor === codigo )
        {
        return this.listaPais[i].descripcion;
        }
      }
    } 

    // País
    if (lista === 'listaUbicacionGeografica') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaUbicacionGeografica.length; i++ )      
      {
        if ( this.listaUbicacionGeografica[i].codigo === codigo )
        {
        return this.listaUbicacionGeografica[i].descripcion;
        }
      }
    } 

    // Estado
    if (lista === 'listaEstado') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEstado.length; i++ )      
      {
        if ( this.listaEstado[i].valor === codigo )
        {
         return this.listaEstado[i].descripcion;
        }
      }
    } 
    
  }  

  onBlurSocioNegocioRUC(): void
  {

    if ( this.enteForm.value.ruc.replace(/\s+$/g, '') === null || this.enteForm.value.ruc.replace(/\s+$/g, '') === '')
    {
      this.enteForm.controls['ruc'].reset();
    }
    else
    {
      this.ente.ruc = this.enteForm.value.ruc;
      if (this.ente.ruc !== this.ruc)
      {
        this.enteService.contarRuc(this.ente).subscribe(data => {
          this.valorcontador = data;
    
          if (this.valorcontador.contador > 0)
            {
              this._matSnackBar.open('Error, Socio de Negocio, ya existe', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
              this.enteForm.controls['ruc'].reset();
            }
          }, 
          err => 
          {
              this._matSnackBar.open('Error, error de servicio', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
        );
      }

    }
  }
}
