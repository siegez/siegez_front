import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-socionegocio-anexo',
  templateUrl: './socionegocio-anexo.component.html',
  styleUrls: ['./socionegocio-anexo.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SocionegocioAnexoComponent implements OnInit {

  accion: string;
 
  anexo;  
  valoresiniciales;
  valorcampo;
  anexoForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaSocioNegocio: any[];
  listaEstado: any[];
  checkEstado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<SocionegocioAnexoComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService
  ) 
  { 
    this.accion = _data.accion; 
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   

    this.anexo = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Anexo';    
      this.checkEstado = (this.anexo.estado === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Anexo';
      this.checkEstado = true;
    }     
    this.anexoForm = this.crearAnexoForm();  
    this.listarSocioNegocio();       
 
  }

  crearAnexoForm(): FormGroup
  {
      return this._formBuilder.group({
          socionegocio             : [this.anexo.socionegocio],
          descripcionsocionegocio  : [this.anexo.descripcionsocionegocio],
          estado                   : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarAnexo(anexoForm){

    this.anexo.socionegocio     = anexoForm.value.socionegocio;    
    this.anexo.estado           = (this.anexoForm.value.estado) ? 'A' : 'I';

    localStorage.setItem(TABLAS, JSON.stringify(this.anexo));
    localStorage.setItem(REFRESH, 'S');

    this.matDialogRef.close();

  }

  // tslint:disable-next-line:typedef
  listarSocioNegocio(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDSOCIONEGOCIO';
    this.valorcampo.valor = this.anexo.descripcionestado;
    this.consultaService.listarCatalogoDescripcionFiltro(this.valorcampo).subscribe(data => {
      this.listaSocioNegocio = data;
    });
  }

}    
  



