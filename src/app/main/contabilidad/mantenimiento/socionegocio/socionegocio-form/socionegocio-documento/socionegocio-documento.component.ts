import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-socionegocio-documento',
  templateUrl: './socionegocio-documento.component.html',
  styleUrls: ['./socionegocio-documento.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SocionegocioDocumentoComponent implements OnInit {

  accion: string;
 
  documento;  
  valoresiniciales;
  valorcampo;
  documentoForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaDocumento: any[];
  listaEstado: any[];
  checkEstado = false;
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<SocionegocioDocumentoComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService
  ) 
  {
    this.accion = _data.accion; 
  }

  ngOnInit(): void 
  {
    this.documento = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Documento';    
      this.checkEstado = (this.documento.estado === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Documento';
      this.checkEstado = true;
    }     
    this.documentoForm = this.crearDocumentoForm();  
    this.listarDocumento();   
  }

  crearDocumentoForm(): FormGroup
  {
      return this._formBuilder.group({
        documentoidentidad             : [this.documento.documentoidentidad],
        descripciondocumentoidentidad  : [this.documento.descripciondocumentoidentidad],
        numerodocumento                : [this.documento.numerodocumento],
        estado                         : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarDocumento(documentoForm){

    this.documento.documentoidentidad  = documentoForm.value.documentoidentidad;  
    this.documento.numerodocumento     = documentoForm.value.numerodocumento;  
    this.documento.estado              = (this.documentoForm.value.estado) ? 'A' : 'I';

    localStorage.setItem(TABLAS, JSON.stringify(this.documento));
    localStorage.setItem(REFRESH, 'S');

    this.matDialogRef.close();

  }

  // tslint:disable-next-line:typedef
  listarDocumento(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDDOCIDENTIDAD';
    this.valorcampo.valor = this.documento.descripcionestado;
    this.consultaService.listarCatalogoDescripcionFiltro(this.valorcampo).subscribe(data => {
      this.listaDocumento = data;
    });
  }


}
