import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-socionegocio-domicilio',
  templateUrl: './socionegocio-domicilio.component.html',
  styleUrls: ['./socionegocio-domicilio.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SocionegocioDomicilioComponent implements OnInit {

  accion: string;
 
  domicilio;  
  valoresiniciales;
  valorcampo;
  domicilioForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaTipoDomicilio: any[];
  listaPais: any[];
  listaUbicacionGeografica: any[];
  listaEstado: any[];
  checkEstado = false;
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<SocionegocioDomicilioComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService
  ) 
  {
    this.accion = _data.accion; 
  }

  ngOnInit(): void 
  {
    this.domicilio = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Domicilio';    
      this.checkEstado = (this.domicilio.estado === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Domicilio';
      this.checkEstado = true;
    }     
    this.domicilioForm = this.crearDomicilioForm();  
    this.listarTipoDomicilio();   
    this.listarPais(); 
    this.listarUbicacionGeografica(); 
  }

  crearDomicilioForm(): FormGroup
  {
      return this._formBuilder.group({
        tipodomicilio             : [this.domicilio.tipodomicilio],
        descripciontipodomicilio  : [this.domicilio.descripciontipodomicilio],
        pais                      : [this.domicilio.pais],
        nombrepais                : [this.domicilio.nombrepais],
        ubigeo                    : [this.domicilio.ubigeo],
        descripcionubigeo         : [this.domicilio.descripcionubigeo],
        direccion                 : [this.domicilio.direccion],
        estado                    : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarDomicilio(domicilioForm){

    this.domicilio.tipodomicilio            = domicilioForm.value.tipodomicilio;  
    this.domicilio.descripciontipodomicilio = domicilioForm.value.descripciontipodomicilio;  
    this.domicilio.pais                     = domicilioForm.value.pais; 
    this.domicilio.nombrepais               = domicilioForm.value.nombrepais; 
    this.domicilio.ubigeo                   = domicilioForm.value.ubigeo; 
    this.domicilio.descripcionubigeo        = domicilioForm.value.descripcionubigeo; 
    this.domicilio.direccion                = domicilioForm.value.direccion; 
    this.domicilio.estado                   = (this.domicilioForm.value.estado) ? 'A' : 'I';

    localStorage.setItem(TABLAS, JSON.stringify(this.domicilio));
    localStorage.setItem(REFRESH, 'S');

    this.matDialogRef.close();

  }

  // tslint:disable-next-line:typedef
  listarPais(){
    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 35).subscribe(data => {
      this.listaPais = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoDomicilio(){
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPODOMICILIO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoDomicilio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarUbicacionGeografica(){

    this.consultaService.listarUbicacionGeografica().subscribe(data => {
      this.listaUbicacionGeografica = data;
    });
  }


}

