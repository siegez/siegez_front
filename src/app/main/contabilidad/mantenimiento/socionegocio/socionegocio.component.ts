import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DATOS, VALORES_INICIALES, MESSAGEBOX } from 'app/_shared/var.constant';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ExcelService } from 'app/services/excel.service';
import { EnteService } from 'app/services/ente.service';
import { CreateEnteDto } from 'app/dto/create.ente.dto';

@Component({
  selector: 'gez-socionegocio',
  templateUrl: './socionegocio.component.html',
  styleUrls: ['./socionegocio.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class SocionegocioComponent implements OnInit {

  dialogRef: any;
  ente;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['entidad', 'nombres', 'tipopersona', 'ruc', 'nodomiciliado',
                      'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private enteService: EnteService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.ente = new CreateEnteDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.entidad.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.nombres.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripciontipopersona.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.ruc.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.descripcionnodomiciliado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.enteService.listar(this.valoresiniciales.empresa).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearSocioNegocio(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`contabilidad/mantenimiento/socionegocio-form`]);
  }

  editarSocioNegocio(entidad: string): void
  {
    const misDatos =  { 
                        action: 'editar',
                        parametro1: entidad
                      };

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`contabilidad/mantenimiento/socionegocio-form`]);
    
  }

  eliminarSocioNegocio(entidad: string): void
  {
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Socio Negocio: ' + entidad,
          subTitulo: '¿Está seguro de eliminar el socio de negocio?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

  // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.ente.empresa    = this.valoresiniciales.empresa;
          this.ente.entidad    = entidad;

          this._matSnackBar.open('Eliminando socio de negocio ...', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 200
            });

          this.enteService.eliminar(this.ente).subscribe(data => {
          this._matSnackBar.open('Se eliminó la socio de negocio', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, socio de negocio no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );    
  }

  exportarSocioNegocio(): void
  {
    this.enteService.exportar(this.valoresiniciales.empresa).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Código'] = item.entidad;
          objectReport['Nombre'] = item.nombres;   
          objectReport['Tipo Persona'] = item.descripciontipopersona; 
          objectReport['Documento de Identidad'] = item.ruc; 
          objectReport['No Domiciliado'] = item.descripcionnodomiciliado; 
          objectReport['Estado'] = item.descripcionestado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'SocioNegocio');

      }

    }); 
  }

}

