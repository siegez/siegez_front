
import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO, MESSAGEBOX, TABLAS } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { GrupoCentroCostoService } from 'app/services/grupocentrocosto.service';
import { GrupoCentroCostoDetalleService } from 'app/services/grupocentrocostodetalle.service';
import { CreateGrupoCentroCostoDto } from 'app/dto/create.grupocentrocosto.dto';
import { CreateGrupoCentroCostoDetalleDto } from 'app/dto/create.grupocentrocostodetalle.dto';
import { GrupocentrocostoCentrocostoComponent } from './grupocentrocosto-centrocosto/grupocentrocosto-centrocosto.component';
import { CentroCostoService } from 'app/services/centrocosto.service';

@Component({
  selector: 'gez-grupocentrocosto-form',
  templateUrl: './grupocentrocosto-form.component.html',
  styleUrls: ['./grupocentrocosto-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class GrupocentrocostoFormComponent implements OnInit, AfterContentChecked {
  
  displayedColumnsCentroCosto = ['nombrecentrocosto', 'porcentaje', 'estado', 'editar', 'eliminar'];
 
  misDatos;  
  grupocentrocostoForm: FormGroup;
  grupocentrocosto;
  grupocentrocostodetalle;
  centrocosto;
  resultadosp;
  valorcampo;
  valoresiniciales;
  valorcontador;
  valorcatalogo;
  excluir;
  edit = true;  
  listaCentroCosto: any = [];
  listaCatalogoCentroCosto: any = [];
  listaEstado: any[];
  listaExcluir: any[];
  checkEstado = false;
  dialogRef: any;

  codigos;
  
  constructor(
    private grupocentrocostoService: GrupoCentroCostoService,
    private consultaService: ConsultaService,
    private grupocentrocostodetalleService: GrupoCentroCostoDetalleService,
    private centrocostoService: CentroCostoService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private cdref: ChangeDetectorRef
  ) 
  {    
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }
  
  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.valorcontador = new ViewValorContadorDto();   
    this.grupocentrocosto = new CreateGrupoCentroCostoDto();

    this.grupocentrocostoForm = this.createGrupoCentroCostoForm();

    this.listarCatalogoCentroCosto(); 
    this.listarEstado(); 

    // campos pk
    this.grupocentrocosto.empresa            = this.valoresiniciales.empresa;
    this.grupocentrocosto.ejerciciocontable  = this.valoresiniciales.annoproceso;

    if ( this.misDatos.action === 'editar' )
    {
      // campo pk
      this.grupocentrocosto.grupocentrocosto       = this.misDatos.parametro1;      
      this.obtenerGrupoCentroCosto();  
      this.obtenerCentroCosto();
  
    }
    else{
      this.edit = false;
      this.checkEstado = true;
      this.iniciarGrupoCentroCosto();

    }  

  }

  // tslint:disable-next-line:typedef
  obtenerGrupoCentroCosto(){

    this.grupocentrocostoService.consultar(this.grupocentrocosto.empresa, this.grupocentrocosto.ejerciciocontable, this.grupocentrocosto.grupocentrocosto).subscribe(data => {
      this.grupocentrocosto = data;    
      this.checkEstado            = (this.grupocentrocosto.estado === 'A' ) ? true : false;

      this.grupocentrocostoForm = this.createGrupoCentroCostoForm();

      }, err => {
        this._matSnackBar.open('Error de consulta el grupo de centro de costo', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );  
  }

  // tslint:disable-next-line:typedef
  iniciarGrupoCentroCosto(){
    setTimeout(() => {
      this.grupocentrocostoForm.controls['grupocentrocosto'].setValue('');
      this.grupocentrocostoForm.controls['nombregrupocentrocosto'].setValue('');
      this.grupocentrocostoForm.controls['estado'].setValue(true);
      
    });
  }
  
  createGrupoCentroCostoForm(): FormGroup
  {
    return this._formBuilder.group({
      grupocentrocosto          : [this.grupocentrocosto.grupocentrocosto],
      nombregrupocentrocosto    : [this.grupocentrocosto.nombregrupocentrocosto],
      estado                    : [this.checkEstado]    
      });

  }
   

  grabarGrupoCentroCosto(): void
  {
    // Solicitar confirmación
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Grabar Grupo Centro de Costo',
          subTitulo: '¿Desea continuar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    this.dialogRef.afterClosed().subscribe(async result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          localStorage.setItem(MESSAGEBOX, 'N');  
    
          this.grupocentrocosto.grupocentrocosto        = this.grupocentrocostoForm.value.grupocentrocosto;
          this.grupocentrocosto.nombregrupocentrocosto  = this.grupocentrocostoForm.value.nombregrupocentrocosto;
          this.grupocentrocosto.estado                  = (this.grupocentrocostoForm.value.estado) ? 'A' : 'I';
          this.grupocentrocosto.creacionUsuario         = this.valoresiniciales.username;
          this.grupocentrocosto.creacionFecha           = '2020-01-01';
          this.grupocentrocosto.modificacionUsuario     = this.valoresiniciales.username;
          this.grupocentrocosto.modificacionFecha       = '2020-01-01';
          this.grupocentrocosto.grupocentrocostodetalle = this.listaCentroCosto;  

          if ( this.misDatos.action === 'nuevo' )
          {
            this.grupocentrocosto.creacionFecha         = '0000-00-00';
          }

          this.resultadosp = new ViewResultadoSPDto();
      
          // consumir servicios para grabar

          this.grupocentrocostoService.registrar(this.grupocentrocosto).subscribe(data => {
            this.resultadosp = data;  
            if (this.resultadosp.indicadorexito === 'S')
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                verticalPosition: 'bottom',
                duration        : 2000
                  });
        
                const misDatosNew =  { 
                    action: 'editar',
                    parametro1: this.grupocentrocosto.grupocentrocosto
                  };
        
                localStorage.setItem(DATOS, JSON.stringify(misDatosNew));
        
                this.misDatos = JSON.parse(localStorage.getItem(DATOS));
        
                this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
        
                this.edit = true;
    
                this.ngOnInit();

              }
            else
              {
                this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
                  verticalPosition: 'bottom',
                  panelClass: ['my-snack-bar'] 
                  });
              }
    
            }, 
            err => 
            {
                this._matSnackBar.open('Error, grupo centro de costo no registrado', '', {
                  verticalPosition: 'bottom',
                  duration        : 2000
                });
            }
          );           
        }
      }        
    ); 

  }

  // tslint:disable-next-line:typedef
  listarEstado(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ESTADO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaEstado = data;
    });
  }

  // tslint:disable-next-line:typedef
  async listarCatalogoCentroCosto(){

    this.listaCatalogoCentroCosto = (await this.centrocostoService.listarCatalogo(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).toPromise());

  }


  crearGrupoCentroCosto(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.ngOnInit();

  }

  // tslint:disable-next-line:typedef
  async obtenerCentroCosto() 
  {  
    // tslint:disable-next-line:max-line-length
    this.listaCentroCosto = (await this.grupocentrocostodetalleService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.grupocentrocosto.grupocentrocosto).toPromise());
          
  }  
  
  registrarCentroCosto(centrocosto: string, accion: string): void
  {
    this.codigos = '';
    this.grupocentrocostodetalle = new CreateGrupoCentroCostoDetalleDto();

    if (accion === 'insertar')
    { 
      this.grupocentrocostodetalle.empresa            = this.valoresiniciales.empresa;
      this.grupocentrocostodetalle.ejerciciocontable  = this.valoresiniciales.annoproceso;
      this.grupocentrocostodetalle.grupocentrocosto   = this.grupocentrocostoForm.value.grupocentrocosto;
      this.grupocentrocostodetalle.centrocosto        = '';
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCentroCosto.length; i++ )
      {        
        this.codigos = this.codigos + `'` + this.listaCentroCosto[i].centrocosto + `',`;       
      }
      
      if (this.codigos !== '')
      {      
        this.codigos = this.codigos.slice(0, -1);                   
      }          
      this.grupocentrocostodetalle.descripcionestado  = this.codigos; 
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCentroCosto.length; i++ )
      {        
        if ( this.listaCentroCosto[i].centrocosto === centrocosto)
        {
          this.grupocentrocostodetalle.empresa                  = this.listaCentroCosto[i].empresa;
          this.grupocentrocostodetalle.ejerciciocontable        = this.listaCentroCosto[i].ejerciciocontable;
          this.grupocentrocostodetalle.grupocentrocosto         = this.listaCentroCosto[i].grupocentrocosto;
          this.grupocentrocostodetalle.nombregrupocentrocosto   = this.listaCentroCosto[i].nombregrupocentrocosto;
          this.grupocentrocostodetalle.centrocosto              = this.listaCentroCosto[i].centrocosto;
          this.grupocentrocostodetalle.nombrecentrocosto        = this.listaCentroCosto[i].nombrecentrocosto;
          // this.grupocentrocostodetalle.cuentacontablecargo      = this.listaCentroCosto[i].cuentacontablecargo;
          // this.grupocentrocostodetalle.cuentacontableabono      = this.listaCentroCosto[i].cuentacontableabono;
          this.grupocentrocostodetalle.porcentaje               = this.listaCentroCosto[i].porcentaje;
          this.grupocentrocostodetalle.estado                   = this.listaCentroCosto[i].estado;
        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.grupocentrocostodetalle));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(GrupocentrocostoCentrocostoComponent, { 
        panelClass: 'grupocentrocosto-centrocosto-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {                 

          this.grupocentrocostodetalle = JSON.parse(localStorage.getItem(TABLAS));   

          if (accion === 'insertar')
          {
            
            this.grupocentrocostodetalle.nombrecentrocosto  = this.obtenerTextoSelect(this.grupocentrocostodetalle.centrocosto, 'listaCatalogoCentroCosto');
            this.grupocentrocostodetalle.descripcionestado  = this.obtenerTextoSelect(this.grupocentrocostodetalle.estado, 'listaEstado');
            this.listaCentroCosto.push(this.grupocentrocostodetalle);  
            this.listaCentroCosto = this.listaCentroCosto.slice();  
           
          }
          else
          {            
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < this.listaCentroCosto.length; i++ )
            {        
              if ( this.listaCentroCosto[i].centrocosto === this.grupocentrocostodetalle.centrocosto)
              {
                this.listaCentroCosto[i].nombrecentrocosto  = this.obtenerTextoSelect(this.grupocentrocostodetalle.centrocosto, 'listaCatalogoCentroCosto');
                this.listaCentroCosto[i].porcentaje         = this.grupocentrocostodetalle.porcentaje;
                this.listaCentroCosto[i].estado             = this.grupocentrocostodetalle.estado;  
                this.listaCentroCosto[i].descripcionestado  = this.obtenerTextoSelect(this.grupocentrocostodetalle.estado, 'listaEstado');
              }
            }
          }

        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }
  
  // tslint:disable-next-line:typedef
  async eliminarCentroCosto(centrocosto: string)
  {
    // Elimina registros por destino
    for ( let i = this.listaCentroCosto.length - 1; i > -1; i-- )
    {                
      if ( this.listaCentroCosto[i].centrocosto === centrocosto)
      {
        this.listaCentroCosto.splice(i, 1);
      }                
    }

    this.listaCentroCosto = this.listaCentroCosto.slice();  

  }

  // tslint:disable-next-line:typedef
  obtenerTextoSelect(codigo: string, lista: string)
  {   

    // Estado
    if (lista === 'listaEstado') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEstado.length; i++ )      
      {
        if ( this.listaEstado[i].valor === codigo )
        {
         return this.listaEstado[i].descripcion;
        }
      }
    } 
    
    // Centro de Costo
    if (lista === 'listaCatalogoCentroCosto')
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCatalogoCentroCosto.length; i++ )      
      {
        if ( this.listaCatalogoCentroCosto[i].codigo === codigo )
        {
         return this.listaCatalogoCentroCosto[i].descripcion;
        }
      }
    }
    
  }  
  
}
