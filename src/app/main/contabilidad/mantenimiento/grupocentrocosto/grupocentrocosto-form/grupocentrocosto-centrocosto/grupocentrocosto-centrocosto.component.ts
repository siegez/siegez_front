import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { CentroCostoService } from 'app/services/centrocosto.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-grupocentrocosto-centrocosto',
  templateUrl: './grupocentrocosto-centrocosto.component.html',
  styleUrls: ['./grupocentrocosto-centrocosto.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class GrupocentrocostoCentrocostoComponent implements OnInit {

  accion: string;
 
  centrocosto;  
  valoresiniciales;
  valorcampo;
  centrocostoForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Aceptar';

  listaCentroCosto: any[];
  listaEstado: any[];
  checkEstado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<GrupocentrocostoCentrocostoComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private centrocostoService: CentroCostoService,
  ) 
  { 
    this.accion = _data.accion; 
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   

    this.centrocosto = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();

    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Centro de Costo';    
      this.checkEstado = (this.centrocosto.estado === 'A' ) ? true : false;   
    }
    else
    {
      this.dialogTitle = 'Nuevo Centro de Costo';
      this.checkEstado = true;
    }     
    this.centrocostoForm = this.crearCentroCostoForm();  
    this.listarCentroCosto();       
 
  }

  crearCentroCostoForm(): FormGroup
  {
      return this._formBuilder.group({
        centrocosto       : [this.centrocosto.centrocosto],
        nombrecentrocosto : [this.centrocosto.nombrecentrocosto],
        porcentaje        : [this.centrocosto.porcentaje],
        estado            : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  aceptarCentroCosto(centrocostoForm){

    this.centrocosto.centrocosto    = centrocostoForm.value.centrocosto;  
    this.centrocosto.porcentaje     = centrocostoForm.value.porcentaje;   
    this.centrocosto.estado         = (this.centrocostoForm.value.estado) ? 'A' : 'I';

    localStorage.setItem(TABLAS, JSON.stringify(this.centrocosto));
    localStorage.setItem(REFRESH, 'S');

    this.matDialogRef.close();

  }

  // tslint:disable-next-line:typedef
  async listarCentroCosto(){

    this.listaCentroCosto = (await this.centrocostoService.listarCatalogoFiltroNotIn(this.centrocosto).toPromise());

  }

}    
  




