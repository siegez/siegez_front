import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { EnteCuentaBancariaService } from 'app/services/entecuentabancaria.service';
import { PlanContableService } from 'app/services/plancontable.service';
import { REFRESH, VALORES_INICIALES, TABLAS } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-entidadfinanciera-cuentabancaria',
  templateUrl: './entidadfinanciera-cuentabancaria.component.html',
  styleUrls: ['./entidadfinanciera-cuentabancaria.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class EntidadfinancieraCuentabancariaComponent implements OnInit, AfterContentChecked {

  accion: string;
 
  cuentabancaria;  
  valoresiniciales;
  valorcampo;
  valorcontador;
  cuentabancariaForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  listaTipoCuenta: any[];
  listaCuentaContable: any[];
  listaEstado: any[];
  edit = true;
  checkEstado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<EntidadfinancieraCuentabancariaComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private consultaService: ConsultaService,
    private plancontableService: PlanContableService,
    private entecuentabancariaService: EnteCuentaBancariaService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion; 
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   

    this.cuentabancaria = JSON.parse(localStorage.getItem(TABLAS));

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    
    if ( this.accion === 'editar' )
    {
      // tslint:disable-next-line:max-line-length
      this.entecuentabancariaService.consultar(this.cuentabancaria.empresa, this.cuentabancaria.ejerciciocontable, this.cuentabancaria.entidad, this.cuentabancaria.cuentabancaria).subscribe(data => {
        this.cuentabancaria = data;         
        this.checkEstado = (this.cuentabancaria.estado === 'A' ) ? true : false;  
        this.cuentabancariaForm = this.crearCuentaBancariaForm();  

        }, err => {
          this._matSnackBar.open('Error de consulta cuenta bancaria', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      );     
      this.dialogTitle = 'Editar Cuenta Bancaria';           
    }
    else
    {
      this.dialogTitle = 'Nueva Cuenta Bancaria';
      this.checkEstado = true;
      this.edit = false;
    }     
    this.cuentabancariaForm = this.crearCuentaBancariaForm();  
    this.listarTipoCuenta();      
    this.listarCuentaContable(); 
 
  }

  crearCuentaBancariaForm(): FormGroup
  {
      return this._formBuilder.group({
        cuentabancaria        : [this.cuentabancaria.cuentabancaria],
        cuentainterbancaria   : [this.cuentabancaria.cuentainterbancaria],
        nombrecuentabancaria  : [this.cuentabancaria.nombrecuentabancaria],
        tipocuentabancaria    : [this.cuentabancaria.tipocuentabancaria],
        cuenta                : [this.cuentabancaria.cuenta],
        estado                : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  grabarCuentaBancaria(cuentabancariaForm){

    this.cuentabancaria.cuentabancaria         = cuentabancariaForm.value.cuentabancaria;    
    this.cuentabancaria.cuentainterbancaria    = cuentabancariaForm.value.cuentainterbancaria;  
    this.cuentabancaria.nombrecuentabancaria   = cuentabancariaForm.value.nombrecuentabancaria;
    this.cuentabancaria.tipocuentabancaria     = cuentabancariaForm.value.tipocuentabancaria;
    this.cuentabancaria.cuenta                 = cuentabancariaForm.value.cuenta;
    this.cuentabancaria.estado                 = (this.cuentabancariaForm.value.estado) ? 'A' : 'I';
    this.cuentabancaria.creacionUsuario        = this.valoresiniciales.username;
    this.cuentabancaria.creacionFecha          = '2020-01-01';
    this.cuentabancaria.modificacionUsuario    = this.valoresiniciales.username;
    this.cuentabancaria.modificacionFecha      = '2020-01-01';
    
    if ( this.accion === 'editar' )
    {
      this.entecuentabancariaService.editar(this.cuentabancaria).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Cuenta Bancaria no actualizada', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    else
    {
      this.entecuentabancariaService.crear(this.cuentabancaria).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Cuenta Bancaria no creada', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    

  }

   // tslint:disable-next-line:typedef
   listarTipoCuenta(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOCUENTABANCARIA';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoCuenta = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarCuentaContable(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCuentaContable = data;
    });
  }

  onBlurCuentaBancaria(): void
  {
    if ( this.cuentabancariaForm.value.cuentabancaria.replace(/\s+$/g, '') === null || this.cuentabancariaForm.value.cuentabancaria.replace(/\s+$/g, '') === '')
    {
      this.cuentabancariaForm.controls['cuentabancaria'].reset();
    }
    else
    {
      
      this.cuentabancaria.cuentabancaria = this.cuentabancariaForm.value.cuentabancaria;
  
      // tslint:disable-next-line:max-line-length
      this.entecuentabancariaService.contar(this.cuentabancaria.empresa, this.cuentabancaria.ejerciciocontable, this.cuentabancaria.entidad, this.cuentabancaria.cuentabancaria).subscribe(data => {
        this.valorcontador = data;
  
        if (this.valorcontador.contador > 0)
          {
            this._matSnackBar.open('Error, Cuenta Bancaria ya existe', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
            this.cuentabancariaForm.controls['cuentabancaria'].reset();
          }
        }, 
        err => 
        {
            this._matSnackBar.open('Error, error de servicio', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
  
    }
  }

}    
  



