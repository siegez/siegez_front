import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO, MESSAGEBOX, TABLAS } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EnteService } from 'app/services/ente.service';
import { CreateEnteDto } from 'app/dto/create.ente.dto';
import { EnteCuentaBancariaService } from 'app/services/entecuentabancaria.service';
import { PlanContableService } from 'app/services/plancontable.service';
import { CreateEnteCuentaBancariaDto } from 'app/dto/create.entecuentabancaria.dto';
import { EntidadfinancieraCuentabancariaComponent } from './entidadfinanciera-cuentabancaria/entidadfinanciera-cuentabancaria.component';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';

@Component({
  selector: 'gez-cuentabancaria-form',
  templateUrl: './cuentabancaria-form.component.html',
  styleUrls: ['./cuentabancaria-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class CuentabancariaFormComponent implements OnInit {

  
  displayedColumnsCuentaBancaria = ['cuentabancaria', 'cuentainterbancaria', 'denominacion', 'tipocuenta', 'cuentacontable', 'estado', 'editar', 'eliminar'];

  misDatos;  
  ente;
  cuentabancaria;  
  resultadosp;
  valorcampo;
  valoresiniciales;
  valorcontador;
  valorcatalogo;
  excluir;
  edit = true;  
  listaEnteCuentaBancaria: any = [];

  listaTipoCuenta: any[];
  listaCuentaContable: any[];
  listaEstado: any[];

  dialogRef: any;

  constructor(
    private enteService: EnteService,
    private consultaService: ConsultaService,
    private entecuentabancariaService: EnteCuentaBancariaService,
    private plancontableService: PlanContableService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog
  ) 
  {    
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.valorcontador = new ViewValorContadorDto();   
    this.ente = new CreateEnteDto();

    // this.listarTipoCuenta();
    // this.listarCuentaContable();
    // this.listarEstado(); 

    // campos pk
    this.ente.empresa            = this.valoresiniciales.empresa;

    // campo pk
    this.ente.entidad       = this.misDatos.parametro1;      
    this.obtenerEnte();  
    this.obtenerCuentaBancaria();

  }

  // tslint:disable-next-line:typedef
  obtenerEnte(){

    this.enteService.consultar(this.ente.empresa, this.ente.entidad).subscribe(data => {
      this.ente = data;    

      }, err => {
        this._matSnackBar.open('Error de consulta al socio de negocio', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );  
  }

  // tslint:disable-next-line:typedef
  listarTipoCuenta(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOCUENTABANCARIA';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoCuenta = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarCuentaContable(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCuentaContable = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarEstado(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'ESTADO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaEstado = data;
    });
  }
    
  // tslint:disable-next-line:typedef
  async obtenerCuentaBancaria() 
  {  
    // tslint:disable-next-line:max-line-length
    this.listaEnteCuentaBancaria = (await this.entecuentabancariaService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.ente.entidad).toPromise());
          
  }  

  registrarCuentaBancaria(cuentabancaria: string, accion: string): void
  {

    this.cuentabancaria = new CreateEnteCuentaBancariaDto();

    if (accion === 'insertar')
    { 
      this.cuentabancaria.empresa            = this.valoresiniciales.empresa;
      this.cuentabancaria.ejerciciocontable  = this.valoresiniciales.annoproceso;
      this.cuentabancaria.entidad            = this.ente.entidad;
      this.cuentabancaria.cuentabancaria     = '';     
    }
    else
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEnteCuentaBancaria.length; i++ )
      {        
        if ( this.listaEnteCuentaBancaria[i].cuentabancaria === cuentabancaria)
        {
          this.cuentabancaria.empresa                       = this.listaEnteCuentaBancaria[i].empresa;
          this.cuentabancaria.ejerciciocontable             = this.listaEnteCuentaBancaria[i].ejerciciocontable;
          this.cuentabancaria.entidad                       = this.listaEnteCuentaBancaria[i].entidad;
          this.cuentabancaria.cuentabancaria                = this.listaEnteCuentaBancaria[i].cuentabancaria;

        }
      }
    }

    localStorage.setItem(TABLAS, JSON.stringify(this.cuentabancaria));

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(EntidadfinancieraCuentabancariaComponent, {  
        panelClass: 'entidadfinanciera-cuentabancaria-dialog',
        data      : {
          accion: accion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarCuentaBancaria(cuentabancaria: string)
  {
    
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Cuenta Bancaria: ' + cuentabancaria,
          subTitulo: '¿Está seguro de eliminar la Cuenta Bancaria?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {

          this.cuentabancaria = new CreateEnteCuentaBancariaDto();

          this.cuentabancaria.empresa            = this.valoresiniciales.empresa;
          this.cuentabancaria.ejerciciocontable  = this.valoresiniciales.annoproceso;
          this.cuentabancaria.entidad            = this.ente.entidad;
          this.cuentabancaria.cuentabancaria     = cuentabancaria;    

          this.entecuentabancariaService.eliminar(this.cuentabancaria).subscribe(data => {
          this._matSnackBar.open('Se eliminó la Cuenta Bancaria', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, Cuenta Bancaria no eliminada', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );

  }

  // tslint:disable-next-line:typedef
  obtenerTextoSelect(codigo: string, lista: string)
  {
 
    // Tipo Cuenta
    if (lista === 'listaTipoCuenta') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaTipoCuenta.length; i++ )      
      {
        if ( this.listaTipoCuenta[i].valor === codigo )
        {
         return this.listaTipoCuenta[i].descripcion;
        }
      }
    }  

    // Cuenta Contable
    if (lista === 'listaCuenaContable') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaCuentaContable.length; i++ )      
      {
        if ( this.listaCuentaContable[i].codigo === codigo )
        {
        return this.listaCuentaContable[i].descripcion;
        }
      }
    }  

    // Estado
    if (lista === 'listaEstado') 
    {
      // tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.listaEstado.length; i++ )      
      {
        if ( this.listaEstado[i].valor === codigo )
        {
         return this.listaEstado[i].descripcion;
        }
      }
    } 
    
  }  

}
