import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ExcelService } from 'app/services/excel.service';
import { ParametroService } from 'app/services/parametro.service';
import { ParametroFormComponent } from '../parametro-form/parametro-form.component';


@Component({
  selector: 'gez-subdiarioinicio',
  templateUrl: './subdiarioinicio.component.html',
  styleUrls: ['./subdiarioinicio.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class SubdiarioinicioComponent implements OnInit {

  dialogRef: any;
  parametro;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['descripcion', 'descripcionvalor', 'estado', 'editar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private parametroService: ParametroService,
    private _matDialog: MatDialog,
    private paginatorlabel: MatPaginatorIntl,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.descripcion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcionvalor.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.estado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.parametroService.listarsubmodulo(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso,  'CON', 'SUO').toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  registrarValor(parametro: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(ParametroFormComponent, { 
        panelClass: 'parametro-form-dialog',
        data      : {
          modulo: 'CON',
          submodulo: 'SUO',
          codigo: parametro
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  exportarSubDiario(): void
  {
    this.parametroService.listarsubmodulo(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, 'CON', 'SUO').subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Nombre'] = item.descripcion;
          objectReport['SubDiario'] = item.descripcionvalor;
          objectReport['Estado'] = item.descripcionestado;          
          reportExcelExcel.push(objectReport);
        }        

        this.excelService.exportAsExcelFile(reportExcelExcel, 'SubDiarioPorOperacion');

      }

    }); 
  }

}


