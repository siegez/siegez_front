import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateSubDiarioDto } from 'app/dto/create.subdiario.dto';
import { SubDiarioService } from 'app/services/subdiario.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-subdiario-form',
  templateUrl: './subdiario-form.component.html',
  styleUrls: ['./subdiario-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class SubdiarioFormComponent implements OnInit, AfterContentChecked {

  accion: string;
  codigo: string;
 
  subdiario;  
  valoresiniciales;
  valorcampo;
  valorcontador;
  subdiarioForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  edit = true;
  checkEstado = false;
  checkIndicadorcompra = false;
  checkIndicadorhonorario = false;
  checkIndicadorventa = false;	
  checkIndicadorcaja = false;
  checkIndicadoregreso = false;
  checkIndicadoringreso = false;
  checkIndicadordiario = false;
  checkIndicadorimportacion = false;
  checkIndicadornodomiciliado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<SubdiarioFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private subdiarioService: SubDiarioService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion; 
    this.codigo = _data.codigo;
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.subdiario = new CreateSubDiarioDto();
    
    if ( this.accion === 'editar' )
    {
      // tslint:disable-next-line:max-line-length
      this.subdiarioService.consultar(this.valoresiniciales.empresa, this.codigo).subscribe(data => {
        this.subdiario = data;         
        this.checkIndicadorcompra        = (this.subdiario.indicadorcompra === 'S' ) ? true : false; 
        this.checkIndicadorhonorario     = (this.subdiario.indicadorhonorario === 'S' ) ? true : false; 
        this.checkIndicadorventa         = (this.subdiario.indicadorventa === 'S' ) ? true : false; 
        this.checkIndicadorcaja          = (this.subdiario.indicadorcaja === 'S' ) ? true : false; 
        this.checkIndicadoregreso        = (this.subdiario.indicadoregreso === 'S' ) ? true : false; 
        this.checkIndicadoringreso       = (this.subdiario.indicadoringreso === 'S' ) ? true : false; 
        this.checkIndicadordiario        = (this.subdiario.indicadordiario === 'S' ) ? true : false; 
        this.checkIndicadorimportacion   = (this.subdiario.indicadorimportacion === 'S' ) ? true : false; 
        this.checkIndicadornodomiciliado = (this.subdiario.indicadornodomiciliado === 'S' ) ? true : false; 
        this.checkEstado                 = (this.subdiario.estado === 'A' ) ? true : false;  
        this.subdiarioForm = this.crearSubDiarioForm();  

        }, err => {
          this._matSnackBar.open('Error de consulta SubDiario', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      );     
      this.dialogTitle = 'Editar SubDiario';           
    }
    else
    {
      this.dialogTitle = 'Nuevo SubDiario';
      this.checkEstado = true;
      this.edit = false;
    }     
    this.subdiarioForm = this.crearSubDiarioForm();  
 
  }

  crearSubDiarioForm(): FormGroup
  {
      return this._formBuilder.group({
        subdiario               : [this.subdiario.subdiario],
        descripcion             : [this.subdiario.descripcion],
        indicadorcompra         : [this.checkIndicadorcompra],
        columnacompra           : [this.subdiario.columnacompra],
        columnaigvcompra        : [this.subdiario.columnaigvcompra],
        indicadorhonorario      : [this.checkIndicadorhonorario],
        columnahonorario        : [this.subdiario.columnahonorario],
        columnaigvhonorario     : [this.subdiario.columnaigvhonorario],
        indicadorventa          : [this.checkIndicadorventa],
        columnaventa            : [this.subdiario.columnaventa],
        columnaigvventa         : [this.subdiario.columnaigvventa],
        indicadorcaja           : [this.checkIndicadorcaja],
        columnacaja             : [this.subdiario.columnacaja],
        columnaigvcaja          : [this.subdiario.columnaigvcaja],
        indicadoregreso         : [this.checkIndicadoregreso],
        columnaegreso           : [this.subdiario.columnaegreso],
        columnaigvegreso        : [this.subdiario.columnaigvegreso],
        indicadoringreso        : [this.checkIndicadoringreso],
        columnaingreso          : [this.subdiario.columnaingreso],
        columnaigvingreso       : [this.subdiario.columnaigvingreso],
        indicadordiario         : [this.checkIndicadordiario],
        columnadiario           : [this.subdiario.columnadiario],
        columnaigvdiario        : [this.subdiario.columnaigvdiario],
        indicadorimportacion    : [this.checkIndicadorimportacion],
        columnaimportacion      : [this.subdiario.columnaimportacion],
        columnaigvimportacion   : [this.subdiario.columnaigvimportacion],
        indicadornodomiciliado  : [this.checkIndicadornodomiciliado],
        columnanodomiciliado    : [this.subdiario.columnanodomiciliado],
        columnaigvnodomiciliado : [this.subdiario.columnaigvnodomiciliado],
        estado                  : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  grabarSubDiario(subdiarioForm){

    this.subdiario.empresa                 = this.valoresiniciales.empresa;   
    this.subdiario.subdiario               = this.subdiarioForm.value.subdiario;
    this.subdiario.descripcion             = this.subdiarioForm.value.descripcion;  
    this.subdiario.indicadorcompra         = (this.subdiarioForm.value.indicadorcompra) ? 'S' : 'N';
    this.subdiario.columnacompra           = this.subdiarioForm.value.columnacompra;  
    this.subdiario.columnaigvcompra        = this.subdiarioForm.value.columnaigvcompra;  
    this.subdiario.indicadorhonorario      = (this.subdiarioForm.value.indicadorhonorario) ? 'S' : 'N';
    this.subdiario.columnahonorario        = this.subdiarioForm.value.columnahonorario;  
    this.subdiario.columnaigvhonorario     = this.subdiarioForm.value.columnaigvhonorario;  
    this.subdiario.indicadorventa          = (this.subdiarioForm.value.indicadorventa) ? 'S' : 'N';
    this.subdiario.columnaventa            = this.subdiarioForm.value.columnaventa;  
    this.subdiario.columnaigvventa         = this.subdiarioForm.value.columnaigvventa;  
    this.subdiario.indicadorcaja           = (this.subdiarioForm.value.indicadorcaja) ? 'S' : 'N';
    this.subdiario.columnacaja             = this.subdiarioForm.value.columnacaja;  
    this.subdiario.columnaigvcaja          = this.subdiarioForm.value.columnaigvcaja;  
    this.subdiario.indicadoregreso         = (this.subdiarioForm.value.indicadoregreso) ? 'S' : 'N';
    this.subdiario.columnaegreso           = this.subdiarioForm.value.columnaegreso;  
    this.subdiario.columnaigvegreso        = this.subdiarioForm.value.columnaigvegreso;  
    this.subdiario.indicadoringreso        = (this.subdiarioForm.value.indicadoringreso) ? 'S' : 'N';
    this.subdiario.columnaingreso          = this.subdiarioForm.value.columnaingreso;  
    this.subdiario.columnaigvingreso       = this.subdiarioForm.value.columnaigvingreso;  
    this.subdiario.indicadordiario         = (this.subdiarioForm.value.indicadordiario) ? 'S' : 'N';
    this.subdiario.columnadiario           = this.subdiarioForm.value.columnadiario;  
    this.subdiario.columnaigvdiario        = this.subdiarioForm.value.columnaigvdiario;  
    this.subdiario.indicadorimportacion    = (this.subdiarioForm.value.indicadorimportacion) ? 'S' : 'N';
    this.subdiario.columnaimportacion      = this.subdiarioForm.value.columnaimportacion;  
    this.subdiario.columnaigvimportacion   = this.subdiarioForm.value.columnaigvimportacion;  
    this.subdiario.indicadornodomiciliado  = (this.subdiarioForm.value.indicadornodomiciliado) ? 'S' : 'N';
    this.subdiario.columnanodomiciliado    = this.subdiarioForm.value.columnanodomiciliado;  
    this.subdiario.columnaigvnodomiciliado = this.subdiarioForm.value.columnaigvnodomiciliado;  
    this.subdiario.estado                  = (this.subdiarioForm.value.estado) ? 'A' : 'I';
    this.subdiario.creacionUsuario         = this.valoresiniciales.username;
    this.subdiario.creacionFecha           = '2020-01-01';
    this.subdiario.modificacionUsuario     = this.valoresiniciales.username;
    this.subdiario.modificacionFecha       = '2020-01-01';
  
    if ( this.accion === 'editar' )
    {
      this.subdiarioService.editar(this.subdiario).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, SubDiario no actualizado', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    else
    {
      this.subdiarioService.crear(this.subdiario).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, SubDiario no creado', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    

  }

  onBlurSubDiario(): void
  {
    if (this.edit === false)
    {
      if ( this.subdiarioForm.value.subdiario.replace(/\s+$/g, '') === null || this.subdiarioForm.value.subdiario.replace(/\s+$/g, '') === '')
      {
        this.subdiarioForm.controls['subdiario'].reset();
      }
      else
      {
  
        this.subdiarioService.contar(this.valoresiniciales.empresa, this.subdiarioForm.value.subdiario).subscribe(data => {
          this.valorcontador = data;
    
          if (this.valorcontador.contador > 0)
            {
              this._matSnackBar.open('Error, SubDiario ya existe', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
              this.subdiarioForm.controls['subdiario'].reset();
            }
          }, 
          err => 
          {
              this._matSnackBar.open('Error, error de servicio', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
        );
    
      }
    }
  }

}    

