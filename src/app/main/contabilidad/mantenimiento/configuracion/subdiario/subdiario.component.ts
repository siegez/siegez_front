import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ExcelService } from 'app/services/excel.service';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { SubDiarioService } from 'app/services/subdiario.service';
import { SubdiarioFormComponent } from './subdiario-form/subdiario-form.component';
import { CreateSubDiarioDto } from 'app/dto/create.subdiario.dto';

@Component({
  selector: 'gez-subdiario',
  templateUrl: './subdiario.component.html',
  styleUrls: ['./subdiario.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class SubdiarioComponent implements OnInit {

  dialogRef: any;
  subdiario;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['subdiario', 'descripcion', 'indicadorcompra', 'indicadorhonorario', 'indicadorventa', 'indicadorcaja',
                      'indicadoregreso', 'indicadoringreso', 'indicadordiario', 'indicadorimportacion', 
                      'indicadornodomiciliado', 
                      'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private subdiarioService: SubDiarioService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.subdiario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.indicadorcompra.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.indicadorhonorario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorventa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorcaja.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadoregreso.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadoringreso.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadordiario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorimportacion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadornodomiciliado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.estado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.subdiarioService.listar(this.valoresiniciales.empresa).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  registrarSubDiario(subdiario: string, accion: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(SubdiarioFormComponent, { 
        panelClass: 'subdiario-form-dialog',
        data      : {
          accion: accion,
          codigo: subdiario
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarSubDiario(subdiario: string)
  {
    
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar SubDiario: ' + subdiario,
          subTitulo: '¿Está seguro de eliminar el SubDiario?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });
  
    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
  
          this.subdiario = new CreateSubDiarioDto();
  
          this.subdiario.empresa    = this.valoresiniciales.empresa;
          this.subdiario.subdiario  = subdiario;   
  
          this.subdiarioService.eliminar(this.subdiario).subscribe(data => {
          this._matSnackBar.open('Se eliminó el SubDiario', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, SubDiario no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );
  
  }

  exportarSubDiario(): void
  {
    this.subdiarioService.listar(this.valoresiniciales.empresa).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['SubDiario'] = item.subdiario;
          objectReport['Descripción'] = item.descripcion;  
          objectReport['Compra'] = item.indicadorcompra; 
          objectReport['Compra Columna Base Imponible'] = item.columnacompra;
          objectReport['Compra Columna IGV'] = item.columnaigvcompra;
          objectReport['Honorario'] = item.indicadorhonorario;
          objectReport['Honorario Columna Base Imponible'] = item.columnahonorario;
          objectReport['Honorario Columna IGV'] = item.columnaigvhonorario; 
          objectReport['Venta'] = item.indicadorventa; 
          objectReport['Venta Columna Base Imponible'] = item.columnaventa;
          objectReport['Venta Columna IGV'] = item.columnaigvventa;
          objectReport['Caja'] = item.indicadorcaja; 
          objectReport['Caja Columna Base Imponible'] = item.columnacaja;
          objectReport['Caja Columna IGV'] = item.columnaigvcaja;
          objectReport['Egreso'] = item.indicadoregreso; 
          objectReport['Egreso Columna Base Imponible'] = item.columnaegreso;
          objectReport['Egreso Columna IGV'] = item.columnaigvegreso;
          objectReport['Ingreso'] = item.indicadoringreso; 
          objectReport['Ingreso Columna Base Imponible'] = item.columnaingreso;
          objectReport['Ingreso Columna IGV'] = item.columnaigvingreso;
          objectReport['Diario'] = item.indicadordiario; 
          objectReport['Diario Columna Base Imponible'] = item.columnadiario;
          objectReport['Diario Columna IGV'] = item.columnaigvdiario;
          objectReport['Importación'] = item.indicadorimportacion; 
          objectReport['Importación Columna Base Imponible'] = item.columnaimportacion;
          objectReport['Importación Columna IGV'] = item.columnaigvimportacion;
          objectReport['No Domiciliado'] = item.indicadornodomiciliado; 
          objectReport['No Domiciliado Columna Base Imponible'] = item.columnanodomiciliado;
          objectReport['No Domiciliado Columna IGV'] = item.columnaigvnodomiciliado;
          objectReport['Estado'] = item.descripcionestado;          
          reportExcelExcel.push(objectReport);
        }        

        this.excelService.exportAsExcelFile(reportExcelExcel, 'SubDiario');

      }

    }); 
  }

}
