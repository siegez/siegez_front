import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateTipoDocumentoDto } from 'app/dto/create.tipodocumento.dto';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-tipodocumento-form',
  templateUrl: './tipodocumento-form.component.html',
  styleUrls: ['./tipodocumento-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class TipodocumentoFormComponent implements OnInit, AfterContentChecked {

  accion: string;
  codigo: string;
 
  tipodocumento;  
  valoresiniciales;
  valorcampo;
  valorcontador;
  tipodocumentoForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  edit = true;
  checkEstado = false;
  checkIndicadorcompra = false;
  checkIndicadorhonorario = false;
  checkIndicadorventa = false;	
  checkIndicadorcaja = false;
  checkIndicadoregreso = false;
  checkIndicadoringreso = false;
  checkIndicadordiario = false;
  checkIndicadorimportacion = false;
  checkIndicadornodomiciliado = false;
  checkIndicadorsustentonodom = false;
  checkIndicadorsunat = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<TipodocumentoFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private tipodocumentoService: TipoDocumentoService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion; 
    this.codigo = _data.codigo;
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.tipodocumento = new CreateTipoDocumentoDto();
    
    if ( this.accion === 'editar' )
    {
      // tslint:disable-next-line:max-line-length
      this.tipodocumentoService.consultar(this.valoresiniciales.empresa, this.codigo).subscribe(data => {
        this.tipodocumento = data;         
        this.checkIndicadorcompra = (this.tipodocumento.indicadorcompra === 'S' ) ? true : false; 
        this.checkIndicadorhonorario = (this.tipodocumento.indicadorhonorario === 'S' ) ? true : false; 
        this.checkIndicadorventa = (this.tipodocumento.indicadorventa === 'S' ) ? true : false; 
        this.checkIndicadorcaja = (this.tipodocumento.indicadorcaja === 'S' ) ? true : false; 
        this.checkIndicadoregreso = (this.tipodocumento.indicadoregreso === 'S' ) ? true : false; 
        this.checkIndicadoringreso = (this.tipodocumento.indicadoringreso === 'S' ) ? true : false; 
        this.checkIndicadordiario = (this.tipodocumento.indicadordiario === 'S' ) ? true : false; 
        this.checkIndicadorimportacion = (this.tipodocumento.indicadorimportacion === 'S' ) ? true : false; 
        this.checkIndicadornodomiciliado = (this.tipodocumento.indicadornodomiciliado === 'S' ) ? true : false; 
        this.checkIndicadorsustentonodom = (this.tipodocumento.indicadorsustentonodom === 'S' ) ? true : false;
        this.checkIndicadorsunat = (this.tipodocumento.indicadorsunat === 'S' ) ? true : false;
        this.checkEstado = (this.tipodocumento.estado === 'A' ) ? true : false;  
        this.tipodocumentoForm = this.crearTipoDocumentoForm();  

        }, err => {
          this._matSnackBar.open('Error de consulta tipo documento', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      );     
      this.dialogTitle = 'Editar Tipo Documento';           
    }
    else
    {
      this.dialogTitle = 'Nuevo Tipo Documento';
      this.checkEstado = true;
      this.edit = false;
    }     
    this.tipodocumentoForm = this.crearTipoDocumentoForm();  
 
  }

  crearTipoDocumentoForm(): FormGroup
  {
      return this._formBuilder.group({
        tipodocumento           : [this.tipodocumento.tipodocumento],
        descripcion             : [this.tipodocumento.descripcion],
        abreviatura             : [this.tipodocumento.abreviatura],
        indicadorcompra         : [this.checkIndicadorcompra],
        indicadorhonorario      : [this.checkIndicadorhonorario],
        indicadorventa          : [this.checkIndicadorventa],
        indicadorcaja           : [this.checkIndicadorcaja],
        indicadoregreso         : [this.checkIndicadoregreso],
        indicadoringreso        : [this.checkIndicadoringreso],
        indicadordiario         : [this.checkIndicadordiario],
        indicadorimportacion    : [this.checkIndicadorimportacion],
        indicadornodomiciliado  : [this.checkIndicadornodomiciliado],
        indicadorsustentonodom  : [this.checkIndicadorsustentonodom],
        indicadorsunat          : [this.checkIndicadorsunat],
        estado                  : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  grabarTipoDocumento(tipodocumentoForm){

    this.tipodocumento.empresa                = this.valoresiniciales.empresa;   
    this.tipodocumento.tipodocumento          = this.tipodocumentoForm.value.tipodocumento;
    this.tipodocumento.descripcion            = this.tipodocumentoForm.value.descripcion;  
    this.tipodocumento.abreviatura            = this.tipodocumentoForm.value.abreviatura;
    this.tipodocumento.indicadorcompra        = (this.tipodocumentoForm.value.indicadorcompra) ? 'S' : 'N';
    this.tipodocumento.indicadorhonorario     = (this.tipodocumentoForm.value.indicadorhonorario) ? 'S' : 'N';
    this.tipodocumento.indicadorventa         = (this.tipodocumentoForm.value.indicadorventa) ? 'S' : 'N';
    this.tipodocumento.indicadorcaja          = (this.tipodocumentoForm.value.indicadorcaja) ? 'S' : 'N';
    this.tipodocumento.indicadoregreso        = (this.tipodocumentoForm.value.indicadoregreso) ? 'S' : 'N';
    this.tipodocumento.indicadoringreso       = (this.tipodocumentoForm.value.indicadoringreso) ? 'S' : 'N';
    this.tipodocumento.indicadordiario        = (this.tipodocumentoForm.value.indicadordiario) ? 'S' : 'N';
    this.tipodocumento.indicadorimportacion   = (this.tipodocumentoForm.value.indicadorimportacion) ? 'S' : 'N';
    this.tipodocumento.indicadornodomiciliado = (this.tipodocumentoForm.value.indicadornodomiciliado) ? 'S' : 'N';
    this.tipodocumento.indicadorsustentonodom = (this.tipodocumentoForm.value.indicadorsustentonodom) ? 'S' : 'N';
    this.tipodocumento.indicadorsunat         = (this.tipodocumentoForm.value.indicadorsunat) ? 'S' : 'N';
    this.tipodocumento.estado                 = (this.tipodocumentoForm.value.estado) ? 'A' : 'I';
    this.tipodocumento.creacionUsuario        = this.valoresiniciales.username;
    this.tipodocumento.creacionFecha          = '2020-01-01';
    this.tipodocumento.modificacionUsuario    = this.valoresiniciales.username;
    this.tipodocumento.modificacionFecha      = '2020-01-01';
  
    if ( this.accion === 'editar' )
    {
      this.tipodocumentoService.editar(this.tipodocumento).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Tipo Documento no actualizada', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    else
    {
      this.tipodocumentoService.crear(this.tipodocumento).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Tipo Documento no creado', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    

  }

  onBlurTipoDocumento(): void
  {
    if (this.edit === false)
    {
      if ( this.tipodocumentoForm.value.tipodocumento.replace(/\s+$/g, '') === null || this.tipodocumentoForm.value.tipodocumento.replace(/\s+$/g, '') === '')
      {
        this.tipodocumentoForm.controls['tipodocumento'].reset();
      }
      else
      {
  
        this.tipodocumentoService.contar(this.valoresiniciales.empresa, this.tipodocumentoForm.value.tipodocumento).subscribe(data => {
          this.valorcontador = data;
    
          if (this.valorcontador.contador > 0)
            {
              this._matSnackBar.open('Error, Tipo Documento ya existe', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
              this.tipodocumentoForm.controls['tipodocumento'].reset();
            }
          }, 
          err => 
          {
              this._matSnackBar.open('Error, error de servicio', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
        );
    
      }
    }
  }

}    
