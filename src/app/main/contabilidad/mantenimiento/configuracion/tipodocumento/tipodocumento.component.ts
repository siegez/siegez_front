import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ExcelService } from 'app/services/excel.service';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { TipodocumentoFormComponent } from './tipodocumento-form/tipodocumento-form.component';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { CreateTipoDocumentoDto } from 'app/dto/create.tipodocumento.dto';

@Component({
  selector: 'gez-tipodocumento',
  templateUrl: './tipodocumento.component.html',
  styleUrls: ['./tipodocumento.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class TipodocumentoComponent implements OnInit {

  dialogRef: any;
  tipodocumento;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['tipodocumento', 'descripcion', 'indicadorcompra', 'indicadorhonorario', 'indicadorventa', 'indicadorcaja',
                      'indicadoregreso', 'indicadoringreso', 'indicadordiario', 'indicadorimportacion', 
                      'indicadornodomiciliado', 'indicadorsustentonodom', 
                      'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private tipodocumentoService: TipoDocumentoService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.tipodocumento.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.indicadorcompra.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.indicadorhonorario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorventa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorcaja.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadoregreso.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadoringreso.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadordiario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorimportacion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadornodomiciliado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.indicadorsustentonodom.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.estado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.tipodocumentoService.listar(this.valoresiniciales.empresa).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  registrarTipoDocumento(tipodocumento: string, accion: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(TipodocumentoFormComponent, { 
        panelClass: 'tipodocumento-form-dialog',
        data      : {
          accion: accion,
          codigo: tipodocumento
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarTipoDocumento(tipodocumento: string)
  {
    
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Tipo Documento: ' + tipodocumento,
          subTitulo: '¿Está seguro de eliminar el Tipo de Documento?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });
  
    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
  
          this.tipodocumento = new CreateTipoDocumentoDto();
  
          this.tipodocumento.empresa        = this.valoresiniciales.empresa;
          this.tipodocumento.tipodocumento  = tipodocumento;   
  
          this.tipodocumentoService.eliminar(this.tipodocumento).subscribe(data => {
          this._matSnackBar.open('Se eliminó el Tipo Documento', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, Tipo Documento no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );
  
  }

  exportarTipoDocumento(): void
  {
    this.tipodocumentoService.listar(this.valoresiniciales.empresa).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Tipo Documento'] = item.tipodocumento;
          objectReport['Descripción'] = item.descripcion;  
          objectReport['Abreviatura'] = item.abreviatura; 
          objectReport['Compra'] = item.indicadorcompra; 
          objectReport['Honorario'] = item.indicadorhonorario; 
          objectReport['Venta'] = item.indicadorventa; 
          objectReport['Caja'] = item.indicadorcaja; 
          objectReport['Egreso'] = item.indicadoregreso; 
          objectReport['Ingreso'] = item.indicadoringreso; 
          objectReport['Diario'] = item.indicadordiario; 
          objectReport['Importación'] = item.indicadorimportacion; 
          objectReport['No Domiciliado'] = item.indicadornodomiciliado; 
          objectReport['Sustento No Domiciliado'] = item.indicadorsustentonodom; 
          objectReport['SUNAT'] = item.indicadorsunat; 
          objectReport['Estado'] = item.descripcionestado;          
          reportExcelExcel.push(objectReport);
        }        

        this.excelService.exportAsExcelFile(reportExcelExcel, 'TipoDocumento');

      }

    }); 
  }

}
