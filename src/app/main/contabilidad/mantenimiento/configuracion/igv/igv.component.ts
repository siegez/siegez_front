import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ExcelService } from 'app/services/excel.service';
import { ValorFechaService } from 'app/services/valorfecha.service';
import { ValorfechaFormComponent } from '../valorfecha-form/valorfecha-form.component';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { CreateValorFechaDto } from 'app/dto/create.valorfecha.dto';


@Component({
  selector: 'gez-igv',
  templateUrl: './igv.component.html',
  styleUrls: ['./igv.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class IgvComponent implements OnInit {

  dialogRef: any;
  valorfecha;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['fechainicio', 'fechafin', 'valor', 'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  private _matSnackBar: any;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private valorfechaService: ValorFechaService,
    private _matDialog: MatDialog,
    private paginatorlabel: MatPaginatorIntl,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.fechainicio.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.fechafin.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.valor.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.estado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.valorfechaService.listar(this.valoresiniciales.empresa, 'IGV').toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  registrarValor(vez: number, accion: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(ValorfechaFormComponent, { 
        panelClass: 'valorfecha-form-dialog',
        data      : {
          accion: accion,
          codigo: 'IGV',
          vez: vez
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarValor(vez: number)
  {
    
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar IGV: ',
          subTitulo: '¿Está seguro de eliminar el IGV?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });
  
    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
  
          this.valorfecha = new CreateValorFechaDto();
  
          this.valorfecha.empresa = this.valoresiniciales.empresa;
          this.valorfecha.codigo  = 'IGV';   
          this.valorfecha.vez     = vez;
  
          this.valorfechaService.eliminar(this.valorfecha).subscribe(data => {
          this._matSnackBar.open('Se eliminó el IGV', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, IGV no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );
  
  }

  exportarValor(): void
  {
    this.valorfechaService.listar(this.valoresiniciales.empresa, 'IGV').subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Fecha Inicio'] = item.fechainicio;
          objectReport['Fecha Fin'] = item.fechafin;
          objectReport['Valor'] = item.valor;
          objectReport['Estado'] = item.descripcionestado;          
          reportExcelExcel.push(objectReport);
        }        

        this.excelService.exportAsExcelFile(reportExcelExcel, 'IGV');

      }

    }); 
  }

}


