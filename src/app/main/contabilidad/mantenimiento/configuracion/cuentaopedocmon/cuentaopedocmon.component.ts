import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { ExcelService } from 'app/services/excel.service';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { TipoDocMonCtaService } from 'app/services/tipodocmoncta.service';
import { CuentaopedocmonFormComponent } from './cuentaopedocmon-form/cuentaopedocmon-form.component';
import { CreateTipoDocMonCtaDto } from 'app/dto/create.tipodocmoncta.dto';

@Component({
  selector: 'gez-cuentaopedocmon',
  templateUrl: './cuentaopedocmon.component.html',
  styleUrls: ['./cuentaopedocmon.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class CuentaopedocmonComponent implements OnInit {

  dialogRef: any;
  cuentaopedocmon;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['tipodocumento', 'moneda', 'compratercero', 'comprarelacionado', 'ventatercero', 'ventarelacionado',
                      'honorario', 'importacion', 'nodomiciliado', 'nodomiciliadorelacionado',
                      'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private tipodocmonctaService: TipoDocMonCtaService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.descripciontipodocumento.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcionmoneda.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.cuentacompra.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.cuentacomprarel.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentahonorario.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentaventa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentaventarel.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentaimportacion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentanodomiciliado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  || 
       data.cuentanodomiciliadorel.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  ||  
       data.estado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1  
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.tipodocmonctaService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  registrarCtaOpeDocMon(tipodocumento: string, moneda: string, accion: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(CuentaopedocmonFormComponent, { 
        panelClass: 'cuentaopedocmon-form-dialog',
        data      : {
          accion: accion,
          tipodocumento: tipodocumento,
          moneda: moneda
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {       
          this.ngOnInit();    
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );     
  }

  // tslint:disable-next-line:typedef
  async eliminarCtaOpeDocMon(tipodocumento: string, descripciontipodocumento: string, moneda: string, descripcionmoneda: string)
  {
    
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Documento: ' + descripciontipodocumento + ' / Moneda: ' + moneda + '-' + descripcionmoneda,
          subTitulo: '¿Está seguro de eliminar?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });
  
    // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
  
          this.cuentaopedocmon = new CreateTipoDocMonCtaDto();
  
          this.cuentaopedocmon.empresa              = this.valoresiniciales.empresa;
          this.cuentaopedocmon.ejerciciocontable    = this.valoresiniciales.annoproceso;
          this.cuentaopedocmon.tipodocumento        = tipodocumento;   
          this.cuentaopedocmon.moneda               = moneda; 
  
          this.tipodocmonctaService.eliminar(this.cuentaopedocmon).subscribe(data => {
          this._matSnackBar.open('Se eliminó el Tipo Documento - Moneda', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, Tipo Documento - Moneda, no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );
  
  }

  exportarCtaOpeDocMon(): void
  {
    this.tipodocmonctaService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Ejercicio Contable'] = this.valoresiniciales.annoproceso;
          objectReport['Tipo Documento'] = item.tipodocumento;
          objectReport['Descripción Tipo Documento'] = item.descripciontipodocumento;  
          objectReport['Moneda'] = item.moneda;
          objectReport['Descripción Moneda'] = item.descripcionmoneda;  
          objectReport['Cuenta Compra'] = item.cuentacompra; 
          objectReport['Cuenta Compra Relacionado'] = item.cuentacomprarel;
          objectReport['Cuenta Honorario'] = item.cuentahonorario;
          objectReport['Cuenta Venta'] = item.cuentaventa;
          objectReport['Cuenta Venta Relacionado'] = item.cuentaventarel;
          objectReport['Cuenta Caja'] = item.cuentacaja; 
          objectReport['Cuenta Importación'] = item.cuentaimportacion; 
          objectReport['Cuenta No Domiciliado'] = item.cuentanodomiciliado;
          objectReport['Cuenta No Domiciliado Relacionado'] = item.cuentanodomiciliadorel;
          objectReport['Estado'] = item.descripcionestado;          
          reportExcelExcel.push(objectReport);
        }        

        this.excelService.exportAsExcelFile(reportExcelExcel, 'CuentaOperacion');

      }

    }); 
  }

}

