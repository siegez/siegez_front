import { IfStmt } from '@angular/compiler';
import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateTipoDocMonCtaDto } from 'app/dto/create.tipodocmoncta.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { PlanContableService } from 'app/services/plancontable.service';
import { TipoDocMonCtaService } from 'app/services/tipodocmoncta.service';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-cuentaopedocmon-form',
  templateUrl: './cuentaopedocmon-form.component.html',
  styleUrls: ['./cuentaopedocmon-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class CuentaopedocmonFormComponent implements OnInit, AfterContentChecked {

  accion: string;
  tipodocumento: string;
  moneda: string;
 
  cuentaopedocmon;  
  valoresiniciales;
  valorcampo;
  valorcontador;
  cuentaopedocmonForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  edit = true;
  checkEstado = false;

  listaCuentaContable: any[]; 
  listaTipoDocumento: any[];
  listaMoneda: any[];
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<CuentaopedocmonFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private tipodocmonctaService: TipoDocMonCtaService,
    private plancontableService: PlanContableService,
    private tipodocumentoService: TipoDocumentoService,
    private consultaService: ConsultaService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion; 
    this.tipodocumento = _data.tipodocumento;
    this.moneda = _data.moneda;
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.cuentaopedocmon = new CreateTipoDocMonCtaDto();
    
    if ( this.accion === 'editar' )
    {
      // tslint:disable-next-line:max-line-length
      this.tipodocmonctaService.consultar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.tipodocumento, this.moneda).subscribe(data => {
        this.cuentaopedocmon = data;         
        this.checkEstado     = (this.cuentaopedocmon.estado === 'A' ) ? true : false;  
        this.cuentaopedocmonForm = this.crearCuentaOpeDocMonForm();  

        }, err => {
          this._matSnackBar.open('Error de consulta Cuenta Contable por Operación', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      );     
      this.dialogTitle = 'Editar Cuentas Contables por Operación';           
    }
    else
    {
      this.dialogTitle = 'Nueva Cuentas Contables por Operación';
      this.checkEstado = true;
      this.edit = false;
    }         

    this.cuentaopedocmonForm = this.crearCuentaOpeDocMonForm();  
    this.listarCuentaContable();
    this.listarTipoDocumento();
    this.listarMoneda();
 
  }

  crearCuentaOpeDocMonForm(): FormGroup
  {
      return this._formBuilder.group({
        tipodocumento          : [this.cuentaopedocmon.tipodocumento],
        descripciontipodocumento : [this.cuentaopedocmon.descripciontipodocumento],
        moneda                 : [this.cuentaopedocmon.moneda],
        descripcionmoneda      : [this.cuentaopedocmon.descripcionmoneda],
        cuentacompra           : [this.cuentaopedocmon.cuentacompra],
        cuentacomprarel        : [this.cuentaopedocmon.cuentacomprarel],
        cuentahonorario        : [this.cuentaopedocmon.cuentahonorario],
        cuentaventa            : [this.cuentaopedocmon.cuentaventa],
        cuentaventarel         : [this.cuentaopedocmon.cuentaventarel],
        cuentaimportacion      : [this.cuentaopedocmon.cuentaimportacion],
        cuentanodomiciliado    : [this.cuentaopedocmon.cuentanodomiciliado],
        cuentanodomiciliadorel : [this.cuentaopedocmon.cuentanodomiciliadorel],
        estado                 : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  grabarCuentaOpeDocMon(subdiarioForm){

    this.cuentaopedocmon.empresa                 = this.valoresiniciales.empresa;   
    this.cuentaopedocmon.ejerciciocontable       = this.valoresiniciales.annoproceso;  
    this.cuentaopedocmon.tipodocumento           = this.cuentaopedocmonForm.value.tipodocumento;
    this.cuentaopedocmon.moneda                  = this.cuentaopedocmonForm.value.moneda;  
    this.cuentaopedocmon.cuentacompra            = this.cuentaopedocmonForm.value.cuentacompra; 
    this.cuentaopedocmon.cuentacomprarel         = this.cuentaopedocmonForm.value.cuentacomprarel; 
    this.cuentaopedocmon.cuentahonorario         = this.cuentaopedocmonForm.value.cuentahonorario; 
    this.cuentaopedocmon.cuentaventa             = this.cuentaopedocmonForm.value.cuentaventa; 
    this.cuentaopedocmon.cuentaventarel          = this.cuentaopedocmonForm.value.cuentaventarel; 
    this.cuentaopedocmon.cuentaimportacion       = this.cuentaopedocmonForm.value.cuentaimportacion; 
    this.cuentaopedocmon.cuentanodomiciliado     = this.cuentaopedocmonForm.value.cuentanodomiciliado; 
    this.cuentaopedocmon.cuentanodomiciliadorel  = this.cuentaopedocmonForm.value.cuentanodomiciliadorel; 
    this.cuentaopedocmon.estado                  = (this.cuentaopedocmonForm.value.estado) ? 'A' : 'I';
    this.cuentaopedocmon.creacionUsuario         = this.valoresiniciales.username;
    this.cuentaopedocmon.creacionFecha           = '2020-01-01';
    this.cuentaopedocmon.modificacionUsuario     = this.valoresiniciales.username;
    this.cuentaopedocmon.modificacionFecha       = '2020-01-01';
  
    if ( this.accion === 'editar' )
    {
      this.tipodocmonctaService.editar(this.cuentaopedocmon).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Cuenta Contable por Operación, no actualizado', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    else
    {
      this.tipodocmonctaService.crear(this.cuentaopedocmon).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');
        this.matDialogRef.close();
  
      }, err => {
          this._matSnackBar.open('Error, Cuenta Contable por Operación, no creado', '', {
            verticalPosition: 'bottom',
            duration        : 2000
          });
      }
      );
    }
    

  }

  onBlurCuentaOpeDocMon(): void
  {
    if (this.edit === false)
    {
      if ( this.cuentaopedocmonForm.value.tipodocumento.replace(/\s+$/g, '') !== null && this.cuentaopedocmonForm.value.moneda.replace(/\s+$/g, '') !== '')
      {
        // tslint:disable-next-line:max-line-length
        this.tipodocmonctaService.contar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, this.cuentaopedocmonForm.value.tipodocumento, this.cuentaopedocmonForm.value.moneda).subscribe(data => {
          this.valorcontador = data;
    
          if (this.valorcontador.contador > 0)
            {
              this._matSnackBar.open('Error, Tipo Documento y Moneda ya existe', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
              this.cuentaopedocmonForm.controls['tipodocumento'].reset();
              this.cuentaopedocmonForm.controls['moneda'].reset();
            }
          }, 
          err => 
          {
              this._matSnackBar.open('Error, error de servicio', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
        );
    
      }
    }
  }

  // tslint:disable-next-line:typedef
  listarCuentaContable(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCuentaContable = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoDocumento(){

    this.tipodocumentoService.listarCatalogoCompras(this.valoresiniciales.empresa).subscribe(data => {
      this.listaTipoDocumento = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarMoneda(){

    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 4).subscribe(data => {
      this.listaMoneda = data;
    });
  }


}    


