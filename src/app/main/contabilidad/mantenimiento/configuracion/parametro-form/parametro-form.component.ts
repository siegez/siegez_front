import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateParametroDto } from 'app/dto/create.parametro.dto';
import { ParametroService } from 'app/services/parametro.service';
import { PlanContableService } from 'app/services/plancontable.service';
import { SubDiarioService } from 'app/services/subdiario.service';
import { TipoDocumentoService } from 'app/services/tipodocumento.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-parametro-form',
  templateUrl: './parametro-form.component.html',
  styleUrls: ['./parametro-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class ParametroFormComponent implements OnInit, AfterContentChecked {

  modulo: string;
  submodulo: string;
  codigo: string;
 
  parametro;  
  valoresiniciales;
  valorcampo;
  valorcontador;
  parametroForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';

  edit = true;
  checkEstado = false;

  listaCuentaContable: any[]; 
  listaTipoDocumento: any[];
  listaSubDiario: any[];
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<ParametroFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private parametroService: ParametroService,
    private plancontableService: PlanContableService,
    private tipodocumentoService: TipoDocumentoService,
    private subdiarioService: SubDiarioService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.modulo     = _data.modulo; 
    this.submodulo  = _data.submodulo;
    this.codigo     = _data.codigo;
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.parametro = new CreateParametroDto();
    
    // tslint:disable-next-line:max-line-length
    this.parametroService.consultar(this.valoresiniciales.empresa, this.modulo, this.codigo).subscribe(data => {
      this.parametro = data;         
      this.checkEstado   = (this.parametro.estado === 'A' ) ? true : false;  
      this.parametroForm = this.crearParametroForm();  

      switch (this.submodulo) {
        case 'CCA':
          this.dialogTitle = this.parametro.descripcion; 
          break;  
        case 'TDO':
          this.dialogTitle = this.parametro.descripcion; 
          break;
        case 'SUO':
          this.dialogTitle = this.parametro.descripcion; 
          break;    
        default:
          this.dialogTitle = 'Editar';
          break;
      }    

      }, err => {
        this._matSnackBar.open('Error de consulta', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );                    
   
    this.parametroForm = this.crearParametroForm();  
    this.listarCuentaContable();
    this.listarTipoDocumento();
    this.listarSubDiario();
 
  }

  crearParametroForm(): FormGroup
  {
      return this._formBuilder.group({
        descripcion             : [this.parametro.descripcion],
        valor                   : [this.parametro.valor],
        estado                  : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  grabarParametro(parametroForm){

    this.parametro.empresa                 = this.valoresiniciales.empresa;  
    this.parametro.modulo                  = this.modulo;  
    this.parametro.parametro               = this.codigo;   
    this.parametro.valor                   = this.parametroForm.value.valor;
    this.parametro.estado                  = (this.parametroForm.value.estado) ? 'A' : 'I';
    this.parametro.creacionUsuario         = this.valoresiniciales.username;
    this.parametro.creacionFecha           = '2020-01-01';
    this.parametro.modificacionUsuario     = this.valoresiniciales.username;
    this.parametro.modificacionFecha       = '2020-01-01';
  

    this.parametroService.editar(this.parametro).subscribe(data => {
      this._matSnackBar.open('Grabación exitosa', '', {
      verticalPosition: 'bottom',
      duration        : 2000
      });

      localStorage.setItem(REFRESH, 'S');
      this.matDialogRef.close();

    }, err => {
        this._matSnackBar.open('Error, No actualizado', '', {
          verticalPosition: 'bottom',
          duration        : 2000
        });
    }
    );  

  }

  // tslint:disable-next-line:typedef
  listarCuentaContable(): void
  {
    this.plancontableService.listarCatalogoSimple(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listaCuentaContable = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoDocumento(){
  
    this.tipodocumentoService.listarCatalogo(this.valoresiniciales.empresa).subscribe(data => {
      this.listaTipoDocumento = data;
    });
  }
  
  // tslint:disable-next-line:typedef
  listarSubDiario(){
  
    this.subdiarioService.listarCatalogo(this.valoresiniciales.empresa).subscribe(data => {
      this.listaSubDiario = data;
    });
  }
}    


