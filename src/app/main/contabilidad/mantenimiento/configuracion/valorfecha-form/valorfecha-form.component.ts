import { Component, Inject, ViewEncapsulation, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fuseAnimations } from '@fuse/animations';
import { CreateValorFechaDto } from 'app/dto/create.valorfecha.dto';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { ValorFechaService } from 'app/services/valorfecha.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';


@Component({
  selector: 'gez-valorfecha-form',
  templateUrl: './valorfecha-form.component.html',
  styleUrls: ['./valorfecha-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class ValorfechaFormComponent implements OnInit, AfterContentChecked {

  accion: string;
  codigo: string;
  vez: number;
 
  valorfecha;  
  resultadosp;
  valoresiniciales;
  valorcampo;
  valorcontador;
  valorfechaForm: FormGroup;
  dialogTitle: string;
  dialogRef: any;
  botonExit = 'Cancelar';
  botonAgregar = 'Agregar';
  botonAceptar = 'Grabar';
  hidegrabar = false;
  edit = true;
  checkEstado = false;
 
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<ValorfechaFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private valorfechaService: ValorFechaService,
    private cdref: ChangeDetectorRef
  ) 
  { 
    this.accion = _data.accion; 
    this.codigo = _data.codigo;
    this.vez = _data.vez;
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {   
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorfecha = new CreateValorFechaDto();
    
    if ( this.accion === 'editar' )
    {
      // tslint:disable-next-line:max-line-length
      this.valorfechaService.consultar(this.valoresiniciales.empresa, this.codigo, this.vez).subscribe(data => {
        this.valorfecha = data;         
        this.checkEstado                 = (this.valorfecha.estado === 'A' ) ? true : false;  
        this.valorfechaForm = this.crearValorFechaForm();  

        }, err => {
          this._matSnackBar.open('Error de consulta', 'ok', {
              verticalPosition: 'bottom',
              panelClass: ['my-snack-bar'] 
              });
        }
      );     
      this.dialogTitle = 'Editar';           
    }
    else
    {
      this.dialogTitle = 'Nuevo';
      this.checkEstado = true;
      this.edit = false;
    }     
    this.valorfechaForm = this.crearValorFechaForm();  
 
  }

  crearValorFechaForm(): FormGroup
  {
      return this._formBuilder.group({
        fechainicio   : [this.valorfecha.fechainicio],
        fechafin      : [this.valorfecha.fechafin],
        valor         : [this.valorfecha.valor],
        estado        : [this.checkEstado] 
      });
  }

  // tslint:disable-next-line:typedef
  registrarValorFecha(valorfechaForm){

    this.valorfecha.empresa                 = this.valoresiniciales.empresa;   
    this.valorfecha.codigo                  = this.codigo; 
    this.valorfecha.vez                     = this.vez; 
    this.valorfecha.fechainicio             = this.valorfechaForm.value.fechainicio;
    this.valorfecha.fechafin                = this.valorfechaForm.value.fechafin; 
    this.valorfecha.valor                   = this.valorfechaForm.value.valor;  
    this.valorfecha.estado                  = (this.valorfechaForm.value.estado) ? 'A' : 'I';
    this.valorfecha.creacionUsuario         = this.valoresiniciales.username;
    this.valorfecha.creacionFecha           = '2020-01-01';
    this.valorfecha.modificacionUsuario     = this.valoresiniciales.username;
    this.valorfecha.modificacionFecha       = '2020-01-01';  

    this.resultadosp = new ViewResultadoSPDto();

    this.valorfechaService.registrar(this.valorfecha).subscribe(data => {
      this.resultadosp = data;  
      // console.log(this.resultadosp);
      if (this.resultadosp.indicadorexito === 'S')
      {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;
        this._matSnackBar.open('Grabación exitosa', 'ok', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

      }
      else
      {
        this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
      }

    }, err => {
      this._matSnackBar.open('Error al registrar', 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
    }
    );     

  }
    
}    
