import { Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';
import { CreateTipoCambioDto } from 'app/dto/create.tipocambio.dto';
import { ViewResultadoSPDto } from 'app/dto/view.resultadosp.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { TipoCambioService } from 'app/services/tipocambio.service';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-tipocambio-form',
  templateUrl: './tipocambio-form.component.html',
  styleUrls: ['./tipocambio-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class TipocambioFormComponent implements OnInit  {
  // @ViewChild('fecha') fechaField: ElementRef;
  
  accion: string;
  vez: number; 
  tipocambio;
  resultadosp;
  tipocambioForm: FormGroup;
  dialogTitle: string;
  botonExit = 'Cancelar';
  botonAgregar = 'Grabar';
  botonAceptar = 'Grabar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  valoresiniciales;
  valorcontador;
  checkEstado = false;
  checkIndicadorCierre = false;
  checkIndicadorReplica = false;

  listaMoneda: any[];

  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<TipocambioFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private tipocambioService: TipoCambioService,
    private consultaService: ConsultaService,
  ) 
  { 
    this.accion = _data.accion;
    this.vez = _data.vez;
  }


  ngOnInit(): void 
  {   

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.listarMoneda();
    
    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Tipo de Cambio';  
      this.tipocambio = new CreateTipoCambioDto();

      this.tipocambioService.consultar(this.valoresiniciales.empresa, this.vez).subscribe(data => {
        this.tipocambio = data;  
        this.checkIndicadorCierre = (this.tipocambio.indicadorcierre === 'S' ) ? true : false; 
        this.checkEstado = (this.tipocambio.estado === 'A' ) ? true : false;    
        this.checkIndicadorReplica = false; 
        this.tipocambioForm = this.createTipoCambioForm();
      }, err => {
        this._matSnackBar.open('Error de consulta', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
      ); 
    }
    else
    {
      this.dialogTitle = 'Nuevo Tipo de Cambio';
      this.edit = false;
      this.checkEstado = true;
      this.checkIndicadorReplica = true;
      this.tipocambio = new CreateTipoCambioDto();
      this.tipocambioForm = this.createTipoCambioForm();
    }
    
    this.tipocambioForm = this.createTipoCambioForm();

  }

  createTipoCambioForm(): FormGroup
  {
    return this._formBuilder.group({

      fecha              : [this.tipocambio.fecha],
      moneda             : [this.tipocambio.moneda],
      descripcionmoneda  : [this.tipocambio.descripcionmoneda],
      tipocambiocompra   : [this.tipocambio.tipocambiocompra],
      tipocambioventa    : [this.tipocambio.tipocambioventa],
      // indicadorcierre    : [this.checkIndicadorCierre],
      indicadorcierre    : [{value: this.checkIndicadorCierre, disabled: this.edit}],
      indicadorreplica   : [this.checkIndicadorReplica],
      estado             : [this.checkEstado]

    });
  }

  // tslint:disable-next-line:typedef
  listarMoneda(){

    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 4).subscribe(data => {
      this.listaMoneda = data;
    });
  }

  // tslint:disable-next-line:typedef
  registrarTipoCambio(tipocambioForm){

    this.tipocambio.empresa = this.valoresiniciales.empresa;    
    // this.tipocambio.vez = tipocambioForm.value.vez;
    this.tipocambio.fecha = tipocambioForm.value.fecha;
    this.tipocambio.moneda = tipocambioForm.value.moneda;
    this.tipocambio.tipocambiocompra = Number(tipocambioForm.value.tipocambiocompra);
    this.tipocambio.tipocambioventa = Number(tipocambioForm.value.tipocambioventa);
    this.tipocambio.indicadorcierre = (this.tipocambioForm.value.indicadorcierre) ? 'S' : 'N';
    this.tipocambio.indicadorreplica = (this.tipocambioForm.value.indicadorreplica) ? 'S' : 'N';
    this.tipocambio.estado = (this.tipocambioForm.value.estado) ? 'A' : 'I';
    this.tipocambio.creacionUsuario = this.valoresiniciales.username;
    this.tipocambio.creacionFecha = '2020-01-01';
    this.tipocambio.modificacionUsuario = this.valoresiniciales.username;
    this.tipocambio.modificacionFecha = '2020-01-01';
    
    this.resultadosp = new ViewResultadoSPDto();

    this.tipocambioService.registrar(this.tipocambio).subscribe(data => {
      this.resultadosp = data;  
      // console.log(this.resultadosp);
      if (this.resultadosp.indicadorexito === 'S')
      {
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;
        this._matSnackBar.open('Grabación exitosa', 'ok', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        localStorage.setItem(REFRESH, 'S');

      }
      else
      {
        this._matSnackBar.open(this.resultadosp.mensaje, 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
      }

    }, err => {
      this._matSnackBar.open('Error al registrar tipo de cambio', 'ok', {
          verticalPosition: 'bottom',
          panelClass: ['my-snack-bar'] 
          });
    }
    ); 
  }    

}
