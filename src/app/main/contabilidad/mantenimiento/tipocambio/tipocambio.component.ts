import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { CreateTipoCambioDto } from 'app/dto/create.tipocambio.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ExcelService } from 'app/services/excel.service';
import { TipoCambioService } from 'app/services/tipocambio.service';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { TipocambioFormComponent } from './tipocambio-form/tipocambio-form.component';

@Component({
  selector: 'gez-tipocambio',
  templateUrl: './tipocambio.component.html',
  styleUrls: ['./tipocambio.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class TipocambioComponent implements OnInit {
  
  dialogRef: any;
  tipocambio;
  valoresiniciales;
  resultado;

  displayedColumns = ['fecha', 'moneda', 'tipocambiocompra', 'tipocambioventa', 'tipocambiocierre', 'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private tipocambioService: TipoCambioService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService,
    
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {
    this.tipocambio = new CreateTipoCambioDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.fecha.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcionmoneda.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       // data.tipocambiocompra.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       // data.tipocambioventa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 || 
       data.descripcionindicadorcierre.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1
       );

    this.setSettingTableCourse();

  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.tipocambioService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearTipoCambio(): void
  {
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(TipocambioFormComponent, {
        panelClass: 'tipocambio-form-dialog',
        data      : {
          accion: 'nuevo'
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {            
          this.ngOnInit();
        }

        localStorage.setItem(REFRESH, 'N');

      }        
    );     
  }

  editarTipoCambio(vez: number): void
  {
    
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(TipocambioFormComponent, {
        panelClass: 'tipocambio-form-dialog',
        data      : {
          accion: 'editar',
          vez: vez
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {
          this.ngOnInit();
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );
  }

  eliminarTipoCambio(fecha: string, vez: number): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(MessageboxComponent, {
        panelClass: 'confirm-form-dialog',
        data      : {
            titulo: 'Eliminar Tipo de Cambio: ' + fecha,
            subTitulo: '¿Está seguro de eliminar el tipo de cambio?',
            botonAceptar: 'Si',
            botonCancelar: 'No'    
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.tipocambio.empresa = this.valoresiniciales.empresa;
          this.tipocambio.vez     = vez;

          this.tipocambioService.eliminar(this.tipocambio).subscribe(data => {
          this._matSnackBar.open('Se eliminó el tipo de cambio', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });

          this.ngOnInit();

          }, err => {
              this._matSnackBar.open('Error, tipo de cambio no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');

      }        
    );
  }

  exportarTipoCambio(): void
  {
    this.tipocambioService.exportar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Ejercicio Contable'] = this.valoresiniciales.annoproceso;
          objectReport['Fecha'] = item.fecha;
          objectReport['Moneda'] = item.descripcionmoneda;   
          objectReport['Compra'] = item.tipocambiocompra; 
          objectReport['Venta'] = item.tipocambioventa; 
          objectReport['Tipo Cambio Cierre'] = item.descripcionindicadorcierre;  
          objectReport['Estado'] = item.descripcionestado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'TipoCambio');

      }

    }); 
  }

}
