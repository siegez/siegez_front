import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { PlancontableComponent } from './plancontable/plancontable.component';
import { CuentacontableComponent } from './plancontable/cuentacontable/cuentacontable.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { TipocambioComponent } from './tipocambio/tipocambio.component';
import { TipocambioFormComponent } from './tipocambio/tipocambio-form/tipocambio-form.component';
import { SocionegocioComponent } from './socionegocio/socionegocio.component';
import { SocionegocioFormComponent } from './socionegocio/socionegocio-form/socionegocio-form.component';
import { SocionegocioAnexoComponent } from './socionegocio/socionegocio-form/socionegocio-anexo/socionegocio-anexo.component';
import { SocionegocioDocumentoComponent } from './socionegocio/socionegocio-form/socionegocio-documento/socionegocio-documento.component';
import { SocionegocioDomicilioComponent } from './socionegocio/socionegocio-form/socionegocio-domicilio/socionegocio-domicilio.component';
import { CuentabancariaComponent } from './cuentabancaria/cuentabancaria.component';
import { CuentabancariaFormComponent } from './cuentabancaria/cuentabancaria-form/cuentabancaria-form.component';
import { EntidadfinancieraCuentabancariaComponent } from './cuentabancaria/cuentabancaria-form/entidadfinanciera-cuentabancaria/entidadfinanciera-cuentabancaria.component';
import { TipodocumentoComponent } from './configuracion/tipodocumento/tipodocumento.component';
import { TipodocumentoFormComponent } from './configuracion/tipodocumento/tipodocumento-form/tipodocumento-form.component';
import { SubdiarioComponent } from './configuracion/subdiario/subdiario.component';
import { SubdiarioFormComponent } from './configuracion/subdiario/subdiario-form/subdiario-form.component';
import { CuentaopedocmonComponent } from './configuracion/cuentaopedocmon/cuentaopedocmon.component';
import { CuentaopedocmonFormComponent } from './configuracion/cuentaopedocmon/cuentaopedocmon-form/cuentaopedocmon-form.component';
import { CuentaautomaticaComponent } from './configuracion/cuentaautomatica/cuentaautomatica.component';
import { ParametroFormComponent } from './configuracion/parametro-form/parametro-form.component';
import { SubdiarioinicioComponent } from './configuracion/subdiarioinicio/subdiarioinicio.component';
import { TipodocumentoinicioComponent } from './configuracion/tipodocumentoinicio/tipodocumentoinicio.component';
import { IgvComponent } from './configuracion/igv/igv.component';
import { RentacuartaComponent } from './configuracion/rentacuarta/rentacuarta.component';
import { ValorfechaFormComponent } from './configuracion/valorfecha-form/valorfecha-form.component';
import { CentrocostoComponent } from './centrocosto/centrocosto.component';
import { CentrocostoFormComponent } from './centrocosto/centrocosto-form/centrocosto-form.component';
import { GrupocentrocostoComponent } from './grupocentrocosto/grupocentrocosto.component';
import { GrupocentrocostoFormComponent } from './grupocentrocosto/grupocentrocosto-form/grupocentrocosto-form.component';
import { GrupocentrocostoCentrocostoComponent } from './grupocentrocosto/grupocentrocosto-form/grupocentrocosto-centrocosto/grupocentrocosto-centrocosto.component';
import { DetraccionComponent } from './detraccion/detraccion.component';
import { DetraccionFormComponent } from './detraccion/detraccion-form/detraccion-form.component';
// import { DecimalNumberDirectiveTwo } from 'app/directive/valid-decimal-directive-Two';
// import { DecimalNumberDirectiveThree } from 'app/directive/valid-decimal-directive-three';


const routes: Routes = [
    {
        path     : 'plancontable',
        component: PlancontableComponent,
        // resolve  : {
        //     data: UsuariosService
        // }
     },
     {
        path     : 'cuentacontable',
        component: CuentacontableComponent,
        // resolve  : {
        //     data: UsuariosService
        // }
     },  
     {
        path     : 'socionegocio',
        component: SocionegocioComponent,
     },
     {
        path     : 'socionegocio-form',
        component: SocionegocioFormComponent,

     },
     {
        path     : 'cuentabancaria',
        component: CuentabancariaComponent,
     },
     {
        path     : 'cuentabancaria-form',
        component: CuentabancariaFormComponent,
     },
     {
        path     : 'igv',
        component: IgvComponent,
     },
     {
        path     : 'rentacuarta',
        component: RentacuartaComponent,
     },
     {
        path     : 'tipocambio',
        component: TipocambioComponent,
     }, 
     {
        path     : 'tipodocumento',
        component: TipodocumentoComponent,
     },
     {
        path     : 'tipodocumento-form',
        component: TipodocumentoFormComponent,
     },
     {
        path     : 'tipodocumentoinicio',
        component: TipodocumentoinicioComponent,
     }, 
     {
        path     : 'subdiario',
        component: SubdiarioComponent,
     },
     {
        path     : 'subdiario-form',
        component: SubdiarioFormComponent,
     }, 
     {
        path     : 'subdiarioinicio',
        component: SubdiarioinicioComponent,
     }, 
     {
        path     : 'cuentaopedocmon',
        component: CuentaopedocmonComponent,
     },
     {
        path     : 'cuentaopedocmon-form',
        component: CuentaopedocmonFormComponent,
     }, 
     {
        path     : 'cuentaautomatica',
        component: CuentaautomaticaComponent,
     }, 
     {
        path     : 'parametro',
        component: ParametroFormComponent,
     }, 
     {
        path     : 'centrocosto',
        component: CentrocostoComponent,
     },
     {
        path     : 'centrocosto-form',
        component: CentrocostoFormComponent,
     }, 
     {
        path     : 'grupocentrocosto',
        component: GrupocentrocostoComponent,
     },
     {
        path     : 'grupocentrocosto-form',
        component: GrupocentrocostoFormComponent,
     }, 
     {
        path     : 'detraccion',
        component: DetraccionComponent,
     },
     {
        path     : 'detraccion-form',
        component: DetraccionFormComponent,
     }
];

@NgModule({
    declarations: [
        // DecimalNumberDirectiveTwo,
        // DecimalNumberDirectiveThree,
        PlancontableComponent,
        CuentacontableComponent,
        TipocambioComponent,
        TipocambioFormComponent,
        SocionegocioComponent,
        SocionegocioFormComponent,
        SocionegocioAnexoComponent,
        SocionegocioDocumentoComponent,
        SocionegocioDomicilioComponent,
        CuentabancariaComponent,
        CuentabancariaFormComponent,
        EntidadfinancieraCuentabancariaComponent,
        TipodocumentoComponent,
        TipodocumentoFormComponent,
        SubdiarioComponent,
        SubdiarioFormComponent,
        CuentaopedocmonComponent,
        CuentaopedocmonFormComponent,
        CuentaautomaticaComponent,
        ParametroFormComponent,
        SubdiarioinicioComponent,
        TipodocumentoinicioComponent,
        IgvComponent,
        RentacuartaComponent,
        ValorfechaFormComponent,
        CentrocostoComponent,
        CentrocostoFormComponent,
        GrupocentrocostoComponent,
        GrupocentrocostoFormComponent,
        GrupocentrocostoCentrocostoComponent,
        DetraccionComponent,
        DetraccionFormComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatSlideToggleModule,
         
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],

    exports: [
      // DecimalNumberDirectiveTwo,
      // DecimalNumberDirectiveThree
    ],

    providers   : [
        
    ]
})
export class MantenimientoModule
{
}
