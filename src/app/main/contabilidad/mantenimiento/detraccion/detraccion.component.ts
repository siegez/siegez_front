import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { CreateDetraccionDto } from 'app/dto/create.detraccion.dto';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { DetraccionService } from 'app/services/detraccion.service';
import { ExcelService } from 'app/services/excel.service';
import { MESSAGEBOX, REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { DetraccionFormComponent } from './detraccion-form/detraccion-form.component';

@Component({
  selector: 'gez-detraccion',
  templateUrl: './detraccion.component.html',
  styleUrls: ['./detraccion.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class DetraccionComponent implements OnInit {
  
  dialogRef: any;
  detraccion;
  valoresiniciales;
  resultado;

  displayedColumns = ['detraccion', 'definicion', 'tasa', 'montominimo', 'descripcionestado', 'editar', 'eliminar'];

  listarReporte: any = [];

  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private detraccionService: DetraccionService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private excelService: ExcelService,
    
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {
    this.detraccion = new CreateDetraccionDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.detraccion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.definicion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       // data.tasa.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.tasa.toString().indexOf(filter.trim().toLowerCase()) !== -1 || 
       // data.montominimo.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.montominimo.toString().indexOf(filter.trim().toLowerCase()) !== -1 || 
       data.descripcionestado.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1
       );

    this.setSettingTableCourse();

  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.detraccionService.listar(this.valoresiniciales.empresa).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearDetraccion(): void
  {
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(DetraccionFormComponent, {
        panelClass: 'detraccion-form-dialog',
        data      : {
          accion: 'nuevo'
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {            
          this.ngOnInit();
        }

        localStorage.setItem(REFRESH, 'N');

      }        
    );     
  }

  editarDetraccion(detraccion: string): void
  {
    
    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(DetraccionFormComponent, {
        panelClass: 'detraccion-form-dialog',
        data      : {
          accion: 'editar',
          codigo: detraccion
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(REFRESH) === 'S' )
        {
          this.ngOnInit();
        }
        localStorage.setItem(REFRESH, 'N');
      }        
    );
  }

  eliminarDetraccion(detraccion: string): void
  {

    localStorage.setItem(REFRESH, 'N');

    this.dialogRef = this._matDialog.open(MessageboxComponent, {
        panelClass: 'confirm-form-dialog',
        data      : {
            titulo: 'Eliminar Detracción: ' + detraccion,
            subTitulo: '¿Está seguro de eliminar la detracción?',
            botonAceptar: 'Si',
            botonCancelar: 'No'    
        }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.detraccion.empresa  = this.valoresiniciales.empresa;
          this.detraccion.detraccion = detraccion;

          this.detraccionService.eliminar(this.detraccion).subscribe(data => {
          this._matSnackBar.open('Se eliminó la detracción', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });

          this.ngOnInit();

          }, err => {
              this._matSnackBar.open('Error, detracción no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');

      }        
    );
  }

  exportarDetraccion(): void
  {
    this.detraccionService.exportar(this.valoresiniciales.empresa).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Código Detracción'] = item.detraccion;
          objectReport['Definición'] = item.definicion;   
          objectReport['Descripción'] = item.descripcion; 
          objectReport['Tasa'] = item.tasa; 
          objectReport['Monto Mínimo'] = item.montominimo;          
          objectReport['Estado'] = item.descripcionestado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'Detraccion');

      }

    }); 
  }

}
