import { Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';
import { REFRESH, VALORES_INICIALES } from 'app/_shared/var.constant';
import { DetraccionService } from 'app/services/detraccion.service';
import { CreateDetraccionDto } from 'app/dto/create.detraccion.dto';

@Component({
  selector: 'gez-detraccion-form',
  templateUrl: './detraccion-form.component.html',
  styleUrls: ['./detraccion-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class DetraccionFormComponent implements OnInit  {
  // @ViewChild('fecha') fechaField: ElementRef;
  
  accion: string;
  codigo: string; 
  detraccion;  
  detraccionForm: FormGroup;
  dialogTitle: string;
  botonExit = 'Cancelar';
  botonAgregar = 'Grabar';
  botonAceptar = 'Grabar';
  hide = true;
  hidegrabar = false;
  rows: any[];
  edit = true;
  valoresiniciales;
  valorcontador;
  checkEstado = false;


  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<DetraccionFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private detraccionService: DetraccionService,
  ) 
  { 
    this.accion = _data.accion;
    this.codigo = _data.codigo;
  }


  ngOnInit(): void 
  {   

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    
    if ( this.accion === 'editar' )
    {
      this.dialogTitle = 'Editar Detracción';  
      this.detraccion = new CreateDetraccionDto();

      this.detraccionService.consultar(this.valoresiniciales.empresa, this.codigo).subscribe(data => {
        this.detraccion = data;  
        this.checkEstado = (this.detraccion.estado === 'A' ) ? true : false;    
        this.detraccionForm = this.createDetraccionForm();
      }, err => {
        this._matSnackBar.open('Error de consulta', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
      ); 
    }
    else
    {
      this.dialogTitle = 'Nuevo Detracción';
      this.edit = false;
      this.checkEstado = true;
      this.detraccion = new CreateDetraccionDto();
      this.detraccionForm = this.createDetraccionForm();
    }
    
    this.detraccionForm = this.createDetraccionForm();

  }

  createDetraccionForm(): FormGroup
  {
    return this._formBuilder.group({

      detraccion    : [this.detraccion.detraccion],
      definicion    : [this.detraccion.definicion],
      descripcion   : [this.detraccion.descripcion],
      tasa          : [this.detraccion.tasa],
      montominimo   : [this.detraccion.montominimo],
      estado        : [this.checkEstado]

    });
  }

  // tslint:disable-next-line:typedef
  registrarDetraccion(detraccionForm){

    this.detraccion.empresa = this.valoresiniciales.empresa;     
    this.detraccion.detraccion = this.detraccionForm.value.detraccion;
    this.detraccion.definicion = this.detraccionForm.value.definicion;
    this.detraccion.descripcion = this.detraccionForm.value.descripcion;
    this.detraccion.tasa = this.detraccionForm.value.tasa;
    this.detraccion.montominimo = this.detraccionForm.value.montominimo;
    this.detraccion.estado = (this.detraccionForm.value.estado) ? 'A' : 'I';
    this.detraccion.creacionUsuario = this.valoresiniciales.username;
    this.detraccion.creacionFecha = '2020-01-01';
    this.detraccion.modificacionUsuario = this.valoresiniciales.username;
    this.detraccion.modificacionFecha = '2020-01-01';
    
    // consumir servicios para grabar
    if ( this.accion === 'nuevo' )
    {
      this.detraccionService.crear(this.detraccion).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
          });
        
        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;

        localStorage.setItem(REFRESH, 'S');

        }, 
        err => 
        {
            this._matSnackBar.open('Error, detracción no registrado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
    else
    {

      this.detraccionService.editar(this.detraccion).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        this.botonExit = 'Salir';
        this.hidegrabar = true;
        this.edit = true;

        localStorage.setItem(REFRESH, 'S');

        }, err => {
            this._matSnackBar.open('Error, detracción no actualizado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
  }   

}


