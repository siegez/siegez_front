import { Component, OnInit, ViewEncapsulation, ElementRef, AfterViewChecked, AfterViewInit, AfterContentInit } from '@angular/core';
import { DATOS, VALORES_INICIALES, REFRESH, CATALOGO } from 'app/_shared/var.constant';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PlanContableService } from 'app/services/plancontable.service';
import { CreatePlanContableDto } from 'app/dto/create.plancontable.dto';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CreateCuentaContableDistribucionDto } from 'app/dto/create.cuentacontabledistribucion.dto';
import { ValorCampoDto } from 'app/dto/create.valorcampo.dto';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValorContadorDto } from 'app/dto/view.contador.dto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SearchboxComponent } from 'app/main/shared/searchbox/searchbox.component';


@Component({
  selector: 'gez-cuentacontable',
  templateUrl: './cuentacontable.component.html',
  styleUrls: ['./cuentacontable.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class CuentacontableComponent implements OnInit {

  misDatos;
  cuentacontableForm: FormGroup;
  distribucionForm: FormGroup;
  cuentacontable;
  distribucion;
  valorcampo;
  valoresiniciales;
  valorcontador;
  valorcatalogo;
  edit = true;  
  listaCategoria: any[];
  listaTipoCuenta: any[];
  listaMoneda: any[];
  listaNivelSaldo: any[];
  listaSocioNegocio: any[];
  listaTipoCambio: any[];
  listaEstado: any[];

  checkDocumentoReferencia = false;
  checkCuentaManual = false;
  checkMedioPago = false;
  checkCentroCosto = false;
  checkAjustePendiente = false;
  checkAjusteCancelado = false;
  checkSaldoClase9 = false;
  checkTransferencia = false;
  checkCobro = false;
  checkPago = false;
  checkEstado = false;
  dialogRef: any;
  
  constructor(
    private plancontableService: PlanContableService,
    private consultaService: ConsultaService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar,
    private _matDialog: MatDialog
  ) 
  {    

  }

  // tslint:disable-next-line:typedef
  async ngOnInit() 
  {

    this.misDatos = JSON.parse(localStorage.getItem(DATOS));
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

    this.valorcampo = new ValorCampoDto();
    this.valorcontador = new ViewValorContadorDto();   
    this.cuentacontable = new CreatePlanContableDto();
    this.distribucion = new CreateCuentaContableDistribucionDto();

    this.cuentacontableForm = this.createPlanContableForm();
    this.distribucionForm   = this.createDistribucionForm();

    this.listarCategoria();
    this.listarTipoCuenta();
    this.listarMoneda();
    this.listarNivelSaldo();
    this.listarSocioNegocio();
    this.listarTipoCambio();    

    // campos pk
    this.cuentacontable.empresa            = this.valoresiniciales.empresa;
    this.cuentacontable.ejerciciocontable  = this.valoresiniciales.annoproceso;

    if ( this.misDatos.action === 'editar' )
    {
      // campo pk
      this.cuentacontable.cuenta       = this.misDatos.parametro1;      
      this.obtenerCuentaContable();        
    }
    else{
      this.edit = false;
      this.checkEstado = true;
      this.iniciarCuentaContable();
    }

  }

  // tslint:disable-next-line:typedef
  obtenerCuentaContable(){

    this.plancontableService.consultar(this.cuentacontable.empresa, this.cuentacontable.ejerciciocontable, this.cuentacontable.cuenta).subscribe(data => {
      this.cuentacontable = data;    
      this.checkDocumentoReferencia = (this.cuentacontable.documentoreferencia === 'S' ) ? true : false;   
      this.checkCuentaManual        = (this.cuentacontable.cuentamanual === 'M' ) ? true : false;  
      this.checkMedioPago           = (this.cuentacontable.mediopago === 'S' ) ? true : false;  
      this.checkCentroCosto         = (this.cuentacontable.centrocosto === 'S' ) ? true : false; 
      this.checkAjustePendiente     = (this.cuentacontable.ajustependiente === 'S' ) ? true : false; 
      this.checkAjusteCancelado     = (this.cuentacontable.ajustecancelado === 'S' ) ? true : false; 
      this.checkSaldoClase9         = (this.cuentacontable.saldoclase9 === 'S' ) ? true : false; 
      this.checkTransferencia       = (this.cuentacontable.transferencia === 'S' ) ? true : false; 
      this.checkCobro               = (this.cuentacontable.cobro === 'S' ) ? true : false; 
      this.checkPago                = (this.cuentacontable.pago === 'S' ) ? true : false;
      this.checkEstado              = (this.cuentacontable.estado === 'A' ) ? true : false;

      this.cuentacontableForm = this.createPlanContableForm();

      }, err => {
        this._matSnackBar.open('Error de consulta a la cuenta contable', 'ok', {
            verticalPosition: 'bottom',
            panelClass: ['my-snack-bar'] 
            });
      }
    );  
  }

  // tslint:disable-next-line:typedef
  iniciarCuentaContable(){
    setTimeout(() => {
      this.cuentacontableForm.controls['cuenta'].setValue('');
      this.cuentacontableForm.controls['descripcioncuenta'].setValue('');
      this.cuentacontableForm.controls['categoria'].setValue('');
      this.cuentacontableForm.controls['tipocuenta'].setValue('');
      this.cuentacontableForm.controls['moneda'].setValue('');
      this.cuentacontableForm.controls['nivelsaldo'].setValue('');
      this.cuentacontableForm.controls['socionegocio'].setValue('');
      this.cuentacontableForm.controls['estado'].setValue(true);
      
    });
  }

  
  createPlanContableForm(): FormGroup
  {
    return this._formBuilder.group({
          cuenta                    : [this.cuentacontable.cuenta],
          descripcioncuenta         : [this.cuentacontable.descripcioncuenta],
          categoria                 : [this.cuentacontable.categoria],
          tipocuenta                : [this.cuentacontable.tipocuenta],
          moneda                    : [this.cuentacontable.moneda],
          nivelsaldo                : [this.cuentacontable.nivelsaldo],
          socionegocio              : [this.cuentacontable.socionegocio],
          documentoreferencia       : [this.checkDocumentoReferencia],
          cuentamanual              : [this.checkCuentaManual],
          mediopago                 : [this.checkMedioPago],  
          centrocosto               : [this.checkCentroCosto],
          ajustependiente           : [this.checkAjustePendiente],
          ajustecancelado           : [this.checkAjusteCancelado],
          tipocambio                : [this.cuentacontable.tipocambio],
          asientodebe               : [this.cuentacontable.asientodebe],
          asientohaber              : [this.cuentacontable.asientohaber],
          saldoclase9               : [this.checkSaldoClase9],
          transferencia             : [this.checkTransferencia],
          cobro                     : [this.checkCobro],
          pago                      : [this.checkPago],    
          estado                    : [this.checkEstado]    
      });

  }

  createDistribucionForm(): FormGroup
  {
      return this._formBuilder.group({
        tabla              : [this.distribucion.tabla],   
        estado             : [this.distribucion.estado]       
      });
  }

  onBlurCuentaContable(): void
  {
    if ( this.cuentacontableForm.value.cuenta.replace(/\s+$/g, '') === null || this.cuentacontableForm.value.cuenta.replace(/\s+$/g, '') === '')
    {
      this.cuentacontableForm.controls['cuenta'].reset();
    }
    else
    {
      if ( this.edit !== true )
      {
        this.cuentacontable.cuenta = this.cuentacontableForm.value.cuenta;
    
        this.plancontableService.contar(this.cuentacontable.empresa, this.cuentacontable.ejerciciocontable, this.cuentacontable.cuenta).subscribe(data => {
          this.valorcontador = data;
    
          if (this.valorcontador.contador > 0)
            {
              this._matSnackBar.open('Error, Cuenta contable ya existe', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
              this.cuentacontableForm.controls['cuenta'].reset();
            }
          }, 
          err => 
          {
              this._matSnackBar.open('Error, error de servicio', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
        );
      }
    }
  }

  grabarCuentaContable(): void
  {
   
    if ( this.misDatos.action === 'nuevo' )
    {
      this.cuentacontable.cuenta = this.cuentacontableForm.value.cuenta;
    }    

    if (this.cuentacontableForm.value.centrocosto)
    {
      this.cuentacontable.asientodebe = '';
      this.cuentacontable.asientohaber = '';
    }
    else
    {
      // this.cuentacontableForm.controls['asientodebe'].enable();
      // this.cuentacontableForm.controls['asientohaber'].enable();
      this.cuentacontable.asientodebe = this.cuentacontableForm.value.asientodebe;
      this.cuentacontable.asientohaber = this.cuentacontableForm.value.asientohaber;
      // this.cuentacontableForm.controls['asientodebe'].disable();
      // this.cuentacontableForm.controls['asientohaber'].disable();
    }
    
    this.cuentacontable.descripcioncuenta = this.cuentacontableForm.value.descripcioncuenta;
    this.cuentacontable.categoria = this.cuentacontableForm.value.categoria;
    this.cuentacontable.tipocuenta = this.cuentacontableForm.value.tipocuenta;
    this.cuentacontable.moneda = this.cuentacontableForm.value.moneda;
    this.cuentacontable.nivelsaldo = this.cuentacontableForm.value.nivelsaldo;
    this.cuentacontable.socionegocio = this.cuentacontableForm.value.socionegocio;
    this.cuentacontable.documentoreferencia = (this.cuentacontableForm.value.documentoreferencia) ? 'S' : 'N';
    this.cuentacontable.cuentamanual = (this.cuentacontableForm.value.cuentamanual) ? 'M' : 'A';
    this.cuentacontable.mediopago = (this.cuentacontableForm.value.mediopago) ? 'S' : 'N';
    this.cuentacontable.centrocosto = (this.cuentacontableForm.value.centrocosto) ? 'S' : 'N';
    this.cuentacontable.ajustependiente = (this.cuentacontableForm.value.ajustependiente) ? 'S' : 'N';
    this.cuentacontable.ajustecancelado = (this.cuentacontableForm.value.ajustecancelado) ? 'S' : 'N';
    this.cuentacontable.tipocambio = this.cuentacontableForm.value.tipocambio.trim();
    this.cuentacontable.saldoclase9 = (this.cuentacontableForm.value.saldoclase9) ? 'S' : 'N';
    this.cuentacontable.transferencia = (this.cuentacontableForm.value.transferencia) ? 'S' : 'N';
    this.cuentacontable.cobro = (this.cuentacontableForm.value.cobro) ? 'S' : 'N';
    this.cuentacontable.pago = (this.cuentacontableForm.value.pago) ? 'S' : 'N';
    this.cuentacontable.estado = (this.cuentacontableForm.value.estado) ? 'A' : 'I';
    // this.cuentacontable.estado = this.cuentacontableForm.value.estado;
    this.cuentacontable.creacionUsuario = this.valoresiniciales.username;
    this.cuentacontable.creacionFecha = '2020-01-01';
    this.cuentacontable.modificacionUsuario = this.valoresiniciales.username;
    this.cuentacontable.modificacionFecha = '2020-01-01';

      
    // consumir servicios para grabar
    if ( this.misDatos.action === 'nuevo' )
    {
      this.plancontableService.crear(this.cuentacontable).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
          });

        const misDatosNew =  { 
            action: 'editar',
            parametro1: this.cuentacontable.cuenta
          };

        localStorage.setItem(DATOS, JSON.stringify(misDatosNew));

        this.misDatos = JSON.parse(localStorage.getItem(DATOS));

        this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));

        this.edit = true;

        // this.cuentacontableForm.controls['cuenta'].disable();

        }, 
        err => 
        {
            this._matSnackBar.open('Error, cuenta contable no registrado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }
    else
    {

      this.plancontableService.editar(this.cuentacontable).subscribe(data => {
        this._matSnackBar.open('Grabación exitosa', '', {
        verticalPosition: 'bottom',
        duration        : 2000
        });

        }, err => {
            this._matSnackBar.open('Error, cuenta contable no actualizado', '', {
              verticalPosition: 'bottom',
              duration        : 2000
            });
        }
      );
    }

  }

  // tslint:disable-next-line:typedef
  listarCategoria(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDCATEGORIACUENTA';
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaCategoria = data;
     });
  }

  // tslint:disable-next-line:typedef
  listarTipoCuenta(){   
    
    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDTIPOCUENTA';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaTipoCuenta = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarMoneda(){

    this.consultaService.listarTablaDescripcion(this.valoresiniciales.empresa, 4).subscribe(data => {
      this.listaMoneda = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarNivelSaldo(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDNIVELSALDO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaNivelSaldo = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarSocioNegocio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'CON';
    this.valorcampo.campo = 'IDSOCIONEGOCIO';
    this.consultaService.listarCatalogoDescripcion(this.valorcampo).subscribe(data => {
      this.listaSocioNegocio = data;
    });
  }

  // tslint:disable-next-line:typedef
  listarTipoCambio(){

    this.valorcampo.empresa = this.valoresiniciales.empresa;
    this.valorcampo.ambito = 'SHA';
    this.valorcampo.campo = 'IDTIPOCAMBIO';    
    this.consultaService.listarCatalogoCodigo(this.valorcampo).subscribe(data => {
      this.listaTipoCambio = data;
    });
  }

  // tslint:disable-next-line:typedef
  onChangeCentroCosto(event){

    if (event.checked) {
      // this.cuentacontableForm.controls['asientodebe'].reset();
      // this.cuentacontableForm.controls['asientohaber'].reset();
      this.cuentacontableForm.controls['asientodebe'].setValue('');
      this.cuentacontableForm.controls['asientohaber'].setValue('');
      this.checkCentroCosto         = true; 
    } else {
      this.checkCentroCosto         = false; 
    }    
  }

  // tslint:disable-next-line:typedef
  buscarCuentaContable(campo: string){

    this.valorcatalogo = { codigo: '', descripcion: ''};
    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));
    
    this.dialogRef = this._matDialog.open(SearchboxComponent, 
      {
      panelClass: 'searchbox-dialog',
      data      : {
          catalogo: 'cuentacontable',
          titulo: 'Buscar Cuenta Contable',
          codigo: 'Cuenta Contable',
          descripcion: 'Nombre',
          estado: 'Estado'
      }
    });

    this.dialogRef.afterClosed().subscribe(result => 
      {
        this.valorcatalogo = JSON.parse(localStorage.getItem(CATALOGO));

        if ( this.valorcatalogo.codigo !== '' )
        {         

          if ( campo === 'asientodebe' )
          {
          // this.cuentacontableForm.controls['asientodebe'].enable();
          this.cuentacontableForm.controls['asientodebe'].setValue(this.valorcatalogo.codigo);
          // this.cuentacontableForm.controls['asientodebe'].disable();
          }

          if ( campo === 'asientohaber' )
          {
          // this.cuentacontableForm.controls['asientohaber'].enable();
          this.cuentacontableForm.controls['asientohaber'].setValue(this.valorcatalogo.codigo);
          //  this.cuentacontableForm.controls['asientohaber'].disable();
          }
        }

        this.valorcatalogo = { codigo: '', descripcion: ''};
        localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));

      }        
    ); 

  }

  crearCuentaContable(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.ngOnInit();

  }

}
