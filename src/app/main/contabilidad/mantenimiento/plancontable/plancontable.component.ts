import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { CreatePlanContableDto } from 'app/dto/create.plancontable.dto';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { PlanContableService } from 'app/services/plancontable.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DATOS, VALORES_INICIALES, MESSAGEBOX } from 'app/_shared/var.constant';
import { MessageboxComponent } from 'app/main/shared/messagebox/messagebox.component';
import { ExcelService } from 'app/services/excel.service';

@Component({
  selector: 'gez-plancontable',
  templateUrl: './plancontable.component.html',
  styleUrls: ['./plancontable.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class PlancontableComponent implements OnInit {

  dialogRef: any;
  cuentacontable;
  valoresiniciales;
  resultado;
  
  displayedColumns = ['cuenta', 'descripcioncuenta', 'descripcioncategoria', 'descripciontipo', 'descripcionmoneda',
                      'estado', 'editar', 'eliminar'];

  listarReporte: any = [];

   
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }

  constructor(
    private plancontableService: PlanContableService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private paginatorlabel: MatPaginatorIntl,
    private router: Router,
    private excelService: ExcelService
  ) 
  { 
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void 
  {

    this.cuentacontable = new CreatePlanContableDto();
    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.resultado = '';

    this.obtenerVista(); 

    this.dataSource.filterPredicate = (data: any, filter: string) =>
     (
       data.cuenta.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcioncuenta.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripcioncategoria.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       data.descripciontipo.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
       (data.descripcionmoneda.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 && data.descripcionmoneda)
     
       );

    this.setSettingTableCourse();
  
  }

  // tslint:disable-next-line:typedef
  async obtenerVista() 
  {
    this.resultado = 'Cargando ... ';
    this.dataSource.data = (await this.plancontableService.listar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).toPromise());
    this.dataSource.sort = this.sort;
    if ( this.dataSource.data.length === 0)
    {
      this.resultado = 'Resultado de la búsqueda, 0 registros... ';
    }
    else
    {
      this.resultado = '';
    }

  } 

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  crearCuentaContable(): void
  {
    // localStorage.setItem(PARAMETRO_VAR, username);
    const misDatos = { action: 'nuevo'};

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`contabilidad/mantenimiento/cuentacontable`]);
  }

  editarCuentaContable(cuenta: string): void
  {
    const misDatos =  { 
                        action: 'editar',
                        parametro1: cuenta
                      };

    localStorage.setItem(DATOS, JSON.stringify(misDatos));
    
    this.router.navigate([`contabilidad/mantenimiento/cuentacontable`]);
    
  }

  eliminarCuentaContable(cuenta: string): void
  {
    this.dialogRef = this._matDialog.open(MessageboxComponent, {
      panelClass: 'confirm-form-dialog',
      data      : {
          titulo: 'Eliminar Cuenta Contable: ' + cuenta,
          subTitulo: '¿Está seguro de eliminar la cuenta contable?',
          botonAceptar: 'Si',
          botonCancelar: 'No'    
      }
    });

  // tslint:disable-next-line:no-conditional-assignment
    this.dialogRef.afterClosed().subscribe(result => 
      {
        if ( localStorage.getItem(MESSAGEBOX) === 'S' )
        {
          // campos pk
          this.cuentacontable.empresa            = this.valoresiniciales.empresa;
          this.cuentacontable.ejerciciocontable  = this.valoresiniciales.annoproceso;
          this.cuentacontable.cuenta             = cuenta;

          this._matSnackBar.open('Eliminando cuenta contable ...', '', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration        : 200
            });

          this.plancontableService.eliminar(this.cuentacontable).subscribe(data => {
          this._matSnackBar.open('Se eliminó la cuenta contable', '', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          duration        : 2000
          });
  
          this.ngOnInit();
  
          }, err => {
              this._matSnackBar.open('Error, cuenta contable no eliminado', '', {
                verticalPosition: 'bottom',
                duration        : 2000
              });
          }
          );
        }
        
        localStorage.setItem(MESSAGEBOX, 'N');
  
      }        
    );    
  }

  exportarPlanContable(): void
  {
    this.plancontableService.exportar(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso).subscribe(data => {
      this.listarReporte = data;

      if (this.listarReporte.length > 0) {
        const reportExcelExcel = [];
        for (const item of this.listarReporte ){
          const objectReport = {};
          objectReport['Empresa'] = this.valoresiniciales.nombreempresa;
          objectReport['Ejercicio Contable'] = this.valoresiniciales.annoproceso;
          objectReport['Cuenta'] = item.cuenta;
          objectReport['Nombre'] = item.descripcioncuenta;   
          objectReport['Categoría'] = item.descripcioncategoria; 
          objectReport['Tipo'] = item.descripciontipo; 
          objectReport['Moneda'] = item.descripcionmoneda; 
          objectReport['Nivel de Saldo'] = item.descripcionnivel; 
          objectReport['Socio Negocio'] = item.descripcionsocionegocio;  
          objectReport['Documento de Referencia'] = item.descripciondocumentoreferencia;  
          objectReport['Cuenta Manual'] = item.descripcioncuentamanual;  
          objectReport['Medio de Pago'] = item.descripcionmediopago;  
          objectReport['Centro de Costo'] = item.descripcioncentrocosto;  
          objectReport['Asiento Automático - Debe'] = item.asientodebe; 
          objectReport['Asiento Automático - Haber'] = item.asientohaber; 
          objectReport['Ajuste por Diferencia de Cambio - Pendiente'] = item.descripcionajustependiente;  
          objectReport['Ajuste por Diferencia de Cambio - Cancelado'] = item.descripcionajustecancelado;  
          objectReport['Tipo de Cambio'] = item.descripciontipocambio;  
          objectReport['Cierre del Ejercicio - Saldo Clase 9'] = item.descripcionsaldoclase9;  
          objectReport['Cierre del Ejercicio - Transferencia por naturaleza'] = item.descripciontransferencia;  
          objectReport['Cuenta de Cobro'] = item.descripcioncobro;  
          objectReport['Cuenta de Pago'] = item.descripcionpago;  
          objectReport['Estado'] = item.descripcionestado;  
          objectReport['Creado por'] = item.creacionUsuario;  
          objectReport['Fecha de Creación'] = item.creacionFecha;  
          objectReport['Modificado por'] = item.modificacionUsuario;  
          objectReport['Fecha de Modificación'] = item.modificacionFecha;  
          
          reportExcelExcel.push(objectReport);
        }  

        this.excelService.exportAsExcelFile(reportExcelExcel, 'PlanContable');

      }

    }); 
  }

}
