import { Component, OnInit, ViewEncapsulation, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { VALORES_INICIALES, CATALOGO } from 'app/_shared/var.constant';
import { PlanContableService } from 'app/services/plancontable.service';
import { CreateOperacionContableDto } from 'app/dto/create.operacioncontable.dto';
import { OperacionContableService } from 'app/services/operacioncontable.service';
import { MatInput } from '@angular/material/input';
import { EnteService } from 'app/services/ente.service';

@Component({
  selector: 'gez-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class SearchboxComponent implements OnInit, AfterViewInit {

  @ViewChild('buscar') buscarInputField: MatInput;

  catalogo = this._data.catalogo;
  titulo = this._data.titulo;  
  codigo = this._data.codigo;
  descripcion = this._data.descripcion;
  detalle = this._data.detalle;
  buscarForm: FormGroup;
  displayedColumns = ['codigocatalogo', 'descripcioncatalogo', 'detallecatalogo'];
  valoresiniciales;
  valorcatalogo;
  cuentacontable;
  operacioncontable;
  resultado;
  
  public dataSource = new MatTableDataSource<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) 
  set paginator(v: MatPaginator) { this.dataSource.paginator = v; }
  
  constructor(
    private _matSnackBar: MatSnackBar,
    public matDialogRef: MatDialogRef<SearchboxComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private plancontableService: PlanContableService,
    private operacioncontableService: OperacionContableService,
    private enteService: EnteService,
    private paginatorlabel: MatPaginatorIntl,
  ) 
  {
    this.paginatorlabel.itemsPerPageLabel = 'Registros por página';
    this.paginatorlabel.firstPageLabel = 'Primera página';
    this.paginatorlabel.nextPageLabel = 'Página siguiente';
    this.paginatorlabel.previousPageLabel = 'Página anterior';
    this.paginatorlabel.lastPageLabel = 'Última página';       
    // tslint:disable-next-line:max-line-length
    this.paginatorlabel.getRangeLabel = (page: number, pageSize: number, length: number) => { if (length === 0 || pageSize === 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} – ${endIndex} de ${length}`; };      
  }

  ngOnInit(): void {

    this.buscarForm = this.createbuscarForm();

    // this.dataSource.filterPredicate = (data: any, filter: string) =>
    //  (
    //    data.codigo.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 ||
    //    data.descripcion.trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1 
    //  );
    this.setSettingTableCourse();

    this.valoresiniciales = JSON.parse(localStorage.getItem(VALORES_INICIALES));
    this.valorcatalogo    = JSON.parse(localStorage.getItem(CATALOGO));
    this.resultado = '';

    if ( this.catalogo !== 'operacioncontable' ) 
    {
      this.buscarForm.get('buscar').setValidators(Validators.required);
    } else {
      this.buscarForm.get('buscar').clearValidators();
      this.buscarOperacionContable('');
    }

    setTimeout(() => {
      this.buscarInputField.focus();
    });    
 
  }

  // tslint:disable-next-line:typedef
  ngAfterViewInit() {

    setTimeout(() => {
            this.buscarInputField.focus();
    });

  }

  // tslint:disable-next-line:typedef
  private setSettingTableCourse() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;       
  }

  createbuscarForm(): FormGroup
  {
      return this._formBuilder.group({
        buscar         : ''
      });
  }

  buscarCatalogo(): void
  {

    if ( this.catalogo === 'cuentacontable' )
    {
      this.buscarCuentaContable(this.buscarForm.value.buscar);
    }

    if ( this.catalogo === 'operacioncontable' )
    {
      this.buscarOperacionContable(this.buscarForm.value.buscar);
    }

    if ( this.catalogo === 'proveedor' )
    {
      this.buscarProveedor(this.buscarForm.value.buscar);
    }

    if ( this.catalogo === 'cliente' )
    {
      this.buscarCliente(this.buscarForm.value.buscar);
    }

    if ( this.catalogo === 'proveedornodom' )
    {
      this.buscarProveedorNoDom(this.buscarForm.value.buscar);
    }

    if ( this.catalogo === 'socionegocio' )
    {
      this.buscarSocioNegocio(this.buscarForm.value.buscar);
    }
    
    this.setSettingTableCourse();

  }

  buscarCuentaContable(cadena: string): void
  {

    // const cadenalike = '%' + cadena + '%';
    this.plancontableService.listarCatalogo(this.valoresiniciales.empresa, this.valoresiniciales.annoproceso, cadena).subscribe(data => {
        this.dataSource.data = data;   
    });  

  }

  buscarOperacionContable(cadena: string): void
  {
    this.resultado = 'Cargando ... ';
    
    this.operacioncontable = new CreateOperacionContableDto();
    this.operacioncontable.empresa            = this.valoresiniciales.empresa;
    this.operacioncontable.ejerciciocontable  = this.valoresiniciales.annoproceso;
    this.operacioncontable.operacioncontable  = this.valorcatalogo.operacioncontable;
    this.operacioncontable.entidad            = this.valorcatalogo.entidad;
    this.operacioncontable.glosa              = '%' + cadena + '%';
  
    this.operacioncontableService.listarCatalogoRef(this.operacioncontable).subscribe(data => {
        this.dataSource.data = data;   

        setTimeout(() => {
          if ( this.dataSource.data.length === 0)
          {
            this.resultado = 'Resultado de la búsqueda, 0 registros... ';
          }
          else
          {
            this.resultado = '';
          }
        });
    });  

  }

  buscarProveedor(cadena: string): void
  {
    this.enteService.listarProveedorBusqueda(this.valoresiniciales.empresa, cadena).subscribe(data => {
        this.dataSource.data = data;   
    });  

  }

  buscarCliente(cadena: string): void
  {
    this.enteService.listarClienteBusqueda(this.valoresiniciales.empresa, cadena).subscribe(data => {
        this.dataSource.data = data;   
    });  

  }

  buscarProveedorNoDom(cadena: string): void
  {
    this.enteService.listarProveedorNoDomBusqueda(this.valoresiniciales.empresa, cadena).subscribe(data => {
        this.dataSource.data = data;   
    });  

  }

  buscarSocioNegocio(cadena: string): void
  {
    this.enteService.listarSocioNegocioBusqueda(this.valoresiniciales.empresa, cadena).subscribe(data => {
        this.dataSource.data = data;   
    });  

  }

  seleccionarRegistro(codigo: string, descripcion: string): void
  {
    this.valorcatalogo = { codigo: codigo, descripcion: descripcion};

    localStorage.setItem(CATALOGO, JSON.stringify(this.valorcatalogo));

    this.matDialogRef.close();
    
  }
}
