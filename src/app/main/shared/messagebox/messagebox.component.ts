import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MESSAGEBOX } from 'app/_shared/var.constant';

@Component({
  selector: 'gez-messagebox',
  templateUrl: './messagebox.component.html',
  styleUrls: ['./messagebox.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MessageboxComponent  {
  
  titulo = this._data.titulo;
  subTitulo = this._data.subTitulo;
  botonAceptar = this._data.botonAceptar;
  botonCancelar = this._data.botonCancelar;
 
  constructor(
    public matDialogRef: MatDialogRef<MessageboxComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any
  ) 
  { }

  // tslint:disable-next-line:typedef
  aceptar(): void {     
      localStorage.setItem(MESSAGEBOX, 'S');
      this.matDialogRef.close();
  }
}
