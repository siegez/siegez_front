import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PrincipalComponent } from './principal.component';

const routes = [
    {
        path : 'principal',
        component : PrincipalComponent
    }
];

@NgModule({
    declarations: [
        PrincipalComponent
        
    ],
    imports     : [
        RouterModule.forChild(routes),
        NgbModule,
        TranslateModule,

        NgxDatatableModule,

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,


        FuseSharedModule
    ],
    exports     : [
        PrincipalComponent
    ]
})

export class PrincipalModule
{
}
