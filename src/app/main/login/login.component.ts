import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { LoginService } from '../../services/login.service';
import { UtilService } from '../../services/util.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { VALORES_INICIALES } from 'app/_shared/var.constant';
import { MatTableDataSource } from '@angular/material/table';
import { ConsultaService } from 'app/services/consulta.service';
import { ViewValoresInicialesDto } from 'app/dto/view.valoresiniciales.dto';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    providers: [MatSnackBar]
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;
    hide = true;
    public dataSource = new MatTableDataSource<any>();
    valoresiniciales;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _matSnackBar: MatSnackBar,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private loginService: LoginService,
        private utilService: UtilService,
        private router: Router,
        private consultaService: ConsultaService,
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.valoresiniciales = new ViewValoresInicialesDto();
        
        this.loginForm = this._formBuilder.group({
            usuario   : ['RNUNEZ', Validators.required],
            contraseña: ['123456', Validators.required]
        });
    }

    // tslint:disable-next-line:typedef
    login(){
        
        this.loginService.login(this.loginForm.controls['usuario'].value,
        this.loginForm.controls['contraseña'].value).subscribe(token => {

            this.utilService.saveAccessToken(token );

            this.consultaService.listarValoresIniciales(this.loginForm.controls['usuario'].value).subscribe(data => {
                this.valoresiniciales = data;    
                localStorage.setItem(VALORES_INICIALES, JSON.stringify(this.valoresiniciales));
                this.router.navigate([`contabilidad/tablerocontabilidad`]);    
            });            
            
        }, err => {
            this._matSnackBar.open('Ingrese correctamente, el usuario y contraseña', '', {
                verticalPosition: 'bottom',
                duration        : 2000,
                panelClass: ['my-snack-bar'] 
                });
            
        });
    }


}
