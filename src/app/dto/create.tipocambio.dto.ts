export class CreateTipoCambioDto{
    empresa: number;
    vez = 0 ;
    fecha = '' ;
    moneda = '' ;
    descripcionmoneda = '' ;
    tipocambiocompra = 0 ;
    tipocambioventa = 0 ;	
    indicadorcierre = '' ;
    descripcionindicadorcierre = '' ;
    indicadorreplica = '';
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
