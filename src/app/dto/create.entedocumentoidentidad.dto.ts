export class CreateEnteDocumentoIdentidadDto{
    empresa: number;
    entidad = ' ' ;
    vez: number;
    documentoidentidad = '' ;
    descripciondocumentoidentidad = '' ;
    numerodocumento = '';
    estado = 'A' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;    
}
