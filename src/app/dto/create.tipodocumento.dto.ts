export class CreateTipoDocumentoDto{
    empresa: number;
    tipodocumento = '' ;
    descripcion = '' ;
    abreviatura = '' ;
    indicadorcompra = '' ;
    indicadorhonorario = '' ;
    indicadorventa = '' ;	
    indicadorcaja = '' ;
    indicadoregreso = '' ;
    indicadoringreso = '' ;
    indicadordiario = '' ;
    indicadorimportacion = '' ;
    indicadornodomiciliado = '' ;
    indicadorsustentonodom = '' ;
    indicadorsunat = '' ;	    	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
