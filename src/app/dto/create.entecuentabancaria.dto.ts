export class CreateEnteCuentaBancariaDto{
    empresa: number;
    ejerciciocontable: number;
    entidad = '' ;
    cuentabancaria = '';
    cuentainterbancaria = '';
    nombrecuentabancaria = '' ;	
    tipocuentabancaria = '' ;
    descripciontipocuentabancaria = '' ;
    cuenta = '' ;
    descripcioncuenta = '' ;   	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
