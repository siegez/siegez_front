export class CreateCentroCostoDto{
    empresa: number;
    ejerciciocontable: number;
    centrocosto: '';	
    nombrecentrocosto = '' ;
    cuentacontablecargo = '' ;
    cuentacontableabono = '' ;   
    porcentaje: number; 	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}

