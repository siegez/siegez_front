import { CreateEnteDireccionDto } from './create.entedireccion.dto';
import { CreateEnteDocumentoIdentidadDto } from './create.entedocumentoidentidad.dto';
import { CreateEnteSocioNegocioDto } from './create.entesocionegocio.dto';

export class CreateEnteDto{
    empresa: number;
    entidad = ' ' ;
    apellidopaterno = ' ' ;
    apellidomaterno = ' ' ;
    nombres = ' ' ;
    tipopersona = 'J' ;
    descripciontipopersona = ' ' ;
    ruc = ' ' ;
    nodomiciliado = ' ' ;
    descripcionnodomiciliado = ' ' ;
    pais = ' ' ;
    nombrepais = ' ' ;
    estado = 'A' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;    
    entesocionegocio = CreateEnteSocioNegocioDto;
    entedocumentoidentidad = CreateEnteDocumentoIdentidadDto;
    entedireccion = CreateEnteDireccionDto;
}
