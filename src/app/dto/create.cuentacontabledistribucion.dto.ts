export class CreateCuentaContableDistribucionDto{
    empresa = 6;
    annoproceso = 2019;
    cuenta = '101111';
    descripcioncuenta = 'CAJA MN';
    tabla = 1;
    descripciontabla = 'Dimensión 1';    
    estado = 'A';
    descripcionestado = 'Activo';
    creacionUsuario = 'RNUNEZ';
    creacionFecha = '2014-04-28';
    modificacionUsuario = 'ZDELGADO';
    modificacionFecha = '2016-11-01';
    }
