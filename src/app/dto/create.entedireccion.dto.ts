export class CreateEnteDireccionDto{
    empresa: number;
    entidad = ' ' ;
    vez: number;
    tipodomicilio = '' ;
    descripciontipodomicilio = '' ;
    pais = '' ;
    nombrepais = '' ;
    ubigeo = '' ;
    descripcionubigeo = '' ;
    direccion = '' ;
    estado = 'A' ;
    descripcionestado = ' ' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;    
}
