import { CreateGrupoCentroCostoDetalleDto } from './create.grupocentrocostodetalle.dto';

export class CreateGrupoCentroCostoDto{
    empresa: number;
    ejerciciocontable: number;
    grupocentrocosto: '';	
    nombregrupocentrocosto = '' ;
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
    grupocentrocostodetalle = CreateGrupoCentroCostoDetalleDto;
}

