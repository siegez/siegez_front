export class CreateGrupoCentroCostoDetalleDto{
    empresa: number;
    ejerciciocontable: number;
    grupocentrocosto: '';
    nombregrupocentrocosto: '';
    centrocosto: '';	
    nombrecentrocosto = '' ;
    cuentacontablecargo = '' ;
    cuentacontableabono = '' ;   
    porcentaje = 0; 	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
