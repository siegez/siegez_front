export class CreateValorFechaDto{
    empresa: number;
    codigo = '' ;
    vez: number;
    fechainicio = '' ;
    fechafin = '' ;
    valor: number ;	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
