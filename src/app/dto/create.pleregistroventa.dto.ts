export class CreatePLERegistroVentaDto{
    c1: string;
    c2: string;
    c3: string;
    c4: string;
    c5: string;
    c6: string;
    c7: string;
    c8: string;
    c9: string;
    c10: string;
    c11: string;
    c12: string;
    c13: number;
    c14: number;
    c15: number;
    c16: number;
    c17: number;
    c18: number;
    c19: number;
    c20: number;
    c21: number;
    c22: number;
    c23: number;
    c24: number;
    c25: number;
    c26: string;
    c27: string;
    c28: string;
    c29: string;
    c30: string;
    c31: string;
    c32: string;
    c33: string;
    c34: string;  
    c35: string;
}

