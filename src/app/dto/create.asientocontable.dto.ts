export class CreateAsientoContableDto{
    empresa: number;
    ejerciciocontable: number;
    asientocontable: number;	
    operacioncontable = '' ;
    tipoasiento = '' ;
    descripciontipoasiento = '' ;    	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
