export class ValorCampoDto{
    empresa: number;
    // tslint:disable-next-line:ban-types
    ambito: String;
    // tslint:disable-next-line:ban-types
    campo: String;
    // tslint:disable-next-line:ban-types
    valor: String;
}
