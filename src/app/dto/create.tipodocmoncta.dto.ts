export class CreateTipoDocMonCtaDto{
    empresa: number;
    ejerciciocontable: number;
    tipodocumento = '' ;
    descripciontipodocumento = '' ;
    moneda = '' ;
    descripcionmoneda = '' ;
    cuentacompra = '' ;
    cuentacomprarel = '' ;
    cuentahonorario = '' ;
    cuentaventa = '' ;
    cuentaventarel = '' ;
    cuentacaja = '' ;
    cuentaegreso = '' ;
    cuentaingreso = '' ;
    cuentadiario = '' ;
    cuentaimportacion = '' ;
    cuentanodomiciliado = '' ;	
    cuentanodomiciliadorel = '' ;	
    estado = '' ;
    descripcionestado = '' ;
    creacionUsuario = '' ;
    creacionFecha = '' ;
    modificacionUsuario = '' ;
    modificacionFecha = '' ;
}
