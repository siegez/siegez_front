
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';

import { LoginService } from './services/login.service';

import { UtilService } from './services/util.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsuarioService } from './services/usuario.service';
import { MatDialogModule } from '@angular/material/dialog';
import { OverlayModule } from '@angular/cdk/overlay';
import { MessageboxComponent } from './main/shared/messagebox/messagebox.component';
import { SearchboxComponent } from './main/shared/searchbox/searchbox.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatMenuModule } from '@angular/material/menu';
// import { DecimalNumberDirectiveTwo } from './directive/valid-decimal-directive-Two';
// import { DecimalNumberDirectiveThree } from './directive/valid-decimal-directive-three';



const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full' },
    {
        path        : 'auth', 
        loadChildren: () => import('./main/login/login.module').then(m => m.LoginModule)
    },

    //
    {
        path        : 'pages',
        loadChildren: () => import('./main/sample/sample.module').then(m => m.SampleModule)
    },

    //
    {
        path: 'principal',
        loadChildren: () => import('./main/principal/principal.module').then(m => m.PrincipalModule)
    },

    //
    {
        path        : 'sistema',
        loadChildren: () => import('./main/sistema/sistema.module').then(m => m.SistemaModule)
    },

    //
    {
        path        : 'contabilidad',
        loadChildren: () => import('./main/contabilidad/contabilidad.module').then(m => m.ContabilidadModule)
    },
];

@NgModule({
    declarations: [
        AppComponent,
        MessageboxComponent,
        SearchboxComponent
        // DecimalNumberDirectiveTwo,
        // DecimalNumberDirectiveThree,   
    ],
    imports     : [
        BrowserModule,
        NgbModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatToolbarModule,
        MatFormFieldModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        OverlayModule,

       //
        MatChipsModule,
        MatExpansionModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatMenuModule        
     
    ],

    exports: [
        // DecimalNumberDirectiveTwo,
        // DecimalNumberDirectiveThree
    ],

    providers: [LoginService, UtilService, UsuarioService, { provide: LOCALE_ID, useValue: 'es' }], // en_US es-PY es
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
